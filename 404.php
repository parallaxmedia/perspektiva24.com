<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION;
$APPLICATION->SetTitle("Ошибка 404, страница не найдена!");

?>

    <div class="section">
        <div class="section__inner">
            <h1>Ошибка 404, страница не найдена!</h1>
            <p>Запрашиваемая вами страница не существует либо произошла ошибка.</p>
            <p>Если вы уверены в правильности указанного адреса, то данная страница уже не существует на сервере или была
                переименована.</p>
            <p>Откройте <a href="/">главную страницу</a> сайта и попробуйте самостоятельно найти нужную вам страницу или
                кликните кнопкой
                "Назад" в браузере, чтобы вернуться к предыдущей странице.</p>
        </div>
    </div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>