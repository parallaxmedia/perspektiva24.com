<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?><div class="company">
 <section class="section section-bc">
	<div class="section__inner">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"",
	Array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	)
);?>
	</div>
 </section> <section class="section company__intro">
	<div class="section__inner">
		<h1> <span class="visually-hidden">Перспектива24</span> <img width="289" alt="Перспектива24" src="/local/templates/perspektiva/images/company-logo.svg" height="66"> </h1>
		<p class="section__title">
			 Помогаем продавать и покупать недвижимость с 2013 года
		</p>
	</div>
 </section> <section class="section company__owner">
	<div class="section__inner">
		<div class="company__owner-image">
 <img width="411" alt="Рим Хасанов" src="/local/templates/perspektiva/images/rim-hasanov.jpg" height="535">
		</div>
		<div class="company__owner-content">
			<p class="company__owner-title">
				 Рим Хасанов
			</p>
			<p class="company__owner-subtitle">
				 Основатель "Перспектива 24"
			</p>
			<div class="company-owner__text">
				<p>
					 Наша компания успешно работает на рынке недвижимости с 2013 года. За это время мы зарекомендовали себя как надежное, успешное и современное агентство недвижимости. Мы каждый день улучшаем наш сервис, получая обратную связь от клиентов и партнеров. Наша цель – в недалекой перспективе стать агентством недвижимости №1 в России.
				</p>
			</div>
			 <!--                     <div class="company-owner__grid">
                        <div class="company-owner__grid-item"><img class="company-owner__grid-image"
                                                                   src="<?= SITE_TEMPLATE_PATH ?>/images/building-1.svg" width="68" height="65"
                                                                   alt="">
                            <div class="company-owner__grid-content">
                                <p>22 года</p><span>Закончил Университет такйо с таким дипломом</span>
                            </div>
                        </div>
                        <div class="company-owner__grid-item"><img class="company-owner__grid-image"
                                                                   src="<?= SITE_TEMPLATE_PATH ?>/images/building-2.svg" width="68" height="65"
                                                                   alt="">
                            <div class="company-owner__grid-content">
                                <p>25 лет</p><span>Достиг того и того и другого</span>
                            </div>
                        </div>
                        <div class="company-owner__grid-item"><img class="company-owner__grid-image"
                                                                   src="<?= SITE_TEMPLATE_PATH ?>/images/building-3.svg" width="68" height="65"
                                                                   alt="">
                            <div class="company-owner__grid-content">
                                <p>30 лет</p><span>Достиг того и того и другого</span>
                            </div>
                        </div>
                        <div class="company-owner__grid-item"><img class="company-owner__grid-image"
                                                                   src="<?= SITE_TEMPLATE_PATH ?>/images/building-4.svg" width="68" height="65"
                                                                   alt="">
                            <div class="company-owner__grid-content">
                                <p>40 лет</p><span>Достиг того и того и другого</span>
                            </div>
                        </div>
                    </div> -->
		</div>
	</div>
 </section> <section class="section company__goals">
	<div class="section__inner">
		<div class="company__video">
			<div class="company__video-content">
				<h3>Наши ценности и убеждения</h3>
 				<a class="company__goals-link" href="https://www.youtube.com/watch?v=TIY8IeyiXA4" data-fancybox="">Смотрите видео</a>
			</div>
			<a class="company__video-item" data-fancybox href="https://www.youtube.com/watch?v=TIY8IeyiXA4" style="background-image: url(/local/templates/perspektiva/images/company-video-bg.jpg)"></a>
		</div>
		<ul class="company__goals-grid">
			<li>
			<h4>Миссия</h4>
			<p>
			</p>
			<p>
				 «Перспектива24» – всероссийский оператор недвижимости, работающий на рынке с 2013 года. Наша миссия – помощь в продаже, покупке и аренде недвижимости с максимальной защитой для клиента.
			</p>
 </li>
			<li>
			<h4>Цель</h4>
			<p>
				 Стать номер 1 среди агентств недвижимости в России
			</p>
 </li>
		</ul>
	</div>
 </section> <section class="section company__map">
	<div class="section__inner">
		<div class="company__map-title">
			<div class="company__map-image">
 <img width="41" src="/local/templates/perspektiva/images/74.svg" height="43" alt="">
			</div>
			<p>
				 Представительства по всей России
			</p>
		</div>
		<div class="company__map-item">
 <img src="map.png" alt="">
		</div>
		<div class="company__map-info">
			<ul class="company__map-row">
				<li><strong><img width="30" src="/local/templates/perspektiva/images/home.svg" height="30" alt="">35 136</strong>
				<p>
					 Объектов на продаже
				</p>
 </li>
				<li><strong><img width="27" src="/local/templates/perspektiva/images/receipt-2.svg" height="30" alt="">28 531</strong>
				<p>
					 Продано объектов
				</p>
 </li>
				<li><strong><img width="29" src="/local/templates/perspektiva/images/houses.svg" height="30" alt="">5 283</strong>
				<p>
					 Новостроек в базе
				</p>
 </li>
			</ul>
			<ul class="company__map-row">
				<li><strong><img width="31" src="/local/templates/perspektiva/images/finger-up.svg" height="29" alt="">8 из 10</strong>
				<p>
					 Рекомендуют нас
				</p>
 </li>
				<li><strong><img width="32" src="/local/templates/perspektiva/images/big-star.svg" height="30" alt="">2 670</strong>
				<p>
					 Агентов в сети по РФ
				</p>
 </li>
				<li><strong><img width="33" src="/local/templates/perspektiva/images/smile.svg" height="33" alt="">от 4,5%</strong>
				<p>
					 Низкий % ипотеки
				</p>
 </li>
			</ul>
		</div>
	</div>
 </section> <section class="section company__plan">
	<div class="section__inner">
		<div class="company__plan-header">
			<h3>Планы развития до 2025 года</h3>
			<p>
				 Ежегодно доля рынка компании увеличивается на 7%
			</p>
		</div>
		<div class="company__plan-body">
			<ul class="company__plan-tree">
				<li><strong>2021</strong>
				<p>
					 Охватить 20 новых регионов
				</p>
 </li>
				<li><strong>2022</strong>
				<p>
					 Монополизировать рынок недвижимости
				</p>
 </li>
				<li><strong>2023</strong>
				<p>
					 Открыть 10 новых направлений
				</p>
 </li>
				<li><span class="crown"></span><strong>20 <img width="40" alt="Образовать холдинг" src="/local/templates/perspektiva/images/24.svg" height="40"></strong>
				<p>
					 Образовать холдинг
				</p>
 </li>
				<li><strong>2025</strong>
				<p>
					 Стать независимой экосистемой
				</p>
 </li>
			</ul>
		</div>
	</div>
 </section>
</div>
 <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>