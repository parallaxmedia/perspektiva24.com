<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");
?>
    <section class="section section-bc">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
        </div>
    </section>
    <link rel="stylesheet" type="text/css" href="css/styles.css">

    <section class="section services">
        <div class="section__inner">
            <h1>Работа в команде Перспектива24 </h1>
            <?php
            $question_list = array(
                array(
                    'question' => 'Дата рождения',
                    'type' => 'text',
                ),
                array(
                    'question' => 'В какой сфере приходилось работать',
                    'type' => 'checkbox',
                    'answers' => array(
                        'Нет опыта',
                        'Администрация, руководство среднего звена',
                        'Продажи, Офисный сотрудник',
                        'Гостинично-ресторанный бизнес, туризм',
                        'Розничная торговля',
                        'Недвижимость',
                        'Образование, наука',
                        'Рабочие специальности, производство',
                        'Маркетинг, реклама, PR',
                        'Охрана, безопасность',
                        'Финансы, банки',
                        'Другое',
                    ),
                ),
                array(
                    'question' => 'Опыт работы',
                    'type' => 'radio',
                    'answers' => array(
                        'до года',
                        'от года до 3',
                        'от 3 до 5',
                        'более 5 лет',
                    ),
                ),
                array(
                    'question' => 'Желаемый уровень дохода',
                    'type' => 'radio',
                    'answers' => array(
                        'до 30000',
                        'от 30000 до 50000',
                        'от 50000 до 100000',
                        'более 100 тысяч рублей',
                    ),
                ),
            );
            ?>
            <div class="quiz_container">
                <div class="quiz_bl">
                    <div class="quiz_name">Проверь, подходит ли для тебя работа Агент по Недвижимости</div>
                    <form class="quiz_form" action="/local/templates/perspektiva/ajax/formSendProcessing.php">
                        <input type="hidden" name="type" value="Работа">
                        <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                        <div class="quiz_tabs">
                            <div class="quiz_tabs_header">
                                <?
                                foreach ($question_list as $key => $question) {
                                    $counter = $key + 1;
                                    ?>
                                    <div class="quiz_tabs_header_item <?= (($counter == 1) ? 'active' : '') ?>"
                                         data-tab="<?= $counter ?>"></div>
                                    <?
                                }
                                ?>
                            </div>
                            <div class="quiz_tabs_body">
                                <?
                                foreach ($question_list as $key => $question) {
                                    $counter = $key + 1;
                                    ?>
                                    <div class="quiz_tabs_body_item <?= (($counter == 1) ? 'active' : '') ?>"
                                         data-tab="<?= $counter ?>">
                                        <div class="quest_number">Вопрос <?= $counter ?>
                                            из <?= count($question_list) ?></div>
                                        <div class="question"><?= $question['question'] ?></div>
                                        <div class="answer_list">
                                            <?
                                            if ($question['type'] == 'radio') {
                                                foreach ($question['answers'] as $answer) {
                                                    ?>
                                                    <label class="answer_item">
                                                        <input type="radio" name="ans_<?= $counter ?>"
                                                               value="<?= $answer ?>">
                                                        <span></span>
                                                        <p><?= $answer ?></p>
                                                    </label>
                                                    <?
                                                }
                                            } elseif ($question['type'] == 'select') {
                                                ?>
                                                <div class="quiz_inps">
                                                    <div class="input_bl" style="margin: 0;">
                                                        <!-- <input type="text" name="ans_4" placeholder="Название района"> -->
                                                        <select class="form-control" size="6" multiple
                                                                name="ans_<?= $counter ?>[]">
                                                            <option selected disabled value="">--Выберите вариант--
                                                            </option>
                                                            <? foreach ($question['answers'] as $answer) {
                                                                ?>
                                                                <option value="<?= $answer ?>"><?= $answer ?></option>
                                                                <?
                                                            } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <?
                                            } elseif ($question['type'] == 'checkbox') {
                                                ?>
                                                <? foreach ($question['answers'] as $answer) {
                                                    ?>
                                                    <label class="answer_item">
                                                        <input type="checkbox" name="ans_<?= $counter ?>[]"
                                                               value="<?= $answer ?>">
                                                        <span></span>
                                                        <p><?= $answer ?></p>
                                                    </label>
                                                    <?
                                                } ?>
                                                <?
                                            } elseif ($question['type'] == 'text') {
                                                ?>
                                                <div class="quiz_inps">
                                                    <div class="input_bl" style="margin: 0;">
                                                        <input class="form-control"
                                                            <?= $counter == 1 ? 'id="date"' : '' ?>
                                                               type="text" name="ans_<?= $counter ?>"
                                                               placeholder="<?= $question['question'] ?>">
                                                    </div>
                                                </div>
                                                <?
                                            } ?>

                                        </div>
                                        <div class="quiz_footer">
                                            <? if ($counter != 1) { ?>
                                                <div class="g_btn r_btn quiz_btn" data-slide="<?= ($counter - 1) ?>">
                                                    <span>Назад</span></div>
                                            <? } ?>
                                            <? if ($counter >= count($question_list)) { ?>
                                                <div class="d_btn r_btn quiz_btn" data-slide="forma">
                                                    <i></i><span>Далее</span></div>
                                            <? } else { ?>
                                                <div class="d_btn r_btn quiz_btn" data-slide="<?= ($counter + 1) ?>">
                                                    <i></i><span>Далее</span></div>
                                            <? } ?>

                                        </div>
                                        <div class="err_msg">Выберите вариант ответа!</div>
                                    </div>
                                    <?
                                }
                                ?>
                                <div class="quiz_tabs_body_item quiz_form" data-tab="forma">
                                    <div class="quiz_form_name">
                                        Отлично. Последний шаг!<span><br>Напишите свой номер телефона, и мы свяжемся с вами в ближайшее время</span>
                                    </div>
                                    <div class="quiz_inps">
                                        <!-- <div class="input_bl">
                                            <input type="text" name="name" placeholder="Имя">
                                        </div> -->

                                        <div class="custom-form">
                                            <ul class="custom-form-toggle">
                                                <!--                                                 <li class="custom-form-toggle-item">
                                                                                                    <label class="custom-form-toggle__btn custom-form-toggle__btn_active">
                                                                                                        <input type="radio" name="contact" data-placeholder="Введите свой мобильный:" value="Мобильный: " checked="" id="custom-form__phone" required>
                                                                                                        <span class="icon-technolog"></span> Мобильный (смс со ссылкой)
                                                                                                    </label>
                                                                                                </li>
                                                                                                <li class="custom-form-toggle-item">
                                                                                                    <label class="custom-form-toggle__btn">
                                                                                                        <input type="radio" name="contact" data-placeholder="Введите свой WhatsApp:" value="WhatsApp: ">
                                                                                                        <span class="icon-whatsapp"></span> WhatsApp
                                                                                                    </label>
                                                                                                </li>
                                                                                                <li class="custom-form-toggle-item">
                                                                                                    <label class="custom-form-toggle__btn">
                                                                                                        <input type="radio" name="contact" data-placeholder="Введите свой Viber:" value="Viber: ">
                                                                                                        <span class="icon-viber"></span> Viber
                                                                                                    </label>
                                                                                                </li> -->
                                                <!-- <li class="custom-form-toggle-item custom-form-toggle_mail__btn">
                                                    <label class="custom-form-toggle__btn">
                                                        <input type="radio" name="contact" data-placeholder="Введите свой e-mail:" value="На e-mail: ">
                                                        <span class="icon-email"></span> На e-mail
                                                    </label>
                                                </li> -->
                                            </ul>
                                            <div class="custom-form-grid">
                                                <div class="custom-form-col">
                                                    <input type="tel" name="phone" class="custom-form__input" id="custom-form__input_pole" placeholder="Введите телефон" required>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <button class="q_btn r_btn" type="submit"><i></i><span>Отправить заявку</span>
                                    </button>
                                    <!-- <ul style="text-align: left; text-align: left; font-weight: bold; margin-top: 35px; font-size: 18px; list-style: none;">
                                        <li style="margin-bottom: 15px;">У нас для Вас бесплатное юридическое сопровождение. Опытные юристы обеспечат законность Вашей сделки.</li>
                                        <li>Одобряем ипотеку для покупки новостройки, С ДИСКОНТОМ от ставки банков.</li>
                                    </ul> -->
                                    <!-- <div class="priv_form">Нажимая "Получить цену", Вы соглашаетесь с <a href="/privacy.pdf" target="_blank">политикой конфиденциальности</a></div> -->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="preloader">
        <div class='sk-fading-circle'>
            <div class='sk-circle sk-circle-1'></div>
            <div class='sk-circle sk-circle-2'></div>
            <div class='sk-circle sk-circle-3'></div>
            <div class='sk-circle sk-circle-4'></div>
            <div class='sk-circle sk-circle-5'></div>
            <div class='sk-circle sk-circle-6'></div>
            <div class='sk-circle sk-circle-7'></div>
            <div class='sk-circle sk-circle-8'></div>
            <div class='sk-circle sk-circle-9'></div>
            <div class='sk-circle sk-circle-10'></div>
            <div class='sk-circle sk-circle-11'></div>
            <div class='sk-circle sk-circle-12'></div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.maskedinput.js"></script>
    <script src="js/scripts.js"></script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>