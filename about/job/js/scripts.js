$('.show_pop').on('click', function (e) {
    e.preventDefault();
    var pop_id = $(this).attr('data-pop');
    $('#' + pop_id).addClass('active');
    if ($('#' + pop_id).hasClass('map_p')) {
        $('#' + pop_id).find('iframe').attr('src', $('#' + pop_id).find('iframe').attr('data-src'));
    }
})

function go_mainpage() {
    document.location.href = "/";
}

$(document).on('click', '.close_pop', function () {
    $('.pop_bl').removeClass('active');
})

$(document).on('click', '.quiz_btn', function () {
    var slide = $(this).attr('data-slide');
    var parent_block = $(this).closest('.quiz_tabs_body_item');
    var checked = 0;
    parent_block.find('input:radio').each(function () {
        if ($(this).is(':checked')) {
            checked = 1;
        }
    })
    parent_block.find('input:checkbox').each(function () {
        if ($(this).is(':checked')) {
            checked = 1;
        }
    })
    parent_block.find('input:text').each(function () {
        if ($(this).val() != '') {
            checked = 1;
        }
    })
    parent_block.find('select option:selected').each(function () {
        if (1) {
            checked = 1;
        }
    })
    if (checked == 1 || $(this).hasClass('g_btn')) {
        $('.quiz_tabs_body_item, .quiz_tabs_header_item').removeClass('active');
        $('.quiz_tabs_body_item[data-tab="' + slide + '"], .quiz_tabs_header_item[data-tab="' + slide + '"]').addClass('active');
    } else {
        parent_block.find('.err_msg').addClass('active');
        setTimeout(function () {
            parent_block.find('.err_msg').removeClass('active');
        }, 1500)
    }

    if (slide == 'forma') {
        $('.quiz_present_item').removeClass('locked');
    }

    if ($(window).width() <= 767) {
        $("html, body").animate({scrollTop: 0}, "slow");
    }
})


/*$('.fb_form').submit(function (e) {
    e.preventDefault();
    $('.preloader').addClass('active');
    var form_par = $(this).closest('.pop_cont');
    var data = $(this).serialize();
    $.ajax({
        type: 'POST',
        url: 'mail.php',
        data: data,
        success: function (response) {
            form_par.html(response);
            $('.preloader').removeClass('active');
            yaCounter56734858.reachGoal('push');
        }
    })
})*/

$('.quiz_form').submit(function (e) {
    e.preventDefault();
    $('#quiz_finish').remove();
    $('.preloader').addClass('active');
    var data = $(this).serialize();
    $.ajax({
        type: 'POST',
        url: '/local/templates/perspektiva/ajax/quizProcessing.php',
        data: data,
        success: function (response) {
            $('.quiz_container').html(response);
            $('.preloader').removeClass('active');
            /*
            $('.quiz_tabs_body_item, .quiz_tabs_header_item').removeClass('active');
            $('.quiz_tabs_body_item[data-tab="1"], .quiz_tabs_header_item[data-tab="1"]').addClass('active');*/
        }
    })
})


// $.fn.setCursorPosition = function(pos) {
//   if ($(this).get(0).setSelectionRange) {
//     $(this).get(0).setSelectionRange(pos, pos);
//   } else if ($(this).get(0).createTextRange) {
//     var range = $(this).get(0).createTextRange();
//     range.collapse(true);
//     range.moveEnd('character', pos);
//     range.moveStart('character', pos);
//     range.select();
//   }
// };

$(document).ready(function () {
    $("#date").mask("99.99.9999");
    $.mask.definitions['~'] = '[78]';
    $("[name=phone]").mask("~ (999) 999-9999");

    // $('input[type="tel"]').click(function(){
    //   $(this).setCursorPosition(0);  // set position number
    // });

});

function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

$(document).ready(function () {
    $('.custom-form-toggle__btn').click(function (event) {
        var placeholder = $(this).find('input').attr('data-placeholder');

        $('.custom-form-toggle__btn').removeClass('custom-form-toggle__btn_active');
        $(this).addClass('custom-form-toggle__btn_active');

        $(this).parents('.custom-form').find('.custom-form__input').attr('placeholder', placeholder);
    });

});