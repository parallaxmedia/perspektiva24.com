<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php"); ?>

    <section class="section">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
            <h1><? $APPLICATION->ShowTitle(); ?></h1>

            <? $APPLICATION->IncludeComponent("bitrix:news.detail", "agent", [
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "USE_SHARE" => "Y",
                    "SHARE_HIDE" => "N",
                    "SHARE_TEMPLATE" => "",
                    "SHARE_HANDLERS" => ["delicious"],
                    "SHARE_SHORTEN_URL_LOGIN" => "",
                    "SHARE_SHORTEN_URL_KEY" => "",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "",
                    "IBLOCK_ID" => "4",
                    "ELEMENT_ID" => $_REQUEST["CODE"],
                    "ELEMENT_CODE" => $_REQUEST["CODE"],
                    "CHECK_DATES" => "Y",
                    "FIELD_CODE" => ["ID"],
                    "PROPERTY_CODE" => ["DESCRIPTION"],
                    "IBLOCK_URL" => "",
                    "DETAIL_URL" => "/agents/#ELEMENT_ID#/",
                    "SET_TITLE" => "Y",
                    "SET_CANONICAL_URL" => "Y",
                    "SET_BROWSER_TITLE" => "Y",
                    "BROWSER_TITLE" => "-",
                    "SET_META_KEYWORDS" => "Y",
                    "META_KEYWORDS" => "-",
                    "SET_META_DESCRIPTION" => "Y",
                    "META_DESCRIPTION" => "-",
                    "SET_STATUS_404" => "Y",
                    "SET_LAST_MODIFIED" => "Y",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "ADD_ELEMENT_CHAIN" => "N",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "USE_PERMISSIONS" => "N",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "3600",
                    "CACHE_GROUPS" => "Y",
                    "SHOW_404" => "Y",
                    "MESSAGE_404" => "",
                    "STRICT_SECTION_CHECK" => "Y",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N"
                ]
            ); ?>
        </div>
    </section>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
?>