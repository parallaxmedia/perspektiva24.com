<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION;
$APPLICATION->SetTitle("Агенты по недвижимости");
global $agentFilter;
global $arCurCity;

$agentFilter = [
    'PROPERTY_FILIAL_ID' => $arCurCity['FILIALS'],
    "!PROPERTY_STATUS" => [1, 3]
];
?>

    <section class="section intro">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
            <h1><? $APPLICATION->ShowTitle(); ?></h1>
            <?
            $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
            $uri = new \Bitrix\Main\Web\Uri($request->getRequestUri());

            if (($request['sort'] == 'name')) {
                $sort = 'NAME';
            }

            if (!isset($request['sort'])) {
                $sort = 'NAME';
            }

            ?>
            <div class="pagination-wrapper">
                <nav aria-label="Сортировка агентов">
                    <ul class="pagination">
                        <li class="page-item <?= ($request['sort'] == 'name') ? 'active' : '' ?> ">
                            <?
                            $uri->addParams(["sort" => "name"]);
                            $sortURL = $uri->getUri();
                            ?>
                            <a class="page-link" href="<?= $sortURL ?>">По алфавиту</a>
                        </li>
                        <li class="page-item <?= ($request['sort'] == 'objects') ? 'active' : '' ?>">
                            <?
                            $uri->addParams(["sort" => "objects"]);
                            $sortURL = $uri->getUri();
                            ?>
                            <a class="page-link" href="<?= $sortURL ?>">С объектами</a>
                        </li>
                    </ul>
                </nav>
            </div>

            <?php $APPLICATION->IncludeComponent("bitrix:news.list", "agents", [
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "",
                    "IBLOCK_ID" => "4",
                    "NEWS_COUNT" => "24",
                    "SORT_BY1" => $sort,
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "agentFilter",
                    "FIELD_CODE" => ["ID"],
                    "PROPERTY_CODE" => ["DESCRIPTION"],
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "agents",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "Y",
                    "PAGER_BASE_LINK_ENABLE" => "Y",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => "",
                    "PAGER_BASE_LINK" => "",
                    "PAGER_PARAMS_NAME" => "arrPager",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
                ]
            ); ?>
        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>