<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Сравнение");
?>

    <section class="section compare-section">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
            <h1><? $APPLICATION->ShowTitle(); ?></h1>
            <div class="toolbar">
                <a class="toolbar__item" href="#">
                    <svg class="icon icon-print icon_gray">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#print"></use>
                    </svg>
                    <span>Печать</span>
                </a>
                <a class="toolbar__item" id="downloadPDF" href="#">
                    <svg class="icon icon-download icon_gray">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#download"></use>
                    </svg>
                    <span>Скачать PDF</span>
                </a>
                <a class="toolbar__item" href="<?= SITE_TEMPLATE_PATH ?>/ajax/getExcelCompare.php">
                    <svg class="icon icon-download icon_gray">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#download"></use>
                    </svg>
                    <span>Скачать EXCEL</span>
                </a>
                <div class="share js-share" href="#">
                    <a class="share__item share__item_insta" href="#" aria-label="Instagram"></a>
                    <a class="share__item share__item_fb" href="#" aria-label="Facebook"></a>
                    <a class="share__item share__item_vk" href="#" aria-label="Vkontakte"></a>
                    <a class="share__item share__item_tg" href="#" aria-label="Telegram"></a>
                    <a class="share__item share__item_sk" href="#" aria-label="Skype"></a>
                    <a class="share__item share__item_wa" href="#" aria-label="Whatspapp"></a>
                    <a class="share__item share__item_link" href="#" aria-label="Прямая ссылка"></a>
                    <button class="share__button" type="button">
                        <svg class="icon icon-share icon_gray share__icon-open">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#share"></use>
                        </svg>
                        <svg class="icon icon-close icon_gray share__icon-close">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#close"></use>
                        </svg>
                        <span>Поделиться</span>
                    </button>
                </div>
            </div>
            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.compare.result",
                "compare",
                [
                    "AJAX_MODE" => "N",
                    "NAME" => "CATALOG_COMPARE_LIST",
                    "IBLOCK_TYPE" => "",
                    "IBLOCK_ID" => "2",
                    "FIELD_CODE" => ['NAME', 'PREVIEW_PICTURE'],
                    "PROPERTY_CODE" => ['PRICE'],
                    //"PROPERTY_CODE" => [29, 34],
                    "OFFERS_FIELD_CODE" => [],
                    "OFFERS_PROPERTY_CODE" => [],
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "DETAIL_URL" => "/estate/#SECTION_CODE#/#ELEMENT_ID#/",
                    "BASKET_URL" => "",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "PRICE_CODE" => [],
                    "USE_PRICE_COUNT" => "Y",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "DISPLAY_ELEMENT_SELECT_BOX" => "Y",
                    "ELEMENT_SORT_FIELD_BOX" => "name",
                    "ELEMENT_SORT_ORDER_BOX" => "asc",
                    "ELEMENT_SORT_FIELD_BOX2" => "id",
                    "ELEMENT_SORT_ORDER_BOX2" => "desc",
                    "HIDE_NOT_AVAILABLE" => "N",
                    "AJAX_OPTION_SHADOW" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CONVERT_CURRENCY" => "Y",
                    "CURRENCY_ID" => "RUB",
                    "TEMPLATE_THEME" => "blue",
                ]
            ); ?>

        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>