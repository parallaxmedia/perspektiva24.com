<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");

global $arCurCity;

\Bitrix\Main\Loader::includeModule('iblock');

$rsCity = CIBlockElement::GetList([], ['ID' => $arCurCity['FILIALS']], false, false, ['*']);

$arFilials = [];
while ($arCity = $rsCity->GetNextElement()) {
    $arFilial = $arCity->GetFields();
    $arFilial['PROPS'] = $arCity->GetProperties();
    $arFilials[] = $arFilial;
}

if (count($arFilials) > 1) {
    require $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/include/contacts_filials.inc.php';
} else {
    require $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/include/contacts_non_filials.inc.php';
}
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>