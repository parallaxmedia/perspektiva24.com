<?php
$_SERVER['DOCUMENT_ROOT'] = '/home/bitrix/www';
include_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';

\Bitrix\Main\Loader::includeModule('main');
\Bitrix\Main\Loader::includeModule('iblock');

$arObjects = [];
$rsObjects = CIBlockElement::GetList([], ['IBLOCK_ID' => 2, '>=DATE_CREATE' => array(ConvertTimeStamp(time() - 86400 * 1, "FULL"))], false, false, ['ID', 'IBLOCK_ID', 'PROPERTY_ADDRESS', 'DETAIL_PAGE_URL', 'NAME']);

$itemsId = [];

while ($arObject = $rsObjects->GetNext()) {
    $itemsId[] = $arObject['ID'];
}

use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Mail\Event;

Loader::includeModule("highloadblock");
$hlbl = 6;
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$arEmails = [];
$arEmailsObject = $entity_data_class::getList(["select" => array("*"),
    "order" => array("ID" => "ASC"),
    "filter" => []])->fetchAll();

foreach ($arEmailsObject as $emailObject) {
    $arEmails[] = $emailObject['UF_EMAIL'];
}

$arEmails = array_unique($arEmails);
$itemsId = array_slice(array_unique($itemsId), 0, 12);


foreach ($arEmails as $email) {
    Event::send(array(
        "EVENT_NAME" => "CATALOG_UPDATE",
        "LID" => "s1",
        "C_FIELDS" => array(
            "EMAIL" => $email,
            "ITEMS_ID" => implode(',', $itemsId)
        ),
    ));
}