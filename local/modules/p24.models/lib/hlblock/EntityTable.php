<?php


namespace P24\Models\HlBlock;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\Validators\LengthValidator;

Loc::loadMessages(__FILE__);

/**
 * Class EntityTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> NAME string(100) mandatory
 * <li> TABLE_NAME string(64) mandatory
 * </ul>
 *
 * @package Bitrix\Hlblock
 **/

class EntityTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_hlblock_entity';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('ENTITY_ENTITY_ID_FIELD')
                ]
            ),
            new StringField(
                'NAME',
                [
                    'required' => true,
                    'validation' => [__CLASS__, 'validateName'],
                    'title' => Loc::getMessage('ENTITY_ENTITY_NAME_FIELD')
                ]
            ),
            new StringField(
                'TABLE_NAME',
                [
                    'required' => true,
                    'validation' => [__CLASS__, 'validateTableName'],
                    'title' => Loc::getMessage('ENTITY_ENTITY_TABLE_NAME_FIELD')
                ]
            ),
        ];
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return [
            new LengthValidator(null, 100),
        ];
    }

    /**
     * Returns validators for TABLE_NAME field.
     *
     * @return array
     */
    public static function validateTableName()
    {
        return [
            new LengthValidator(null, 64),
        ];
    }
}