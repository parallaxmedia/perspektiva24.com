<?php

namespace P24\Models\HlBlock;

use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
use P24\Models\HlBlock\EntityTable;

class Main
{

    const REWARDS_AGENT_CODE = 'AgentRewards';

    public static function getHlBlockIdByCode(string $code): ?int
    {
        $getRes = EntityTable::getList([
            'select' => ['ID'],
            'filter' => ['NAME' => $code],
        ]);

        $getArray = $getRes->fetch();

        return $getArray['ID'];
    }

    public static function getHlBlockElementByCode(string $code, string $hlBlockCode)
    {
        $entity = HL\HighloadBlockTable::compileEntity(self::getHlBlockIdByCode($hlBlockCode))->getDataClass();

        $getRes = $entity::getlist([
            'select' => ['*'],
            'filter' => ['UF_CODE' => $code],
        ]);

        $getResArray = $getRes->fetch();

        return $getResArray;

    }

    public static function addElement(array $arAdd, string $hlBlockCode)
    {
        $entity = HL\HighloadBlockTable::compileEntity(self::getHlBlockIdByCode($hlBlockCode))->getDataClass();
        $getRes = $entity::add($arAdd);

        return $getRes;
    }

    public static function deleteMany(array $ids, string $hlBlockCode)
    {
        $entity = HL\HighloadBlockTable::compileEntity(self::getHlBlockIdByCode($hlBlockCode))->getDataClass();
        foreach ($ids as $id) {
            $entity::Delete($id);
        }
    }

    public static function checkExistElementByUfId(int $id, string $hlBlockCode)
    {
        $entity = HL\HighloadBlockTable::compileEntity(self::getHlBlockIdByCode($hlBlockCode))->getDataClass();
        $getRes = $entity::getlist([
            'select' => ['ID'],
            'filter' => ['UF_ID' => $id],
        ]);

        $getResArray = $getRes->fetch();

        return $getResArray['ID'];
    }

    public static function getOneElement(array $params, int $hlBlockCode)
    {
        $entity = HL\HighloadBlockTable::compileEntity(self::getHlBlockIdByCode($hlBlockCode))->getDataClass();
        $getRes = $entity::getlist($params);

        $getResArray = $getRes->fetch();

        return $getResArray;
    }

    public static function getHlBlockObjByHlBlockCode($hlBlockCode)
    {
        $entity = HL\HighloadBlockTable::compileEntity(self::getHlBlockIdByCode($hlBlockCode))->getDataClass();

        return $entity;
    }

    public static function getArrayEl(array $params, int $hlBlockCode)
    {
        $entity = HL\HighloadBlockTable::compileEntity(self::getHlBlockIdByCode($hlBlockCode))->getDataClass();
        $getRes = $entity::getlist($params);

        $elArray = [];

        while ($row = $getRes->fetch()) {
            $elArray[] = $row;
        }

        return $elArray;
    }

    public static function getArrayElId(array $filter, string $hlBlockCode)
    {
        $entity = HL\HighloadBlockTable::compileEntity(self::getHlBlockIdByCode($hlBlockCode))->getDataClass();
        $getRes = $entity::getlist([
            'select' => ['ID'],
            'filter' => $filter
        ]);

        $elArray = [];

        while ($row = $getRes->fetch()) {
            $elArray[] = $row['ID'];
        }

        return $elArray;
    }

}