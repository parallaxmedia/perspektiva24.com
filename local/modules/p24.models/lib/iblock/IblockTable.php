<?php

namespace P24\Models\Iblock;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\BooleanField,
    Bitrix\Main\ORM\Fields\DatetimeField,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\Relations\Reference,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\TextField,
    Bitrix\Main\ORM\Fields\Validators\LengthValidator,
    Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);

/**
 * Class IblockTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TIMESTAMP_X datetime optional default current datetime
 * <li> IBLOCK_TYPE_ID string(50) mandatory
 * <li> LID string(2) mandatory
 * <li> CODE string(50) optional
 * <li> API_CODE string(50) optional
 * <li> NAME string(255) mandatory
 * <li> ACTIVE bool ('N', 'Y') optional default 'Y'
 * <li> SORT int optional default 500
 * <li> LIST_PAGE_URL string(255) optional
 * <li> DETAIL_PAGE_URL string(255) optional
 * <li> SECTION_PAGE_URL string(255) optional
 * <li> CANONICAL_PAGE_URL string(255) optional
 * <li> PICTURE int optional
 * <li> DESCRIPTION text optional
 * <li> DESCRIPTION_TYPE enum ('text', 'html') optional default 'text'
 * <li> RSS_TTL int optional default 24
 * <li> RSS_ACTIVE bool ('N', 'Y') optional default 'Y'
 * <li> RSS_FILE_ACTIVE bool ('N', 'Y') optional default 'N'
 * <li> RSS_FILE_LIMIT int optional
 * <li> RSS_FILE_DAYS int optional
 * <li> RSS_YANDEX_ACTIVE bool ('N', 'Y') optional default 'N'
 * <li> XML_ID string(255) optional
 * <li> TMP_ID string(40) optional
 * <li> INDEX_ELEMENT bool ('N', 'Y') optional default 'Y'
 * <li> INDEX_SECTION bool ('N', 'Y') optional default 'N'
 * <li> WORKFLOW bool ('N', 'Y') optional default 'Y'
 * <li> BIZPROC bool ('N', 'Y') optional default 'N'
 * <li> SECTION_CHOOSER string(1) optional
 * <li> LIST_MODE string(1) optional
 * <li> RIGHTS_MODE string(1) optional
 * <li> SECTION_PROPERTY string(1) optional
 * <li> PROPERTY_INDEX string(1) optional
 * <li> VERSION int optional default 1
 * <li> LAST_CONV_ELEMENT int optional default 0
 * <li> SOCNET_GROUP_ID int optional
 * <li> EDIT_FILE_BEFORE string(255) optional
 * <li> EDIT_FILE_AFTER string(255) optional
 * <li> SECTIONS_NAME string(100) optional
 * <li> SECTION_NAME string(100) optional
 * <li> ELEMENTS_NAME string(100) optional
 * <li> ELEMENT_NAME string(100) optional
 * <li> PICTURE reference to {@link \Bitrix\File\FileTable}
 * <li> IBLOCK_TYPE_ID reference to {@link \Bitrix\Iblock\IblockTypeTable}
 * <li> LID reference to {@link \Bitrix\Lang\LangTable}
 * <li> SOCNET_GROUP_ID reference to {@link \Bitrix\Sonet\SonetGroupTable}
 * </ul>
 *
 * @package Bitrix\Iblock
 **/

class IblockTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('IBLOCK_ENTITY_ID_FIELD')
                ]
            ),
            new DatetimeField(
                'TIMESTAMP_X',
                [
                    'default' => function()
                    {
                        return new DateTime();
                    },
                    'title' => Loc::getMessage('IBLOCK_ENTITY_TIMESTAMP_X_FIELD')
                ]
            ),
            new StringField(
                'IBLOCK_TYPE_ID',
                [
                    'required' => true,
                    'validation' => [__CLASS__, 'validateIblockTypeId'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_IBLOCK_TYPE_ID_FIELD')
                ]
            ),
            new StringField(
                'LID',
                [
                    'required' => true,
                    'validation' => [__CLASS__, 'validateLid'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_LID_FIELD')
                ]
            ),
            new StringField(
                'CODE',
                [
                    'validation' => [__CLASS__, 'validateCode'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_CODE_FIELD')
                ]
            ),
            new StringField(
                'API_CODE',
                [
                    'validation' => [__CLASS__, 'validateApiCode'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_API_CODE_FIELD')
                ]
            ),
            new StringField(
                'NAME',
                [
                    'required' => true,
                    'validation' => [__CLASS__, 'validateName'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_NAME_FIELD')
                ]
            ),
            new BooleanField(
                'ACTIVE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'Y',
                    'title' => Loc::getMessage('IBLOCK_ENTITY_ACTIVE_FIELD')
                ]
            ),
            new IntegerField(
                'SORT',
                [
                    'default' => 500,
                    'title' => Loc::getMessage('IBLOCK_ENTITY_SORT_FIELD')
                ]
            ),
            new StringField(
                'LIST_PAGE_URL',
                [
                    'validation' => [__CLASS__, 'validateListPageUrl'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_LIST_PAGE_URL_FIELD')
                ]
            ),
            new StringField(
                'DETAIL_PAGE_URL',
                [
                    'validation' => [__CLASS__, 'validateDetailPageUrl'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_DETAIL_PAGE_URL_FIELD')
                ]
            ),
            new StringField(
                'SECTION_PAGE_URL',
                [
                    'validation' => [__CLASS__, 'validateSectionPageUrl'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_SECTION_PAGE_URL_FIELD')
                ]
            ),
            new StringField(
                'CANONICAL_PAGE_URL',
                [
                    'validation' => [__CLASS__, 'validateCanonicalPageUrl'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_CANONICAL_PAGE_URL_FIELD')
                ]
            ),
            new IntegerField(
                'PICTURE',
                [
                    'title' => Loc::getMessage('IBLOCK_ENTITY_PICTURE_FIELD')
                ]
            ),
            new TextField(
                'DESCRIPTION',
                [
                    'title' => Loc::getMessage('IBLOCK_ENTITY_DESCRIPTION_FIELD')
                ]
            ),
            new StringField(
                'DESCRIPTION_TYPE',
                [
                    'values' => array('text', 'html'),
                    'default' => 'text',
                    'title' => Loc::getMessage('IBLOCK_ENTITY_DESCRIPTION_TYPE_FIELD')
                ]
            ),
            new IntegerField(
                'RSS_TTL',
                [
                    'default' => 24,
                    'title' => Loc::getMessage('IBLOCK_ENTITY_RSS_TTL_FIELD')
                ]
            ),
            new BooleanField(
                'RSS_ACTIVE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'Y',
                    'title' => Loc::getMessage('IBLOCK_ENTITY_RSS_ACTIVE_FIELD')
                ]
            ),
            new BooleanField(
                'RSS_FILE_ACTIVE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'N',
                    'title' => Loc::getMessage('IBLOCK_ENTITY_RSS_FILE_ACTIVE_FIELD')
                ]
            ),
            new IntegerField(
                'RSS_FILE_LIMIT',
                [
                    'title' => Loc::getMessage('IBLOCK_ENTITY_RSS_FILE_LIMIT_FIELD')
                ]
            ),
            new IntegerField(
                'RSS_FILE_DAYS',
                [
                    'title' => Loc::getMessage('IBLOCK_ENTITY_RSS_FILE_DAYS_FIELD')
                ]
            ),
            new BooleanField(
                'RSS_YANDEX_ACTIVE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'N',
                    'title' => Loc::getMessage('IBLOCK_ENTITY_RSS_YANDEX_ACTIVE_FIELD')
                ]
            ),
            new StringField(
                'XML_ID',
                [
                    'validation' => [__CLASS__, 'validateXmlId'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_XML_ID_FIELD')
                ]
            ),
            new StringField(
                'TMP_ID',
                [
                    'validation' => [__CLASS__, 'validateTmpId'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_TMP_ID_FIELD')
                ]
            ),
            new BooleanField(
                'INDEX_ELEMENT',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'Y',
                    'title' => Loc::getMessage('IBLOCK_ENTITY_INDEX_ELEMENT_FIELD')
                ]
            ),
            new BooleanField(
                'INDEX_SECTION',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'N',
                    'title' => Loc::getMessage('IBLOCK_ENTITY_INDEX_SECTION_FIELD')
                ]
            ),
            new BooleanField(
                'WORKFLOW',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'Y',
                    'title' => Loc::getMessage('IBLOCK_ENTITY_WORKFLOW_FIELD')
                ]
            ),
            new BooleanField(
                'BIZPROC',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'N',
                    'title' => Loc::getMessage('IBLOCK_ENTITY_BIZPROC_FIELD')
                ]
            ),
            new StringField(
                'SECTION_CHOOSER',
                [
                    'validation' => [__CLASS__, 'validateSectionChooser'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_SECTION_CHOOSER_FIELD')
                ]
            ),
            new StringField(
                'LIST_MODE',
                [
                    'validation' => [__CLASS__, 'validateListMode'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_LIST_MODE_FIELD')
                ]
            ),
            new StringField(
                'RIGHTS_MODE',
                [
                    'validation' => [__CLASS__, 'validateRightsMode'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_RIGHTS_MODE_FIELD')
                ]
            ),
            new StringField(
                'SECTION_PROPERTY',
                [
                    'validation' => [__CLASS__, 'validateSectionProperty'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_SECTION_PROPERTY_FIELD')
                ]
            ),
            new StringField(
                'PROPERTY_INDEX',
                [
                    'validation' => [__CLASS__, 'validatePropertyIndex'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_PROPERTY_INDEX_FIELD')
                ]
            ),
            new IntegerField(
                'VERSION',
                [
                    'default' => 1,
                    'title' => Loc::getMessage('IBLOCK_ENTITY_VERSION_FIELD')
                ]
            ),
            new IntegerField(
                'LAST_CONV_ELEMENT',
                [
                    'default' => 0,
                    'title' => Loc::getMessage('IBLOCK_ENTITY_LAST_CONV_ELEMENT_FIELD')
                ]
            ),
            new IntegerField(
                'SOCNET_GROUP_ID',
                [
                    'title' => Loc::getMessage('IBLOCK_ENTITY_SOCNET_GROUP_ID_FIELD')
                ]
            ),
            new StringField(
                'EDIT_FILE_BEFORE',
                [
                    'validation' => [__CLASS__, 'validateEditFileBefore'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_EDIT_FILE_BEFORE_FIELD')
                ]
            ),
            new StringField(
                'EDIT_FILE_AFTER',
                [
                    'validation' => [__CLASS__, 'validateEditFileAfter'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_EDIT_FILE_AFTER_FIELD')
                ]
            ),
            new StringField(
                'SECTIONS_NAME',
                [
                    'validation' => [__CLASS__, 'validateSectionsName'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_SECTIONS_NAME_FIELD')
                ]
            ),
            new StringField(
                'SECTION_NAME',
                [
                    'validation' => [__CLASS__, 'validateSectionName'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_SECTION_NAME_FIELD')
                ]
            ),
            new StringField(
                'ELEMENTS_NAME',
                [
                    'validation' => [__CLASS__, 'validateElementsName'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_ELEMENTS_NAME_FIELD')
                ]
            ),
            new StringField(
                'ELEMENT_NAME',
                [
                    'validation' => [__CLASS__, 'validateElementName'],
                    'title' => Loc::getMessage('IBLOCK_ENTITY_ELEMENT_NAME_FIELD')
                ]
            ),
            new Reference(
                'FILE',
                '\Bitrix\File\File',
                ['=this.PICTURE' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'IBLOCK_TYPE',
                '\Bitrix\Iblock\IblockType',
                ['=this.IBLOCK_TYPE_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'LANG',
                '\Bitrix\Lang\Lang',
                ['=this.LID' => 'ref.LID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'SOCNET_GROUP',
                '\Bitrix\Sonet\SonetGroup',
                ['=this.SOCNET_GROUP_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        ];
    }

    /**
     * Returns validators for IBLOCK_TYPE_ID field.
     *
     * @return array
     */
    public static function validateIblockTypeId()
    {
        return [
            new LengthValidator(null, 50),
        ];
    }

    /**
     * Returns validators for LID field.
     *
     * @return array
     */
    public static function validateLid()
    {
        return [
            new LengthValidator(null, 2),
        ];
    }

    /**
     * Returns validators for CODE field.
     *
     * @return array
     */
    public static function validateCode()
    {
        return [
            new LengthValidator(null, 50),
        ];
    }

    /**
     * Returns validators for API_CODE field.
     *
     * @return array
     */
    public static function validateApiCode()
    {
        return [
            new LengthValidator(null, 50),
        ];
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for LIST_PAGE_URL field.
     *
     * @return array
     */
    public static function validateListPageUrl()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for DETAIL_PAGE_URL field.
     *
     * @return array
     */
    public static function validateDetailPageUrl()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for SECTION_PAGE_URL field.
     *
     * @return array
     */
    public static function validateSectionPageUrl()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for CANONICAL_PAGE_URL field.
     *
     * @return array
     */
    public static function validateCanonicalPageUrl()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for XML_ID field.
     *
     * @return array
     */
    public static function validateXmlId()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for TMP_ID field.
     *
     * @return array
     */
    public static function validateTmpId()
    {
        return [
            new LengthValidator(null, 40),
        ];
    }

    /**
     * Returns validators for SECTION_CHOOSER field.
     *
     * @return array
     */
    public static function validateSectionChooser()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for LIST_MODE field.
     *
     * @return array
     */
    public static function validateListMode()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for RIGHTS_MODE field.
     *
     * @return array
     */
    public static function validateRightsMode()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for SECTION_PROPERTY field.
     *
     * @return array
     */
    public static function validateSectionProperty()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for PROPERTY_INDEX field.
     *
     * @return array
     */
    public static function validatePropertyIndex()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for EDIT_FILE_BEFORE field.
     *
     * @return array
     */
    public static function validateEditFileBefore()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for EDIT_FILE_AFTER field.
     *
     * @return array
     */
    public static function validateEditFileAfter()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for SECTIONS_NAME field.
     *
     * @return array
     */
    public static function validateSectionsName()
    {
        return [
            new LengthValidator(null, 100),
        ];
    }

    /**
     * Returns validators for SECTION_NAME field.
     *
     * @return array
     */
    public static function validateSectionName()
    {
        return [
            new LengthValidator(null, 100),
        ];
    }

    /**
     * Returns validators for ELEMENTS_NAME field.
     *
     * @return array
     */
    public static function validateElementsName()
    {
        return [
            new LengthValidator(null, 100),
        ];
    }

    /**
     * Returns validators for ELEMENT_NAME field.
     *
     * @return array
     */
    public static function validateElementName()
    {
        return [
            new LengthValidator(null, 100),
        ];
    }
}