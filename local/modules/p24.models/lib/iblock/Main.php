<?php


namespace P24\Models\Iblock;

use P24\Models\Iblock\ElementTable;
use P24\Models\Iblock\IblockTable;
use P24\Models\Iblock\PropertyTable;
use Bitrix\Main\Loader;
use P24\Models\Iblock\SectionTable;
use Bitrix\Iblock\Model;
use P24\Rest\Model\Company;

Loader::includeModule('iblock');

class Main
{
    const GEO_FIELDS = ['COUNTRY', 'REGION', 'CITY', 'DISTRICT'];
    const IMG_FIELDS = ['IMAGES', 'IMAGES_LAYOUT'];

    public static $propTypesPrepare = ['L', 'G', 'E', 'N'];

    public static function getIblockIdByCode(string $code): ?int
    {

        $iblockArray = IblockTable::getList([
            'select' => ['ID'],
            'filter' => ['CODE' => $code]
        ]);

        $getRes = $iblockArray->fetch();

        return $getRes['ID'];
    }

    /**
     * @param  string  $fileldName
     * @param  int  $iblockCode
     * @param $value
     * Property Type name
     *   S - string
     *   N - int
     *   L - list
     *   F - file
     *   G - relationship to section
     *   E - relationshop to element
     */
    public static function preparePropertyFieldData($value, string $code, int $iblockId)
    {
        if (in_array($code, self::GEO_FIELDS)) {
            $CityValue = self::prepareGeoField(trim($value), $code);
            return $CityValue ?: '';
        }

        if (in_array($code, self::IMG_FIELDS)) {
            $resArray = [];
            foreach ($value as $key => $val) {
                $keyy = 'n'.$key;
                $resArray[$keyy]['VALUE'] = $val['image'];
                $resArray[$keyy]['DESCRIPTION'] = $val['thumb'];
            }

            return $resArray;
        }

        $getPropInfo = self::getPropertyInfo($code, $iblockId);
        $propValue = "";

        if (in_array($getPropInfo['PROPERTY_TYPE'], self::$propTypesPrepare)) {
            switch ($getPropInfo['PROPERTY_TYPE']) {
                case 'L':
                    if (is_array($value)) {
                        $propValue = [];
                        foreach ($value as $val) {
                            $propValue[] = self::getPropertyListId($val, $getPropInfo['ID']);
                        }
                    } else {
                        $propValue = self::getPropertyListId($value, $getPropInfo['ID']);
                    }
                    break;
                case 'E':
                    if (is_array($value)) {
                        $propValue = [];
                        foreach ($value as $val) {
                            $propValue[] = self::getPropertyIblockElementId($val, $getPropInfo['LINK_IBLOCK_ID']);
                        }
                    } else {
                        $propValue = self::getPropertyIblockElementId($value, $getPropInfo['LINK_IBLOCK_ID']);
                    }
                    break;
                case 'G':
                    if (is_array($value)) {
                        $propValue = [];
                        foreach ($value as $val) {
                            $propValue[] = self::getPropertyIblockSectionId($val, $getPropInfo['LINK_IBLOCK_ID']);
                        }
                    } else {
                        $propValue = self::getPropertyIblockSectionId($value, $getPropInfo['LINK_IBLOCK_ID']);
                    }
                    break;
                case 'N':
                    if ($value == 0) {
                        $propValue = '';
                    } else {
                        $propValue = $value;
                    }
                    break;
            }
        } else {
            $propValue = $value;
        }

        return $propValue;
    }

    /**
     * @param  string  $fileldName
     * @param  int  $iblockCode
     * @param $value
     * Property Type name
     *   S - string
     *   N - int
     *   L - list
     *   F - file
     *   G - relationship to section
     *   E - relationshop to element
     */
    public static function prepareUserFieldData($value, string $code, int $iblockId)
    {
        if (in_array($code, self::GEO_FIELDS)) {
            $CityValue = self::prepareGeoField(trim($value), $code);
            return $CityValue ?: '';
        }

        $getPropInfo = self::getPropertyInfo($code, $iblockId);
        $propValue = "";
        if (in_array($getPropInfo['PROPERTY_TYPE'], self::$propTypesPrepare)) {
            switch ($getPropInfo['PROPERTY_TYPE']) {
                case 'L':
                    $propValue = self::getPropertyListId($value, $getPropInfo['ID']);
                    break;
                case 'E':
                    $propValue = self::getPropertyIblockElementId($value, $getPropInfo['LINK_IBLOCK_ID']);
                    break;
                case 'G':
                    $propValue = self::getPropertyIblockSectionId($value, $getPropInfo['LINK_IBLOCK_ID']);
                    break;
            }
        } else {
            $propValue = $value;
        }

        return $propValue;
    }

    public static function prepareGeoField(string $geoName, string $fieldName)
    {
        $cityId = Company::checkExistGeoByName($geoName, $fieldName);
        if (empty($cityId)) {
            $cityId = Company::addAddressByName($geoName, $fieldName);
        }

        return $cityId;
    }

    public static function getPropertyInfo(string $code, int $iblockId): ?array
    {
        $getResult = PropertyTable::getLIst([
            'select' => ['PROPERTY_TYPE', 'LINK_IBLOCK_ID', 'MULTIPLE', 'ID'],
            'filter' => ['CODE' => $code, 'IBLOCK_ID' => $iblockId],
            'order' => ['VERSION' => 'DESC']
        ]);

        $getArray = $getResult->fetch();

        if ($getArray) {
            return $getArray;
        } else {
            return null;
        }
    }

    public static function getPropertyListId($xml_id, int $fieldId)
    {

        /* Bitrix not support xml_id zero value.
         * And this make support
         */
        if ($xml_id == '0') {
            $xml_id = '00';
        }

        $property_enums = \CIBlockPropertyEnum::GetList(array("SORT" => "ASC"),
            array("XML_ID" => $xml_id, "PROPERTY_ID" => $fieldId));
        $getData = [];

        while ($enum_fields = $property_enums->GetNext()) {
            $getData = $enum_fields;
        }

        return $getData['ID'];
    }

    public static function getPropertyIblockElementId($xml_id, int $iblockId): ?int
    {
        $getResult = ElementTable::getlist([
            'select' => ['ID'],
            'filter' => ['XML_ID' => $xml_id, 'IBLOCK_ID' => $iblockId]
        ]);

        $getArray = $getResult->fetch();

        return $getArray['ID'];
    }

    public static function getPropertyIblockSectionId($xml_id, int $iblockId)
    {
        $getResult = SectionTable::getlist([
            'select' => ['ID'],
            'filter' => ['XML_ID' => $xml_id, 'IBLOCK_ID' => $iblockId]
        ]);

        $getArray = $getResult->fetch();

        return $getArray['ID'];
    }

    public static function addIblockSection(array $addArray, string $iblockCode)
    {
        $entity = Model\Section::compileEntityByIblock(self::getIblockIdByCode($iblockCode));


    }

    /**
     * @param  int  $crmId
     * @param  int  $iblockId
     * @return int|null
     * Xml_id is filled form api crm object id field.
     */
    public static function getExistIblockElementByXmlId(int $crmId, int $iblockId): ?int
    {
        $getResArray = ElementTable::getList([
            'select' => ['ID',],
            'filter' => ['XML_ID' => $crmId, 'IBLOCK_ID' => $iblockId]
        ]);

        $arRes = $getResArray->fetch();

        return $arRes['ID'];
    }

    public static function getElementArrayIdByXmlIds(array $crmIds, int $iblockId): ?array
    {
        $getRes = ElementTable::getList([
            'select' => ['ID'],
            'filter' => ['XML_ID' => $crmIds, 'IBLOCK_ID' => $iblockId]
        ]);

        return $getRes->fetchAll();
    }

    public static function getSectionArrayIdByXmlIds(array $crmIds, int $iblockId): ?array
    {
        $getRes = SectionTable::getList([
            'select' => ['ID'],
            'filter' => ['XML_ID' => $crmIds, 'IBLOCK_ID' => $iblockId]
        ]);

        return $getRes->fetchAll();
    }


    public static function getExistIblockSectionByXmlId($crmId, int $iblockId): ?int
    {
        $getResArray = SectionTable::getList([
            'select' => ['ID',],
            'filter' => ['XML_ID' => $crmId, 'IBLOCK_ID' => $iblockId]
        ]);

        $arRes = $getResArray->fetch();

        return $arRes['ID'];
    }

    public static function clearIblockAllRecords(array $iblockId = [5, 2, 1, 4, 6,])
    {
        $getElements = self::getAllIblocksElement($iblockId);
        $getSection = self::getAllIblocksSection($iblockId);
        $resArray = [];

        $resArray['elements'] = self::clearIblockElements($getElements);
        $resArray['sections'] = self::clearIblockSections($getSection);

        return $resArray;
    }

    public static function getAllIblocksElement(array $iblockId)
    {
        $getResult = ElementTable::Getlist([
            'select' => ['ID'],
            'filter' => ['IBLOCK_ID' => $iblockId]
        ]);

        $getArray = $getResult->fetchAll();

        return $getArray;
    }

    public static function getAllIblocksSection(array $iblockId)
    {
        $getResult = SectionTable::Getlist([
            'select' => ['ID'],
            'filter' => ['IBLOCK_ID' => $iblockId]
        ]);

        $getArray = $getResult->fetchAll();

        return $getArray;
    }

    public static function clearIblockElements(array $ids)
    {
        $result = [];
        global $DB;

        foreach ($ids as $id) {
            $DB->StartTransaction();
            if (array_key_exists('ID', $id)) {
                $removeEl = \CIBlockElement::Delete($id['ID']);
            } else {
                $removeEl = \CIBlockElement::Delete($id);
            }
            if (!$removeEl) {
                $result[$id['ID'] ?: $id] = 'Error!';
                $DB->Rollback();
            } else {
                $DB->Commit();
                $result[$id['ID'] ?: $id] = $removeEl;
            }
        }

        return $result;
    }

    public static function clearIblockSections(array $ids)
    {
        $result = [];
        global $DB;

        foreach ($ids as $id) {
            $DB->StartTransaction();
            $removeSec = \CIBlockSection::Delete($id['ID']);
            if (!$removeSec) {
                $result[$id] = 'Error!';
                $DB->Rollback();
            } else {
                $DB->Commit();
                $result[$id] = $removeSec;
            }
        }

        return $result;
    }

    public static function disableIbElement(array $ids)
    {
        $updateParams = [
            'ACTIVE' => 'N'
        ];

        $getRes = ElementTable::updateMulti($ids, $updateParams);

        return $getRes->isSuccess();

    }

}