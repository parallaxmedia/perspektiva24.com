<?php


namespace P24\Models\Iblock;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\BooleanField,
    Bitrix\Main\ORM\Fields\DatetimeField,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\Relations\Reference,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\TextField,
    Bitrix\Main\ORM\Fields\Validators\LengthValidator,
    Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);

/**
 * Class PropertyTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TIMESTAMP_X datetime optional default current datetime
 * <li> IBLOCK_ID int mandatory
 * <li> NAME string(255) mandatory
 * <li> ACTIVE bool ('N', 'Y') optional default 'Y'
 * <li> SORT int optional default 500
 * <li> CODE string(50) optional
 * <li> DEFAULT_VALUE text optional
 * <li> PROPERTY_TYPE string(1) optional default 'S'
 * <li> ROW_COUNT int optional default 1
 * <li> COL_COUNT int optional default 30
 * <li> LIST_TYPE string(1) optional default 'L'
 * <li> MULTIPLE bool ('N', 'Y') optional default 'N'
 * <li> XML_ID string(100) optional
 * <li> FILE_TYPE string(200) optional
 * <li> MULTIPLE_CNT int optional
 * <li> TMP_ID string(40) optional
 * <li> LINK_IBLOCK_ID int optional
 * <li> WITH_DESCRIPTION string(1) optional
 * <li> SEARCHABLE bool ('N', 'Y') optional default 'N'
 * <li> FILTRABLE bool ('N', 'Y') optional default 'N'
 * <li> IS_REQUIRED string(1) optional
 * <li> VERSION int optional default 1
 * <li> USER_TYPE string(255) optional
 * <li> USER_TYPE_SETTINGS text optional
 * <li> HINT string(255) optional
 * <li> IBLOCK_ID reference to {@link \Bitrix\Iblock\IblockTable}
 * <li> LINK_IBLOCK_ID reference to {@link \Bitrix\Iblock\IblockTable}
 * </ul>
 *
 * @package Bitrix\Iblock
 **/

class PropertyTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_property';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('PROPERTY_ENTITY_ID_FIELD')
                ]
            ),
            new DatetimeField(
                'TIMESTAMP_X',
                [
                    'default' => function()
                    {
                        return new DateTime();
                    },
                    'title' => Loc::getMessage('PROPERTY_ENTITY_TIMESTAMP_X_FIELD')
                ]
            ),
            new IntegerField(
                'IBLOCK_ID',
                [
                    'required' => true,
                    'title' => Loc::getMessage('PROPERTY_ENTITY_IBLOCK_ID_FIELD')
                ]
            ),
            new StringField(
                'NAME',
                [
                    'required' => true,
                    'validation' => [__CLASS__, 'validateName'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_NAME_FIELD')
                ]
            ),
            new BooleanField(
                'ACTIVE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'Y',
                    'title' => Loc::getMessage('PROPERTY_ENTITY_ACTIVE_FIELD')
                ]
            ),
            new IntegerField(
                'SORT',
                [
                    'default' => 500,
                    'title' => Loc::getMessage('PROPERTY_ENTITY_SORT_FIELD')
                ]
            ),
            new StringField(
                'CODE',
                [
                    'validation' => [__CLASS__, 'validateCode'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_CODE_FIELD')
                ]
            ),
            new TextField(
                'DEFAULT_VALUE',
                [
                    'title' => Loc::getMessage('PROPERTY_ENTITY_DEFAULT_VALUE_FIELD')
                ]
            ),
            new StringField(
                'PROPERTY_TYPE',
                [
                    'default' => 'S',
                    'validation' => [__CLASS__, 'validatePropertyType'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_PROPERTY_TYPE_FIELD')
                ]
            ),
            new IntegerField(
                'ROW_COUNT',
                [
                    'default' => 1,
                    'title' => Loc::getMessage('PROPERTY_ENTITY_ROW_COUNT_FIELD')
                ]
            ),
            new IntegerField(
                'COL_COUNT',
                [
                    'default' => 30,
                    'title' => Loc::getMessage('PROPERTY_ENTITY_COL_COUNT_FIELD')
                ]
            ),
            new StringField(
                'LIST_TYPE',
                [
                    'default' => 'L',
                    'validation' => [__CLASS__, 'validateListType'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_LIST_TYPE_FIELD')
                ]
            ),
            new BooleanField(
                'MULTIPLE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'N',
                    'title' => Loc::getMessage('PROPERTY_ENTITY_MULTIPLE_FIELD')
                ]
            ),
            new StringField(
                'XML_ID',
                [
                    'validation' => [__CLASS__, 'validateXmlId'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_XML_ID_FIELD')
                ]
            ),
            new StringField(
                'FILE_TYPE',
                [
                    'validation' => [__CLASS__, 'validateFileType'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_FILE_TYPE_FIELD')
                ]
            ),
            new IntegerField(
                'MULTIPLE_CNT',
                [
                    'title' => Loc::getMessage('PROPERTY_ENTITY_MULTIPLE_CNT_FIELD')
                ]
            ),
            new StringField(
                'TMP_ID',
                [
                    'validation' => [__CLASS__, 'validateTmpId'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_TMP_ID_FIELD')
                ]
            ),
            new IntegerField(
                'LINK_IBLOCK_ID',
                [
                    'title' => Loc::getMessage('PROPERTY_ENTITY_LINK_IBLOCK_ID_FIELD')
                ]
            ),
            new StringField(
                'WITH_DESCRIPTION',
                [
                    'validation' => [__CLASS__, 'validateWithDescription'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_WITH_DESCRIPTION_FIELD')
                ]
            ),
            new BooleanField(
                'SEARCHABLE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'N',
                    'title' => Loc::getMessage('PROPERTY_ENTITY_SEARCHABLE_FIELD')
                ]
            ),
            new BooleanField(
                'FILTRABLE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'N',
                    'title' => Loc::getMessage('PROPERTY_ENTITY_FILTRABLE_FIELD')
                ]
            ),
            new StringField(
                'IS_REQUIRED',
                [
                    'validation' => [__CLASS__, 'validateIsRequired'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_IS_REQUIRED_FIELD')
                ]
            ),
            new IntegerField(
                'VERSION',
                [
                    'default' => 1,
                    'title' => Loc::getMessage('PROPERTY_ENTITY_VERSION_FIELD')
                ]
            ),
            new StringField(
                'USER_TYPE',
                [
                    'validation' => [__CLASS__, 'validateUserType'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_USER_TYPE_FIELD')
                ]
            ),
            new TextField(
                'USER_TYPE_SETTINGS',
                [
                    'title' => Loc::getMessage('PROPERTY_ENTITY_USER_TYPE_SETTINGS_FIELD')
                ]
            ),
            new StringField(
                'HINT',
                [
                    'validation' => [__CLASS__, 'validateHint'],
                    'title' => Loc::getMessage('PROPERTY_ENTITY_HINT_FIELD')
                ]
            ),
            new Reference(
                'IBLOCK',
                '\Bitrix\Iblock\Iblock',
                ['=this.IBLOCK_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'LINK_IBLOCK',
                '\Bitrix\Iblock\Iblock',
                ['=this.LINK_IBLOCK_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        ];
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for CODE field.
     *
     * @return array
     */
    public static function validateCode()
    {
        return [
            new LengthValidator(null, 50),
        ];
    }

    /**
     * Returns validators for PROPERTY_TYPE field.
     *
     * @return array
     */
    public static function validatePropertyType()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for LIST_TYPE field.
     *
     * @return array
     */
    public static function validateListType()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for XML_ID field.
     *
     * @return array
     */
    public static function validateXmlId()
    {
        return [
            new LengthValidator(null, 100),
        ];
    }

    /**
     * Returns validators for FILE_TYPE field.
     *
     * @return array
     */
    public static function validateFileType()
    {
        return [
            new LengthValidator(null, 200),
        ];
    }

    /**
     * Returns validators for TMP_ID field.
     *
     * @return array
     */
    public static function validateTmpId()
    {
        return [
            new LengthValidator(null, 40),
        ];
    }

    /**
     * Returns validators for WITH_DESCRIPTION field.
     *
     * @return array
     */
    public static function validateWithDescription()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for IS_REQUIRED field.
     *
     * @return array
     */
    public static function validateIsRequired()
    {
        return [
            new LengthValidator(null, 1),
        ];
    }

    /**
     * Returns validators for USER_TYPE field.
     *
     * @return array
     */
    public static function validateUserType()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for HINT field.
     *
     * @return array
     */
    public static function validateHint()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }
}