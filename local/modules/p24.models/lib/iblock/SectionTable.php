<?php


namespace P24\Models\Iblock;


use Bitrix\Main\Localization\Loc,
    Bitrix\Main\ORM\Data\DataManager,
    Bitrix\Main\ORM\Fields\BooleanField,
    Bitrix\Main\ORM\Fields\DatetimeField,
    Bitrix\Main\ORM\Fields\IntegerField,
    Bitrix\Main\ORM\Fields\Relations\Reference,
    Bitrix\Main\ORM\Fields\StringField,
    Bitrix\Main\ORM\Fields\TextField,
    Bitrix\Main\ORM\Fields\Validators\LengthValidator,
    Bitrix\Main\Type\DateTime;

Loc::loadMessages(__FILE__);

/**
 * Class SectionTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TIMESTAMP_X datetime optional default current datetime
 * <li> MODIFIED_BY int optional
 * <li> DATE_CREATE datetime optional
 * <li> CREATED_BY int optional
 * <li> IBLOCK_ID int mandatory
 * <li> IBLOCK_SECTION_ID int optional
 * <li> ACTIVE bool ('N', 'Y') optional default 'Y'
 * <li> GLOBAL_ACTIVE bool ('N', 'Y') optional default 'Y'
 * <li> SORT int optional default 500
 * <li> NAME string(255) mandatory
 * <li> PICTURE int optional
 * <li> LEFT_MARGIN int optional
 * <li> RIGHT_MARGIN int optional
 * <li> DEPTH_LEVEL int optional
 * <li> DESCRIPTION text optional
 * <li> DESCRIPTION_TYPE enum ('text', 'html') optional default 'text'
 * <li> SEARCHABLE_CONTENT text optional
 * <li> CODE string(255) optional
 * <li> XML_ID string(255) optional
 * <li> TMP_ID string(40) optional
 * <li> DETAIL_PICTURE int optional
 * <li> SOCNET_GROUP_ID int optional
 * <li> DETAIL_PICTURE reference to {@link \Bitrix\File\FileTable}
 * <li> IBLOCK_ID reference to {@link \Bitrix\Iblock\IblockTable}
 * <li> IBLOCK_SECTION_ID reference to {@link \Bitrix\Iblock\IblockSectionTable}
 * <li> SOCNET_GROUP_ID reference to {@link \Bitrix\Sonet\SonetGroupTable}
 * <li> CREATED_BY reference to {@link \Bitrix\User\UserTable}
 * </ul>
 *
 * @package Bitrix\Iblock
 **/
class SectionTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_section';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return [
            new IntegerField(
                'ID',
                [
                    'primary' => true,
                    'autocomplete' => true,
                    'title' => Loc::getMessage('SECTION_ENTITY_ID_FIELD')
                ]
            ),
            new DatetimeField(
                'TIMESTAMP_X',
                [
                    'default' => function () {
                        return new DateTime();
                    },
                    'title' => Loc::getMessage('SECTION_ENTITY_TIMESTAMP_X_FIELD')
                ]
            ),
            new IntegerField(
                'MODIFIED_BY',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_MODIFIED_BY_FIELD')
                ]
            ),
            new DatetimeField(
                'DATE_CREATE',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_DATE_CREATE_FIELD')
                ]
            ),
            new IntegerField(
                'CREATED_BY',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_CREATED_BY_FIELD')
                ]
            ),
            new IntegerField(
                'IBLOCK_ID',
                [
                    'required' => true,
                    'title' => Loc::getMessage('SECTION_ENTITY_IBLOCK_ID_FIELD')
                ]
            ),
            new IntegerField(
                'IBLOCK_SECTION_ID',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_IBLOCK_SECTION_ID_FIELD')
                ]
            ),
            new BooleanField(
                'ACTIVE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'Y',
                    'title' => Loc::getMessage('SECTION_ENTITY_ACTIVE_FIELD')
                ]
            ),
            new BooleanField(
                'GLOBAL_ACTIVE',
                [
                    'values' => array('N', 'Y'),
                    'default' => 'Y',
                    'title' => Loc::getMessage('SECTION_ENTITY_GLOBAL_ACTIVE_FIELD')
                ]
            ),
            new IntegerField(
                'SORT',
                [
                    'default' => 500,
                    'title' => Loc::getMessage('SECTION_ENTITY_SORT_FIELD')
                ]
            ),
            new StringField(
                'NAME',
                [
                    'required' => true,
                    'validation' => [__CLASS__, 'validateName'],
                    'title' => Loc::getMessage('SECTION_ENTITY_NAME_FIELD')
                ]
            ),
            new IntegerField(
                'PICTURE',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_PICTURE_FIELD')
                ]
            ),
            new IntegerField(
                'LEFT_MARGIN',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_LEFT_MARGIN_FIELD')
                ]
            ),
            new IntegerField(
                'RIGHT_MARGIN',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_RIGHT_MARGIN_FIELD')
                ]
            ),
            new IntegerField(
                'DEPTH_LEVEL',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_DEPTH_LEVEL_FIELD')
                ]
            ),
            new TextField(
                'DESCRIPTION',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_DESCRIPTION_FIELD')
                ]
            ),
            new StringField(
                'DESCRIPTION_TYPE',
                [
                    'values' => array('text', 'html'),
                    'default' => 'text',
                    'title' => Loc::getMessage('SECTION_ENTITY_DESCRIPTION_TYPE_FIELD')
                ]
            ),
            new TextField(
                'SEARCHABLE_CONTENT',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_SEARCHABLE_CONTENT_FIELD')
                ]
            ),
            new StringField(
                'CODE',
                [
                    'validation' => [__CLASS__, 'validateCode'],
                    'title' => Loc::getMessage('SECTION_ENTITY_CODE_FIELD')
                ]
            ),
            new StringField(
                'XML_ID',
                [
                    'validation' => [__CLASS__, 'validateXmlId'],
                    'title' => Loc::getMessage('SECTION_ENTITY_XML_ID_FIELD')
                ]
            ),
            new StringField(
                'TMP_ID',
                [
                    'validation' => [__CLASS__, 'validateTmpId'],
                    'title' => Loc::getMessage('SECTION_ENTITY_TMP_ID_FIELD')
                ]
            ),
            new IntegerField(
                'DETAIL_PICTURE',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_DETAIL_PICTURE_FIELD')
                ]
            ),
            new IntegerField(
                'SOCNET_GROUP_ID',
                [
                    'title' => Loc::getMessage('SECTION_ENTITY_SOCNET_GROUP_ID_FIELD')
                ]
            ),
            new Reference(
                'FILE',
                '\Bitrix\File\File',
                ['=this.DETAIL_PICTURE' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'IBLOCK',
                '\Bitrix\Iblock\Iblock',
                ['=this.IBLOCK_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'IBLOCK_SECTION',
                '\Bitrix\Iblock\IblockSection',
                ['=this.IBLOCK_SECTION_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'SOCNET_GROUP',
                '\Bitrix\Sonet\SonetGroup',
                ['=this.SOCNET_GROUP_ID' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
            new Reference(
                'USER',
                '\Bitrix\User\User',
                ['=this.CREATED_BY' => 'ref.ID'],
                ['join_type' => 'LEFT']
            ),
        ];
    }

    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for CODE field.
     *
     * @return array
     */
    public static function validateCode()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for XML_ID field.
     *
     * @return array
     */
    public static function validateXmlId()
    {
        return [
            new LengthValidator(null, 255),
        ];
    }

    /**
     * Returns validators for TMP_ID field.
     *
     * @return array
     */
    public static function validateTmpId()
    {
        return [
            new LengthValidator(null, 40),
        ];
    }
}