<?php

namespace P24\Rest;

use P24\Rest\Curl\Curlsend;

class Request
{
    const CRM_API_URL = 'https://onperspektiva24.com/frontend/web/api/site_api_v2/';
    const CRM_API_LOGIN = 'site_api_v2';
    const CRM_API_PASSWORD = '735Y9egfet5whYr045x';
    const CRM_API_GET_AUTH = 'login/auth';
    const CRM_API_GET_FILIAL = 'filial/index';
    const CRM_API_GET_AGENT = 'user/index';
    const CRM_API_GET_DEVELOPERS = 'developer/index';
    const CRM_API_GET_QUARTER = 'developer/get-quarters';
    const CRM_API_GET_COMPLEX = 'developer/get-complexes';
    const CRM_API_GET_REALTY = 'object/index';
    const CRM_API_GET_CATEGORY = 'object/categories';
    const CRM_API_GET_SUBCATEGORY = 'object/sub-categories';
    const CRM_API_SEND_LEAD = 'user/create-lead';
    const CRM_API_ADD_AGENT_REVIEW = 'user/add-review-to-agent';

    public $authKey = '';

    function __construct()
    {
        $queryAuthKey = [
            'username' => self::CRM_API_LOGIN,
            'password' => self::CRM_API_PASSWORD,
        ];
        $urlAuth = self::CRM_API_URL . self::CRM_API_GET_AUTH;

        $getAuthArray = Curlsend::sendPost($urlAuth, http_build_query($queryAuthKey),'');

        if ($getAuthArray['status'] == 'success') {
            $this->authKey = $getAuthArray['data']['access_token'];
        } else {
            throw new \Exception('Authorization error.');
        }
    }

    // TODO write method this check exist authorization in api
    protected static function checkApiKey()
    {

    }

    public function getFilial(int $page = 1, int $limit = 10)
    {
        $getFilialUrl = self::CRM_API_URL . self::CRM_API_GET_FILIAL . "?page=" . $page . "&limit=" . $limit;

        return Curlsend::sendGet($getFilialUrl, $this->authKey);
    }

    public function getAgents(int $page = 1, int $limit = 10, int $status = 2)
    {
        $getFilialUrl = self::CRM_API_URL . self::CRM_API_GET_AGENT . "?status=" . $status . "&page=" . $page . "&limit=" . $limit;

        return Curlsend::sendGet($getFilialUrl, $this->authKey);
    }

    public function getDevelopers(int $page = 1, int $limit = 10)
    {
        $getFilialUrl = self::CRM_API_URL . self::CRM_API_GET_DEVELOPERS . "?&page=" . $page . "&limit=" . $limit;

        return Curlsend::sendGet($getFilialUrl, $this->authKey);
    }

    public function getQuarters(int $page = 1, int $limit = 10)
    {
        $getFilialUrl = self::CRM_API_URL . self::CRM_API_GET_QUARTER . "?&page=" . $page . "&limit=" . $limit;

        return Curlsend::sendGet($getFilialUrl, $this->authKey);
    }

    public function getComplex(int $page = 1, int $limit = 10)
    {
        $getFilialUrl = self::CRM_API_URL . self::CRM_API_GET_COMPLEX . "?&page=" . $page . "&limit=" . $limit;

        return Curlsend::sendGet($getFilialUrl, $this->authKey);
    }

    public function getCategories()
    {
        $getFilialUrl = self::CRM_API_URL . self::CRM_API_GET_CATEGORY;

        return Curlsend::sendGet($getFilialUrl, $this->authKey);
    }

    public function getSubCategories()
    {
        $getFilialUrl = self::CRM_API_URL . self::CRM_API_GET_SUBCATEGORY;

        return Curlsend::sendGet($getFilialUrl, $this->authKey);
    }

    public function getRealty(int $page = 1, int $limit = 10)
    {
        $status = http_build_query([
            'status' => [3,7]
        ]);
        $getFilialUrl = self::CRM_API_URL . self::CRM_API_GET_REALTY . "?&page=" . $page . "&limit=" . $limit. "&".$status;

        return Curlsend::sendGet($getFilialUrl, $this->authKey);
    }


    public function getTotalCountObject(string $apiMethodName, int $itemsOnOnePage = 50, array $queryParams = []): ?array
    {
        $getQuery = http_build_query($queryParams) ?: 'page=1&limit=2';

        $getUrl = self::CRM_API_URL . $apiMethodName . "?" . $getQuery;
        $getResponse = Curlsend::sendGet($getUrl, $this->authKey);

        $pages = ($getResponse['data']['count'] / $itemsOnOnePage) + 1;

        $returnAr = [
            'total' => $getResponse['data']['count'],
            'pages' => ceil($pages),
            'limit' => $itemsOnOnePage,
        ];

        return $returnAr;
    }

    public function sendLead(array $query)
    {
        $getQueryStr = http_build_query($query);
        $getUrl = self::CRM_API_URL . self::CRM_API_SEND_LEAD;

        return Curlsend::sendPost($getUrl, $getQueryStr, $this->authKey);
    }

    public function sendAgentReview(array $query)
    {
        $getQueryStr = http_build_query($query);
        $getUrl = self::CRM_API_URL . self::CRM_API_ADD_AGENT_REVIEW;

        return Curlsend::sendPost($getUrl, $getQueryStr, $this->authKey);
    }


}