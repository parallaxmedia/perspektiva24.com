<?php

namespace P24\Rest\Api;

use P24\Rest\Model\Company;
use P24\Rest\Model\Realty;

class Main
{

    public $object;
    public $request;
    public $uri;
    public $method;
    public $error = [];

    private const API_KEY = "GSdfew323AEWq32e7uk7L&*jt6";

    function __construct(array $request, string $uri)
    {
        $this->request = $request;
        $this->uri = $uri;
    }

    public function run()
    {
        $clearUrl = str_replace("/p24-api/", "", $this->uri);
        $getArrayFromUrl = explode('/', $clearUrl);
        $this->object = $getArrayFromUrl[0];
        $this->method = $getArrayFromUrl[1];

        return $this->routeRequest();
    }

    public function routeRequest()
    {
        switch ($this->object) {
            case 'filial':
            case 'agent':
            case 'quarter':
            $res =  Company::apiController($this);
                break;
            case 'developers':
            case 'complex':
            case 'category':
            case 'subcategory':
            case 'realty':
            $res =  Realty::apiController($this);
                break;
            default:
                echo $this->error['error'] = 'Undefinded method';
        }

        return $res;
    }

    public static function checkKey($key): bool
    {
        if ($key == self::API_KEY) {
            return true;
        } else {
            return false;
        }
    }
}