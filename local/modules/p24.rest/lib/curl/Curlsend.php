<?php

namespace P24\Rest\Curl;

use Bitrix\Main\Diag\Debug;

class Curlsend
{

    /**
     * @param $url
     * @param $query
     * @return bool|string
     * Json converted to array
     */
    public static function sendPost(string $url, string $query, string $access_token)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $access_token));


        $output = curl_exec($ch);
        curl_close($ch);

        if (!empty($output)) {
            return json_decode($output, true);
        } else {
            return null;
        }
    }

    public static function sendGet(string $url, string $access_token)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $access_token));

        $output = curl_exec($ch);
        curl_close($ch);

        if (!empty($output)) {
            return json_decode($output, true);
        } else {
            return null;
        }
    }

    public static function sendJson(string $query, string $url)
    {
        $ch = curl_init($url);
        // Attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        $output = curl_exec($ch);
        curl_close($ch);
        if (!empty($output)) {
            return $output;
        } else {
            return 'faild';
        }
    }

}