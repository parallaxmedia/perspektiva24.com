<?php

namespace P24\Rest\Handlers;

use P24\Models\HlBlock\Main;


class Event
{

    public function OnAfterIBlockElementDeleteHandler($arFields)
    {
        if ($arFields['IBLOCK_ID'] == 4) {
            $filterParams = ['UF_AGENT_ID' => $arFields['ID']];
            $rewardsId = Main::getArrayElId($filterParams, 'AgentRewards');
            Main::deleteMany($rewardsId, 'AgentRewards');
        }
    }

}