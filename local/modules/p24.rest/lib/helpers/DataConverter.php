<?php


namespace P24\Rest\Helpers;

use  Bitrix\Main\Loader;

Loader::includeModule('main');

class DataConverter
{
    public static function stringTranslitEngToRu($string, array $options = array("replace_space" => "-", "replace_other" => "-"))
    {
        $trans = \Cutil::translit($string, "ru", $options);
        return $trans;
    }

}