<?php


namespace P24\Rest\Model;

use P24\Models\Iblock\Main;
use P24\Rest\Request;
use P24\Models\Iblock\ElementTable;

class Base
{

    public $error;
    public $successAdd;
    public $successUpdate;

    /**
     * Start full import and telegram bot report
     * @return int telegram bot message id
     */
    public static function fullImport()
    {
        $runRequest = new  Request();
        $logArray = [];

        $logArray['filial'] = Company::fullStoreFilial($runRequest);
        $logArray['agent'] = Company::fullStoreAgent($runRequest);
        $logArray['developer'] = Realty::fullStoreDevelopers($runRequest);
        $logArray['quarter'] = Company::fullStoreQuarter($runRequest);
        $logArray['complex'] = Realty::fullStoreComplex($runRequest);
        $logArray['category'] = Realty::storeRequestCategory($runRequest);
        $logArray['subcategory'] = Realty::storeRequestSubCategory($runRequest);
        $logArray['realty'] = Realty::fullStoreRealty($runRequest);

        $logMessage = self::prepareTelegramMessage($logArray);

        return self::sendTelegrammMessage($logMessage);
    }

    /**
     * Delete multiple elements
     * @param array $ids
     * @param string $type
     * @param string $idPrefix
     * @return array
     */
    public static function delete(array $ids, string $type, string $idPrefix = '')
    {
        $resArray = [];
        switch ($type) {
            case 'element':
                $IblockObject = new \CIBlockElement;
                break;
            case  'section':
                $IblockObject = new \CIBlockSection;
                break;
        }

        foreach ($ids as $id) {
            if (!empty($idPrefix)) {
                $fullId = $idPrefix . $id['ID'];
            } else {
                $fullId = $id['ID'];
            }
            if ($productId = $IblockObject->Delete($fullId)) {
                $resArray[] = $productId;
            } else {
                $resArray[] = $IblockObject->LAST_ERROR;
            }
        }
        return $resArray;
    }

    /** This prototype class not used yet.
     * @param array $items
     * @param string $iblockCode
     * @param string $method
     * @param string $type
     * @return array
     */
    public static function store(array $items, string $iblockCode, string $method, string $type)
    {

        $statusArrayElementId = [];
        $getIblockId = Main::getIblockIdByCode($iblockCode);

        if ($type == 'element') {
            $IblockObject = new \CIBlockElement;
        } elseif ($type == 'section') {
            $IblockObject = new \CIBlockSection;
        }

        foreach ($items as $Item) {
            if ($type == 'element') {
                $getExistId = Main::getExistIblockElementByXmlId($Item['id'], $getIblockId);
            } elseif ($type == 'section') {
                $getExistId = Main::getExistIblockSectionByXmlId($Item['id'], $getIblockId);
            }

            if (empty($getExistId)) {
                $statusArrayElementId['add'][] = self::add($IblockObject, $Item, $getIblockId);
            } else {
                $statusArrayElementId['update'][] = self::update($IblockObject, $getExistId, $Item, $getIblockId);
            }

        }
        return $statusArrayElementId;
    }

    public function add($object, array $arField, int $iblockId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $PROP[strtoupper($key)] = $prepareFIeld;
        }
        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $iblockId,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        if ($productId = $object->Add($arLoadProductArray))
            return $productId;
        else
            return $object->LAST_ERROR;
    }

    public static function update($object, int $id, array $arField, int $iblockId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $PROP[strtoupper($key)] = $prepareFIeld;
        }

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        if ($productId = $object->Update($id, $arLoadProductArray))
            return $productId;
        else
            return $object->LAST_ERROR;
    }

    /**
     * Prepare logging error data
     * @param $result
     */
    public function logging($result)
    {
        if (strpos($result, 'Обязательное') !== false) {
            $this->error['required_fields'] += 1;
        } else {
            $this->error['other'] += 1;
        }
    }

    /**
     * Create simple telegram bot message
     * @param array $arrayLog
     * @return string
     */
    public static function prepareTelegramMessage(array $arrayLog)
    {
        $message = '';
        foreach ($arrayLog as $key => $item) {
            $message .= "\r\n";
            switch ($key) {
                case 'filial';
                    $message .= 'Филиалы: ';
                    break;
                case 'agent';
                    $message .= 'Агенты: ';
                    break;
                case 'developer';
                    $message .= 'Застройщики: ';
                    break;
                case 'quarter';
                    $message .= 'Кварталы: ';
                    break;
                case 'complex';
                    $message .= 'ЖК: ';
                    break;
                case 'category';
                    $message .= 'Категории: ';
                    break;
                case 'subcategory';
                    $message .= 'Подкатегории: ';
                    break;
                case 'realty';
                    $message .= 'Недвижимость: ';
                    break;
            }
            foreach ($item as $ikey => $it) {
                if ($it) {
                    $message .= "\n";
                    switch ($ikey) {
                        case 'add':
                            $message .= 'Добавлено - ' . $it;
                            break;
                        case 'update':
                            $message .= 'Обновлено - ' . $it;
                            break;
                        case 'error':
                            foreach ($it as $erKey => $error) {
                                switch ($erKey) {
                                    case 'required_fields':
                                        $message .= 'Ошибка, нет обязательных полей - ' . $error;
                                        break;
                                    case 'other':
                                        $message .= 'Ошибка - ' . $error;
                                        break;
                                }
                            }
                            break;
                    }
                }
            }
            $message .= "\n";
        }
        return $message;
    }

    public static function sendTelegrammMessage(string $message)
    {
        $bot = new \TelegramBot\Api\BotApi('1257200628:AAEEqQDkJhg9z66tu6ncD6KdwEIW5XxEqJ4');
        $chatId = '-467647654';
        $res = $bot->sendMessage($chatId, $message);

        return $res->getMessageId();
    }

    /**
     * Clear data which is not in import
     * Default clear data 2 weeks old
     * @param null $dateFilter
     * @return mixed
     */
    public static function clearNonActualData($dateFilter = null)
    {
        if (empty($dateFilter)) {
            $dateFilter = date('d.m.Y H:i:s', strtotime("-2 week -2 days"));
        }

        $iblockIdAgent = Main::getIblockIdByCode('agents');
        $getAgentIds = self::getNotUpdatedElement($iblockIdAgent, $dateFilter);
        $resReturn['removeAgent'] = Main::clearIblockElements($getAgentIds);

        $iblockIdFilial = Main::getIblockIdByCode('geo');
        $getFilialIds = self::getNotUpdatedElement($iblockIdFilial, $dateFilter);
        $resReturn['disableFilial'] = Main::disableIbElement($getFilialIds);

        return $resReturn;
    }

    public static function getNotUpdatedElement(int $iblockId, $filterDateTime)
    {
        $getRes = ElementTable::getList([
                'filter' => ['IBLOCK_ID' => $iblockId, '<=TIMESTAMP_X' => $filterDateTime],
                'select' => ['ID']
            ]
        );

        $getArray = [];

        while ($row = $getRes->fetch()) {
            $getArray[] = $row['ID'];
        }

        return $getArray;
    }

}