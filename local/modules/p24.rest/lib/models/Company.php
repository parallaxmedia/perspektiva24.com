<?php

namespace P24\Rest\Model;

use P24\Models\Iblock\SectionTable;
use P24\Rest\Request;
use P24\Models\Iblock\Main;
use \P24\Rest\Api\Main as ApiMain;
use Bitrix\Main\Loader;
use P24\Models\Iblock\ElementTable;
use P24\Rest\Helpers\DataConverter;
use P24\Models\HlBlock\Main as HLMain;

Loader::includeModule('iblock');

class Company extends Base
{
    const IBLOCK_AGENT_CODE = 'agents';
    const IBLOCK_FILIAL_CODE = 'geo';
    const IBLOCK_REVIEW_CODE = 'review';

    public $error = [];
    public $successAdd = 0;
    public $successUpdate = 0;

    public static function fullStoreFilial(Request $request)
    {
        $getTotalArray = $request->getTotalCountObject(Request::CRM_API_GET_FILIAL);

        return self::storeRequestFilial($getTotalArray['total'], $getTotalArray['limit'], $request);
    }

    public static function storeRequestFilial(int $amountElement = 20, int $limitElements = 10, Request $requestModel)
    {
        $pages = ($amountElement / $limitElements) + 1;
        $i = 0;

        $IblockObject = new \CIBlockElement;
        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_FILIAL_CODE);
        $resultAr = [];
        $model = new self();

        while ($pages > $i) {
            $getRequestArray = $requestModel->getFilial($i, $limitElements);
            if (!empty($getRequestArray['data']['items'])) {
                $model->storeFilial($getRequestArray['data']['items'], $IblockObject, $getIblockId);
            }
            $i++;
        }

        $resultAr['error'] = $model->error;
        $resultAr['add'] = $model->successAdd;
        $resultAr['update'] = $model->successUpdate;

        return $resultAr;
    }

    public function storeFilial($arData, $dataObject, int $iblockId)
    {
        foreach ($arData as $filialItem) {
            $getExistId = Main::getExistIblockElementByXmlId($filialItem['id'], $iblockId);
            if (empty($getExistId)) {
                $this->addFilial($dataObject, $filialItem, $iblockId);
            } else {
                $this->updateFilial($dataObject, $getExistId, $filialItem, $iblockId);
            }
        }
    }

    public function addFilial($object, array $arField, int $iblockId)
    {
        $PROP = array();

        foreach ($arField as $key => $field) {
            if ($key == "phones" and !empty($field)) {
                $getArray = explode(',', $field);
                $PROP['PHONES'] = $getArray[0];
                continue;
            }

            if ($key == "email" and !empty($field)) {
                $getArray = explode(',', $field);
                $PROP['EMAIL'] = $getArray[0];
                continue;
            }

            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $PROP[strtoupper($key)] = $prepareFIeld;
        }
        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => $PROP['CITY'] ?: false,
            "IBLOCK_ID" => $iblockId,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        if ($id = $object->Add($arLoadProductArray)) {
            $this->successAdd += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public function updateFilial($object, int $id, array $arField, int $iblockId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            if ($key == "phones" and !empty($field)) {
                $getArray = explode(',', $field);
                $PROP['PHONES'] = $getArray[0];
                continue;
            }

            if ($key == "email" and !empty($field)) {
                $getArray = explode(',', $field);
                $PROP['EMAIL'] = $getArray[0];
                continue;
            }
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $PROP[strtoupper($key)] = $prepareFIeld;
        }

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => $PROP['CITY'] ?: false,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        if ($idU = $object->Update($id, $arLoadProductArray)) {
            $this->successUpdate += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public static function fullStoreAgent(Request $request)
    {
        $getTotalArray = $request->getTotalCountObject(Request::CRM_API_GET_AGENT);

        return self::storeRequestAgent($getTotalArray['total'], $getTotalArray['limit'], $request);
    }

    public static function storeRequestAgent(int $amountElement = 20, int $limitElements = 10, Request $requestModel)
    {
        $pages = ($amountElement / $limitElements) + 1;
        $i = 0;

        $IblockObject = new \CIBlockElement;
        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_AGENT_CODE);
        $resultAr = [];

        $model = new self();

        while ($pages > $i) {
            $getRequestArray = $requestModel->getAgents($i, $limitElements);
            if (!empty($getRequestArray['data']['items'])) {
                $model->storeAgent($getRequestArray['data']['items'], $IblockObject, $getIblockId);
            }
            $i++;
        }

        $resultAr['error'] = $model->error;
        $resultAr['add'] = $model->successAdd;
        $resultAr['update'] = $model->successUpdate;

        return $resultAr;
    }

    public function storeAgent($arData, $dataObject, int $iblockId)
    {
        $statusArrayElementId = [];

        foreach ($arData as $Item) {
            $getExistId = Main::getExistIblockElementByXmlId($Item['id'], $iblockId);
            if (empty($getExistId)) {
                $this->addAgent($dataObject, $Item, $iblockId);
            } else {
                $this->updateAgent($dataObject, $getExistId, $Item, $iblockId);
            }
        }

        return $statusArrayElementId;
    }

    public function addAgent($object, array $arField, int $iblockId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            $prepareField = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $PROP[strtoupper($key)] = $prepareField;
        }

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $iblockId,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        if ($id = $object->Add($arLoadProductArray)) {
            if (!empty($arField['reviews'])) {
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REVIEW_CODE);
                self::storeReview($arField['reviews'], $getIblockId, $id);
            }
            if (!empty($arField['rewards'])) {
                self::storeRewards($arField['rewards'], $id);
            }
            $this->successAdd += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public function updateAgent($object, int $id, array $arField, int $iblockId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            if ($key == 'reviews' and !empty($field)) {
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REVIEW_CODE);
                self::storeReview($field, $getIblockId, $id);
            } elseif ($key == 'rewards' and !empty($field)) {
                self::storeRewards($field, $id);
            } else {
                $prepareField = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
                $PROP[strtoupper($key)] = $prepareField;
            }
        }

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );
        if ($idU = $object->Update($id, $arLoadProductArray))
            $this->successUpdate += 1;
        else
            $this->logging($object->LAST_ERROR);
    }

    public static function storeReview(array $storeArray, int $iblockId, int $agentId)
    {
        $IblockObject = new \CIBlockElement;

        foreach ($storeArray as $item) {
            $checkExist = Main::getExistIblockElementByXmlId($item['id'], $iblockId);

            if (empty($checkExist)) {
                self::addReview($IblockObject, $item, $iblockId, $agentId);
            } else {
                self::updateReview($IblockObject, $checkExist, $item, $iblockId, $agentId);
            }
        }
    }

    public static function addReview($object, array $arField, int $iblockId, int $agentId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $PROP[strtoupper($key)] = $prepareFIeld;
        }
        $PROP = array_merge($PROP, ['AGENT_ID' => $agentId]);
        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $iblockId,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        if ($id = $object->Add($arLoadProductArray))
            return $id;
        else
            return $object->LAST_ERROR;
    }

    public static function updateReview($object, int $id, array $arField, int $iblockId, int $agentId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $PROP[strtoupper($key)] = $prepareFIeld;
        }
        $PROP = array_merge($PROP, ['AGENT_ID' => $agentId]);
        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        if ($id = $object->Update($id, $arLoadProductArray))
            return $id;
        else
            return $object->LAST_ERROR;
    }

    public static function storeRewards(array $storeArray, int $agentId)
    {
        $hlBlockCode = 'AgentRewards';
        $hlBlockObj = HLMain::getHlBlockObjByHlBlockCode($hlBlockCode);

        foreach ($storeArray as $item) {
            $checkExist = HLMain::checkExistElementByUfId($item['id'], $hlBlockCode);
            if (empty($checkExist)) {
                self::addRewards($hlBlockObj, $item, $agentId);
            } else {
                self::updateRewards($hlBlockObj, $checkExist, $item, $agentId);
            }
        }
    }

    public static function addRewards($object, array $arField, int $agentId)
    {
        $getRes = $object::add([
            "UF_AGENT_ID" => $agentId,
            "UF_TITLE" => $arField['title'],
            "UF_CREATED_AT" => $arField['created_at'],
            "UF_ID" => $arField['id'],
        ]);

        return $getRes;
    }

    public static function updateRewards($object, int $id, array $arField, int $agentId)
    {
        $getRes = $object::update($id, [
            "UF_AGENT_ID" => $agentId,
            "UF_TITLE" => $arField['title'],
            "UF_CREATED_AT" => $arField['created_at'],
            "UF_ID" => $arField['id'],
        ]);

        return $getRes;
    }

    public static function fullStoreQuarter(Request $request)
    {
        $getTotalArray = $request->getTotalCountObject(Request::CRM_API_GET_QUARTER);

        return self::storeRequestQuarter($getTotalArray['total'], $getTotalArray['limit'], $request);
    }

    public static function storeRequestQuarter(int $amountElement = 20, int $limitElements = 10, Request $requestModel)
    {
        $pages = ($amountElement / $limitElements) + 1;
        $i = 0;

        $IblockObject = new \CIBlockSection;
        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_FILIAL_CODE);
        $resultAr = [];
        $model = new self();

        while ($pages > $i) {
            $getRequestArray = $requestModel->getQuarters($i, $limitElements);
            if (!empty($getRequestArray['data']['items'])) {
                $model->storeQuarter($getRequestArray['data']['items'], $IblockObject, $getIblockId);
            }
            $i++;
        }

        $resultAr['error'] = $model->error;
        $resultAr['add'] = $model->successAdd;
        $resultAr['update'] = $model->successUpdate;

        return $resultAr;
    }

    public function storeQuarter($arData, $dataObject, int $iblockId)
    {
        foreach ($arData as $item) {
            $getExistId = Main::getExistIblockSectionByXmlId($item['id'], $iblockId);
            if (empty($getExistId)) {
                $this->addQuarter($dataObject, $item, $iblockId);
            } else {
                $this->updateQuarter($dataObject, $getExistId, $item, $iblockId);
            }
        }
    }

    public function addQuarter($object, array $arField, int $iblockId)
    {
        $UF = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $UF['UF_' . strtoupper($key)] = $prepareFIeld;
        }
        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $iblockId,
            "NAME" => $arField['name'] ?: $arField['id'],
            "UF_OBJ_TYPE" => 5,
            "XML_ID" => $arField['id'],
            "CODE" => DataConverter::stringTranslitEngToRu($arField['name']),
            "DESCRIPTION" => $arField['description'],
            "ACTIVE" => "Y",            // активен
        );

        $arrayWithUfFields = array_merge($arLoadProductArray, $UF);

        if ($id = $object->Add($arrayWithUfFields)) {
            $this->successAdd += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public function updateQuarter($object, int $id, array $arField, int $iblockId)
    {
        $UF = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $UF['UF_' . strtoupper($key)] = $prepareFIeld;
        }

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        $arrayWithUfFields = array_merge($arLoadProductArray, $UF);

        if ($idU = $object->Update($id, $arrayWithUfFields)) {
            $this->successUpdate += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public static function checkExistGeoByName(string $name, string $fieldCode): ?int
    {
        $iblockId = Main::getIblockIdByCode(self::IBLOCK_FILIAL_CODE);
        $geoCodeId = HLMain::getHlBlockElementByCode($fieldCode, 'P24ObjectType');
        $arFilter = array('IBLOCK_ID' => $iblockId, 'UF_OBJ_TYPE' => $geoCodeId['ID'], 'NAME' => $name, 'CHECK_PERMISSIONS' => 'N');
        $rsSect = \CIBlockSection::GetList(array(), $arFilter);
        $getRes = $rsSect->fetch();

        return $getRes['ID'];
    }

    public static function addAddressByName(string $name, string $objTypeName, int $parentAddressId = 0)
    {
        $entity = new \CIBlockSection;
        $getTransileration = DataConverter::stringTranslitEngToRu($name);
        $getGeoIblockId = Main::getIblockIdByCode('geo');
        $getObjTypeId = HLMain::getHlBlockElementByCode($objTypeName, 'P24ObjectType');
        $getres = $entity->add(
            [
                'NAME' => $name,
                'UF_OBJ_TYPE' => $getObjTypeId['ID'],
                'IBLOCK_ID' => $getGeoIblockId,
                'XML_ID' => $getTransileration,
                'CODE' => $getTransileration,
                'IBLOCK_SECTION_ID' => $parentAddressId,
            ]
        );

        if ($getres) {
            return $getres;
        } else {
            return $entity->LAST_ERROR;
        }
    }


    // Store request method
    public static function apiController(ApiMain $api)
    {
        $res = [];
        switch ($api->object) {
            case 'filial':
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_FILIAL_CODE);
                $IblockObject = new \CIBlockElement;
                switch ($api->method) {
                    case 'add':
                    case 'update':
                        $model = new self();
                        $model->storeFilial($api->request['data'], $IblockObject, $getIblockId);
                        $res = $model->error ?: $model->successAdd;
                        break;
                    case 'delete':
                        $ids = Main::getElementArrayIdByXmlIds($api->request['data'], $getIblockId);
                        $res = self::delete($ids, 'element');
                        break;
                }
                break;

            case 'agent':
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_AGENT_CODE);
                $IblockObject = new \CIBlockElement;
                switch ($api->method) {
                    case 'add':
                    case 'update':
                        $model = new self();
                        $model->storeAgent($api->request['data'], $IblockObject, $getIblockId);
                        $res = $model->error ?: $model->successAdd;
                        break;
                    case 'delete':
                        $ids = Main::getElementArrayIdByXmlIds($api->request['data'], $getIblockId);
                        $res = self::delete($ids, 'element');
                        break;
                }
                break;

            case 'quarter':
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_FILIAL_CODE);
                $IblockObject = new \CIBlockSection;
                switch ($api->method) {
                    case 'add':
                    case 'update':
                        $model = new self();
                        $model->storeQuarter($api->request['data'], $IblockObject, $getIblockId);
                        $res = $model->error ?: $model->successAdd;
                        break;
                    case 'delete':
                        $ids = Main::getElementArrayIdByXmlIds($api->request['data'], $getIblockId);
                        $res = self::delete($ids, 'element');
                        break;
                }
                break;

        }

        return $model ?: $res;
    }
}