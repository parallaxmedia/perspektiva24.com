<?php

namespace P24\Rest\Model;

use P24\Rest\Api\Main as ApiMain;
use P24\Rest\Helpers\DataConverter;
use P24\Rest\Request;
use P24\Models\Iblock\Main;
use Bitrix\Main\Loader;
use P24\Models\Iblock\ElementTable;
use P24\Rest\Model\Company;

Loader::includeModule('iblock');

class Realty extends Base
{
    const IBLOCK_REALTY_CODE = 'realty';
    const IBLOCK_DEVELOPERS_CODE = 'developers';

    public $error;
    public $successAdd;
    public $successUpdate;
    public $importRealtyStatus = [3, 7];

    public static function fullStoreDevelopers(Request $request)
    {
        $getTotalArray = $request->getTotalCountObject(Request::CRM_API_GET_DEVELOPERS);

        return self::storeRequestDeveloper($getTotalArray['total'], $getTotalArray['limit'], $request);
    }

    public static function storeRequestDeveloper(
        int $amountElement = 20,
        int $limitElements = 10,
        Request $requestModel
    ) {
        $pages = ($amountElement / $limitElements) + 1;
        $i = 0;

        $IblockObject = new \CIBlockSection;
        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_DEVELOPERS_CODE);
        $resultAr = [];
        $model = new self();

        while ($pages > $i) {
            $getRequestArray = $requestModel->getDevelopers($i, $limitElements);
            if (!empty($getRequestArray['data']['items'])) {
                $model->storeDevelopers($getRequestArray['data']['items'], $IblockObject, $getIblockId);
            }
            $i++;
            sleep(1);
        }

        $resultAr['error'] = $model->error;
        $resultAr['add'] = $model->successAdd;
        $resultAr['update'] = $model->successUpdate;

        return $resultAr;
    }

    public function storeDevelopers($arData, $dataObject, int $iblockId)
    {
        foreach ($arData as $item) {
            $getExistId = Main::getExistIblockSectionByXmlId($item['id'], $iblockId);
            if (empty($getExistId)) {
                $this->addDeveloper($dataObject, $item, $iblockId);
            } else {
                $this->updateDeveloper($dataObject, $getExistId, $item, $iblockId);
            }
        }
    }

    public function addDeveloper($object, array $arField, int $iblockId)
    {
        $UF = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field ?: '', strtoupper($key), $iblockId);
            $UF['UF_'.strtoupper($key)] = $prepareFIeld;

        }
        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $iblockId,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        $arrayWithUfFields = array_merge($arLoadProductArray, $UF);

        if ($id = $object->Add($arrayWithUfFields)) {
            $this->successAdd += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public function updateDeveloper($object, int $id, array $arField, int $iblockId)
    {
        $UF = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field ?: '', strtoupper($key), $iblockId);
            $UF['UF_'.strtoupper($key)] = $prepareFIeld;

        }

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        $arrayWithUfFields = array_merge($arLoadProductArray, $UF);

        if ($idU = $object->Update($id, $arrayWithUfFields)) {
            $this->successUpdate += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public static function fullStoreComplex(Request $request)
    {
        $getTotalArray = $request->getTotalCountObject(Request::CRM_API_GET_COMPLEX);

        return self::storeRequestComplex($getTotalArray['total'], $getTotalArray['limit'], $request);
    }

    public static function storeRequestComplex(int $amountElement = 20, int $limitElements = 10, Request $requestModel)
    {
        $pages = ($amountElement / $limitElements) + 1;
        $i = 0;

        $IblockObject = new \CIBlockElement;
        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_DEVELOPERS_CODE);
        $resultAr = [];
        $model = new self();

        while ($pages > $i) {
            $getRequestArray = $requestModel->getComplex($i, $limitElements);
            if (!empty($getRequestArray['data']['items'])) {
                $model->storeComplex($getRequestArray['data']['items'], $IblockObject, $getIblockId);
            }
            $i++;
            sleep(1);
        }

        $resultAr['error'] = $model->error;
        $resultAr['add'] = $model->successAdd;
        $resultAr['update'] = $model->successUpdate;

        return $resultAr;
    }

    public function storeComplex($arData, $dataObject, int $iblockId)
    {
        foreach ($arData as $item) {
            $getExistId = Main::getExistIblockElementByXmlId($item['id'], $iblockId);
            if (empty($getExistId)) {
                $statusArrayElementId['add'][] = $this->addComplex($dataObject, $item, $iblockId);
            } else {
                $statusArrayElementId['update'][] = $this->updateComplex($dataObject, $getExistId, $item, $iblockId);
            }
        }
    }

    public function addComplex($object, array $arField, int $iblockId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field ?: '', strtoupper($key), $iblockId);
            $PROP[strtoupper($key)] = $prepareFIeld;
        }

        $getSectionId = Main::getPropertyIblockSectionId($arField['dev_id'], $iblockId);

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => $getSectionId,
            "IBLOCK_ID" => $iblockId,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "CODE" => DataConverter::stringTranslitEngToRu($arField['name']),
            "ACTIVE" => "Y",            // активен
        );

        if ($id = $object->Add($arLoadProductArray)) {
            $this->successAdd += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public function updateComplex($object, int $id, array $arField, int $iblockId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            $prepareFIeld = Main::preparePropertyFieldData($field ?: '', strtoupper($key), $iblockId);
            $PROP[strtoupper($key)] = $prepareFIeld;
        }

        $getSectionId = Main::getPropertyIblockSectionId($arField['dev_id'], $iblockId);

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => $getSectionId,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        if ($idU = $object->update($id, $arLoadProductArray)) {
            $this->successUpdate += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public static function storeRequestCategory()
    {
        $resultAr = [];
        $model = new self();
        $model->storeCategory();

        $resultAr['error'] = $model->error;
        $resultAr['add'] = $model->successAdd;
        $resultAr['update'] = $model->successUpdate;

        return $resultAr;
    }

    public function storeCategory()
    {
        $requestModel = new Request();
        $IblockObject = new \CIBlockSection;
        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);

        $getRequestArray = $requestModel->getCategories();
        foreach ($getRequestArray['data'] as $item) {
            $getExistId = Main::getExistIblockSectionByXmlId($item['id'], $getIblockId);
            if (empty($getExistId)) {
                $this->addCategory($IblockObject, $item, $getIblockId);
            } else {
                $this->updateCategory($IblockObject, $getExistId, $item, $getIblockId);
            }
        }
    }

    public function addCategory($object, array $arField, int $iblockId)
    {
        $UF = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $UF['UF_'.strtoupper($key)] = $prepareFIeld;

        }
        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $iblockId,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "CODE" => DataConverter::stringTranslitEngToRu($arField['name']),
            "ACTIVE" => "Y",            // активен
        );

        $arrayWithUfFields = array_merge($arLoadProductArray, $UF);

        if ($id = $object->Add($arrayWithUfFields)) {
            $this->successAdd += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public function updateCategory($object, int $id, array $arField, int $iblockId)
    {
        $UF = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $UF['UF_'.strtoupper($key)] = $prepareFIeld;

        }

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_ID" => $iblockId,
            "IBLOCK_SECTION_ID" => false,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "ACTIVE" => "Y",            // активен
        );

        $arrayWithUfFields = array_merge($arLoadProductArray, $UF);

        if ($idU = $object->Update($id, $arrayWithUfFields)) {
            $this->successUpdate += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public static function storeRequestSubCategory()
    {
        $resultAr = [];
        $model = new self();
        $model->storeSubCategory();

        $resultAr['error'] = $model->error;
        $resultAr['add'] = $model->successAdd;
        $resultAr['update'] = $model->successUpdate;

        return $resultAr;
    }

    public function storeSubCategory()
    {
        $requestModel = new Request();

        $IblockObject = new \CIBlockSection;
        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);

        $getRequestArray = $requestModel->getSubCategories();
        foreach ($getRequestArray['data'] as $item) {
            $getExistId = Main::getExistIblockSectionByXmlId('sub_'.$item['id'], $getIblockId);
            if (empty($getExistId)) {
                $this->addSubCategory($IblockObject, $item, $getIblockId);
            } else {
                $this->updateSubCategory($IblockObject, $getExistId, $item, $getIblockId);
            }
        }
        $resultAr['error'] = $this->error;
        $resultAr['add'] = $this->successAdd;
        $resultAr['update'] = $this->successUpdate;

        return $resultAr;
    }

    public function addSubCategory($object, array $arField, int $iblockId)
    {
        $UF = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $UF['UF_'.strtoupper($key)] = $prepareFIeld;

        }

        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);
        $getSectionId = Main::getPropertyIblockSectionId($arField['category_id'], $getIblockId);

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => $getSectionId,
            "IBLOCK_ID" => $iblockId,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => 'sub_'.$arField['id'],
            "CODE" => DataConverter::stringTranslitEngToRu($arField['name']),
            "ACTIVE" => "Y",            // активен
        );

        $arrayWithUfFields = array_merge($arLoadProductArray, $UF);

        if ($id = $object->Add($arrayWithUfFields)) {
            $this->successAdd += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public function updateSubCategory($object, int $id, array $arField, int $iblockId)
    {
        $UF = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            $prepareFIeld = Main::preparePropertyFieldData($field, strtoupper($key), $iblockId);
            $UF['UF_'.strtoupper($key)] = $prepareFIeld;

        }

        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);
        $getSectionId = Main::getPropertyIblockSectionId($arField['category_id'], $getIblockId);

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION_ID" => $getSectionId,
            "NAME" => $arField['name'] ?: $arField['id'],
            "XML_ID" => 'sub_'.$arField['id'],
            "ACTIVE" => "Y",            // активен          // активен
        );

        $arrayWithUfFields = array_merge($arLoadProductArray, $UF);

        if ($idU = $object->Update($id, $arrayWithUfFields)) {
            $this->successUpdate += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public static function fullStoreRealty(Request $request)
    {
        $getTotalArray = $request->getTotalCountObject(Request::CRM_API_GET_REALTY);

        return self::storeRequestRealty($getTotalArray['total'], $getTotalArray['limit'], $request);
    }


    public static function storeRequestRealty(int $amountElement = 20, int $limitElements = 10, Request $requestModel)
    {
        $pages = ($amountElement / $limitElements) + 1;
        $i = 0;

        $IblockObject = new \CIBlockElement;
        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);
        $resultAr = [];
        $model = new self();

        while ($pages > $i) {
            $getRequestArray = $requestModel->getRealty($i, $limitElements);
            if (!empty($getRequestArray['data']['items'])) {
                $model->storeRealty($getRequestArray['data']['items'], $IblockObject, $getIblockId);
            }
            $i++;
            sleep(1);
        }

        $resultAr['error'] = $model->error;
        $resultAr['add'] = $model->successAdd;
        $resultAr['update'] = $model->successUpdate;

        return $resultAr;
    }

    public function storeRealty($arData, $dataObject, int $iblockId)
    {
        foreach ($arData as $item) {
            $getExistId = Main::getExistIblockElementByXmlId($item['id'], $iblockId);
            if (empty($getExistId)) {
                $this->addRealty($dataObject, $item, $iblockId);
            } else {
                $this->updateRealty($dataObject, $getExistId, $item, $iblockId);
            }
        }
    }

    public function addRealty($object, array $arField, int $iblockId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            if ($key == 'object_meta') {
                foreach ($field as $keyMeta => $metaItem) {
                    $prepareFIeld = Main::preparePropertyFieldData($metaItem ?: '', strtoupper($keyMeta), $iblockId);
                    $PROP[strtoupper($keyMeta)] = $prepareFIeld;
                }
            } else {
                $prepareFIeld = Main::preparePropertyFieldData($field ?: '', strtoupper($key), $iblockId);
                $PROP[strtoupper($key)] = $prepareFIeld;
            }

        }

        $getSectionsId = [];

        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);

        $firstSection = Main::getPropertyIblockSectionId($arField['object_type'], $getIblockId);
        if ($firstSection) {
            array_push($getSectionsId, $firstSection);
        }

        $secSection = Main::getPropertyIblockSectionId('sub_'.$arField['object_sub_type'], $getIblockId);
        if ($secSection) {
            array_push($getSectionsId, $secSection);
        }

        $active = 'Y';

        if (!in_array($arField['status'], $this->importRealtyStatus)) {
            $active = 'N';
        }

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION" => $getSectionsId,
            "IBLOCK_ID" => $iblockId,
            "DETAIL_TEXT" => $arField['description'],
            "DETAIL_TEXT_TYPE" => 'html',
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['title'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "CODE" => DataConverter::stringTranslitEngToRu($arField['name'].'-'.$arField['id']),
            "ACTIVE" => $active,
        );

        if ($id = $object->Add($arLoadProductArray)) {
            $this->successAdd += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    public function updateRealty($object, int $id, array $arField, int $iblockId)
    {
        $PROP = array();
        foreach ($arField as $key => $field) {
            // Need converting crm field to uppercase
            if ($key == 'object_meta') {
                foreach ($field as $keyMeta => $metaItem) {
                    $prepareFIeld = Main::preparePropertyFieldData($metaItem ?: '', strtoupper($keyMeta), $iblockId);
                    $PROP[strtoupper($keyMeta)] = $prepareFIeld;
                }
            } else {
                $prepareFIeld = Main::preparePropertyFieldData($field ?: '', strtoupper($key), $iblockId);
                $PROP[strtoupper($key)] = $prepareFIeld;
            }

        }

        $getSectionsId = [];

        $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);

        $firstSection = Main::getPropertyIblockSectionId($arField['object_type'], $getIblockId);
        if ($firstSection) {
            array_push($getSectionsId, $firstSection);
        }

        $secSection = Main::getPropertyIblockSectionId('sub_'.$arField['object_sub_type'], $getIblockId);
        if ($secSection) {
            array_push($getSectionsId, $secSection);
        }

        $active = 'Y';

        if (!in_array($arField['status'], $this->importRealtyStatus)) {
            $active = 'N';
        }

        $arLoadProductArray = array(
            "MODIFIED_BY" => 1,
            "IBLOCK_SECTION" => $getSectionsId,
            "IBLOCK_ID" => $iblockId,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $arField['title'] ?: $arField['id'],
            "XML_ID" => $arField['id'],
            "DETAIL_TEXT" => $arField['description'],
            "DETAIL_TEXT_TYPE" => 'html',
            "CODE" => DataConverter::stringTranslitEngToRu($arField['name'].'-'.$arField['id']),
            "ACTIVE" => $active,            // активен
        );

        if ($idU = $object->Update($id, $arLoadProductArray)) {
            $this->successUpdate += 1;
        } else {
            $this->logging($object->LAST_ERROR);
        }
    }

    // Store request method
    public static function apiController(ApiMain $api)
    {
        $res = [];
        switch ($api->object) {
            case 'developer':
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_DEVELOPERS_CODE);
                $IblockObject = new \CIBlockSection;
                switch ($api->method) {
                    case 'add':
                    case 'update':
                        $model = new self();
                        $model->storeDevelopers($api->request['data'], $IblockObject, $getIblockId);
                        $res = $model->error ?: $model->successAdd;
                        break;
                    case 'delete':
                        $ids = Main::getExistIblockSectionByXmlId($api->request['data'], $getIblockId);
                        $res = self::delete($ids, 'section');
                        break;
                }
                break;

            case 'complex':
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_DEVELOPERS_CODE);
                $IblockObject = new \CIBlockElement;
                switch ($api->method) {
                    case 'add':
                    case 'update':
                        $model = new self();
                        $model->storeComplex($api->request['data'], $IblockObject, $getIblockId);
                        $res = $model->error ?: $model->successAdd;
                        break;
                    case 'delete':
                        $ids = Main::getElementArrayIdByXmlIds($api->request['data'], $getIblockId);
                        $res = self::delete($ids, 'element');
                        break;
                }
                break;

            case 'category':
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);
                $IblockObject = new \CIBlockSection;
                switch ($api->method) {
                    case 'add':
                    case 'update':
                        $model = new self();
                        $model->storeCategory();
                        $res = $model->error ?: $model->successAdd;
                        break;
                    case 'delete':
                        $ids = Main::getSectionArrayIdByXmlIds($api->request['data'], $getIblockId);
                        $res = self::delete($ids, 'element', 'sub_');
                        break;
                }
                break;

            case 'subcategory':
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);
                $IblockObject = new \CIBlockSection;
                switch ($api->method) {
                    case 'add':
                    case 'update':
                        $model = new self();
                        $model->storeSubCategory();
                        $res = $model->error ?: $model->successAdd;
                        break;
                    case 'delete':
                        $ids = Main::getSectionArrayIdByXmlIds($api->request['data'], $getIblockId);
                        $res = self::delete($ids, 'element', 'sub_');
                        break;
                }
                break;

            case 'realty':
                $getIblockId = Main::getIblockIdByCode(self::IBLOCK_REALTY_CODE);
                $IblockObject = new \CIBlockElement;
                switch ($api->method) {
                    case 'add':
                    case 'update':
                        $model = new self();
                        $model->storeRealty($api->request['data'], $IblockObject, $getIblockId);
                        $res = $model->error ?: $model->successAdd;
                        break;
                    case 'delete':
                        $ids = Main::getElementArrayIdByXmlIds($api->request['data'], $getIblockId);
                        $res = self::delete($ids, 'element');
                        break;
                }
                break;

        }

        return $model ?: $res;
    }
}