<?php

Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
    'P24\\Rest\\Curl\\Curlsend' => '/local/modules/p24.rest/lib/curl/Curlsend.php',
    'P24\\Rest\\Api\\Main' => '/local/modules/p24.rest/lib/api/Main.php',
    'P24\\Rest\\Request' => '/local/modules/p24.rest/lib/Request.php',
    'P24\\Rest\\Model\\Common' => '/local/modules/p24.rest/lib/models/Common.php',
    'P24\\Rest\\Model\\Base' => '/local/modules/p24.rest/lib/models/Base.php',
    'P24\\Rest\\Model\\Company' => '/local/modules/p24.rest/lib/models/Company.php',
    'P24\\Rest\\Model\\Realty' => '/local/modules/p24.rest/lib/models/Realty.php',
    'P24\\Models\\Iblock\\Main' => '/local/modules/p24.models/lib/iblock/Main.php',
    'P24\\Models\\Iblock\\IblockTable' => '/local/modules/p24.models/lib/iblock/IblockTable.php',
    'P24\\Models\\Iblock\\ElementTable' => '/local/modules/p24.models/lib/iblock/ElementTable.php',
    'P24\\Models\\Iblock\\PropertyTable' => '/local/modules/p24.models/lib/iblock/PropertyTable.php',
    'P24\\Models\\Iblock\\SectionTable' => '/local/modules/p24.models/lib/iblock/SectionTable.php',
    'P24\\Models\\HlBlock\\EntityTable' => '/local/modules/p24.models/lib/hlblock/EntityTable.php',
    'P24\\Models\\HlBlock\\Main' => '/local/modules/p24.models/lib/hlblock/Main.php',
    'P24\\Rest\\Helpers\\DataConverter' => '/local/modules/p24.rest/lib/helpers/DataConverter.php',
    'P24\\Rest\\Handlers\\Event' => '/local/modules/p24.rest/lib/handlers/Event.php',
));