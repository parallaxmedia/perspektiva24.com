<?php

use Bitrix\Main\EventManager;

EventManager::getInstance()->addEventHandlerCompatible(
    "iblock",
    "OnAfterIBlockElementDelete",
    array("P24\\Rest\\Handlers\\Event", "OnAfterIBlockElementDeleteHandler")
);