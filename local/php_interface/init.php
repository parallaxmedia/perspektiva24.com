<?php

use Bitrix\Main\EventManager;

if (empty($_SERVER["DOCUMENT_ROOT"])) {

    $_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';
}

use Bitrix\Main\Loader;
use Dev2fun\MultiDomain\Base;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

require $_SERVER["DOCUMENT_ROOT"] . "/local/vendor/autoload.php";

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

function phone_format($phone)
{
    $phone = trim($phone);

    $res = preg_replace(
        array(
            '/[\+]?([7|8])[-|\s]?\([-|\s]?(\d{3})[-|\s]?\)[-|\s]?(\d{3})[-|\s]?(\d{2})[-|\s]?(\d{2})/',
            '/[\+]?([7|8])[-|\s]?(\d{3})[-|\s]?(\d{3})[-|\s]?(\d{2})[-|\s]?(\d{2})/',
            '/[\+]?([7|8])[-|\s]?\([-|\s]?(\d{4})[-|\s]?\)[-|\s]?(\d{2})[-|\s]?(\d{2})[-|\s]?(\d{2})/',
            '/[\+]?([7|8])[-|\s]?(\d{4})[-|\s]?(\d{2})[-|\s]?(\d{2})[-|\s]?(\d{2})/',
            '/[\+]?([7|8])[-|\s]?\([-|\s]?(\d{4})[-|\s]?\)[-|\s]?(\d{3})[-|\s]?(\d{3})/',
            '/[\+]?([7|8])[-|\s]?(\d{4})[-|\s]?(\d{3})[-|\s]?(\d{3})/',
        ),
        array(
            '+7 ($2) $3-$4-$5',
            '+7 ($2) $3-$4-$5',
            '+7 ($2) $3-$4-$5',
            '+7 ($2) $3-$4-$5',
            '+7 ($2) $3-$4',
            '+7 ($2) $3-$4',
        ),
        $phone
    );

    return $res;
}

include_once __DIR__ . '/include/ClassLoader.php';
include_once __DIR__ . '/include/EventLoader.php';

function getCurrentCity()
{
    Loader::includeModule("dev2fun.multidomain");
    Loader::includeModule("iblock");

    $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->toArray();

    try {
        $init = Dev2fun\MultiDomain\Base::InitDomains();
        if ($init) {
            $curCity = Dev2fun\MultiDomain\Base::GetCurrentDomain();
            global $arCurCity;

            if ($request['setCity'] == 'Y') {

                if (!empty($curCity['UF_SUBDOMAIN'])) {
                    $cookie = new \Bitrix\Main\Web\Cookie("setCity", $curCity['UF_SUBDOMAIN'], time() + 86400 * 30);
                    $cookie->setSpread(\Bitrix\Main\Web\Cookie::SPREAD_DOMAIN); // распространять куки на все домены
                    $cookie->setDomain('perspektiva24.com');
                    $cookie->setSecure(false); // безопасное хранение cookie
                    $cookie->setHttpOnly(false);
                    \Bitrix\Main\Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
                } else {
                    $cookie = new \Bitrix\Main\Web\Cookie("setCity", 'N', time() + 86400 * 30);
                    $cookie->setSpread(\Bitrix\Main\Web\Cookie::SPREAD_DOMAIN); // распространять куки на все домены
                    $cookie->setDomain('perspektiva24.com');
                    $cookie->setSecure(false); // безопасное хранение cookie
                    $cookie->setHttpOnly(false);
                    \Bitrix\Main\Application::getInstance()->getContext()->getResponse()->addCookie($cookie);
                }
            }

            $setCityCookie = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getCookie("setCity");

            $dmnParts = explode('.', $_SERVER['HTTP_HOST']);
            $subDomain = array_shift($dmnParts);

            $arCurCity = [];

            if (!empty($curCity['UF_CITY_LINK_ID'][0])) {
                $rsCity = CIBlockElement::GetByID($curCity['UF_CITY_LINK_ID'][0])->GetNextElement();
                if ($rsCity) {
                    $arCurCity = $rsCity->GetFields();
                    $arCurCity['PROPS'] = $rsCity->GetProperties();
                    $arCurCity['FILIALS'] = $curCity['UF_CITY_LINK_ID'];
                    $arCurCity['REL_NAME'] = $curCity['UF_NAME'];

                    $arCurCity['FILIAL_CRM_ID'] = $arCurCity['PROPS']['ID']['VALUE'];

                    $arCurCity['LINK_INSTAGRAM'] = $curCity['UF_LINK_INSTAGRAM'];
                    $arCurCity['LINK_WHATSAPP'] = $curCity['UF_LINK_WHATSAPP'];
                    $arCurCity['LINK_FACEBOOK'] = $curCity['UF_LINK_FACEBOOK'];
                    $arCurCity['LINK_VKONTAKTE'] = $curCity['UF_LINK_VKONTAKTE'];
                    $arCurCity['LINK_YOUTUBE'] = $curCity['UF_LINK_YOUTUBE'];

                    $geo = new \Dev2fun\MultiDomain\Geo();
                    $geo->setIp($_SERVER['REMOTE_ADDR']);
                    $geoCity = mb_strtolower($geo->getCityName('en'));
                    $hlblCity = 3;
                    $hlblockCity = HL\HighloadBlockTable::getById($hlblCity)->fetch();

                    $entity = HL\HighloadBlockTable::compileEntity($hlblockCity);
                    $entity_data_class_CITY = $entity->getDataClass();

                    if (!empty($setCityCookie)) {
                        $geoCity = $setCityCookie;
                    }
                    if ($request['setCity'] == 'Y') {
                        $geoCity = $curCity['UF_SUBDOMAIN'];
                    }

                    if ($subDomain != $setCityCookie['UF_SUBDOMAIN']) {
                        $geoCity = $subDomain;
                    }

                    if (!empty($geoCity)) {
                        $rsDataCity = $entity_data_class_CITY::getList([
                            "select" => ["*"],
                            "order" => ["ID" => "ASC"],
                            "filter" => ["UF_SUBDOMAIN" => $geoCity]
                        ]);

                        if ($rsDataCity->getSelectedRowsCount() > 0) {
                            while ($arDataCity = $rsDataCity->Fetch()) {
                                if ($arDataCity['UF_SUBDOMAIN'] != $curCity['UF_SUBDOMAIN']) {
                                    LocalRedirect('https://' . $arDataCity['UF_SUBDOMAIN'] . '.' . $curCity['UF_DOMAIN']);
                                }
                            }
                        }
                    }
                }
            }
        }
    } catch (\GeoIp2\Exception\AddressNotFoundException $exception) {
    }
}

if (!$request->isAdminSection()) {
    getCurrentCity();
}

AddEventHandler('main', 'OnBeforeEventSend', array("MyForm", "my_OnBeforeEventSend"));

class MyForm
{
    function my_OnBeforeEventSend($arFields, $arTemplate)
    {
        //получим сообщение
        $mess = $arTemplate["MESSAGE"];
        foreach ($arFields as $keyField => $arField)
            $mess = str_replace('#' . $keyField . '#', $arField, $mess); //подставляем значения в шаблон
    }
}

AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "updateTopFloor");
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "updateTopFloor");

function updateTopFloor(&$arFields)
{
    if ($arFields['RESULT']) {
        $rsElement = CIBlockElement::GetByID($arFields['ID'])->GetNextElement();
        $propsElement = $rsElement->GetProperties();
        $floor = $propsElement['FLOOR']['VALUE'][0];
        $floors = $propsElement['FLOORS']['VALUE'][0];
        if ($floor == $floors) {
            CIBlockElement::SetPropertyValuesEx($arFields['ID'], false, ['TOP_FLOOR' => 'Y']);
        }
    }
}

function array_unique_key($array, $key) {
    $tmp = $key_array = array();
    $i = 0;

    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $tmp[$i] = $val;
        }
        $i++;
    }
    return $tmp;
}

function dd($result, $inFile = false)
{
    global $USER;
    if ($inFile) {
        \Bitrix\Main\Diag\Debug::dumpToFile($result);
    }elseif ($USER->IsAdmin()) {
        \Bitrix\Main\Diag\Debug::dump($result);
    }
}


function getAgentById($id){
    $rsAgent = CIBlockElement::GetByID($id);
    $arAgent = $rsAgent->GetNextElement();
    if ($arAgent) {
        $arAgentProperties = $arAgent->GetProperties();
        $arAgent = $arAgent->GetFields();
        $arResult = [
            'NAME' => $arAgent['NAME'],
            'PHONE' => $arAgentProperties['PHONE']['VALUE'],
        ];
    }
    return $arResult;
}