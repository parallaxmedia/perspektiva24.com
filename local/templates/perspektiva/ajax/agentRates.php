<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';

use Bitrix\Main\Loader;
use P24\Rest\Request;
use Bitrix\Highloadblock as HL;

\Bitrix\Main\Loader::includeModule('iblock');

$PROP = array();
$PROP[199] = $_REQUEST['rates'][0];
$PROP[200] = $_REQUEST['rates'][1];
$PROP[201] = $_REQUEST['rates'][2];
$PROP[202] = $_REQUEST['rates'][3];
$PROP[203] = $_REQUEST['agent'];
$PROP[204] = $_REQUEST['text'];

$el = new CIBlockElement;

$arLoadProductArray = array(
    "IBLOCK_ID" => 11,
    "PROPERTY_VALUES" => $PROP,
    "NAME" => $_REQUEST['name'],
    "ACTIVE" => "N",
);

$PRODUCT_ID = $el->Add($arLoadProductArray);

Loader::includeModule("highloadblock");

$hlbl = 3;

$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$domainParts = explode('.', $_SERVER['SERVER_NAME']);
$subdomain = $domainParts[0];

if ($subdomain == 'perspektiva24') {
    $subdomain = '';
}

if ($subdomain) {
    $rsData = $entity_data_class::getList([
        "select" => ["*"],
        "order" => ["ID" => "ASC"],
        "filter" => ["UF_SUBDOMAIN" => $subdomain,]
    ]);

} else {
    $rsData = $entity_data_class::getById(2);
}

$curCity = $rsData->fetch();

if (!empty($curCity['UF_CITY_LINK_ID'][0])) {
    $rsCity = CIBlockElement::GetByID($curCity['UF_CITY_LINK_ID'][0])->GetNextElement();
    if ($rsCity) {
        $arCurCity = $rsCity->GetFields();
        $arCurCity['PROPS'] = $rsCity->GetProperties();
        $arCurCity['FILIAL_CRM_ID'] = $arCurCity['PROPS']['ID']['VALUE'];
    }
}

$request = new Request();

$rsAgent = CIBlockElement::GetByID($_REQUEST['agent'])->GetNextElement();
$arAgentProp = $rsAgent->GetProperties();

$SendParams = [
    'agent_id' => $arAgentProp['ID']['VALUE'], // int id агента в crm
    'name' => $_REQUEST['name'], // Имя того кто оставляет отзыв
    'text' => $_REQUEST['text'], // Текст отзыва
    'rate' => (int)ceil(($_REQUEST['rates'][0] + $_REQUEST['rates'][1] + $_REQUEST['rates'][2] + $_REQUEST['rates'][3]) / 4), // int рейтинг который поставили агенту
    'filial_id' => (int)$arCurCity['FILIAL_CRM_ID'],
];

$res = $request->sendAgentReview($SendParams);

echo 'OK';
die();