<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

use Bitrix\Main\Mail\Event;
use Bitrix\Main\Loader;
use P24\Rest\Request;
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

global $USER;
global $APPLICATION;
global $arCurCity;

Loader::includeModule("highloadblock");

$hlbl = 3;

$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$domainParts = explode('.', $_SERVER['SERVER_NAME']);
$subdomain = $domainParts[0];

if ($subdomain == 'perspektiva24') {
    $subdomain = '';
}

if ($subdomain != '') {
    $rsData = $entity_data_class::getList([
        "select" => ["*"],
        "order" => ["ID" => "ASC"],
        "filter" => ["UF_SUBDOMAIN" => $subdomain,]
    ]);
} else {
    $rsData = $entity_data_class::getById(2);
}

$curCity = $rsData->fetch();

if (!empty($curCity['UF_CITY_LINK_ID'][0])) {
    $rsCity = CIBlockElement::GetByID($curCity['UF_CITY_LINK_ID'][0])->GetNextElement();
    if ($rsCity) {
        $arCurCity = $rsCity->GetFields();
        $arCurCity['PROPS'] = $rsCity->GetProperties();
        $arCurCity['FILIAL_CRM_ID'] = $arCurCity['PROPS']['ID']['VALUE'];
    }
}

$request = new Request();
global $APPLICATION;
$leadSendParams = [
    'email' => $_REQUEST['user_email'],
    'phone' => $_REQUEST['user_phone'],
    'name' => $_REQUEST['user_name'],
    'comment' => $_REQUEST['MESSAGE'],
    'refer' => '',
    'filial_id' => (int)$arCurCity['FILIAL_CRM_ID'],
    'url' => SITE_SERVER_NAME . $APPLICATION->GetCurPage(),
];

$request->sendLead($leadSendParams);

$APPLICATION->RestartBuffer();
?>

<div class="alert alert-success">Спасибо! Ваше сообщение получено.</div>

