<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';

global $USER;

use P24\Rest\Request;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

/** Текущий домен : begin */
Loader::includeModule("highloadblock");

$hlbl = 3;

$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$domainParts = explode('.', $_SERVER['SERVER_NAME']);
$subdomain = $domainParts[0];

if ($subdomain == 'perspektiva24') {
    $subdomain = '';
}

if ($subdomain) {
    $rsData = $entity_data_class::getList([
        "select" => ["*"],
        "order" => ["ID" => "ASC"],
        "filter" => ["UF_SUBDOMAIN" => $subdomain,]
    ]);

} else {
    $rsData = $entity_data_class::getById(2);
}

$curCity = $rsData->fetch();

if (!empty($curCity['UF_CITY_LINK_ID'][0])) {
    $rsCity = CIBlockElement::GetByID($curCity['UF_CITY_LINK_ID'][0])->GetNextElement();
    if ($rsCity) {
        $arCurCity = $rsCity->GetFields();
        $arCurCity['PROPS'] = $rsCity->GetProperties();
        $arCurCity['FILIAL_CRM_ID'] = $arCurCity['PROPS']['ID']['VALUE'];
    }
}

$message = $_REQUEST['message'];

if ($_REQUEST['type'] == 'Работа') {
    $message = "<br>Дата рождения: " . $_REQUEST['ans_1'] . "<br>В какой сфере приходилось работать: " . implode(', ', $_REQUEST['ans_2']) . "<br>Опыт работы: " . $_REQUEST['ans_3'];
}

if ($_REQUEST['type'] == 'Продать') {
    $message = "<br>Вид сделки: " . $_REQUEST['Radios1'] . "
<br>Вид объекта недвижимости: " . $_REQUEST['Radios2'] . "
<br>В каком районе ваша недвижимость?: " . $_REQUEST['checkbox1'][0] . "
<br>Желаемая цена продажи: " . $_REQUEST['Radios3'];
}

if ($_REQUEST['type'] == 'Купить') {
    $message = "<br>Вид объекта недвижимости: " . $_REQUEST['Radios2'] . "
<br>Желаемая цена покупки: " . $_REQUEST['Radios3'] . "
<br>Вид бюджета: " . $_REQUEST['Radios4'];
}

if ($_REQUEST['type'] == 'Обменять') {
    $message = "<br>Вид вашего объекта недвижимости: " . $_REQUEST['Radios2'] . "
<br>Выберите вариант обмена: " . $_REQUEST['Radios5'] . "
<br>Вид бюджета: " . $_REQUEST['Radios4'] . "
<br>Вид объекта недвижимости на который хотите обменять свою: " . $_REQUEST['Radios6'];
}

/** Текущий домен : end */

$request = new Request();

$leadSendParams = [
    'email' => $_REQUEST['email'],
    'phone' => $_REQUEST['phone'],
    'name' => $_REQUEST['name'],
    'comment' => $message,
    'type' => $_REQUEST['type'],
    'refer' => '',
    'filial_id' => (int)$arCurCity['FILIAL_CRM_ID'],
];

if ($_REQUEST['type'] == 'Контакты') {
    $leadSendParams = [
        'email' => $_REQUEST['user_email'],
        'phone' => $_REQUEST['user_phone'],
        'name' => $_REQUEST['user_name'],
        'comment' => $_REQUEST['MESSAGE'],
        'refer' => '',
        'filial_id' => (int)$arCurCity['FILIAL_CRM_ID'],
    ];
}

$request->sendLead($leadSendParams);
?>
<div class="alert alert-success">Спасибо! Ваше сообщение получено.</div>