<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

\Bitrix\Main\Loader::includeModule('iblock');

$itemList = $_SESSION['CATALOG_COMPARE_LIST'][2]['ITEMS'];
$itemIdList = [];



if (!empty($itemList)) {
    foreach ($itemList as $item) {
        $itemIdList[] = $item['ID'];
    }
}

$rsElements = CIBlockElement::GetList([], ['ID' => $itemIdList], false, false,
    [
        'ID',
        'NAME',
        'IBLOCK',
        'PROPERTY_ADDRESS',
        'PROPERTY_PRICE',
        'PROPERTY_ROOMS',
        'PROPERTY_LIVINGSPACE',
        'PROPERTY_KITCHENSPACE',
        'PROPERTY_FLOOR',
        'PROPERTY_WALLSTYPE',
        'PROPERTY_PRICE_PER_METER',
        'PROPERTY_AREA',
        'PROPERTY_FLOORS',
        'PROPERTY_AGENT_ID',
    ]
);
$arElements = [];
$table = [
    [
        'Наименование',
        'Стоимость общая',
        'Стоимость за кв.м',
        'Адрес',
        'Количество комнат',
        'Общая площадь',
        'Жилая площадь',
        'Площадь кухни',
        'Этаж',
        'Этажность',
        'Материал стен',
        'ФИО Агента',
        'Телефон агента',
        'Собственников',
    ]
];
$issetAgent = [];
while ($arElement = $rsElements->Fetch()) {
    $row = [
        $arElement['PROPERTY_ADDRESS_VALUE'],
        $arElement['PROPERTY_PRICE_VALUE'],
        $arElement['PROPERTY_PRICE_PER_METER_VALUE'],
        $arElement['PROPERTY_ADDRESS_VALUE'],
        $arElement['PROPERTY_ROOMS_VALUE'],
        $arElement['PROPERTY_AREA_VALUE'],
        $arElement['PROPERTY_LIVINGSPACE_VALUE'],
        $arElement['PROPERTY_KITCHENSPACE_VALUE'],
        $arElement['PROPERTY_FLOOR_VALUE'],
        $arElement['PROPERTY_FLOORS_VALUE'],
        $arElement['PROPERTY_WALLSTYPE_VALUE'],
    ];

    $agentName="";
    $agentPhone="";


    if (isset($arElement['PROPERTY_AGENT_ID_VALUE'])){
        if (!isset($issetAgent[$arElement['PROPERTY_AGENT_ID_VALUE']])){
            $agentInfo = getAgentById($arElement['PROPERTY_AGENT_ID_VALUE']);
            $issetAgent[$arElement['PROPERTY_AGENT_ID_VALUE']]=$agentInfo;
            $agentName=$agentInfo["NAME"];
            $agentPhone=$agentInfo["PHONE"];
        }
        else{
            $agentName=$issetAgent[$arElement['PROPERTY_AGENT_ID_VALUE']]["NAME"];
            $agentPhone=$issetAgent[$arElement['PROPERTY_AGENT_ID_VALUE']]["PHONE"];
        }
    }

    $row[]=$agentName;
    $row[]=$agentPhone;
    $row[]=1;// количество собственников
    $table[] = $row;
}

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->fromArray($table);

$writer = new Xlsx($spreadsheet);
$writer->save('compare.xlsx');

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename='.basename('compare.xlsx'));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize('compare.xlsx'));
ob_clean();
flush();
readfile('compare.xlsx');
unlink('compare.xlsx');
exit;