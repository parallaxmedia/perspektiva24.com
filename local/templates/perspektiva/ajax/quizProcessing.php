<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';

use Bitrix\Main\Mail\Event;

global $USER;
global $arCurCity;

use P24\Rest\Request;
use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

Loader::includeModule("highloadblock");

$hlbl = 3;

$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$domainParts = explode('.', $_SERVER['SERVER_NAME']);
$subdomain = $domainParts[0];

if ($subdomain == 'perspektiva24') {
    $subdomain = '';
}

if ($subdomain) {
    $rsData = $entity_data_class::getList([
        "select" => ["*"],
        "order" => ["ID" => "ASC"],
        "filter" => ["UF_SUBDOMAIN" => $subdomain,]
    ]);

} else {
    $rsData = $entity_data_class::getById(2);
}

$curCity = $rsData->fetch();

if (!empty($curCity['UF_CITY_LINK_ID'][0])) {
    $rsCity = CIBlockElement::GetByID($curCity['UF_CITY_LINK_ID'][0])->GetNextElement();
    if ($rsCity) {
        $arCurCity = $rsCity->GetFields();
        $arCurCity['PROPS'] = $rsCity->GetProperties();
        $arCurCity['FILIAL_CRM_ID'] = $arCurCity['PROPS']['ID']['VALUE'];
    }
}


$message = "<br>Дата рождения: " . $_REQUEST['ans_1'] . "
<br>В какой сфере приходилось работать: " . implode(', ', $_REQUEST['ans_2']) . "
<br>Опыт работы: " . $_REQUEST['ans_3'];

global $APPLICATION;

$request = new Request();

$leadSendParams = [
    'email' => '',
    'phone' => $_REQUEST['phone'],
    'name' => $_REQUEST['name'],
    'comment' => $message,
    'refer' => '',
    'filial_id' => (int)$arCurCity['FILIAL_CRM_ID'],
    'url' => SITE_SERVER_NAME . $APPLICATION->GetCurPage(),
];

\Bitrix\Main\Diag\Debug::dumpToFile($leadSendParams);

$request->sendLead($leadSendParams);

?>

<div class="alert alert-success">Спасибо! Ваше сообщение получено.</div>
