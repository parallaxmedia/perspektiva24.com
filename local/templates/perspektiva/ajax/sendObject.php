<?php
include_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');
\Bitrix\Main\Loader::includeModule('sale');

use Bitrix\Main\Mail\Event;

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$email = $request['email'];
$product_id = $request['PRODUCT_ID'];

$rsElement = CIBlockElement::GetByID($product_id)->GetNextElement();

$arResult = $rsElement->GetFields();
$arResult['PROPERTIES'] = $rsElement->GetProperties();
ob_start();
?>
    <style>
        h1 {
            position: relative;
            overflow: unset;
            clip: unset;
            height: auto;
            width: auto;
            margin: 30px 0 0 0;
        }

        .product-wrapper {
            display: -ms-grid;
            display: grid;
            -ms-grid-columns: 45% 55%;
            -ms-grid-rows: auto auto;
            grid-template-columns: 45% 55%;
            grid-template-rows: auto auto;
        }

        .product-wrapper__intro {
            -ms-grid-column: 2;
            -ms-grid-column-span: 1;
            grid-column: 2/3;
            -ms-grid-row: 1;
            -ms-grid-row-span: 1;
            grid-row: 1/2;
        }

        .product-wrapper__gallery {
            -ms-grid-column: 1;
            -ms-grid-column-span: 1;
            grid-column: 1/2;
            -ms-grid-row: 1;
            -ms-grid-row-span: 4;
            grid-row: 1/5;
            margin-right: 30px;
        }

        .product-wrapper__short {
            -ms-grid-column: 2;
            -ms-grid-column-span: 1;
            grid-column: 2/3;
            -ms-grid-row: 3;
            -ms-grid-row-span: 1;
            grid-row: 3/4;
        }

        .product-wrapper__info {
            padding-top: 20px;
            -ms-grid-column: 2;
            -ms-grid-column-span: 1;
            grid-column: 2/3;
            -ms-grid-row: 2;
            -ms-grid-row-span: 1;
            grid-row: 2/3;
        }

        .product__price {
            margin: 0;
            font-size: 24px;
            font-weight: 700;
        }

        .product__hypothec {
            top: 42px;
            border: none;
            background-color: #e5e5e5;
            border-radius: 30px;
            padding: 6px 20px 7px 20px;
            transition: all 0.5s;
            color: #333333;
            border-bottom: 1px dotted #AAAAAA;
            display: inline-block;
            position: absolute;
            right: 0;
            font-size: 14px;
        }

        .product__price-label {
            margin: 2px 0;
        }

        sup {
            top: -.5em;
            position: relative;
            font-size: 75%;
            line-height: 0;
        }

        .product__address {
            padding-right: calc(100% - 350px);
            border-bottom: 1px solid #AAAAAA;
            padding-bottom: 20px;
            margin: 30px 0 0 0;
        }

        .product__address .icon {
            font-size: 12px;
            display: inline-block;
            margin-right: 5px;
        }

        .icon_gray {
            color: #777777;
        }

        .icon-direction {
            font-size: 1.4rem;
            width: 1.07143em;
        }

        .icon {
            display: inline-block;
            width: 1em;
            height: 1em;
            fill: currentColor;
        }

        img, svg {
            vertical-align: middle;
        }

        .product-wrapper__gallery {
            -ms-grid-column: 1;
            -ms-grid-column-span: 1;
            grid-column: 1/2;
            -ms-grid-row: 1;
            -ms-grid-row-span: 4;
            grid-row: 1/5;
            margin-right: 30px;
        }

        .product__gallery {
            margin-top: 20px;
        }

        .product__gallery-main {
            position: relative;
        }

        .product__total-badge {
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 50%;
            background-color: #ffffff;
            border: 2px solid #0099EA;
            width: 25px;
            height: 25px;
            font-size: 10px;
            font-weight: 700;
            line-height: 1;
            position: absolute;
            z-index: 3;
            right: 6px;
            bottom: 6px;
        }

        .swiper-container {
            margin-left: auto;
            margin-right: auto;
            position: relative;
            overflow: hidden;
            list-style: none;
            padding: 0;
            z-index: 1;
        }

        .swiper-wrapper {
            z-index: 1;
            display: flex;
            box-sizing: content-box;
            height: 100%;
            position: relative;
            transition-property: transform;
            width: 100%;
        }

        .product__gallery-small {
            display: block;
            margin-top: 8px;
            position: relative;
        }

        .product-wrapper__short {
            -ms-grid-column: 2;
            -ms-grid-column-span: 1;
            grid-column: 2/3;
            -ms-grid-row: 3;
            -ms-grid-row-span: 1;
            grid-row: 3/4;
        }

        .product__short-info {
            margin-top: 40px;
            flex-wrap: wrap;
            color: #777777;
            font-size: 16px;
            display: flex;
            align-items: center;
        }

        ul {
            list-style: none;
            padding-left: 0;
        }

        ul li {
            position: relative;
            padding-left: 12px;
            margin-bottom: 10px;
        }

        .product__short-info strong {
            font-weight: 700;
        }

        .product__short-info p {
            margin: 0 0 0 5px;
        }

        .product-wrapper__info {
            padding-top: 20px;
            ms-grid-column: 2;
            -ms-grid-column-span: 1;
            grid-column: 2/3;
            -ms-grid-row: 2;
            -ms-grid-row-span: 1;
            grid-row: 2/3;
        }

        .product__info table {
            width: 100%;
        }

        table {
            border-collapse: collapse;
        }

        .product__info-buttons {
            padding-top: 20px;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }
    </style>
    <h1><?= $arResult['PROPERTIES']['ADDRESS']['VALUE'] ?></h1>
    <div class="product-wrapper">
        <div class="product-wrapper__intro">
            <? if ($arResult['PROPERTIES']['STATUS']['VALUE'] == 'Завершен') : ?>
                <p class="product__price" style="color: red;">Снят с продажи</p>
            <? else : ?>
                <p class="product__price"><?= CCurrencyLang::CurrencyFormat($arResult['PROPERTIES']['PRICE']['VALUE'], "RUB"); ?></p>
            <? endif; ?>
            <p class="product__price-label"><?= $arResult['PROPERTIES']['PRICE_PER_METER']['VALUE'] ?> &#x20bd; за
                м<sup>2</sup></p>
            <p class="product__address">
                <svg class="icon icon-direction icon_gray">
                    <use xlink:href="local/templates/perspektiva/images/sprite.svg#direction"></use>
                </svg>
                <span><?= $arResult['PROPERTIES']['ADDRESS']['VALUE'] ?></span>
            </p>
        </div>
        <div class="product-wrapper__gallery">
            <?
            if (is_array($arResult['PROPERTIES']['IMAGES']['VALUE'])) {
                $countPhoto = count($arResult['PROPERTIES']['IMAGES']['VALUE']);
            } else {
                $countPhoto = 0;
            }
            ?>
            <div class="product__gallery js-gallery-detail">
                <div class="product__gallery-main">
                    <div class="swiper-container js-main-gallery-detail">
                        <div class="swiper-wrapper">
                            <? if ($countPhoto > 0) : ?>
                                <div class="swiper-slide">
                                    <a class="product__gallery-preview"
                                       href="<?= $arResult['PROPERTIES']['IMAGES']['VALUE'][0] ?>"
                                       data-fancybox="gallery">
                                        <img src="<?= $arResult['PROPERTIES']['IMAGES']['VALUE'][0] ?>" width="882"
                                             height="498" alt="">
                                    </a>
                                </div>
                            <? endif; ?>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-wrapper__short">
            <ul class="product__short-info">
                <li>
                    <span><strong>Лот</strong></span>
                    <p><?= $arResult['PROPERTIES']['ID']['VALUE'] ?></p>
                </li>
                <li>
                    <span class="mobile-hidden">Публикация</span>
                    <p><?= FormatDateFromDB($arResult["DATE_CREATE"], 'SHORT'); ?></p>
                </li>
                <li>
                    <svg class="icon icon-eye icon_blue">
                        <use xlink:href="local/templates/perspektiva/images/sprite.svg#eye"></use>
                    </svg>
                    <p class="mobile-hidden">Просмотров</p>
                    <p><?= ($arResult['SHOW_COUNTER'] > 0) ? $arResult['SHOW_COUNTER'] : '0' ?></p>
                </li>
            </ul>
        </div>
        <div class="product-wrapper__info">
            <div class="product__info">
                <table>
                    <? if (!empty($arResult['PROPERTIES']['ROOMS']['VALUE'])) : ?>
                        <tr>
                            <td>Комнаты</td>
                            <td><?= $arResult['PROPERTIES']['ROOMS']['VALUE'] ?></td>
                        </tr>
                    <? endif; ?>
                    <? if (!empty($arResult['PROPERTIES']['WALLSTYPE']['VALUE'][0])) : ?>
                        <tr>
                            <td>Материал стен</td>
                            <td><?= $arResult['PROPERTIES']['WALLSTYPE']['VALUE'][0] ?></td>
                        </tr>
                    <? endif; ?>
                    <? if (!empty($arResult['PROPERTIES']['FLOOR']['VALUE'][0])) : ?>
                        <tr>
                            <td>Этаж</td>
                            <td><?= $arResult['PROPERTIES']['FLOOR']['VALUE'][0] ?>
                                /<?= $arResult['PROPERTIES']['FLOORS']['VALUE'][0] ?></td>
                        </tr>
                    <? endif; ?>
                    <? if (!empty($arResult['PROPERTIES']['KITCHENSPACE']['VALUE'][0])) : ?>
                        <tr>
                            <td>Площадь кухни</td>
                            <td><?= $arResult['PROPERTIES']['KITCHENSPACE']['VALUE'][0] ?>м<sup>2</sup></td>
                        </tr>
                    <? endif; ?>
                    <? if (!empty($arResult['PROPERTIES']['LIVINGSPACE']['VALUE'][0])) : ?>
                        <tr>
                            <td>Жилая площадь</td>
                            <td><?= $arResult['PROPERTIES']['LIVINGSPACE']['VALUE'][0] ?> м<sup>2</sup></td>
                        </tr>
                    <? endif; ?>

                    <? if (!empty($arResult['PROPERTIES']['CELINGS']['VALUE'])) : ?>
                        <tr>
                            <td>Потолки</td>
                            <td><?= $arResult['PROPERTIES']['CELINGS']['VALUE'] ?> м</td>
                        </tr>
                    <? endif; ?>
                </table>
            </div>
        </div>
    </div>
    <div class="product__desc">
        <div class="product__desc-col">
            <div class="product__desc-content">
                <h3>Описание продавца</h3>
                <?= $arResult['DETAIL_TEXT'] ?>
            </div>
        </div>
    </div>

<?
$HTML = ob_get_contents();
ob_end_clean();

Event::send([
    "EVENT_NAME" => "PRODUCT_SEND",
    "LID" => "s1",
    "C_FIELDS" => [
        "EMAIL" => $email,
        "HTML" => $HTML,
    ],
]);

echo "<p>Информация об объекте отправлена Вам на почту</p>"; ?>