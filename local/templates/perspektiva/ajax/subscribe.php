<?php include_once $_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/main/include/prolog_before.php';

global $USER;

use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$hlbl = 6;
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

global $arCurCity;

$data = array(
    "UF_EMAIL" => $request['email'],
    "UF_CITY_ID" => $arCurCity['PROPS']['CITY']['VALUE'],
);

$result = $entity_data_class::add($data);

\Bitrix\Main\Diag\Debug::dump($result);
