<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION, $arCurCity;

//delayed function must return a string
if (empty($arResult))
    return "";

$curCity = Dev2fun\MultiDomain\Base::GetCurrentDomain();

$city = $arCurCity['REL_NAME'];
$strReturn = '';
$strReturn .= '<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">';

$strReturn .= '<span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" href="/">
                        <span property="name">' . $city . '</span>
                    </a>
                </span><span class="breadcrumbs__sep"><svg class="icon icon-chevron icon_gray"><use xlink:href="' . SITE_TEMPLATE_PATH . '/images/sprite.svg#chevron"></use></svg></span>';

$itemSize = count($arResult);

for ($index = 0; $index < $itemSize; ++$index) {

    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    $title = str_replace('#city#', $arCurCity['REL_NAME'], $title);
    $arrow = '<span class="breadcrumbs__sep"><svg class="icon icon-chevron icon_gray"><use xlink:href="' . SITE_TEMPLATE_PATH . '/images/sprite.svg#chevron"></use></svg></span>';

    if ($arResult[$index]["LINK"] <> "" && $index + 1 != $itemSize) {
        $strReturn .= '<span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" href="' . $arResult[$index]["LINK"] . '">
                        <span property="name">' . $title . '</span>
                    </a>
                </span>' . $arrow;
    } else {
        $strReturn .= '<span property="itemListElement" typeof="ListItem">
                    <a property="item" typeof="WebPage" href="' . $arResult[$index]["LINK"] . '">
                        <span property="name">' . $title . '</span>
                    </a>
                </span>';
    }
}

$strReturn .= '</div>';

return $strReturn;

?>