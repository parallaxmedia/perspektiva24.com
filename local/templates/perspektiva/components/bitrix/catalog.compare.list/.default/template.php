<?php
?>
<a class="header__action" href="/compare/">
    <svg class="icon icon-compare ">
        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#compare"></use>
    </svg>
    <span>Объектов к сравнению</span>
    <i class="compare-cnt"><?= (count($arResult) > 0) ? count($arResult) : '0' ?></i>
</a>
