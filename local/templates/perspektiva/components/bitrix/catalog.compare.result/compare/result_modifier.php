<?php

foreach ($arResult['ITEMS'] as $key => &$arItem) {
    $arSelect = array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*");
    $arFilter = array("IBLOCK_ID" => IntVal($arItem['IBLOCK_ID']), "ID" => $arItem['ID'] , "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 1), $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arItem['PROP'] = $ob->GetProperties();
    }
    $rsAgent = CIBlockElement::GetByID($arItem['PROP']['AGENT_ID']['VALUE']);
    $arAgent = $rsAgent->GetNextElement();

    $arAgentProperties = $arAgent->GetProperties();
    $arAgent = $arAgent->GetFields();

    $arItem['PHONE'] = $arAgentProperties['PHONE']['VALUE'];
}