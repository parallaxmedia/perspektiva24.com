<?php
//\Bitrix\Main\Diag\Debug::dump($arResult['ITEMS']) ;
?>


<div class="compare">
    <a class="btn btn-secondary btn-icon compare__clean" href="#"><span>Очистить список</span>
      <svg class="icon icon-close icon_red">
        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#close"></use>
      </svg>
    </a>
    <div class="compare__header">
        <p class="compare__header-item compare__header-item_price">Стоимость</p>
        <p class="compare__header-item">Адрес</p>
        <p class="compare__header-item">Количество комнат</p>
        <p class="compare__header-item">Жилая площадь</p>
        <p class="compare__header-item">Площадь кухни</p>
        <p class="compare__header-item">Этаж</p>
        <p class="compare__header-item">Материал стен</p>
        <p class="compare__header-item">Собственников</p>
        <p class="compare__header-item">Ипотека</p>
        <p class="compare__header-item">Сертификат</p>
        <p class="compare__header-item">Материнский капитал</p>
    </div>
    <div class="compare__inner">
        <? foreach ($arResult['ITEMS'] as $arItem) : ?>
            <div class="compare__card">
                <div class="compare__card-header">
                    <div class="compare__card-image">
                        <button class="compare__remove" type="button" data-id="<?=$arItem['ID']?>" area-label="Удалить из сравнения"></button>
                        <img src="<?= $arItem['PROP']['IMAGES']['VALUE'][0] ?>" width="312" height="215" alt="">
                        <a class="compare__like" href="#" aria-label="В избранное" title="В избранное">
                          <svg class="icon icon-heart icon_white">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#heart"></use>
                          </svg>
                        </a>
                    </div>
                    <a class="compare__card-link"
                       href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['PROP']['ADDRESS']['VALUE'] ?></a>
                    <p class="comapre__card-price"><?= CurrencyFormat($arItem['PROP']['PRICE']['VALUE'], 'RUB') ?></p>
                    <p class="comapre__card-label"><?= $arItem['PROP']['PRICE_PER_METER']['VALUE'] ?> Р за м<sup>2</sup>
                    </p>
                    <div class="comapre__card-phone js-toggle-phone">
                        <a class="hidden" href="tel:<?= $arItem['PHONE'] ?>"><?= $arItem['PHONE'] ?></a>
                        <button class="btn btn-primary" type="button">Показать телефон</button>
                    </div>
                </div>
                <p class="compare__card-address">
                    <svg class="icon icon-direction icon_gray">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                    </svg>
                    <span><?= $arItem['PROP']['ADDRESS']['VALUE'] ?></span>
                </p>
                <?
                //\Bitrix\Main\Diag\Debug::dump($arItem['PROP']);
                ?>
                <p class="compare__card-row" data-label="Количество комнат"><span><?= $arItem['PROP']['ROOMS']['VALUE'] ?></span></p>
                <p class="compare__card-row" data-label="Жилая площадь"><span><?= $arItem['PROP']['LIVINGSPACE']['VALUE'][0] ?></span></p>
                <p class="compare__card-row" data-label="Площадь кухни"><span><?= $arItem['PROP']['KITCHENSPACE']['VALUE'][0] ?> м<sup>2</sup></span></p>
                <p class="compare__card-row" data-label="Этаж"><span><?= $arItem['PROP']['FLOOR']['VALUE'][0] ?></span></p>
                <p class="compare__card-row" data-label="Материал стен"><span><?= $arItem['PROP']['WALLSTYPE']['VALUE'][0] ?></span></p>
                <p class="compare__card-row" data-label="Собственников"><span>1</span></p>
                <? if ($arItem['PROP']['MORTGAGE']['VALUE'] != false) : ?>
                    <p class="compare__card-row" data-label="Ипотека"><span>Да</span></p>
                <? else : ?>
                    <p class="compare__card-row" data-label="Ипотека"><span>Нет</span></p>
                <? endif; ?>
                <p class="compare__card-row" data-label="Сертификат"><span><?= $arItem['PROP']['SERTIFICATE']['VALUE'] ?></span></p>
                <p class="compare__card-row" data-label="Материнский капитал"><span><?= $arItem['PROP']['MATKAP']['VALUE'] ?></span></p>
            </div>
        <? endforeach; ?>
    </div>
</div>
