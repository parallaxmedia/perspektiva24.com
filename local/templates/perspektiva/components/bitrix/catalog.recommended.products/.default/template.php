<?php
?>

<div class="products-grid">
    <? foreach ($arResult['ITEMS'] as $arItem) : ?>
        <div class="products-grid__item">
            <div class="products-grid__gallery js-gallery">
                <div class="products-grid__gallery-main">
                    <?
                    if ($arItem['PROPERTIES']['IMAGES']['VALUE'] != false) {
                        $count = count($arItem['PROPERTIES']['IMAGES']['VALUE']);
                    } else {
                        $count = 0;
                    }
                    ?>
                    <span class="products-grid__total-badge"><?= $count; ?></span>
                    <? if ($count > 0) : ?>
                        <div class="swiper-container js-main-gallery">
                            <div class="swiper-wrapper">
                                <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $image) : ?>
                                    <div class="swiper-slide">
                                        <div class="products-grid__image">
                                            <img src="<?= $image ?>" width="312" height="312" alt="">
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    <? endif; ?>
                </div>
                <div class="products-grid__gallery-small">
                    <? if ($count > 0) : ?>
                        <div class="swiper-container js-thumbs-gallery">
                            <div class="swiper-wrapper">
                                <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $image) : ?>
                                    <div class="swiper-slide">
                                        <div class="products-grid__thumb">
                                            <img src="<?= $image ?>" width="109" height="59" alt="">
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>
                </div>
            </div>
            <div class="products-grid__body">
                <div class="products-grid__price">
                    <p class="products-grid__price-val"><?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?> &#x20bd;</p>
                    <a class="products-grid__price-link" href="/services/mortgage/">Ипотека</a>
                </div>
                <div class="products-grid__intro">
                    <a class="products-grid__title" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
                    <p class="product-grid__address">
                        <svg class="icon icon-direction icon_gray">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                        </svg>
                        <span><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></span>
                    </p>
                    <dl class="product-grid__info">
                        <dt><?= $arItem['PROPERTIES']['DISTRICT']['VALUE'] ?></dt>
                        <dt>Этаж <?= $arItem['PROPERTIES']['FLOOR']['VALUE'][0] ?>/<?= $arItem['PROPERTIES']['FLOORS']['VALUE'][0] ?></dt>
                        <dt>Публикация <?= $arItem['ACTIVE_FROM'] ?></dt>
                    </dl>
                </div>
                <div class="products-grid__desc">
                    <p><?= $arItem['PREVIEW_TEXT'] ?></p>
                </div>
                <div class="products-grid__buttons">
                    <a class="btn btn-primary" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">Подробнее</a>
                    <a class="products-grid__like" href="#">
                        <svg class="icon icon-heart icon_gray">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#heart"></use>
                        </svg>
                        <span>В избранное</span>
                    </a>
                    <a class="btn btn-outline btn-icon jsToggleCompare" href="<?=SITE_TEMPLATE_PATH?>/ajax/addToCompare.php" data-prod-id="<?=$arItem['ID']?>">
                        <svg class="icon icon-compare icon_gray icon_left">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#compare"></use>
                        </svg>
                        <span class="label-compare">Сравнить</span>
                    </a>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>
