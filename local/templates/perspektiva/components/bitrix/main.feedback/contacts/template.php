<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\UI\Extension::load("ui.bootstrap4");
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<form class="write-us__form" action="<?= SITE_TEMPLATE_PATH ?>/ajax/formContactProcessing.php" method="POST">
    <?= bitrix_sessid_post() ?>
    <p class="write-us__title">Задайте вопрос</p>
    <div class="write-us__form-cols">
        <? if (!empty($arResult["ERROR_MESSAGE"])) {
            foreach ($arResult["ERROR_MESSAGE"] as $v)
                ShowError($v);
        }
        if (strlen($arResult["OK_MESSAGE"]) > 0) {
            ?>
            <div class="alert alert-success"><?= $arResult["OK_MESSAGE"] ?></div><?
        }
        ?>
        <div class="write-us__form-col">
            <label class="visually-hidden" for="write-name">Имя</label>
            <input id="write-name" type="text" name="user_name" placeholder="Имя" required>
            <label class="visually-hidden" for="write-phone">Телефон</label>
            <input id="write-phone" type="tel" name="user_phone" placeholder="Телефон" required>
            <label class="visually-hidden" for="write-email">E-mail</label>
            <input id="write-email" type="email" name="user_email" placeholder="E-mail" required>
        </div>
        <div class="write-us__form-col">
            <label class="visually-hidden" for="write-text">Текст сообщения</label>
            <textarea id="write-text" name="MESSAGE" placeholder="Текст сообщения" required></textarea>
            <div class="write-us__form-submit">
                <input type="hidden" name="PARAMS_HASH" value="<?= $arResult["PARAMS_HASH"] ?>">
                <button class="btn btn-danger btn-sm" name="submit" type="submit">Отправить</button>
            </div>
        </div>
    </div>
</form>