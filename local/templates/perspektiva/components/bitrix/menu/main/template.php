<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<nav class="main-menu">
    <a class="main-menu__open main-menu__control" href="javascript:void(0)"
       aria-label="<?= GetMessage('DEF_TEMPLATE_OPEN_MENU') ?>">
        <svg class="icon icon-menu icon_black">
            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#menu"></use>
        </svg>
    </a>
    <div class="main-menu__wrapper">
        <div class="main-menu__close">
            <a class="main-menu__control" href="javascript:void(0)"
               aria-label="<?= GetMessage('DEF_TEMPLATE_CLOSE_MENU') ?>">
                <svg class="icon icon-close icon_black">
                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#close"></use>
                </svg>
            </a>
        </div>
        <ul class="main-menu__list">
            <? foreach ($arResult["MENU_STRUCTURE"] as $itemID => $arColumns): ?>
                <?
                $class = 'class="';
                if (is_array($arColumns) && count($arColumns) > 0) {
                    $class .= 'main-menu__parent ';
                }
                if ($arResult["ALL_ITEMS"][$itemID]["SELECTED"]) {
                    $class .= 'active';
                }
                $class .= '"';
                ?>
                <li <?= $class ?> >
                <a <?= ($arResult["ALL_ITEMS"][$itemID]["SELECTED"]) ? 'class="active"' : "" ?> <?= (isset($arResult["ALL_ITEMS"][$itemID]['PARAMS']['target']) ? 'target="' . $arResult["ALL_ITEMS"][$itemID]['PARAMS']['target'] . '"' : '') ?>
                        href="<?= $arResult["ALL_ITEMS"][$itemID]['LINK'] ?>"><?= $arResult["ALL_ITEMS"][$itemID]['TEXT'] ?></a>
                <? if (is_array($arColumns) && count($arColumns) > 0): ?>
                    <span class="main-menu__parent-more">
                            <svg class="icon icon-chevron icon_gray">
                              <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                            </svg>
                        </span>
                    <? foreach ($arColumns

                                as $key => $arRow): ?>
                        <ul class="main-menu__sub">
                            <? foreach ($arRow

                                        as $itemIdLevel_2 => $arLevel_3): ?>

                                <?
                                $class2 = 'class="';
                                if (is_array($arLevel_3) && count($arLevel_3) > 0) {

                                    $class2 .= 'main-menu__parent';
                                }
                                if ($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]) {
                                    $class2 .= 'active';
                                }
                                $class2 .= '"';
                                ?>
                                <li <?= $class2 ?>>
                                    <a <? if ($arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]): ?>class="active"<? endif ?>
                                       href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"] ?>">
                                        <?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"] ?>
                                    </a>
                                    <? if (is_array($arLevel_3) && count($arLevel_3) > 0): ?>
                                        <span class="main-menu__parent-more">
                                            <svg class="icon icon-chevron icon_gray">
                                              <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                                            </svg>
                                        </span>
                                        <ul class="main-menu__sub">
                                            <? foreach ($arLevel_3 as $itemIdLevel_3): ?>    <!-- third level-->
                                                <li>
                                                    <a <? if ($arResult["ALL_ITEMS"][$itemIdLevel_3]["SELECTED"]): ?>class="active"<? endif ?>
                                                       href="<?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"] ?>">
                                                        <?= $arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"] ?>
                                                    </a>
                                                </li>
                                            <? endforeach; ?>
                                        </ul>
                                    <? endif ?>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    <? endforeach; ?>
                <? endif; ?>
                </li>
            <? endforeach; ?>
        </ul>
        <ul class="lang-list">
            <li <? if (LANGUAGE_ID == 'ru'): ?>class="active"<? endif; ?>>
                <a href="<?= $APPLICATION->GetCurPage(false); ?>?lang_ui=ru">Ru</a>
            </li>
            <!--<li <?/* if (LANGUAGE_ID == 'en'): */?>class="active"<?/* endif; */?>>
                <a href="<?/*= $APPLICATION->GetCurPage(false); */?>?lang_ui=en">En</a>
            </li>-->
        </ul>
    </div>
</nav>
