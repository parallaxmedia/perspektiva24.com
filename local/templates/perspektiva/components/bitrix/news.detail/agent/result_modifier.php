 <?php


$arFilter = array(
    "IBLOCK_ID" => 2,
    "PROPERTY_AGENT_ID" => $arResult['ID']
);
$estate = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "NAME", "DATE_ACTIVE_FROM"]);

$arResult['OBJECT_COUNT'] = $estate->SelectedRowsCount();

$arFilterReviews = [
    "IBLOCK_ID" => 6,
    "PROPERTY_AGENT_ID" => $arResult['ID']
];

$reviews = CIBlockElement::GetList([], $arFilterReviews, false, false, ["ID", "NAME", "DATE_CREATE", "PROPERTY_TEXT"]);
$arReviews = [];
while ($arReview = $reviews->Fetch()) {
    $arReviews[] = $arReview;
}
$arResult['REVIEWS'] = $arReviews;

