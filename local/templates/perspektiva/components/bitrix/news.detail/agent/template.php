<?php
    if ($arResult['PROPERTIES']['STATUS']['VALUE_XML_ID'] == 3) {
        Bitrix\Iblock\Component\Tools::process404(null, true, true, true);
    }
?>

<div class="agent">
    <div class="agent__card">
        <p class="agent__card-tag">Агент с повышеной квалификацией</p>
        <div class="agent__card-inner">
            <div class="agent__card-image">
                <img src="<?= $arResult['PROPERTIES']['AVATAR']['VALUE'] ?>" width="220" height="220"
                     alt="<?= $arResult['NAME'] ?>">
            </div>
            <!-- <div class="agent__card-gallery">
                <a href="<?= SITE_TEMPLATE_PATH ?>/images/agents/document-1.jpg" data-fancybox="docs">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/agents/document-1.jpg" width="126" height="180" alt="">
                </a>
                <a href="<?= SITE_TEMPLATE_PATH ?>/images/agents/document-2.jpg" data-fancybox="docs">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/agents/document-2.jpg" width="129" height="180" alt="">
                </a>
            </div> -->
            <div class="agent__card-info">
                <p class="agent__card-name"><?= $arResult['NAME'] ?></p>
                <p class="agents__card-label">Объектов недвижимости у агента – <?= $arResult['OBJECT_COUNT'] ?></p>
                <div class="agents-card__achieve agents-card__achieve_yellow">
                    <div class="agents-card__achieve-image">
                        <div class="stars">
                            <? for ($i = 0; $i < ceil($arResult['PROPERTIES']['RATING']['VALUE']); $i++) : ?>
                                <span class="stars__item">
                                <svg class="icon icon-star icon_yellow">
                                  <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#star"></use>
                                </svg>
                            </span>
                            <? endfor; ?>
                        </div>
                    </div>
                    <p class="agents-card__achieve-text"><?= $arResult['PROPERTIES']['REWARDS']['VALUE'] ?></p>
                </div>
                <div class="agent__card-phone js-toggle-phone">
                    <a class="hidden"
                       href="tel:<?= $arResult['PROPERTIES']['PHONE']['VALUE'] ?>"><?= phone_format($arResult['PROPERTIES']['PHONE']['VALUE']) ?></a>
                    <button class="btn btn-primary" type="button">Показать номер</button>
                </div>
                <div class="agent__card-postfix">
                    <span>Средний срок продажи объектов</span><span>1 месяц</span></div>
            </div>
        </div>
    </div>
    <form class="agent__rating">
        <p class="agent__rating-title">Оцените агента</p>
        <div class="agent__rating-row">
            <p>Юридическая компетентность</p>
            <div class="agent__rating-item js-rating" data-rate-value="0"></div>
        </div>
        <div class="agent__rating-row">
            <p>Пунктуальность</p>
            <div class="agent__rating-item js-rating" data-rate-value="0"></div>
        </div>
        <div class="agent__rating-row">
            <p>Клиентоориентированность</p>
            <div class="agent__rating-item js-rating" data-rate-value="0"></div>
        </div>
        <div class="agent__rating-row">
            <p>Общее впечатление</p>
            <div class="agent__rating-item js-rating" data-rate-value="0"></div>
        </div>
        <div class="agent__rating-row">
            <label class="visually-hidden" for="login-name">Имя</label>
            <input id="login-name" type="text" name="name" placeholder="Имя" required>
        </div>
        <textarea required class="agent__rating-textarea"
                  placeholder="Напишите, что вам понравилось или не понравилось в работе агента"></textarea>
        <input class="agentId" type="hidden" name="agent" value="<?= $arResult['ID'] ?>">
        <div class="agent__rating-buttons">
            <button class="btn btn-primary" type="submit">Оставить отзыв</button>
            <button class="btn btn-outline" type="button">Отменить</button>
        </div>
    </form>
</div>
<div class="agent-tabs">
    <div class="agent-tabs__nav">
        <ul class="nav nav-tabs" id="agent-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="objects-tab" data-toggle="tab" href="#objects" role="tab"
                   aria-controls="objects" aria-selected="true">
                    Объекты агента</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab"
                   aria-controls="reviews" aria-selected="false">
                    Отзывы
                </a>
            </li>
        </ul>
    </div>
    <div class="tab-content" id="agent-tabs-content">
        <div class="tab-pane show active" id="objects" role="tabpanel" aria-labelledby="objects-tab">
            <?
            global $arrFilter;
            $arrFilter = [
                "=PROPERTY_AGENT_ID" => (int)$arResult['ID']
            ];

            global $APPLICATION;
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "listing_in_agent",
                [
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "AJAX_MODE" => "N",
                    "IBLOCK_TYPE" => "",
                    "IBLOCK_ID" => "2",
                    "NEWS_COUNT" => (isset($_REQUEST['PAGEN_ON'])) ? $_REQUEST['PAGEN_ON'] : '9',
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_ORDER1" => "ASC",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER2" => "ASC",
                    "FILTER_NAME" => "arrFilter",
                    "FIELD_CODE" => ["ID"],
                    "PROPERTY_CODE" => ["DESCRIPTION"],
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "/estate/#SECTION_CODE#/#ELEMENT_ID#/",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => "",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "N",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "SEF_MODE" => "Y",
                    "SEF_FOLDER" => "/estate/",
                    "SEF_URL_TEMPLATES" => [
                        "detail" => "#SECTION_CODE#/#ELEMENT_ID#/",
                        "section" => "#SECTION_CODE#/",
                    ],
                ]
            );
            ?>
        </div>
        <div class="tab-pane" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
            <div class="reviews-list">
                <? foreach ($arResult['REVIEWS'] as $arReview) : ?>
                    <div class="reviews-list__item">
                        <div class="reviews-list__body">
                            <p class="reviews-list__title"><?=$arReview['NAME']?> <span>#<?=$arReview['ID']?></span></p>
                            <p class="reviews-list__date"><?=FormatDateFromDB($arReview['DATE_CREATE'], 'SHORT');?></p>
                            <div class="reviews-list__text">
                                <p><?=$arReview['PROPERTY_TEXT_VALUE']['TEXT']?></p>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>

        </div>
    </div>
</div>
<div class="agent-forms">
    <div class="agent-forms__review">
        <p class="agent-forms__title">Возникли вопросы?</p>
        <form class="review-form" action="/local/templates/perspektiva/ajax/formSendProcessing.php">
            <input type="hidden" name="type" value="Агент">
            <div class="review-form__main">
                <p class="review-form__title">Задайте их агенту</p>
                <label class="visually-hidden" for="review-name">Имя</label>
                <input id="review-name" type="text" name="name" placeholder="Имя" required>
                <label class="visually-hidden" for="review-phone">Телефон</label>
                <input id="review-phone" type="tel" name="phone" placeholder="Телефон" required>
                <label class="visually-hidden" for="review-email">E-mail</label>
                <input id="review-email" type="email" name="email" placeholder="E-mail" required>
            </div>
            <div class="review-form__side">
                <label class="visually-hidden" for="review-text">Введите вопрос и опишите ситуацию</label>
                <textarea class="review-text" name="review" placeholder="Введите вопрос и опишите ситуацию"
                          required></textarea>
                <div class="review-form__submit">
                    <button class="btn btn-danger btn-sm" type="submit">Отправить</button>
                </div>
            </div>
            <div class="review-form__help"><img src="<?= SITE_TEMPLATE_PATH ?>/images/chat.svg" width="34" height="23"
                                                alt="">
                <p>Чем подробнее вы опишите ваши пожелания и ситуацию, тем больше выгодных решений предложит
                    агент</p>
            </div>
        </form>
    </div>
</div>