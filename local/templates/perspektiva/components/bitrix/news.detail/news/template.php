<div class="news-detail">
    <h1><?= $arResult['NAME'] ?></h1>
    <div class="news-detail__info">
        <p>
            <svg class="icon icon-calendar icon_red">
                <use xlink:href="/images/sprite.svg#calendar"></use>
            </svg>
            <span><?= $arResult['ACTIVE_FROM'] ?></span>
        </p>
        <p>
            <svg class="icon icon-news icon_blue">
                <use xlink:href="/images/sprite.svg#news"></use>
            </svg>
            <span>Новость</span>
        </p>
    </div>
    <br>
    <div class="news-detail__content">
        <img alt="" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
        <br> <br>
        <?= $arResult['DETAIL_TEXT'] ?>
    </div>
    <div class="toolbar-inner">
        <!--<div class="share share_small js-share" href="#">
            <a class="share__item share__item_hand-up" href="#" aria-label="Хорошо"></a>
            <a class="share__item share__item_hand-middle" href="#" aria-label="Средне"></a>
            <a class="share__item share__item_hand-down" href="#" aria-label="Плохо"></a>
            <button class="share__button" type="button">
                <svg class="icon icon-hand icon_gray share__icon-open">
                    <use xlink:href="images/sprite.svg#hand"></use>
                </svg>
                <svg class="icon icon-close icon_gray share__icon-close">
                    <use xlink:href="images/sprite.svg#close"></use>
                </svg>
                <span>Оценить</span>
            </button>
        </div>-->
        <div class="share js-share" href="#">
            <div class="ya-share2" data-services="vkontakte,facebook,viber,whatsapp,skype,telegram"></div>
        </div>
    </div>
</div>