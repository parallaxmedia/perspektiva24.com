<?php

$rsAgent = CIBlockElement::GetByID($arResult['PROPERTIES']['AGENT_ID']['VALUE']);
$arAgent = $rsAgent->GetNextElement();
if ($arAgent) {
    $arAgentProperties = $arAgent->GetProperties();
    $arAgent = $arAgent->GetFields();

    $arFilter = array(
        "IBLOCK_ID" => 2,
        "PROPERTY_AGENT_ID" => $arResult['PROPERTIES']['AGENT_ID']['VALUE']
    );
    $estate = CIBlockElement::GetList([], $arFilter, ["ID", "NAME", "DATE_ACTIVE_FROM"]);

    $arResult['AGENT'] = [
        'LINK' => '/agents/' . $arAgent['ID'] . '/',
        'NAME' => $arAgent['NAME'],
        'PHONE' => $arAgentProperties['PHONE']['VALUE'],
        'AVATAR' => $arAgentProperties['AVATAR']['VALUE'],
        'COUNT_ESATE' => $estate->SelectedRowsCount()
    ];
}

$APPLICATION->SetTitle($arResult['PROPERTIES']['ADDRESS']['VALUE']);