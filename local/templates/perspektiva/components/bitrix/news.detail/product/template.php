<h1><?= $arResult['PROPERTIES']['ADDRESS']['VALUE'] ?></h1>
<div class="toolbar">
    <a class="toolbar__item" data-fancybox href="#send-modal">
        <svg class="icon icon-email icon_gray">
            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#email"></use>
        </svg>
        <span>Отправить на почту</span>
    </a>
    <a id="printPage" class="toolbar__item" href="javascript:(print());">
        <svg class="icon icon-print icon_gray">
            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#print"></use>
        </svg>
        <span>Печать</span>
    </a>
    <!--<a class="toolbar__item" href="#">
        <svg class="icon icon-heart icon_gray">
            <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#heart"></use>
        </svg>
        <span>В избранное</span>
    </a>-->
    <a class="toolbar__item jsToggleCompare" href="<?= SITE_TEMPLATE_PATH ?>/ajax/addToCompare.php"
       data-prod-id="<?= $arResult['ID'] ?>">
        <svg class="icon icon-compare icon_gray">
            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#compare"></use>
        </svg>
        <span class="label-compare">Сравнить</span>
    </a>

    <div class="share js-share" href="#">
        <div class="ya-share2" data-services="vkontakte,facebook,viber,whatsapp,skype,telegram"></div>
    </div>
</div>
<div class="product-wrapper">
    <div class="product-wrapper__intro">
        <? if ($arResult['PROPERTIES']['STATUS']['VALUE'] == 'Завершен') : ?>
            <p class="product__price" style="color: red;">Снят с продажи</p>
        <? else : ?>
            <p class="product__price"><?= CurrencyFormat($arResult['PROPERTIES']['PRICE']['VALUE'], "RUB"); ?></p>
        <? endif; ?>
        <a class="product__hypothec" href="/services/mortgage/">Ипотека</a>
        <p class="product__price-label"><?= $arResult['PROPERTIES']['PRICE_PER_METER']['VALUE'] ?> &#x20bd; за
            м<sup>2</sup></p>
        <!--<a class="product__set-price" href="#">Предложить свою цену</a>-->
        <p class="product__address">
            <svg class="icon icon-direction icon_gray">
                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
            </svg>
            <span><?= $arResult['PROPERTIES']['ADDRESS']['VALUE'] ?></span>
        </p>
    </div>
    <div class="product-wrapper__gallery">
        <?
        if (is_array($arResult['PROPERTIES']['IMAGES']['VALUE'])) {
            $countPhoto = count($arResult['PROPERTIES']['IMAGES']['VALUE']);
        } else {
            $countPhoto = 0;
        }
        ?>
        <div class="product__gallery js-gallery-detail">
            <div class="product__gallery-main">
                <span class="product__total-badge"><?= $countPhoto ?></span>
                <div class="swiper-container js-main-gallery-detail">
                    <div class="swiper-wrapper">
                        <? if ($countPhoto > 0) : ?>
                            <? foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $image) : ?>
                                <div class="swiper-slide">
                                    <a class="product__gallery-preview"
                                       href="<?= $image ?>"
                                       data-fancybox="gallery">
                                        <img src="<?= $image ?>" width="882" height="498" alt="">
                                    </a>
                                </div>
                            <? endforeach; ?>
                        <? endif; ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
            <div class="product__gallery-small">
                <div class="swiper-container js-thumbs-gallery-detail">
                    <div class="swiper-wrapper">
                        <? if ($countPhoto > 0) : ?>
                            <? foreach ($arResult['PROPERTIES']['IMAGES']['VALUE'] as $image) : ?>
                                <div class="swiper-slide">
                                    <div class="product__thumb">
                                        <img src="<?= $image ?>" width="215" height="115" alt="">
                                    </div>
                                </div>
                            <? endforeach; ?>
                        <? endif; ?>
                    </div>
                    <div class="swiper-button-prev">
                        <svg class="icon icon-chevron icon_black">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                        </svg>
                    </div>
                    <div class="swiper-button-next">
                        <svg class="icon icon-chevron icon_black">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-wrapper__short">
        <ul class="product__short-info">
            <li>
                <span><strong>Лот</strong></span>
                <p><?= $arResult['PROPERTIES']['ID']['VALUE'] ?></p>
            </li>
            <li>
                <span class="mobile-hidden">Публикация</span>
                <p><?= FormatDateFromDB($arResult["DATE_CREATE"], 'SHORT'); ?></p>
            </li>
            <li>
                <svg class="icon icon-eye icon_blue">
                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#eye"></use>
                </svg>
                <p class="mobile-hidden">Просмотров</p>
                <p><?= ($arResult['SHOW_COUNTER'] > 0) ? $arResult['SHOW_COUNTER'] : '0' ?></p>
            </li>
        </ul>
    </div>
    <div class="product-wrapper__info">
        <div class="product__info">
            <table>
                <? if (!empty($arResult['PROPERTIES']['ROOMS']['VALUE'])) : ?>
                    <tr>
                        <td>Комнаты</td>
                        <td><?= $arResult['PROPERTIES']['ROOMS']['VALUE'] ?></td>
                    </tr>
                <? endif; ?>
                <? if (!empty($arResult['PROPERTIES']['WALLSTYPE']['VALUE'][0])) : ?>
                    <tr>
                        <td>Материал стен</td>
                        <td><?= $arResult['PROPERTIES']['WALLSTYPE']['VALUE'][0] ?></td>
                    </tr>
                <? endif; ?>
                <? if (!empty($arResult['PROPERTIES']['FLOOR']['VALUE'][0])) : ?>
                    <tr>
                        <td>Этаж</td>
                        <td><?= $arResult['PROPERTIES']['FLOOR']['VALUE'][0] ?>
                            /<?= $arResult['PROPERTIES']['FLOORS']['VALUE'][0] ?></td>
                    </tr>
                <? endif; ?>
                <? if (!empty($arResult['PROPERTIES']['FLOORS']['VALUE'][0])) : ?>
                    <tr>
                        <td>Этажность</td>
                        <td><?= $arResult['PROPERTIES']['FLOORS']['VALUE'][0] ?></td>
                    </tr>
                <? endif; ?>
                <? if (!empty($arResult['PROPERTIES']['KITCHENSPACE']['VALUE'][0])) : ?>
                    <tr>
                        <td>Площадь кухни</td>
                        <td><?= $arResult['PROPERTIES']['KITCHENSPACE']['VALUE'][0] ?>м<sup>2</sup></td>
                    </tr>
                <? endif; ?>
                <? if (!empty($arResult['PROPERTIES']['AREA']['VALUE'])) : ?>
                    <tr>
                        <td>Площадь</td>
                        <td><?= $arResult['PROPERTIES']['AREA']['VALUE'] ?> м<sup>2</sup></td>
                    </tr>
                <? endif; ?>
                <? if (!empty($arResult['PROPERTIES']['LIVINGSPACE']['VALUE'])) : ?>
                    <tr>
                        <td>Жилая площадь</td>
                        <td><?= $arResult['PROPERTIES']['LIVINGSPACE']['VALUE'] ?> м<sup>2</sup></td>
                    </tr>
                <? endif; ?>

                <? if (!empty($arResult['PROPERTIES']['CELINGS']['VALUE'])) : ?>
                    <tr>
                        <td>Потолки</td>
                        <td><?= $arResult['PROPERTIES']['CELINGS']['VALUE'] ?> м</td>
                    </tr>
                <? endif; ?>
            </table>
            <div class="product__info-buttons">
                <div class="product__info-phone js-toggle-phone">
                    <a class="hidden"
                       href="tel:<?= $arResult['AGENT']['PHONE'] ?>"><?= phone_format($arResult['AGENT']['PHONE']); ?></a>
                    <button class="btn btn-primary" type="button">Показать телефон</button>
                </div>
                <!-- <a class="btn btn-secondary btn-icon product__info-3d" href="javascript:void(0);">
                    <svg class="icon icon-3d icon_gray icon_left">
                        <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#3d"></use>
                    </svg>
                    <span>тур по квартире</span></a>-->
            </div>
        </div>
    </div>
</div>
<div class="product__desc">
    <div class="product__desc-col">
        <div class="product__desc-content">
            <h3>Описание продавца</h3>
            <?= $arResult['DETAIL_TEXT'] ?>
        </div>
        <div class="product__desc-grid">
            <div class="product__desc-item">
                <p class="product__desc-header">
                    <svg class="icon icon-union icon_gray">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#union"></use>
                    </svg>
                    <span>Инфраструктура</span>
                </p>
                <div class="product__desc-body">
                    <ul>
                        <? foreach ($arResult['PROPERTIES']['INFRASTRUCTURE']['VALUE'] as $infrastructure): ?>
                            <li><?= $infrastructure ?></li>
                        <? endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="product__desc-item">
                <p class="product__desc-header">
                    <svg class="icon icon-receipt icon_gray">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#receipt"></use>
                    </svg>
                    <span>Легко купить</span>
                </p>
                <div class="product__desc-body">
                    <?if ($arResult['PROPERTIES']['NUMBEROWNERS']['VALUE']!=false){
                        ?>
                        <ul>
                            <li>Количество собственников : <?=$arResult['PROPERTIES']['NUMBEROWNERS']['VALUE'];?></li>
                        </ul>
                        <?
                    }?>
                </div>
            </div>
        </div>
        <div class="product__desc-map" id="productMap" style="height: 224px;">

        </div>
        <script type="text/javascript">
            // Функция ymaps.ready() будет вызвана, когда
            // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
            ymaps.ready(init);

            function init() {
                // Создание карты.
                var myMap = new ymaps.Map("productMap", {
                    center: [<?=$arResult['PROPERTIES']['LAT']['VALUE']?>, <?=$arResult['PROPERTIES']['LNG']['VALUE']?>],
                    zoom: 14
                });

                var myGeoObject = new ymaps.GeoObject({
                    geometry: {
                        type: "Point", // тип геометрии - точка
                        coordinates: [<?=$arResult['PROPERTIES']['LAT']['VALUE']?>, <?=$arResult['PROPERTIES']['LNG']['VALUE']?>] // координаты точки
                    }
                });

                myMap.geoObjects.add(myGeoObject);
            }
        </script>
    </div>
    <form class="product__desc-form desc-form" action="<?= SITE_TEMPLATE_PATH ?>/ajax/formSendProcessing.php">
        <input type="hidden" name="t





         ype" value="Недвижимость">
        <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
        <div class="desc-form__header">
            <img class="desc-form__image" src="<?= $arResult['AGENT']['AVATAR'] ?>" width="88" height="88"
                 alt="<?= $arResult['AGENT']['NAME'] ?>">
            <div class="desc-form__content">
                <p class="desc-form__title">Вам покажет объект</p>
                <a class="desc-form__agent"
                   href="<?= $arResult['AGENT']['LINK'] ?>"><?= $arResult['AGENT']['NAME'] ?></a>
                <p class="desc-form__label">Объектов у агента <?= $arResult['AGENT']['COUNT_ESATE'] ?></p>
            </div>
        </div>
        <div class="desc-form__phone js-toggle-phone">
            <a class="hidden"
               href="tel:<?= $arResult['AGENT']['PHONE'] ?>"><?= phone_format($arResult['AGENT']['PHONE']) ?></a>
            <button class="btn btn-secondary" type="button">Показать телефон</button>
        </div>
        <div class="desc-form__body">
            <p class="desc-form__subtitle">Написать менеджеру</p>
            <div class="desc-form__fields">
                <label class="visually-hidden" for="agent-form-name">Имя</label>
                <input id="agent-form-name" type="text" name="name" placeholder="Имя" required>
                <label class="visually-hidden" for="agent-form-phone">Телефон</label>
                <input id="agent-form-phone" type="tel" name="phone" placeholder="Телефон" required>
                <label class="visually-hidden" for="agent-form-message">Сообщение</label>
                <input id="agent-form-message" type="text" name="message" placeholder="Ваш вопрос"
                       required>
                <input type="hidden" name="back_url" value="<?= $APPLICATION->GetCurPage(true); ?>">
            </div>
            <button class="btn btn-primary" type="submit">Отправить</button>
            <p class="desc-form__terms">Нажимая на кнопку, вы даете согласие на обработку своих персональных данных</p>
        </div>
    </form>
</div>
<div class="product__partners">
    <div class="product__partners-col">
        <h3>Банки партнеры</h3>
        <p>Вы можете получить ипотеку в наших банках партнерах по сниженой % ставке. Рассчитайте ежемесячный
            платеж самостоятельно на <a href="#">ипотечном калькуляторе</a> или закажите консультацию
            кредитного специалиста.</p>
        <div class="product__partners-grid">
            <div class="product__partners-grid-inner"><img
                    src="<?= SITE_TEMPLATE_PATH ?>/images/partners/logo-gazprombank.png"
                    width="69" height="17" alt="Газпромбанк"><img
                    src="<?= SITE_TEMPLATE_PATH ?>/images/partners/logo-sberbank.png" width="61" height="16"
                    alt="Сбербанк"><img
                    src="<?= SITE_TEMPLATE_PATH ?>/images/partners/logo-rosbank.png" width="74" height="15"
                    alt="Росбанк"><img
                    src="<?= SITE_TEMPLATE_PATH ?>/images/partners/logo-promsvyazbank.png" width="53" height="16"
                    alt="ПСБ"><img
                    src="<?= SITE_TEMPLATE_PATH ?>/images/partners/logo-alfa-bank.png" width="63" height="17"
                    alt="Альфа Банк"><img src="<?= SITE_TEMPLATE_PATH ?>/images/partners/logo-unicredit-bank.png"
                                          width="60"
                                          height="15" alt="UniCredit Bank"><img
                    src="<?= SITE_TEMPLATE_PATH ?>/images/partners/logo-bank-otkrytie.png" width="66" height="18"
                    alt="Банк Открытие"><img src="<?= SITE_TEMPLATE_PATH ?>/images/partners/logo-ubrir.png"
                                             width="69" height="12"
                                             alt="Уральский Банк"><img
                    src="<?= SITE_TEMPLATE_PATH ?>/images/partners/logo-ak-bars-bank.png" width="35" height="20"
                    alt="АК БАРС Банк"></div>
        </div>
        <div class="product__partners-all">
            <h4>Все партнеры</h4>
            <ul>
                <li>Запсибкомбанк</li>
                <li>Убрир</li>
                <li>Юникредит</li>
                <li>Сбербанк</li>
                <li>СМП БАНК</li>
                <li>Россельхоз Банк</li>
                <li>Росбанк ДОМ</li>
                <li>Абсолют</li>
                <li>Райффайзен БАНК</li>
                <li>ДомРФ</li>
                <li>Ипотека 24</li>
                <li>Втб</li>
                <li>Совкомбанк</li>
                <li>Газпром</li>
                <li>Банк Россия</li>
                <li>Уралсиб</li>
                <li>Связь Банк</li>
                <li>ВБРР</li>
                <li>Банк ОТКРЫТИЕ</li>
                <li>Ак Барс Банк</li>
                <li>Альфа Банк</li>
                <li>Промсвязь Банк</li>
            </ul>
        </div>
    </div>
    <form class="product__partners-form consult-form"
          action="<?= SITE_TEMPLATE_PATH ?>/ajax/formSendProcessing.php">
        <input type="hidden" name="type" value="Недвижимость">
        <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
        <p class="consult-form__title">Закажите консультацию по ипотеке</p>
        <div class="consult-form__fields">
            <label class="visually-hidden" for="consult-form-name">Имя</label>
            <input id="consult-form-name" type="text" name="name" placeholder="Имя" required>
            <label class="visually-hidden" for="consult-form-phone">Телефон</label>
            <input id="consult-form-phone" type="tel" name="phone" placeholder="Телефон" required>
        </div>
        <button class="btn btn-primary" type="submit">Отправить</button>
        <p class="consult-form__terms">Нажимая на кнопку, вы даете согласие на обработку своих персональных
            данных</p>
    </form>
</div>

<div class="hidden">
    <div id="send-modal">
        <form class="product__partners-form send-form"
              action="<?= SITE_TEMPLATE_PATH ?>/ajax/sendObject.php">
            <div class="consult-form__fields">
                <label class="visually-hidden" for="send-form-name">Введите Ваш email</label>
                <input id="send-form-name" type="email" name="email" placeholder="email" required>
            </div>
            <input type="hidden" name="PRODUCT_ID" value="<?=$arResult['ID']?>">
            <button class="btn btn-primary" type="submit">Отправить</button>
            <p class="consult-form__terms">Нажимая на кнопку, вы даете согласие на обработку своих персональных
                данных</p>
        </form>
    </div>
</div>