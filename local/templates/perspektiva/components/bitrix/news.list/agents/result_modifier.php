<?php

foreach ($arResult['ITEMS'] as &$arAgent) {
    $arFilter = array(
        "IBLOCK_ID" => 2,
        "PROPERTY_AGENT_ID" => $arAgent['ID']
    );
    $estate = CIBlockElement::GetList([], $arFilter, false, false, ["ID", "NAME", "DATE_ACTIVE_FROM"]);

    $arAgent['OBJECT_COUNT'] = $estate->SelectedRowsCount();

    $arFilterReviews = [
        "IBLOCK_ID" => 6,
        "PROPERTY_AGENT_ID" => $arAgent['ID']
    ];

    $reviews = CIBlockElement::GetList([], $arFilterReviews, false, false, ["ID", 'IBLOCK_ID']);
    $arAgent['REVIEW_COUNT'] = $reviews->SelectedRowsCount();
}

function cmp($a, $b)
{
    return strcmp($b["OBJECT_COUNT"], $a["OBJECT_COUNT"]);
}

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
if ($request['sort'] == 'objects') {

    usort($arResult['ITEMS'], "cmp");

}
