<?php
if (!function_exists('BITGetDeclNum')) {

    /**
     * Возврат окончания слова при склонении
     *
     * Функция возвращает окончание слова, в зависимости от примененного к ней числа
     * Например: 5 товаров, 1 товар, 3 товара
     *
     * @param int $value - число, к которому необходимо применить склонение
     * @param array $status - массив возможных окончаний
     * @return mixed
     */
    function BITGetDeclNum($value = 1, $status = array('', 'а', 'ов'))
    {
        $array = array(2, 0, 1, 1, 1, 2);
        return $status[($value % 100 > 4 && $value % 100 < 20) ? 2 : $array[($value % 10 < 5) ? $value % 10 : 5]];
    }
}
?>

    <div class="agents-grid">
        <? foreach ($arResult['ITEMS'] as $arAgent) : ?>
            <div class="agents-grid__item">
                <div class="agents-grid__image">
                    <div class="agents-grid__image-inner">
                        <img src="<?= $arAgent['PROPERTIES']['AVATAR']['VALUE'] ?>" width="112" height="112"
                             alt="<?= $arAgent['NAME'] ?>">
                    </div>
                    <button class="agents-grid__tooltip" type="button" data-toggle="tooltip" data-placement="top"
                            title="Менеджер прошел курсы повышения квалификации установленного образца">
                        <svg class="icon icon-cert icon_red">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#cert"></use>
                        </svg>
                    </button>
                </div>
                <div class="agents-grid__content">
                    <a class="agents-grid__title" href="<?= $arAgent['DETAIL_PAGE_URL'] ?>"><?= $arAgent['NAME'] ?></a>
                    <p class="agents-grid__desc">у агента <?= $arAgent['OBJECT_COUNT'] ?>
                        объект<?= BITGetDeclNum($arAgent['OBJECT_COUNT'], ['', 'а', 'ов']) ?><? if ($arAgent['REVIEW_COUNT'] > 0) : ?>, <?= $arAgent['REVIEW_COUNT'] ?> отзыв<?= BITGetDeclNum($arAgent['REVIEW_COUNT'], ['', 'а', 'ов']) ?><? endif; ?></p>
                    <div class="agents-grid__achieve agents-grid__achieve_yellow">
                        <div class="agents-grid__achieve-image">
                            <div class="stars">
                                <? for ($i = 0; $i < ceil($arAgent['PROPERTIES']['RATING']['VALUE']); $i++) : ?>
                                    <span class="stars__item">
                                <svg class="icon icon-star icon_yellow">
                                  <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#star"></use>
                                </svg>
                            </span>
                                <? endfor; ?>
                            </div>
                        </div>
                        <p class="agents-grid__achieve-text"><?= $arAgent['PROPERTIES']['REWARDS']['VALUE'] ?></p>
                    </div>
                    <div class="agents-grid__phone js-toggle-phone">
                        <a class="hidden"
                           href="tel:<?= $arAgent['PROPERTIES']['PHONE']['VALUE'] ?>"><?= phone_format($arAgent['PROPERTIES']['PHONE']['VALUE']) ?></a>
                        <button class="btn btn-secondary" type="button">Показать номер</button>
                    </div>
                </div>



            </div>
        <? endforeach; ?>
    </div>
<?php
echo $arResult['NAV_STRING'];
?>