<?php
global $APPLICATION;
global $arCity;
$realMetaTitle = str_replace('#city#', $arCity['NAME'], $arResult['IPROPERTY_VALUES']['SECTION_META_TITLE']);
$APPLICATION->SetPageProperty('title', $realMetaTitle);
$realMetaTitle = str_replace('#city#', $arCity['NAME'], $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']);
$APPLICATION->SetTitle($realMetaTitle);