<?php
$obParser = new CTextParser;
foreach ($arResult['ITEMS'] as &$arItem) {
    $arItem["DETAIL_TEXT"] = $obParser->html_cut($arItem["DETAIL_TEXT"], 250);
}


global $arrFilter;

$arMapFilter = $arrFilter;

$arMapFilter['ACTIVE'] = 'Y';

\Bitrix\Main\Loader::includeModule('iblock');

$rsItems = CIBlockElement::GetList([], $arMapFilter, false, false,
    [
        'ID',
        'DETAIL_PAGE_URL',
        'PROPERTY_LAT',
        'PROPERTY_LNG',
        'PROPERTY_ADDRESS',
        'PROPERTY_PRICE',
        'PROPERTY_ROOMS',
    ]
);

$arMapItems = [];
$arMapItemsId = [];
while ($arItem = $rsItems->Fetch()) {
    $arMapItems[$arItem['ID']] = $arItem;
    $arMapItemsId[] = $arItem['ID'];
}

$rsItems = CIBlockElement::GetList([], ['ID' => $arMapItemsId], false, false, ['ID', 'DETAIL_PAGE_URL']);

while ($arItem = $rsItems->GetNextElement()) {
    $arItemFields = $arItem->GetFields();
    $arMapItems[$arItemFields['ID']]['DETAIL_PAGE_URL'] = $arItemFields['DETAIL_PAGE_URL'];
}

$arResult['MAP_ITEMS'] = array_slice($arMapItems, 0, 550);