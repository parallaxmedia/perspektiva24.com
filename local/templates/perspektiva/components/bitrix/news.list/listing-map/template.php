<?php
global $arCurCity;
?>
    <div class="product-map" id="product-Map" style="width: 100%; height: 594px;"></div>

    <script type="text/javascript">
        // Функция ymaps.ready() будет вызвана, когда
        // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
        ymaps.ready(init);

        function init() {
            // Создание карты.
            var myMap = new ymaps.Map("product-Map", {
                center: [<?=$arCurCity['PROPS']['LAT']['VALUE']?>, <?=$arCurCity['PROPS']['LNG']['VALUE']?>],
                zoom: 10
            });
            <? foreach ($arResult['MAP_ITEMS'] as $arItem) : ?>
            <?
            $baloonContent = '';
            $baloonContent .= '<h3>' . $arItem['PROPERTY_ADDRESS_VALUE'] . '</h3>';
            $baloonContent .= '<p>Цена: ' . CurrencyFormat($arItem['PROPERTY_PRICE_VALUE'], "RUB") . '</p>';
            $baloonContent .= '<p>Комнат: ' . $arItem['PROPERTY_ROOMS_VALUE'] . '</p>';
            $baloonContent .= '<a href=\"' . $arItem['DETAIL_PAGE_URL'] . '\">Подробнее</a>';
            ?>
            let myGeoObject<?=$arItem['ID']?> = new ymaps.GeoObject({
                    geometry: {
                        type: "Point", // тип геометрии - точка
                        coordinates: [<?=$arItem['PROPERTY_LAT_VALUE']?>, <?=$arItem['PROPERTY_LNG_VALUE']?>] // координаты точки
                    },
                    properties: {
                        balloonContentBody: "<?=$baloonContent?>"
                    }
                })
            ;
            myMap.geoObjects.add(myGeoObject<?=$arItem['ID']?>);
            <? endforeach; ?>
        }
    </script>

    <div class="products-grid">

        <? if ($USER->IsAdmin()) {
            $arDubugAdmin = [];
            foreach ($arResult['ITEMS'] as $arItem) {
                if (!empty($arItem['PROPERTIES']['LAT']['VALUE']) || !empty($arItem['PROPERTIES']['LNG']['VALUE'])) {
                    $arDubugAdmin[] = [$arItem['PROPERTIES']['LAT']['VALUE'], $arItem['PROPERTIES']['LNG']['VALUE']];
                }

            }

            //\Bitrix\Main\Diag\Debug::dump($arDubugAdmin);
            //\Bitrix\Main\Diag\Debug::dump(count($arDubugAdmin));

        } ?>

        <? foreach ($arResult['ITEMS'] as $arItem) : ?>
            <div class="products-grid__item">
                <div class="products-grid__gallery js-gallery">
                    <div class="products-grid__gallery-main">
                        <?
                        if ($arItem['PROPERTIES']['IMAGES']['VALUE'] != false) {
                            $count = count($arItem['PROPERTIES']['IMAGES']['VALUE']);
                        } else {
                            $count = 0;
                        }
                        ?>
                        <span class="products-grid__total-badge"><?= $count; ?></span>
                        <? if ($count > 0) : ?>
                            <div class="swiper-container js-main-gallery">
                                <div class="swiper-wrapper">
                                    <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $key => $image) : ?>
                                        <div class="swiper-slide">
                                            <div class="products-grid__image">
                                                <img src="<?= $arItem['PROPERTIES']['IMAGES']['DESCRIPTION'][$key] ?>"
                                                     width="312" height="312" alt="">
                                            </div>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        <? else : ?>
                            <div class="swiper-container js-main-gallery">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="products-grid__image">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/no_photo.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                    </div><? if ($count > 0) : ?>
                        <div class="products-grid__gallery-small">

                        <div class="swiper-container js-thumbs-gallery">
                            <div class="swiper-wrapper">
                                <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $key => $image) : ?>
                                    <div class="swiper-slide">
                                        <div class="products-grid__thumb">
                                            <img src="<?= $arItem['PROPERTIES']['IMAGES']['DESCRIPTION'][$key] ?>"
                                                 width="109" height="59" alt="">
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>

                        </div><? endif; ?>
                </div>
                <div class="products-grid__body">
                    <div class="products-grid__price">
                        <p class="products-grid__price-val"><?= CurrencyFormat($arItem['PROPERTIES']['PRICE']['VALUE'], "RUB"); ?></p>
                        <a class="products-grid__price-link" href="/services/mortgage/">Ипотека</a>
                    </div>
                    <div class="products-grid__intro">
                        <a class="products-grid__title"
                           href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></a>
                        <p class="product-grid__address">
                            <svg class="icon icon-direction icon_gray">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                            </svg>
                            <span><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></span>
                        </p>
                        <dl class="product-grid__info">
                            <dt>Комнат: <?= $arItem['PROPERTIES']['ROOMS']['VALUE'] ?></dt>
                            <dt>Лот: <?= $arItem['PROPERTIES']['ID']['VALUE'] ?></dt>
                            <? if (!empty($arItem['PROPERTIES']['FLOOR']['VALUE'][0]) && !empty($arItem['PROPERTIES']['FLOORS']['VALUE'][0])) : ?>
                                <dt>Этаж <?= $arItem['PROPERTIES']['FLOOR']['VALUE'][0] ?>
                                    /<?= $arItem['PROPERTIES']['FLOORS']['VALUE'][0] ?></dt>
                            <? endif; ?>
                            <dt>Публикация <?= FormatDate("d.m.Y", MakeTimeStamp($arItem['DATE_CREATE'])) ?></dt>
                        </dl>
                    </div>
                    <div class="products-grid__desc">
                        <p><?= $arItem['DETAIL_TEXT'] ?></p>
                    </div>
                    <div class="products-grid__buttons">
                        <a class="btn btn-primary" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">Подробнее</a>
                        <!--<a class="products-grid__like" href="#">
                            <svg class="icon icon-heart icon_gray">
                                <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#heart"></use>
                            </svg>
                            <span>В избранное</span>
                        </a>-->
                        <a class="btn btn-outline btn-icon jsToggleCompare"
                           href="<?= SITE_TEMPLATE_PATH ?>/ajax/addToCompare.php" data-prod-id="<?= $arItem['ID'] ?>">
                            <svg class="icon icon-compare icon_gray icon_left">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#compare"></use>
                            </svg>
                            <span class="label-compare">Сравнить</span>
                        </a>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>

<?php
echo $arResult['NAV_STRING'];
?>