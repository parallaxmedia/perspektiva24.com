<?php

use Bitrix\Main\Diag\Debug;

if (count($arResult['ITEMS']) == 0) {
    $empty = true;
}

$items = array_chunk($arResult['ITEMS'], ceil(count($arResult['ITEMS']) / 3));
?>
<?php if (!$empty) : ?>

    <div class="products-grid">
        <? foreach ($items[0] as $arItem) : ?>
            <?
            if ($arItem['PROPERTIES']['IMAGES']['VALUE'] != false) {
                $arItem['PROPERTIES']['IMAGES']['VALUE'] = array_slice($arItem['PROPERTIES']['IMAGES']['VALUE'], 0, 6);
                $count = count($arItem['PROPERTIES']['IMAGES']['VALUE']);
            } else {
                $count = 0;
            }
            ?>
            <div class="products-grid__item">
                <div class="products-grid__gallery <?= ($count > 0) ? 'js-gallery' : '' ?>">
                    <div class="products-grid__gallery-main">
                        <span class="products-grid__total-badge"><?= $count; ?></span>
                        <? if ($count > 0) : ?>
                            <div class="swiper-container js-main-gallery">
                                <div class="swiper-wrapper">
                                    <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $key => $image) : ?>
                                        <div class="swiper-slide">
                                            <div class="products-grid__image">
                                                <img class="swiper-lazy"
                                                     src="<?= $arItem['PROPERTIES']['IMAGES']['DESCRIPTION'][$key] ?>"
                                                     width="312" height="312" alt="">
                                            </div>
                                            <div class="swiper-lazy-preloader"></div>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        <? else : ?>
                            <div class="swiper-container js-main-gallery">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="products-grid__image">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/no_photo.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                    </div>
                    <? if ($count > 0) : ?>
                        <div class="products-grid__gallery-small">
                            <div class="swiper-container js-thumbs-gallery">
                                <div class="swiper-wrapper">
                                    <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $key => $image) : ?>
                                        <div class="swiper-slide">
                                            <div class="products-grid__thumb">
                                                <img src="<?= $arItem['PROPERTIES']['IMAGES']['DESCRIPTION'][$key] ?>"
                                                     width="109" height="59" alt="">
                                            </div>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <? endif; ?>
                </div>
                <div class="products-grid__body">
                    <div class="products-grid__price">
                        <p class="products-grid__price-val"><?= CurrencyFormat($arItem['PROPERTIES']['PRICE']['VALUE'], "RUB"); ?></p>
                        <a class="products-grid__price-link" href="/services/mortgage/">Ипотека</a>
                    </div>
                    <div class="products-grid__intro">
                        <a class="products-grid__title"
                           href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></a>
                        <p class="product-grid__address">
                            <svg class="icon icon-direction icon_gray">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                            </svg>
                            <span><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></span>
                        </p>
                        <dl class="product-grid__info">
                            <dt>Комнат: <?= $arItem['PROPERTIES']['ROOMS']['VALUE'] ?></dt>
                            <dt>Лот: <?= $arItem['PROPERTIES']['ID']['VALUE'] ?></dt>
                            <? if (!empty($arItem['PROPERTIES']['FLOOR']['VALUE'][0]) && !empty($arItem['PROPERTIES']['FLOORS']['VALUE'][0])) : ?>
                                <dt>Этаж <?= $arItem['PROPERTIES']['FLOOR']['VALUE'][0] ?>
                                    /<?= $arItem['PROPERTIES']['FLOORS']['VALUE'][0] ?></dt>
                            <? endif; ?>
                            <dt>Публикация <?= FormatDate("d.m.Y", MakeTimeStamp($arItem['DATE_CREATE'])) ?></dt>
                        </dl>
                    </div>
                    <div class="products-grid__desc">
                        <p><?= $arItem['DETAIL_TEXT'] ?></p>
                    </div>
                    <div class="products-grid__buttons">
                        <a class="btn btn-primary" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">Подробнее</a>
                        <!--<a class="products-grid__like" href="#">
                            <svg class="icon icon-heart icon_gray">
                                <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#heart"></use>
                            </svg>
                            <span>В избранное</span>
                        </a>-->
                        <a class="btn btn-outline btn-icon jsToggleCompare"
                           href="<?= SITE_TEMPLATE_PATH ?>/ajax/addToCompare.php" data-prod-id="<?= $arItem['ID'] ?>">
                            <svg class="icon icon-compare icon_gray icon_left">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#compare"></use>
                            </svg>
                            <span class="label-compare">Сравнить</span>
                        </a>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
    <div class="cite__wrapper m-t-xxl">
        <div class="cite__bubble">
            <p>Если у вас небольшая семья и ограниченный бюджет для приобретения недвижимости, оптимальный
                вариант – купить 1-комнатную квартиру. Кроме того, приобретение недвижимости данного типа станет
                выгодной инвестицией – ее в дальнейшем можно продать или сдавать в аренду. Если вы планируете
                использовать «однушку» для проживания, следует позаботиться о дизайне интерьера. Комната должна
                быть максимально функциональной, с грамотным зонированием пространства. Необходимо выделить
                спальную зону, рабочее место, пространство для приема гостей. Для создания универсального
                интерьера используются мобильные перегородки, разные виды освещения, трансформируемая
                мебель.</p>
        </div>
        <div class="cite__author">
            <div class="cite__author-image">
                <img src="<?= SITE_TEMPLATE_PATH ?>/images/neutral-expert-99px.jpg" width="99" height="99" alt="Алибаева Динара">
            </div>
            <div class="cite__author-content">
                <p class="cite__author-name">Павлова Анастасия</p>
                <p class="cite__author-label">Эксперт по недвижимости</p>
            </div>
        </div>
    </div>
    <div class="products-grid">
        <? foreach ($items[1] as $arItem) : ?>
            <div class="products-grid__item">
                <div class="products-grid__gallery js-gallery">
                    <div class="products-grid__gallery-main">
                        <?
                        if ($arItem['PROPERTIES']['IMAGES']['VALUE'] != false) {
                            $arItem['PROPERTIES']['IMAGES']['VALUE'] = array_slice($arItem['PROPERTIES']['IMAGES']['VALUE'], 0, 6);
                            $count = count($arItem['PROPERTIES']['IMAGES']['VALUE']);
                        } else {
                            $count = 0;
                        }
                        ?>
                        <span class="products-grid__total-badge"><?= $count; ?></span>
                        <? if ($count > 0) : ?>
                            <div class="swiper-container js-main-gallery">
                                <div class="swiper-wrapper">
                                    <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $key => $image) : ?>
                                        <div class="swiper-slide">
                                            <div class="products-grid__image">
                                                <img class="swiper-lazy"
                                                     src="<?= $arItem['PROPERTIES']['IMAGES']['DESCRIPTION'][$key] ?>"
                                                     width="312" height="312" alt="">
                                            </div>
                                            <div class="swiper-lazy-preloader"></div>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                                <div class="swiper-pagination"></div>

                            </div>
                        <? else : ?>
                            <div class="swiper-container js-main-gallery">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="products-grid__image">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/no_photo.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                    </div><? if ($count > 0) : ?>
                        <div class="products-grid__gallery-small">

                        <div class="swiper-container js-thumbs-gallery">
                            <div class="swiper-wrapper">
                                <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $key => $image) : ?>
                                    <div class="swiper-slide">
                                        <div class="products-grid__thumb">
                                            <img src="<?= $arItem['PROPERTIES']['IMAGES']['DESCRIPTION'][$key] ?>"
                                                 width="109" height="59" alt="">
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>

                        </div><? endif; ?>
                </div>
                <div class="products-grid__body">
                    <div class="products-grid__price">
                        <p class="products-grid__price-val"><?= CurrencyFormat($arItem['PROPERTIES']['PRICE']['VALUE'], "RUB"); ?></p>
                        <a class="products-grid__price-link" href="/services/mortgage/">Ипотека</a>
                    </div>
                    <div class="products-grid__intro">
                        <a class="products-grid__title"
                           href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></a>
                        <p class="product-grid__address">
                            <svg class="icon icon-direction icon_gray">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                            </svg>
                            <span><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></span>
                        </p>
                        <dl class="product-grid__info">
                            <dt>Комнат: <?= $arItem['PROPERTIES']['ROOMS']['VALUE'] ?></dt>
                            <dt>Лот: <?= $arItem['PROPERTIES']['ID']['VALUE'] ?></dt>
                            <? if (!empty($arItem['PROPERTIES']['FLOOR']['VALUE'][0]) && !empty($arItem['PROPERTIES']['FLOORS']['VALUE'][0])) : ?>
                                <dt>Этаж <?= $arItem['PROPERTIES']['FLOOR']['VALUE'][0] ?>
                                    /<?= $arItem['PROPERTIES']['FLOORS']['VALUE'][0] ?></dt>
                            <? endif; ?>
                            <dt>Публикация <?= FormatDate("d.m.Y", MakeTimeStamp($arItem['DATE_CREATE'])) ?></dt>
                        </dl>
                    </div>
                    <div class="products-grid__desc">
                        <p><?= $arItem['DETAIL_TEXT'] ?></p>
                    </div>
                    <div class="products-grid__buttons">
                        <a class="btn btn-primary" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">Подробнее</a>
                        <!--<a class="products-grid__like" href="#">
                            <svg class="icon icon-heart icon_gray">
                                <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#heart"></use>
                            </svg>
                            <span>В избранное</span>
                        </a>-->
                        <a class="btn btn-outline btn-icon jsToggleCompare"
                           href="<?= SITE_TEMPLATE_PATH ?>/ajax/addToCompare.php" data-prod-id="<?= $arItem['ID'] ?>">
                            <svg class="icon icon-compare icon_gray icon_left">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#compare"></use>
                            </svg>
                            <span class="label-compare">Сравнить</span>
                        </a>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>

    <? $APPLICATION->IncludeComponent("bitrix:news.list", "main_slider", [
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_MODE" => "N",
            "IBLOCK_TYPE" => "",
            "IBLOCK_ID" => "8",
            "NEWS_COUNT" => "20",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "FILTER_NAME" => "",
            "FIELD_CODE" => ["ID"],
            "PROPERTY_CODE" => ["DESCRIPTION"],
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "SET_TITLE" => "N",
            "SET_BROWSER_TITLE" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_LAST_MODIFIED" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "",
            "PAGER_DESC_NUMBERING" => "Y",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_BASE_LINK_ENABLE" => "Y",
            "SET_STATUS_404" => "N",
            "SHOW_404" => "N",
            "MESSAGE_404" => "",
            "PAGER_BASE_LINK" => "",
            "PAGER_PARAMS_NAME" => "arrPager",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_ADDITIONAL" => ""
        ]
    ); ?>

    <div class="products-grid">
        <? foreach ($items[2] as $arItem) : ?>
            <div class="products-grid__item">
                <div class="products-grid__gallery js-gallery">
                    <div class="products-grid__gallery-main">
                        <?
                        if ($arItem['PROPERTIES']['IMAGES']['VALUE'] != false) {
                            $arItem['PROPERTIES']['IMAGES']['VALUE'] = array_slice($arItem['PROPERTIES']['IMAGES']['VALUE'], 0, 6);
                            $count = count($arItem['PROPERTIES']['IMAGES']['VALUE']);
                        } else {
                            $count = 0;
                        }
                        ?>
                        <span class="products-grid__total-badge"><?= $count; ?></span>
                        <? if ($count > 0) : ?>
                            <div class="swiper-container js-main-gallery">
                                <div class="swiper-wrapper">
                                    <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $key => $image) : ?>
                                        <div class="swiper-slide">
                                            <div class="products-grid__image">
                                                <img class="swiper-lazy"
                                                     src="<?= $arItem['PROPERTIES']['IMAGES']['DESCRIPTION'][$key] ?>"
                                                     width="312" height="312" alt="">
                                            </div>
                                            <div class="swiper-lazy-preloader"></div>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        <? else : ?>
                            <div class="swiper-container js-main-gallery">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="products-grid__image">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/no_photo.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <? endif; ?>
                    </div><? if ($count > 0) : ?>
                        <div class="products-grid__gallery-small">

                        <div class="swiper-container js-thumbs-gallery">
                            <div class="swiper-wrapper">
                                <? foreach ($arItem['PROPERTIES']['IMAGES']['VALUE'] as $key => $image) : ?>
                                    <div class="swiper-slide">
                                        <div class="products-grid__thumb">
                                            <img src="<?= $arItem['PROPERTIES']['IMAGES']['DESCRIPTION'][$key] ?>"
                                                 width="109" height="59" alt="">
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>

                        </div><? endif; ?>
                </div>
                <div class="products-grid__body">
                    <div class="products-grid__price">
                        <p class="products-grid__price-val"><?= CurrencyFormat($arItem['PROPERTIES']['PRICE']['VALUE'], "RUB"); ?></p>
                        <a class="products-grid__price-link" href="/services/mortgage/">Ипотека</a>
                    </div>
                    <div class="products-grid__intro">
                        <a class="products-grid__title"
                           href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></a>
                        <p class="product-grid__address">
                            <svg class="icon icon-direction icon_gray">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                            </svg>
                            <span><?= $arItem['PROPERTIES']['ADDRESS']['VALUE'] ?></span>
                        </p>
                        <dl class="product-grid__info">
                            <dt>Комнат: <?= $arItem['PROPERTIES']['ROOMS']['VALUE'] ?></dt>
                            <dt>Лот: <?= $arItem['PROPERTIES']['ID']['VALUE'] ?></dt>
                            <? if (!empty($arItem['PROPERTIES']['FLOOR']['VALUE'][0]) && !empty($arItem['PROPERTIES']['FLOORS']['VALUE'][0])) : ?>
                                <dt>Этаж <?= $arItem['PROPERTIES']['FLOOR']['VALUE'][0] ?>
                                    /<?= $arItem['PROPERTIES']['FLOORS']['VALUE'][0] ?></dt>
                            <? endif; ?>
                            <dt>Публикация <?= FormatDate("d.m.Y", MakeTimeStamp($arItem['DATE_CREATE'])) ?></dt>
                        </dl>

                    </div>
                    <div class="products-grid__desc">
                        <p><?= $arItem['DETAIL_TEXT'] ?></p>
                    </div>
                    <div class="products-grid__buttons">
                        <a class="btn btn-primary" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">Подробнее</a>
                        <!--<a class="products-grid__like" href="#">
                            <svg class="icon icon-heart icon_gray">
                                <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#heart"></use>
                            </svg>
                            <span>В избранное</span>
                        </a>-->
                        <a class="btn btn-outline btn-icon jsToggleCompare"
                           href="<?= SITE_TEMPLATE_PATH ?>/ajax/addToCompare.php" data-prod-id="<?= $arItem['ID'] ?>">
                            <svg class="icon icon-compare icon_gray icon_left">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#compare"></use>
                            </svg>
                            <span class="label-compare">Сравнить</span>
                        </a>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>

    <?php
    echo $arResult['NAV_STRING'];
    ?>

<?php else : ?>
    <div class="products-grid"> <h3>Нет подходящих объектов</h3></div>
<?php endif; ?>