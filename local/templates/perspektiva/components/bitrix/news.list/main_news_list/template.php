<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="news-list">
    <?php foreach ($arResult['ITEMS'] as $arItem) : ?>
        <?php if ($arItem['PROPERTIES']['UF_YOUTUBE']['VALUE'] == false) : ?>

            <div class="news-list__item">
                <div class="news-list__image">
                    <? if (!is_null($arItem['PREVIEW_PICTURE'])) : ?>
                        <?$image = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'],["width"=>"196","height"=>"196"],BX_RESIZE_IMAGE_EXACT)["src"];?>
                        <img src="<?=$image;?>" width="196" height="196"
                             alt="<?= $arItem['PREVIEW_PICTURE']['ALT'] ?>">
                    <? endif; ?>
                </div>
                <div class="news-list__content">
                    <p class="news-list__date"><?= $arItem['DISPLAY_ACTIVE_FROM'] ?></p>
                    <a class="news-list__title" href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a>
                    <div class="news-list__desc">
                        <?= $arItem['PREVIEW_TEXT'] ?>
                    </div>
                </div>
            </div>

        <?php endif; ?>
    <?php endforeach; ?>
</div>
