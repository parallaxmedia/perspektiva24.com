<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="video-grid">
    <?php foreach ($arResult['ITEMS'] as $arItem) : ?>
        <?php if ($arItem['PROPERTIES']['UF_YOUTUBE']['VALUE'] != false) : ?>

            <? if (strpos($arItem['PROPERTIES']['UF_YOUTUBE']['VALUE'], 'https://youtu.be/') !== false) {
                $arItem['PROPERTIES']['UF_YOUTUBE']['VALUE'] = str_replace('https://youtu.be/', '', $arItem['PROPERTIES']['UF_YOUTUBE']['VALUE']);
            } ?>

            <div class="video-grid__item">
                <a class="video-grid__image"
                   href="https://www.youtube.com/watch?v=<?= $arItem['PROPERTIES']['UF_YOUTUBE']['VALUE'] ?>"
                   style="background-image: url(https://i3.ytimg.com/vi/<?= $arItem['PROPERTIES']['UF_YOUTUBE']['VALUE'] ?>/hqdefault.jpg);"
                   data-fancybox></a>
                <p class="video-grid__date"><?= $arItem['DISPLAY_ACTIVE_FROM'] ?></p>
            </div>

        <?php endif; ?>
    <?php endforeach; ?>
</div>
