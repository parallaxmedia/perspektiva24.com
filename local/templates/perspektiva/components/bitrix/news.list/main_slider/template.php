<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="banner-slider-box j-banner-swiper">
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
    <div class="banner-slider swiper-container">
        <div class="swiper-wrapper">
            <? foreach ($arResult['ITEMS'] as $arSlide) : ?>

                <div class="swiper-slide">
                    <? if (!empty($arSlide['PROPERTIES']['URL']['VALUE'])) : ?>
                    <a class="banner-slider__item" href="<?=$arSlide['PROPERTIES']['URL']['VALUE']?>">
                        <img src="<?=$arSlide['PREVIEW_PICTURE']['SRC']?>" width="1338" height="344" alt="<?=$arSlide['NAME']?>">
                    </a>
                    <? else : ?>
                        <img src="<?=$arSlide['PREVIEW_PICTURE']['SRC']?>" width="1338" height="344" alt="<?=$arSlide['NAME']?>">
                    <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
    <div class="swiper-pagination"></div>
</div>
