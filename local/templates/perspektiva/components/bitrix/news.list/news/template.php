<section class="section news">
    <div class="section__inner">
        <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1"
            )
        ); ?>
        <h1><? $APPLICATION->ShowTitle(); ?></h1>
        <div class="news-list news-list_full">
            <? foreach ($arResult['ITEMS'] as $arNews) : ?>
                <div class="news-list__item">
                    <? if (!is_null($arNews['PREVIEW_PICTURE'])) : ?>
                        <div class="news-list__image">
                            <a href="<?= $arNews['DETAIL_PAGE_URL'] ?>">
                                <?$image = CFile::ResizeImageGet($arNews['PREVIEW_PICTURE']['ID'],["width"=>"300","height"=>"300"],BX_RESIZE_IMAGE_EXACT)["src"];?>
                                <img src="<?=$image;?>" width="300" height="300"
                                     alt="<?= $arNews['NAME'] ?>">
                            </a>
                        </div>
                    <? endif; ?>
                    <div class="news-list__content">
                        <p class="news-list__date">
                            <svg class="icon icon-calendar icon_red">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#calendar"></use>
                            </svg>
                            <span><?= $arNews['ACTIVE_FROM'] ?></span>
                        </p>
                        <a class="news-list__title" href="<?= $arNews['DETAIL_PAGE_URL'] ?>"><?= $arNews['NAME'] ?></a>
                        <div class="news-list__desc">
                            <p><?= $arNews['PREVIEW_TEXT'] ?></p>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <div class="agent-reviews-more">
            <a class="btn btn-outline btn-icon" href="#">
                <span>Показать еще новости</span>
                <svg class="icon icon-arrow icon_blue icon_right">
                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#arrow"></use>
                </svg>
            </a>
        </div>
    </div>
</section>