<?php
global $arCurCity;
$arFilterLabel['IBLOCK_ID'] = 2;
$arFilterLabel['=PROPERTY_FILIAL_ID'] = $arCurCity['ID'];
$arFilterLabel['=PROPERTY_OBJECT_TYPE'] = 1;

$arSelect = [
    "ID",
    "IBLOCK_ID",
    "NAME",
    'PROPERTY_PRICE'
];

$rsElementsProperty = CIBlockElement::GetList(['PROPERTY_PRICE' => 'ASC'], $arFilterLabel, false, false, $arSelect);
$count = $rsElementsProperty->SelectedRowsCount();
$arElementsProperty = $rsElementsProperty->Fetch();
?>
<div class="section__inner">
    <h2 class="section__title">Горящие предложения в г. <?= $arCurCity['REL_NAME'] ?></h2>
    <p class="section__subtitle"><?=$count?> квартир от <?=CurrencyFormat($arElementsProperty['PROPERTY_PRICE_VALUE'], 'RUB')?></p>
    <div class="products-slider-box j-products-swiper-offers">
        <div class="swiper-button-prev">
            <svg class="icon icon-chevron icon_black">
                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
            </svg>
        </div>
        <div class="swiper-button-next">
            <svg class="icon icon-chevron icon_black">
                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
            </svg>
        </div>
        <div class="products-slider products-slider_thin swiper-container">
            <div class="swiper-wrapper">
                <? foreach ($arResult['ITEMS'] as $arItem): ?>
                    <div class="swiper-slide">
                        <a class="products__slider-item" href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                            <?
                            if ($arItem['PROPERTIES']['IMAGES']['VALUE'] != false) {
                                $count = count($arItem['PROPERTIES']['IMAGES']['VALUE']);
                            } else {
                                $count = 0;
                            }
                            ?>
                            <? if ($count > 0) : ?>
                                <div class="products__slider-image">
                                    <img src="<?= $arItem['PROPERTIES']['IMAGES']['DESCRIPTION'][0]?>" width="313" height="215" alt="<?= $arItem['PROPERTIES']['ADDRESS']['VALUE']?>">
                                </div>
                            <? else : ?>
                                <div class="products__slider-image">
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/no_photo.jpg" width="313" height="215" alt="<?= $arItem['PROPERTIES']['ADDRESS']['VALUE']?>">
                                </div>
                            <? endif; ?>
                            <div class="products__slider-body">
                                <p class="products__slider-title"><?= $arItem['PROPERTIES']['ADDRESS']['VALUE']?></p>
                                <table>
                                    <tr>
                                        <td>Общая площадь</td>
                                        <td><?= $arItem['PROPERTIES']['AREA']['VALUE']?> м<sup>2</sup></td>
                                    </tr>
                                    <tr>
                                        <td>Количество комнат</td>
                                        <td><?= $arItem['PROPERTIES']['ROOMS']['VALUE']?></td>
                                    </tr>
                                    <tr>
                                        <td>Этаж</td>
                                        <td><?= $arItem['PROPERTIES']['FLOOR']['VALUE'][0]?></td>
                                    </tr>
                                    <tr>
                                        <td>Стоимость</td>
                                        <td><?= CurrencyFormat($arItem['PROPERTIES']['PRICE']['VALUE'], 'RUB')?></td>
                                    </tr>
                                </table>
                                <span class="btn btn-light">Подробнее</span>
                            </div>
                        </a></div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>