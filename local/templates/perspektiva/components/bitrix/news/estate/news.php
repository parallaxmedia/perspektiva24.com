<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetTitle("Недвижимость в г. #city#");
global $arCurCity;

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
if ($request['view'] == 'map') {
    $templateName = 'listing-map';
} else {
    $templateName = 'listing';
}

?>
<section class="section main-form">
    <div class="section__inner">
        <h1><? $APPLICATION->ShowTitle(false); ?></h1>
        <?php require($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . '/include/filter.php'); ?>
    </div>
</section>
<section class="section section-bc">
    <div class="section__inner">
        <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1"
            )
        ); ?>
    </div>
</section>

<?php require($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . '/include/fastFilter.php'); ?>

<section class="section">
    <div class="section__inner">
        <div class="sorting">
            <p class="sorting__desc">Воспользуйтесь продуманной системой фильтров, чтобы быстро и удобно подобрать
                однокомнатную квартиру, которая соответствует вашим критериям.</p>
            <div class="sorting__item dropdown">
                <button class="sorting__dropdown-btn dropdown-toggle" type="button" id="sorting"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span>Сортировать по</span>
                    <svg class="icon icon-chevron icon_gray">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                    </svg>
                </button>
                <?
                $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
                $uri = new \Bitrix\Main\Web\Uri($request->getRequestUri());
                ?>
                <div class="sorting__dropdown-menu dropdown-menu" aria-labelledby="sorting">
                    <?
                    $uri->addParams(["sort" => "price_up"]);
                    $sortURL = $uri->getUri();
                    ?>
                    <a class="dropdown-item" href="<?= $sortURL ?>" tabIndex="-1" data-value="Цена по возрастанию">Цена
                        по возрастанию</a>
                    <?
                    $uri->addParams(["sort" => "price_down"]);
                    $sortURL = $uri->getUri();
                    ?>
                    <a class="dropdown-item" href="<?= $sortURL ?>" tabIndex="-1" data-value="Цена по убыванию">Цена по
                        убыванию</a>
                    <?
                    $uri->addParams(["sort" => "news"]);
                    $sortURL = $uri->getUri();
                    ?>
                    <a class="dropdown-item" href="<?= $sortURL ?>" tabIndex="-1" data-value="Новые">Новые</a>
                </div>
            </div>
        </div>
        <?
        global $arCurCity;
        global $arrFilter;

        if (Loader::includeModule('search') && isset($_REQUEST['q'])) {
            $searchFilter = [
                '=PROPERTY_FILIAL_ID' => $arCurCity['FILIALS'],
                '?PROPERTY_ADDRESS' => strtolower($_REQUEST['q'])
            ];
            $rsSearchElement = CIBlockElement::GetList([], $searchFilter, false, false, ['ID']);
            $arSearchElements = [];
            while ($arSearchElement = $rsSearchElement->Fetch()) {
                $arSearchElements[] = $arSearchElement['ID'];
            }
            if (!empty($arSearchElements)) {
                $arrFilter = [
                    'ID' => $arSearchElements
                ];
            }
        }

        if (empty($arrFilter['ID'])) :?>
            <div class="products-grid"> <h3>Нет подходящих объектов</h3></div>
        <? else :
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                $templateName,
                array(
                    "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "NEWS_COUNT" => $arParams["NEWS_COUNT"],
                    "SORT_BY1" => $arParams["SORT_BY1"],
                    "SORT_ORDER1" => $arParams["SORT_ORDER1"],
                    "SORT_BY2" => $arParams["SORT_BY2"],
                    "SORT_ORDER2" => $arParams["SORT_ORDER2"],
                    "FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
                    "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
                    "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
                    "SET_TITLE" => 'Y',
                    "SET_BROWSER_TITLE" => 'Y',
                    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                    "MESSAGE_404" => $arParams["MESSAGE_404"],
                    "SET_STATUS_404" => $arParams["SET_STATUS_404"],
                    "SHOW_404" => $arParams["SHOW_404"],
                    "FILE_404" => $arParams["FILE_404"],
                    "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_FILTER" => $arParams["CACHE_FILTER"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
                    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
                    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                    "PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
                    "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                    "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
                    "PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
                    "PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
                    "PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
                    "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
                    "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
                    "PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
                    "ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
                    "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
                    "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
                    "FILTER_NAME" => $arParams['FILTER_NAME'],
                    "HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
                    "CHECK_DATES" => $arParams["CHECK_DATES"],
                    "STRICT_SECTION_CHECK" => $arParams["STRICT_SECTION_CHECK"],

                    "PARENT_SECTION" => $arResult["VARIABLES"]["SECTION_ID"],
                    "PARENT_SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
                    "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
                    "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
                    "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
                ),
                $component
            );
        endif;

        $title = $APPLICATION->GetTitle();

        $title = str_replace('#city#', $arCurCity['REL_NAME'], $title);

        //{Название страницы} / Перспектива24 – Оператор недвижимости в г. {city}
        $APPLICATION->SetTitle($title);
        $pageTitleTemplate = '{title} / Перспектива24 – Оператор недвижимости в г. {city}';
        $pageTitleTemplate = str_replace('{title}', $title, $pageTitleTemplate);
        $pageTitleTemplate = str_replace('{city}', $arCurCity['REL_NAME'], $pageTitleTemplate);

        $APPLICATION->SetPageProperty('title', $pageTitleTemplate);
        ?>
    </div>
</section>>