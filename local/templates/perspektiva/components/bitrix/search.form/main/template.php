<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<form class="header__search" action="<?=$arResult["FORM_ACTION"]?>">
    <label class="visually-hidden" for="header-search"><?=GetMessage("SEARCH_FORM_LABEL");?></label>
    <input class="header__search-input" id="header-search" type="search" name="q" placeholder="Поиск по агентам..."
           required>
    <button class="header__search-button" name="s" type="submit" aria-label="<?=GetMessage("SEARCH_FORM_PLACEHOLDER");?>">
        <svg class="icon icon-search icon_gray">
            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#search"></use>
        </svg>
    </button>
</form>