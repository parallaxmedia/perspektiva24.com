<?php
global $arrFilter;

$arSelect = [
    "ID",
    "IBLOCK_ID",
    "NAME",
    "DATE_ACTIVE_FROM",
    "PROPERTY_ADDRESS"
];


$rsElements = CIBlockElement::GetList([], $arrFilter, false, false, $arSelect);
$arAddress = [];
while ($arElement = $rsElements->Fetch()) {
    $arAddress[] = $arElement['PROPERTY_ADDRESS_VALUE'];
}

$arResult['ADDRESS_LIST'] = $arAddress;