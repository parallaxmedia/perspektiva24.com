<form action="/estate/" class="main-form__search">
    <label for="main-search">Поиск объекта</label>
    <!--<select class="main-form__search-input js-example-basic-single" id="main-search" name="q" placeholder="Введите улицу для поиска недвижимости..." required>
        <?/* foreach ($arResult['ADDRESS_LIST'] as $address): */?>
            <option value="<?/*= ($address); */?>">
                <?/*= ($address); */?>
            </option>
        <?/* endforeach; */?>
    </select>-->
    <input class="main-form__search-input" id="main-search" type="search" name="q"
           placeholder="Введите улицу для поиска недвижимости..." required value="<?=isset($_REQUEST['q']) ? $_REQUEST['q'] : ''?>">
    <button class="main-form__search-button" type="submit" aria-label="Поиск">
        <svg class="icon icon-search icon_gray">
            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#search"></use>
        </svg>
    </button>
</form>

