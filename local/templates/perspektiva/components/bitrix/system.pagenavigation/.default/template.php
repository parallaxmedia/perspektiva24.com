<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>

<div class="pagination-wrapper">
    <nav aria-label="Постраничная навигация">
        <ul class="pagination">
            <li class="page-item disabled">
                <a class="page-link"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"
                   tabindex="-1"><?= GetMessage("nav_prev") ?></a>
            </li>
            <? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>
                <li class="page-item <?= ($arResult['NavPageNomer'] == $arResult["nStartPage"]) ? 'active' : '' ?>">
                    <a class="page-link"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
                </li>
                <? $arResult["nStartPage"]++ ?>
            <? endwhile ?>
            <li class="page-item">
                <a class="page-link"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><?= GetMessage("nav_next") ?></a>
            </li>
        </ul>
    </nav>
    <p class="pagination__total"><?= $arResult["NavFirstRecordShow"] ?>
        - <?= $arResult["NavLastRecordShow"] ?> <?= GetMessage("nav_of") ?> <?= $arResult["NavRecordCount"] ?></p>
    <div class="pagination__dropdown dropdown">
        <p><?= GetMessage('nav_paged')?></p>
        <button class="pagination__dropdown-btn dropdown-toggle" type="button" id="pagination" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
            <span><?=$arResult['NavPageSize']?></span>
            <svg class="icon icon-chevron icon_gray">
                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
            </svg>
        </button>
        <div class="pagination__dropdown-menu dropdown-menu" aria-labelledby="pagination">
            <a class="dropdown-item" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_ON=9">
                <span>9</span>
            </a>
            <a class="dropdown-item" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_ON=18">
                <span>18</span>
            </a>
            <a class="dropdown-item" href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_ON=36">
                <span>36</span>
            </a>
        </div>
    </div>
</div>
