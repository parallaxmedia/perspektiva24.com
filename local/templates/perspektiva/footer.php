<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
global $arCurCity;
?>
</main>

<link rel="stylesheet" href="/local/templates/perspektiva/css/hotfix.css">
<link rel="stylesheet" href="/local/templates/perspektiva/css/fonts.css">
<footer class="section footer">
    <div class="section__inner">
        <div class="footer__col footer__col_whide">
            <? if (count($arCurCity['FILIALS']) == 1) : ?>
                <a class="footer__phone"
                   href="tel:<?= $arCurCity['PROPS']['PHONES']['VALUE'] ?>"><?= $arCurCity['PROPS']['PHONES']['VALUE'] ?></a>
            <? endif; ?>
            <ul class="socials-gray">
                <? if (!empty($arCurCity['LINK_INSTAGRAM'])) : ?>
                    <li>
                        <a target="_blank" href="<?= $arCurCity['LINK_INSTAGRAM'] ?>" aria-lebel="Instagram">
                            <svg class="icon icon-insta ">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#insta"></use>
                            </svg>
                        </a>
                    </li>
                <? endif; ?>
                <? if (!empty($arCurCity['LINK_FACEBOOK'])) : ?>
                    <li>
                        <a target="_blank" href="<?= $arCurCity['LINK_FACEBOOK'] ?>" aria-lebel="Facebook">
                            <svg class="icon icon-fb ">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#fb"></use>
                            </svg>
                        </a>
                    </li>
                <? endif; ?>
                <? if (!empty($arCurCity['LINK_VKONTAKTE'])) : ?>
                    <li>
                        <a target="_blank" href="<?= $arCurCity['LINK_VKONTAKTE'] ?>" aria-lebel="Vkontakte">
                            <svg class="icon icon-vk-2 ">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#vk-2"></use>
                            </svg>
                        </a>
                    </li>
                <? endif; ?>
                <? if (!empty($arCurCity['LINK_YOUTUBE'])) : ?>
                    <li>
                        <a target="_blank" href="<?= $arCurCity['LINK_YOUTUBE'] ?>" aria-lebel="Youtube">
                            <svg class="icon icon-youtube ">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#youtube"></use>
                            </svg>
                        </a>
                    </li>
                <? endif; ?>
                <? if (!empty($arCurCity['LINK_WHATSAPP'])) : ?>
                    <li>
                        <a target="_blank" href="<?= $arCurCity['LINK_WHATSAPP'] ?>" aria-lebel="Whatsapp">
                            <svg class="icon icon-whatsapp ">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#whatsapp"></use>
                            </svg>
                        </a>
                    </li>
                <? endif; ?>
            </ul>
        </div>
        <div class="footer__col footer__col_menu">
            <ul class="footer__menu">
                <li><a href="/about/">О компании</a></li>
                <li><a target="_blank" href="/about/job/">Вакансии</a></li>
                <li><a target="_blank" href="https://www.frpr.ru/?utm_source=traf_main">Франшиза</a></li>
                <li><a target="_blank" href="https://investor.perspektiva24.com/">Партнерам</a></li>
                <li><a href="/contacts/">Контакты</a></li>
            </ul>
        </div>
        <div class="footer__col footer__col_menu">
            <ul class="footer__menu">
                <li><a href="/estate/">Недвижимость</a></li>
                <li><a href="/services/">Услуги</a></li>
                <li class="mobile-hidden"><a href="/services/mortgage/">Ипотека</a></li>
                <li><a href="/agents/">Агенты</a></li>
                <li><a target="_blank" href="https://vk.com/write-99663221">Чат-бот компании</a></li>

            </ul>
        </div>
        <div class="footer__col footer__col_menu mobile-hidden">
            <ul class="footer__menu">
                <!-- <li><a target="_blank" href="https://app.onperspektiva24.com/agent-crm/sign-in/login">Войти в CRM</a></li> -->
                <li><a target="_blank"
                       href="https://docs.google.com/forms/d/e/1FAIpQLScBS4gZzHluwvTUS3MkLXukh8KbQIBdUvtnW7_FRBt8j6WJkA/viewform">Улучшить
                        работу Перспектива24</a></li>
                <li><a target="_blank" href="https://perspectiva-learning.ru/">Обучение в Перспектива24</a></li>
                <li><a href="/site-rules/">Правила пользования сайтом</a></li>
                <li><a href="/personal-data/">Политика конфиденциальности</a></li>
            </ul>
        </div>
        <div class="footer__col footer__col_whide footer__col_special">
            <div class="footer__time">
                <p><span>Пн-Пт</span><span><?= $arCurCity['PROPS']['SCHEDULE_TIME_1_5']['VALUE'] ?></span></p>
                <p><span>Сб</span><span><?= $arCurCity['PROPS']['SCHEDULE_TIME_6']['VALUE'] ?></span></p>
                <p><span>Вс</span><span><?= $arCurCity['PROPS']['SCHEDULE_TIME_7']['VALUE'] ?></span></p>
            </div>
            <p class="footer__address">
                <svg class="icon icon-direction icon_gray">
                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                </svg>
                <span><?= $arCurCity['REL_NAME'] ?> <br><? //= $arCurCity['PROPS']['ADDRESS']['VALUE'] ?></span>
            </p>
        </div>
        <div class="footer__col footer__col_bottom">
            <a class="footer__logo" href="">
                <img src="<?= SITE_TEMPLATE_PATH ?>/images/logo-gray.svg" width="140" height="33" alt="Перспектива 24">
            </a>
            <p class="footer__copy">&copy; 2002-<?= date("Y"); ?> <br>«Перспектива24» – Оператор недвижимости в
                г. <?= $arCurCity['REL_NAME'] ?></p>
        </div>
    </div>
</footer>
<div class="hidden">
    <?php require($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . '/include/regions.php'); ?>
</div>
<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/libs/html2pdf/html2pdf.bundle.min.js"></script>
<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH . '/js/vendor/requirejs/require.min.js' ?>"
        data-main="<?= SITE_TEMPLATE_PATH . '/js/main' ?>"></script>
<?php
$newton = [];
$newton["perm"] = "aa1c0eb56155222cac738cdb1d0e8bef";
$newton["arhangelsk"] = "0d115e3b0ad14368da8fbcd030d98196";
$newton["barnaul"] = "dd6c29c218790517ea77cb2c735ab352";
$newton["blagoveshchensk"] = "95c274e54ddf33b15295b7d6f5c39afd";
$newton["kazan"] = "716e0872cb98d179c9ba090d2bff610e";
$newton["kumertau"] = "d4e4897d58c9a5965cd9d2a5b4cdd028";
$newton["murom"] = "bb2ac1e289f3a37c08df2809b6a66728";
$newton["mytishchi"] = "921a8f69c3e6cd3eac029f2e8330ab97";
$newton["nizhnevartovsk"] = "5b24d67be129413bc0160dbebd6289f2";
$newton["oktyabrskiy"] = "7eb08cf3da79e5d28fab3abababfb162";
$newton["podolsk"] = "4df704ecf9c38d09d79709a421077058";
$newton["severodvinsk"] = "db12bafb38a6874538d14e65472d460d";
$newton["tobolsk"] = "0a2bdb48c2efac4774e856226b1eafdd";
$newton["tolyatti"] = "22c51245b76c4aafab472d2d0abd69d8";
$newton["tuymazy"] = "12a18a567e653d0b303c46a02ca077e8";
$newton["ulyanovsk"] = "d928e6b0ee006b97d8509d4830057eec";
$newton["cheboksary"] = "0d5935b3e12d3eccfefb3f1f830e582b";
$newton["asdasdasd"] = "a1d39c813adf022fd4dc4ab293d52e9b";
$newton["ugansk"] = "e7017bfff1800bc1797e56230a81a785";
$newton["pyshma"] = "a4136bf1f1a8bf914cb1aae646b455f8";
$newton["nn"] = "3588b9a7f5ad83c5ba397796aa9b5174";
$newton["kovrov"] = "c00ff09ed26f8e5276d62b4484ef381f";
$newton["anapa"] = "5c28a03f3f2b32a06f415aee8fe65a6b";
$newton["egor"] = "5d476d173a8c48de188d6b3a41eb214d";
$newton["krsk"] = "daa887fda1dbe590cac7192041335183";
$newton["tomsk"] = "d4a4c95c9557dc2bab24f4acee88c5f5";
$newton["iglino"] = "9d0d34b4a652ebd70b5755b48a3290db";
$newton["irkutsk"] = "3a7f2b392a7ef2d254760b06150dab5c";
$newton["kamchatka"] = "0870134c679965436f0d661964461485";
$newton["biysk"] = "7497a0c55de5e063b5b4fffab9c8162c";
$newton["novoross"] = "6499debc7c82a99eaaf2dc3cf6825ab3";
$newton["smolensk"] = "6c2a80f7c8dc968c0a3353f3dd618b25";
$newton["omsk"] = "b7e50d572a66e7c7b16e08e29500b19d";
$newton["dmitrov"] = "39ed93f3333d8ea5cc769be16d782be9";
$newton["penza"] = "e948715848d6ba8ec8cba2efc6d4c107";
$newton["stupino"] = "daa2b7709dbff2235e4a594c9b366128";
$newton["belgorod"] = "c1c1f3d72c14d4557370d89f1d3908eb";
$newton["neftekamsk"] = "cf86cdd8a4adc072c354a674d45335c4";
$newton["yaroslavl"] = "92c0efa927de876c8f7813600b24d4e5";
$newton["zelenodolsk"] = "bec47c201d3e26d0fdcc0e0398e38c7c";
$newton["angarsk"] = "583bdd8a5c166c342deb4177e60f162c";
$newton["stavropol"] = "78ea2b2b86a1ce8d8bfd02a50a53683a";
$newton["kaliningrad"] = "b4647688e58693657c2c5c8a339f615c";
$newton["belebei"] = "d0117f374b23bc893387c28c1b7cba83";
$newton["tula"] = "f2b11d783c292d5d840c4ec583d88e1a";
$newton["bryansk"] = "4b64ca40d2fac7ea3498a11b75db1c0b";
$newton["tuimazy"] = "ee9e66e73ab3bffc6370c51e04005442";
$newton["astrakhan"] = "05534d24af1460420bc1c9a5cda88a88";
$newton["ekaterinburg"] = "bef6b8e7deb78ef4b30d73494e2a20d9";
$newton["rnd"] = "112bae21e2dd6302c5ca74b6711d1853";
$newton["nsk"] = "6ea894ec8a16172e7ada205cb5a29bcb";
$newton["moscow"] = "d93d5b8d794622fd50e9d90928dc5527";
$newton["tver"] = "757370a5706c1615637099d802985323";
$newton["perspektiva24"] = "73019df5fc042331e7af0f8c863d5763";
$city = 0;
if (isset($_SERVER['SERVER_NAME'])) {
    $cityDomain = explode('.', $_SERVER['SERVER_NAME']);
    if (isset($cityDomain[0])) $city = $cityDomain[0];
}

if (isset($newton[$city])) : ?>
    <script type="text/javascript" src="//eyenewton.ru/scripts/callback.min.js" charset="UTF-8" async="async"></script>
    <script type="text/javascript">/*<![CDATA[*/
        var newton_callback_id = "<?php echo $newton[$city]; ?>";/*]]>*/</script>
<?php endif; ?>


<script src="https://www.google.com/recaptcha/api.js?render=6LciwGgaAAAAAG9HBCDrcdb3e9EsmdnhYinclnAV"></script>
<script>
    grecaptcha.ready(function () {
        grecaptcha.execute('6LciwGgaAAAAAG9HBCDrcdb3e9EsmdnhYinclnAV', { action: 'contact' }).then(function (token) {
            var recaptchaResponse = document.getElementById('recaptchaResponse');
            recaptchaResponse.value = token;
        });
    });
</script>
</body>
</html>
