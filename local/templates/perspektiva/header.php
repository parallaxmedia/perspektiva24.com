<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Page\Asset;
use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

global $APPLICATION, $arCurCity;

?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <!-- Google Tag Manager. Официальный, содержит региональные зависимости -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-NKLJ74Q');</script>
    <!-- End Google Tag Manager -->

    <meta name="yandex-verification" content="292820159fdb2722"/>
    <meta name="google-site-verification" content="cAUY5RclofcPZ7mSr9MQ6m5xeURvYVkAJney30DVSIw"/>

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-PHWJHPH');</script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112942955-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-112942955-4');
    </script>

    <title><? $APPLICATION->ShowTitle(); ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico">
    <link href="//cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>
    <?php
    /** CSS */
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/vendor/normalize.css/normalize.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/vendor/bootstrap/dist/css/bootstrap.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/vendor/swiper/package/css/swiper.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/vendor/fancybox/dist/jquery.fancybox.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/js/vendor/ion-rangeslider/css/ion.rangeSlider.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/style.css");

    $APPLICATION->ShowHead();
    ?>

    <script src="https://api-maps.yandex.ru/2.1/?apikey=2eb479a1-c9d3-44cc-a96b-177c7240f84b&lang=ru_RU"
            type="text/javascript"></script>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-PHWJHPH');</script>
    <!-- End Google Tag Manager -->
    <script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
    <script src="https://yastatic.net/share2/share.js"></script>

</head>
<body>
<!-- Google Tag Manager (noscript). Официальный, содержит региональные зависимости -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NKLJ74Q"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHWJHPH"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="panel"><? $APPLICATION->ShowPanel(); ?></div>

<header class="header">
    <div class="header__top section">
        <div class="section__inner">
            <a class="header__logo" href="/">
                <? if ($arCurCity['PROPS']['FILIAL_LOGO']['VALUE'] > 0) : ?>
                    <?
                    $file = CFile::GetPath($arCurCity['PROPS']['FILIAL_LOGO']['VALUE']);
                    ?>
                    <img src="<?= $file ?>" width="139" height="32" alt="Перспектива 24">
                <? else : ?>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/logo.svg" width="139" height="32" alt="Перспектива 24">
                <? endif; ?>
            </a>
            <? if (count($arCurCity['FILIALS']) == 1) : ?>
                <a class="header__phone"
                   href="tel:<?= $arCurCity['PROPS']['PHONES']['VALUE'] ?>"><?= phone_format($arCurCity['PROPS']['PHONES']['VALUE']) ?></a>
            <? endif; ?>
            <a class="header__city" href="#regions-modal" data-fancybox>
                <svg class="icon icon-direction ">
                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                </svg>
                <span><?= $arCurCity['REL_NAME'] ?></span>
            </a>
            <? $APPLICATION->IncludeComponent("bitrix:search.form", "main", [
                    "USE_SUGGEST" => "N",
                    "PAGE" => "#SITE_DIR#search/index.php"
                ]
            ); ?>
        </div>
    </div>
    <div class="header__main section gray-bg">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "main",
                [
                    "ROOT_MENU_TYPE" => "top_" . LANGUAGE_ID,
                    "MAX_LEVEL" => "3",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "Y",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => [],
                    "COMPONENT_TEMPLATE" => "main"
                ],
                false
            ); ?>
            <div class="header__actions">
                <a class="header__action header__action_blue" href="https://oferta.onperspektiva24.com/">
                    <svg class="icon icon-news ">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#news"></use>
                    </svg>
                    <span>Оферта</span>
                </a>
                <? $APPLICATION->IncludeComponent("bitrix:catalog.compare.list", "",
                    array(
                        "AJAX_MODE" => "N",
                        "IBLOCK_ID" => "2",
                        "NAME" => "CATALOG_COMPARE_LIST",
                    )
                ); ?>
            </div>
        </div>
    </div>
</header>
<main class="main">