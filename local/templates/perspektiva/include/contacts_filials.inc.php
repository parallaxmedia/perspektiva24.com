<section class="section contacts">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
            <h1>Контакты филиалов</h1>
            <div class="toolbar">
                <div class="share js-share" href="#">
                    <div class="ya-share2" data-services="vkontakte,facebook,viber,whatsapp,skype,telegram"></div>
                </div>
            </div>
            	
                <div class="contacts__row" style="justify-content:space-between">
                	<? foreach ($arFilials as $arFilial): ?>
                    <div class="contacts__content" style="width: 30%;">
                        <div class="contacts__content-row">
                            <h3><?= $arFilial['NAME'] ?></h3>
                            <adress><?= $arFilial['PROPS']['ADDRESS']['VALUE'] ?></adress>
                        </div>
                        <div class="contacts__content-row">
                            <p>Офис</p>
                            <a href="tel:<?= $arFilial['PROPS']['PHONE_1']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONES']['VALUE'] ?></a>
                        </div>
                        <div class="contacts__content-row">
                            <p>Операции с недвижимостью</p>
                            <? if ($arFilial['PROPS']['PHONE_2']['VALUE']) : ?>
                                <a href="tel:<?= $arFilial['PROPS']['PHONE_2']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONE_2']['VALUE'] ?></a>
                            <? else : ?>
                                <a href="tel:<?= $arFilial['PROPS']['PHONES']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONES']['VALUE'] ?></a>
                            <? endif; ?>
                        </div>
                    </div>
                 <? endforeach; ?>  
                </div>
            
            <p class="contacts__postfix">ИНН: <?= $arCurCity['PROPS']['INN']['VALUE'] ?>, ОГРН: <?= $arCurCity['PROPS']['OGRN']['VALUE'] ?></p>
        </div>
    </section>