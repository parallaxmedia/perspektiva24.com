<section class="section contacts">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
            <h1>Контакты</h1>
            <div class="toolbar">
                <!--<a class="toolbar__item" href="#">
                    <svg class="icon icon-heart icon_gray">
                        <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#heart"></use>
                    </svg>
                    <span>В избранное</span>
                </a>-->
                <div class="share js-share" href="#">
                    <div class="ya-share2" data-services="vkontakte,facebook,viber,whatsapp,skype,telegram"></div>
                </div>
            </div>
            <? foreach ($arFilials as $arFilial): ?>
                <div class="contacts__row">
                    <div class="contacts__content">
                        <div class="contacts__content-row">
                            <h2><?= $arFilial['NAME'] ?></h2>
                        </div>
                        <div class="contacts__content-row">
                            <p>Офис</p>
                            <a href="tel:<?= $arFilial['PROPS']['PHONE_1']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONES']['VALUE'] ?></a>
                        </div>
                        <div class="contacts__content-row">
                            <p>Операции с недвижимостью</p>
                            <? if ($arFilial['PROPS']['PHONE_2']['VALUE']) : ?>
                                <a href="tel:<?= $arFilial['PROPS']['PHONE_2']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONE_2']['VALUE'] ?></a>
                            <? else : ?>
                                <a href="tel:<?= $arFilial['PROPS']['PHONES']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONES']['VALUE'] ?></a>
                            <? endif; ?>
                        </div>
                        <div class="contacts__content-row">
                            <p>Cлужба контроля качества</p>
                            <? if ($arFilial['PROPS']['PHONE_3']['VALUE']) : ?>
                                <a href="tel:<?= $arFilial['PROPS']['PHONE_3']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONE_3']['VALUE'] ?></a>
                            <? else : ?>
                                <a href="tel:<?= $arFilial['PROPS']['PHONES']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONES']['VALUE'] ?></a>
                            <? endif; ?>
                        </div>
                        <div class="contacts__content-row">
                            <p>Отдел кадров</p>
                            <? if ($arFilial['PROPS']['PHONE_4']['VALUE']) : ?>
                                <a href="tel:<?= $arFilial['PROPS']['PHONE_4']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONE_4']['VALUE'] ?></a>
                            <? else : ?>
                                <a href="tel:<?= $arFilial['PROPS']['PHONES']['VALUE'] ?>"><?= $arFilial['PROPS']['PHONES']['VALUE'] ?></a>
                            <? endif; ?>
                        </div>
                    </div>
                    <div class="contacts__map">
                        <div class="contacts__map-text">
                            <p class="contacts__map-title">
                                <svg class="icon icon-office icon_gray">
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#office"></use>
                                </svg>
                                <span>Офис</span>
                            </p>
                            <p class="contacts__map-address">
                                <svg class="icon icon-direction icon_gray">
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                                </svg>
                                <span><?= $arCurCity['PROPS']['ADDRESS']['VALUE'] ?></span>
                            </p>
                            <div class="contacts__map-time">
                                <table>
                                    <tr>
                                        <th>Пн</th>
                                        <th>Вт</th>
                                        <th>Ср</th>
                                        <th>Чт</th>
                                        <th class="bordered">Пт</th>
                                        <th class="red">Сб</th>
                                        <th class="red">Вс</th>
                                    </tr>
                                    <tr>
                                        <td class="bordered"
                                            colspan="5"><?= $arCurCity['PROPS']['SCHEDULE_TIME_1_5']['VALUE'] ?></td>
                                        <td class="red"
                                            colspan="2"><?= $arCurCity['PROPS']['SCHEDULE_TIME_6']['VALUE'] ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div id="productMap<?= $arFilial['ID'] ?>" style="height: 224px; width: 100%"></div>
                        <?
                        $arFilial['PROPS']['COORD']['VALUE'] = $arFilial['PROPS']['LAT']['VALUE'] . ',' . $arFilial['PROPS']['LNG']['VALUE'];
                        ?>
                        <script type="text/javascript">
                            // Функция ymaps.ready() будет вызвана, когда
                            // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
                            ymaps.ready(init<?=$arFilial['ID']?>);

                            function init<?=$arFilial['ID']?>() {
                                // Создание карты.
                                var myMap<?=$arFilial['ID']?> = new ymaps.Map("productMap<?=$arFilial['ID']?>", {
                                    center: [<?=$arFilial['PROPS']['COORD']['VALUE']?>],
                                    zoom: 16
                                });

                                var myGeoObject = new ymaps.GeoObject({
                                    geometry: {
                                        type: "Point", // тип геометрии - точка
                                        coordinates: [<?=$arFilial['PROPS']['COORD']['VALUE']?>] // координаты точки
                                    }
                                });

                                myMap<?=$arFilial['ID']?>.geoObjects.add(myGeoObject);
                            }
                        </script>
                    </div>
                </div>
                <div class="write-us">
                    <div class="write-us__text">
                        <p class="write-us__title">Напишите нам</p>
                        <div class="write-us__text-inner">
                            <p class="write-us__label">
                                <svg class="icon icon-email icon_red">
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#email"></use>
                                </svg>
                                <span>Для вопросов и предложений</span>
                            </p>
                            <a class="write-us__link"
                               href="mailto:<?= $arFilial['PROPS']['EMAIL_EVENTS']['VALUE'] ?>"><?= $arFilial['PROPS']['EMAIL']['VALUE'] ?></a>
                        </div>
                    </div>
                    <?
                    $APPLICATION->IncludeComponent("bitrix:main.feedback", "contacts", [
                            "USE_CAPTCHA" => "N",
                            "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                            "EMAIL_TO" => $arFilial['PROPS']['EMAIL']['VALUE'],
                            "REQUIRED_FIELDS" => ["AUTHOR", "AUTHOR_EMAIL", "TEXT", "AUTHOR_PHONE"],
                            "EVENT_MESSAGE_ID" => ["7"]
                        ]
                    );
                    ?>
                </div>
            <? endforeach; ?>


            <!--<div class="contacts__office">
                <p class="contacts__office-title">Наш офис</p>
                <div class="gallery-slider-box j-gallery-swiper">
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                    <div class="gallery-slider swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a class="gallery-slider__item" href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/1.jpg"
                                   data-fancybox="office">
                                    <img src="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/1.jpg" width="186" height="135"
                                         alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="gallery-slider__item" href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/2.jpg"
                                   data-fancybox="office">
                                    <img src="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/2.jpg" width="186" height="135"
                                         alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="gallery-slider__item" href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/3.jpg"
                                   data-fancybox="office">
                                    <img src="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/3.jpg" width="186" height="135"
                                         alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="gallery-slider__item" href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/1.jpg"
                                   data-fancybox="office">
                                    <img src="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/1.jpg" width="186" height="135"
                                         alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="gallery-slider__item" href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/2.jpg"
                                   data-fancybox="office">
                                    <img src="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/2.jpg" width="186" height="135"
                                         alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="gallery-slider__item" href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/3.jpg"
                                   data-fancybox="office">
                                    <img src="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/3.jpg" width="186" height="135"
                                         alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="gallery-slider__item" href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/1.jpg"
                                   data-fancybox="office">
                                    <img src="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/1.jpg" width="186" height="135"
                                         alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="gallery-slider__item" href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/2.jpg"
                                   data-fancybox="office">
                                    <img src="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/2.jpg" width="186" height="135"
                                         alt="">
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="gallery-slider__item" href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/3.jpg"
                                   data-fancybox="office">
                                    <img src="<? /*= SITE_TEMPLATE_PATH */ ?>/images/office/3.jpg" width="186" height="135"
                                         alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
            <p class="contacts__postfix">ИНН: <?= $arCurCity['PROPS']['INN']['VALUE'] ?>,
                ОГРН: <?= $arCurCity['PROPS']['OGRN']['VALUE'] ?></p>
        </div>
    </section>