<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$arSelect = [
    "ID",
    "IBLOCK_ID",
    "NAME",
    "DATE_ACTIVE_FROM",
    "PROPERTY_OBJECT_TYPE",
    "PROPERTY_DISTRICT"
];

global $arCurCity;
$cityId = $arCurCity['ID'];

if ($cityId > 0) {
    $arFilter['=PROPERTY_FILIAL_ID'] = $cityId;
}

$arFilter['IBLOCK_ID'] = 2;

/**
 * Студии
 */
$arFilter['PROPERTY_OBJECT_TYPE'] = 1;
$arFilter['=PROPERTY_ROOMS'] = 0;

$rsElements = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$studios = $rsElements->SelectedRowsCount();

/**
 * 1 комнатные
 */
$arFilter['PROPERTY_OBJECT_TYPE'] = 1;
$arFilter['=PROPERTY_ROOMS'] = 1;
$rsElements = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$oneRooms = $rsElements->SelectedRowsCount();

/**
 * 2 комнатные
 */
$arFilter['PROPERTY_OBJECT_TYPE'] = 1;
$arFilter['=PROPERTY_ROOMS'] = 2;
$rsElements = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$twoRooms = $rsElements->SelectedRowsCount();

/**
 * 3 комнатные
 */
$arFilter['PROPERTY_OBJECT_TYPE'] = 1;
$arFilter['=PROPERTY_ROOMS'] = 3;

$rsElements = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$threeRooms = $rsElements->SelectedRowsCount();

/**
 * Дома
 */
$arFilter['PROPERTY_OBJECT_TYPE'] = 3;
unset($arFilter['=PROPERTY_ROOMS']);
$rsElements = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$house = $rsElements->SelectedRowsCount();

/**
 * Земля
 */
$arFilter['PROPERTY_OBJECT_TYPE'] = 5;
$rsElements = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
$ground = $rsElements->SelectedRowsCount();
?>
<section class="section product-links-wrapper thin">
    <div class="section__inner">
        <ul class="product-links">
            <? if ($studios > 0) : ?>
                <li>
                    <a class="btn btn-outline" href="/estate/?OBJECT_TYPE=1&rooms[]=0">
                        <span>Студии</span>
                        <i><?= $studios ?></i>
                    </a>
                </li>
            <? endif; ?>
            <? if ($oneRooms > 0) : ?>
            <li>
                <a class="btn btn-outline" href="/estate/?OBJECT_TYPE=1&rooms[]=1">
                    <span>1 комнатные</span>
                    <i><?= $oneRooms ?></i>
                </a>
            </li>
            <? endif; ?>
            <? if ($twoRooms > 0) : ?>
            <li>
                <a class="btn btn-outline" href="/estate/?OBJECT_TYPE=1&rooms[]=2">
                    <span>2 комнатные</span>
                    <i><?= $twoRooms ?></i>
                </a>
            </li>
            <? endif; ?>
            <? if ($threeRooms > 0) : ?>
            <li>
                <a class="btn btn-outline" href="/estate/?OBJECT_TYPE=1&rooms[]=3">
                    <span>3 комнатные</span>
                    <i><?= $threeRooms ?></i>
                </a>
            </li>
            <? endif; ?>
            <? if ($house > 0) : ?>
            <li>
                <a class="btn btn-outline" href="/estate/?type=Дом">
                    <span>Дачи, дома, коттеджи</span>
                    <i><?= $house ?></i>
                </a>
            </li>
            <? endif; ?>
            <? if ($ground > 0) : ?>
            <li>
                <a class="btn btn-outline" href="/estate/?OBJECT_TYPE=5">
                    <span>Земельные участки</span>
                    <i><?= $ground ?></i>
                </a>
            </li>
            <? endif; ?>
        </ul>
    </div>
</section>
