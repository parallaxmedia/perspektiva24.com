<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('dev2fun.multidomain');

function toNormalNumber($number)
{
    return str_replace(' ', '', $number);
}

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$arFilter = [];

$arFilter['IBLOCK_ID'] = 2;

global $arCurCity;

$cityId = $arCurCity['FILIALS'];

if ($cityId > 0) {
    $arFilter['=PROPERTY_FILIAL_ID'] = $cityId;
}


$arSelect = [
    "ID",
    "IBLOCK_ID",
    "NAME",
    "DATE_ACTIVE_FROM",
    "PROPERTY_DISTRICT",
];

$rsElementsProperty = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);

if ($rsElementsProperty->SelectedRowsCount() > 0) {
    $listIdSectionDistrict = [];
    while ($arElementProperty = $rsElementsProperty->Fetch()) {
        $listIdSectionDistrict[$arElementProperty['PROPERTY_DISTRICT_VALUE']]['ID'] = $arElementProperty['PROPERTY_DISTRICT_VALUE'];
    }

    foreach ($listIdSectionDistrict as $sectionId) {
        $rsSectionDistrict = CIBlockSection::GetByID($sectionId['ID'])->Fetch();
        $listIdSectionDistrict[$rsSectionDistrict['ID']]['NAME'] = $rsSectionDistrict['NAME'];
    }
}


if (!empty($request['OBJECT_TYPE'])) {
    $arFilter['PROPERTY_OBJECT_TYPE'] = $request['OBJECT_TYPE'];
}

if ($request['with-photo'] == 'on') {
    $arFilter['!PROPERTY_IMAGES'] = false;
}

if (!empty($request['rooms'])) {
    $arFilter['=PROPERTY_ROOMS'] = $request['rooms'];
}

if (!empty($request['districts'])) {
    $arFilter['=PROPERTY_DISTRICT'] = $request['districts'];
}

$tmpRooms = [];

if ($request['house-chk-1'] == 'on') {
    $tmpRooms[] = 1;
}

if ($request['house-chk-1'] == 'on') {
    $tmpRooms[] = 1;
}
if ($request['house-chk-2'] == 'on') {
    $tmpRooms[] = 2;
}
if ($request['house-chk-3'] == 'on') {
    if (!empty($tmpRooms)) {
        $tmpRooms = [
            '=PROPERTY_ROOMS' => $tmpRooms,
            '>=PROPERTY_ROOMS' => 3
        ];
    } else {
        $tmpRooms['>=PROPERTY_ROOMS'] = 3;
    }
}

if (!empty($request['buildType'])) {
    $arFilter['=PROPERTY_HOUSETYPE'] = $request['buildType'];
}

if (!empty($request['price_from'])) {
    if (!empty($request['price_to'])) {
        $arFilter["><PROPERTY_PRICE"] = [toNormalNumber($request['price_from']), toNormalNumber($request['price_to'])];
    } else {
        $arFilter[">PROPERTY_PRICE"] = toNormalNumber($request['price_from']);
    }
} elseif (!empty($request['price_to'])) {
    $arFilter["<PROPERTY_PRICE"] = toNormalNumber($request['price_to']);
}

if (!empty($request['square_from'])) {
    if (!empty($request['square_to'])) {
        $arFilter['><PROPERTY_AREA'] = [toNormalNumber($request['square_from']), toNormalNumber($request['square_to'])];
    } else {
        $arFilter['>PROPERTY_AREA'] = $request['square_from'];
    }
} elseif (!empty($request['square_to'])) {
    $arFilter['<PROPERTY_AREA'] = $request['square_to'];
}

if (!empty($request['type'])) {
    if ($request['type'] == 'Вторичная') {
        $arFilter['PROPERTY_IS_NEW_VALUE'] = 'Вторичка';
    }
    if ($request['type'] == 'Новостройка') {
        $arFilter['PROPERTY_IS_NEW_VALUE'] = 'Новостройка';
    }
    if ($request['type'] == 'Комната') {
        $arFilter['PROPERTY_OBJECT_TYPE'] = [(int)$arFilter['PROPERTY_OBJECT_TYPE'], 2];
    }
    if ($request['type'] == 'Дом') {
        $arFilter['PROPERTY_OBJECT_TYPE'] = 3;
    }
    if ($request['type'] == 'Дача') {
        $arFilter['PROPERTY_OBJECT_SUB_TYPE'] = 3;
        unset($arFilter['PROPERTY_OBJECT_TYPE']);
    }
    if ($request['type'] == 'Коттедж') {
        $arFilter['PROPERTY_OBJECT_SUB_TYPE'] = 4;
        unset($arFilter['PROPERTY_OBJECT_TYPE']);
    }
    if ($request['type'] == 'Таунхаус') {
        $arFilter['PROPERTY_OBJECT_TYPE'] = 4;
    }
}

if (!empty($request['flour-start'])) {
    if (!empty($request['flour-end'])) {
        $arFilter['><PROPERTY_FLOOR'] = [$request['flour-start'], $request['flour-end']];
    } else {
        $arFilter['<PROPERTY_FLOOR'] = $request['flour-start'];
    }
} elseif (!empty($request['flour-end'])) {
    $arFilter['>PROPERTY_FLOOR'] = $request['flour-end'];
}

if (!empty($request['kitchen-square'])) {
    $arFilter['PROPERTY_KITCHENSPACE'] = $request['kitchen-square'];
}

if ($request['not-first'] == 'on') {
    $arFilter['!PROPERTY_FLOOR'] = 1;
}

if ($request['not-last'] == 'on') {
    $arFilter['!PROPERTY_TOP_FLOOR'] = 'Y';
}

if (!empty($request['ACTION_TYPE'])) {
    $arFilter['PROPERTY_ACTION_TYPE_VALUE'] = $request['ACTION_TYPE'];
}


$subTypeObject = [];

if ($request['commerce-chk-1'] == 'on') {
    $subTypeObject[] = 65;
}
if ($request['commerce-chk-2'] == 'on') {
    $subTypeObject[] = 61;
}
if ($request['commerce-chk-3'] == 'on') {
    $subTypeObject[] = 60;
}
if ($request['commerce-chk-4'] == 'on') {
    $subTypeObject[] = 66;
}
if ($request['commerce-chk-5'] == 'on') {
    $subTypeObject[] = 63;
}
if ($request['commerce-chk-6'] == 'on') {
    $subTypeObject[] = 64;
}

if (!empty($subTypeObject)) {
    $arFilter['PROPERTY_OBJECT_SUB_TYPE'] = $subTypeObject;
}



$arSelect = [
    "ID",
    "IBLOCK_ID",
    "NAME",
    "DATE_ACTIVE_FROM",
    "PROPERTY_OBJECT_TYPE",
    "PROPERTY_DISTRICT"
];


$rsElements = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);


if ($request['count'] == 'Y') {
    echo $rsElements->SelectedRowsCount();
    die();
}

$arElementsID = [];

while ($arElement = $rsElements->Fetch()) {
    $arElementsID[] = $arElement['ID'];
}

$arrFilter = [
    'ID' => $arElementsID,
    '!PROPERTY_STATUS_VALUE' => 'Завершен'
];

$GLOBALS['arrFilter'] = $arrFilter;

?>


<div class="main-form__wrapper">
    <div class="main-form__nav">
        <button class="main-form__scroller main-form__scroller_left" type="button" aria-label="Пролистать">
            <svg class="icon icon-chevron icon_gray">
                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
            </svg>
        </button>
        <button class="main-form__scroller main-form__scroller_right" type="button" aria-label="Пролистать">
            <svg class="icon icon-chevron icon_gray">
                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
            </svg>
        </button>
        <ul class="nav nav-tabs" id="form-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="apartments-tab" data-toggle="tab" href="#apartments" role="tab"
                   aria-controls="apartments" aria-selected="true">Квартиры</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="commerce-tab" data-toggle="tab" href="#commerce" role="tab"
                   aria-controls="commerce" aria-selected="false">Коммерческая</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="houses-tab" data-toggle="tab" href="#houses" role="tab"
                   aria-controls="houses" aria-selected="false">Дома</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="homesteads-tab" data-toggle="tab" href="#homesteads" role="tab"
                   aria-controls="homesteads" aria-selected="false">Участки</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="garages-tab" data-toggle="tab" href="#garages" role="tab"
                   aria-controls="garages" aria-selected="false">Гаражи</a>
            </li>
        </ul>
    </div>
    <div class="tab-content" id="form-tabs-content">
        <div class="tab-pane show active" id="apartments" role="tabpanel" aria-labelledby="apartments-tab">
            <form class="main-form__item" method="get" action="/estate/">
                <input type="hidden" name="OBJECT_TYPE" value="1">
                <div class="main-form__row">
                    <div class="main-form__col dropdown">
                        <button class="main-form__dropdown-btn dropdown-toggle" type="button"
                                id="dropdownRooms" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <span>Количество комнат</span>
                            <svg class="icon icon-chevron icon_gray">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                            </svg>
                        </button>
                        <div class="main-form__dropdown-menu dropdown-menu js-dropdown-menu"
                             aria-labelledby="dropdownRooms">
                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="Студия">
                                <input id="room-checkbox-1" type="checkbox" name="rooms[]"
                                       value="0" <?= (in_array(0, $_REQUEST['rooms'])) ? 'checked="checked"' : '' ?>>
                                <label for="room-checkbox-1">Студия</label>
                            </a>
                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="1 комнатные">
                                <input id="room-checkbox-2" type="checkbox" name="rooms[]"
                                       value="1" <?= (in_array(1, $_REQUEST['rooms'])) ? 'checked="checked"' : '' ?>>
                                <label for="room-checkbox-2">1 комнатные</label>
                            </a>
                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="2 комнатные">
                                <input id="room-checkbox-3" type="checkbox" name="rooms[]"
                                       value="2" <?= (in_array(2, $_REQUEST['rooms'])) ? 'checked="checked"' : '' ?>>
                                <label for="room-checkbox-3">2 комнатные</label>
                            </a>
                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="3 комнатные">
                                <input id="room-checkbox-4" type="checkbox" name="rooms[]"
                                       value="3" <?= (in_array(3, $_REQUEST['rooms'])) ? 'checked="checked"' : '' ?>>
                                <label for="room-checkbox-4">3 комнатные</label>
                            </a>
                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="4 комнатные">
                                <input id="room-checkbox-5" type="checkbox" name="rooms[]"
                                       value="4" <?= (in_array(4, $_REQUEST['rooms'])) ? 'checked="checked"' : '' ?>>
                                <label for="room-checkbox-5">4 комнатные</label>
                            </a>
                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="5 и более">
                                <input id="room-checkbox-6" type="checkbox" name="rooms[]"
                                       value="5" <?= (in_array(5, $_REQUEST['rooms'])) ? 'checked="checked"' : '' ?>>
                                <label for="room-checkbox-6">5 и более</label>
                            </a>
                        </div>
                    </div>
                    <div class="main-form__col dropdown">
                        <button class="main-form__dropdown-btn dropdown-toggle" type="button"
                                id="dropdownDistricts" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                            <span>Район</span>
                            <svg class="icon icon-chevron icon_gray">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                            </svg>
                        </button>
                        <div class="main-form__dropdown-menu dropdown-menu js-dropdown-menu"
                             aria-labelledby="dropdownDistricts">
                            <? foreach ($listIdSectionDistrict as $propertyDistrict) : ?>
                                <a class="dropdown-item" href="#" tabIndex="-1"
                                   data-value="<?= $propertyDistrict['NAME'] ?>">
                                    <input id="district-checkbox-<?= $propertyDistrict['ID'] ?>" type="checkbox"
                                           name="districts[]"
                                        <?= ($_REQUEST['districts[]'] == $propertyDistrict['ID']) ? 'checked="checked"' : '' ?>
                                           value="<?= $propertyDistrict['ID'] ?>">
                                    <label for="district-checkbox-1"><?= $propertyDistrict['NAME'] ?></label>
                                </a>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="main-form__col main-form__col_special">
                        <div class="main-form__subcol">
                            <input id="price-start-1" type="text" min="0"
                                   value="<?= $_REQUEST['price_from'] ?>" placeholder="от" name="price_from">
                            <label for="price-start-1">Цена</label>
                        </div>
                        <div class="main-form__subcol">
                            <input id="price-end-1" type="text" name="price_to" min="0"
                                   value="<?= $_REQUEST['price_to'] ?>" placeholder="до">
                            <!--<label for="price-end-1">до</label>-->
                        </div>
                    </div>
                    <div class="main-form__col main-form__col_special">
                        <div class="main-form__subcol">
                            <input id="square-start-1" type="number" name="square_from" min="0"
                                   value="<?= $_REQUEST['square_from'] ?>" placeholder="от">
                            <label for="square-start-1">Площадь</label>
                        </div>
                        <div class="main-form__subcol">
                            <input id="square-end-1" type="number" name="square_to" min="0"
                                   value="<?= $_REQUEST['square_to'] ?>" placeholder="до">
                            <!--<label for="square-end-1">до</label>-->
                        </div>
                    </div>
                    <div class="main-form__add collapse" id="flour-add">
                        <div class="main-form__add-radios">
                            <input id="type-radio-1" type="radio" name="type"
                                   value="Все" <?= ($_REQUEST['type'] == 'Все') ? 'checked="checked"' : '' ?>>
                            <label for="type-radio-1">Все</label>
                            <input id="type-radio-2" type="radio" name="type"
                                   value="Вторичная" <?= ($_REQUEST['type'] == 'Вторичная') ? 'checked="checked"' : '' ?>>
                            <label for="type-radio-2">Вторичная</label>
                            <input id="type-radio-3" type="radio" name="type"
                                   value="Новостройка" <?= ($_REQUEST['type'] == 'Новостройка') ? 'checked="checked"' : '' ?>>
                            <label for="type-radio-3">Новостройка</label>
                            <input id="type-radio-4" type="radio" name="type"
                                   value="Комната" <?= ($_REQUEST['type'] == 'Комната') ? 'checked="checked"' : '' ?>>
                            <label for="type-radio-4">Комната</label>
                        </div>
                        <div class="main-form__add-col">
                            <div class="main-form__add-item main-form__add-item_whide">
                                <label for="flour-start-1">Этаж от</label>
                                <input id="flour-start-1" type="number" name="flour-start" min="0"
                                       value="<?= $_REQUEST['flour-start'] ?>">
                                <label for="flour-start-2">до</label>
                                <input id="flour-start-2" type="number" name="flour-end" min="0"
                                       value="<?= $_REQUEST['flour-end'] ?>">
                            </div>
                            <div class="main-form__add-item main-form__add-item_special">
                                <label class="visually-hidden" for="kitchen-square-1">Площадь кухни</label>
                                <input id="kitchen-square-1" type="number" name="kitchen-square"
                                       placeholder="Площадь кухни" value="<?= $_REQUEST['kitchen-square'] ?>">
                                <span class="main-form__postfix">м<sup>2</sup></span>
                            </div>
                            <!--<div class="main-form__add-dropdown dropdown">
                                        <button class="main-form__dropdown-btn dropdown-toggle" type="button"
                                                id="wcType" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false"><span>Тип сан.узла</span>
                                            <svg class="icon icon-chevron icon_gray">
                                                <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#chevron"></use>
                                            </svg>
                                        </button>
                                        <div class="main-form__dropdown-menu dropdown-menu js-dropdown-menu"
                                             aria-labelledby="wcType">
                                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="Тип 1">
                                                <input id="wc-checkbox-1" type="checkbox" name="wcType[]" value="Тип 1">
                                                <label for="wc-checkbox-1">Тип 1</label>
                                            </a>
                                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="Тип 2">
                                                <input id="wc-checkbox-2" type="checkbox" name="wcType[]" value="Тип 2">
                                                <label for="wc-checkbox-2">Тип 2</label>
                                            </a>
                                        </div>
                                    </div>-->
                            <div class="main-form__add-dropdown dropdown">
                                <button class="main-form__dropdown-btn dropdown-toggle" type="button"
                                        id="buildType" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                    <span>Тип здания</span>
                                    <svg class="icon icon-chevron icon_gray">
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                                    </svg>
                                </button>

                                <div class="main-form__dropdown-menu dropdown-menu js-dropdown-menu"
                                     aria-labelledby="buildType">
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Кирпичный">
                                        <input id="build-checkbox-1" type="checkbox" name="buildType[]"
                                               value="Кирпичный" <?= ($_REQUEST['buildType'] == 'Кирпичный') ? 'checked="checked"' : '' ?>>
                                        <label for="build-checkbox-1">Кирпичный</label>
                                    </a>
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Панельный">
                                        <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                               value="Панельный" <?= ($_REQUEST['buildType'] == 'Панельный') ? 'checked="checked"' : '' ?>>
                                        <label for="build-checkbox-2">Панельный</label>
                                    </a>
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Блочный">
                                        <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                               value="Блочный" <?= ($_REQUEST['buildType'] == 'Блочный') ? 'checked="checked"' : '' ?>>
                                        <label for="build-checkbox-2">Блочный</label>
                                    </a>
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Монолитный">
                                        <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                               value="Монолитный" <?= ($_REQUEST['buildType'] == 'Монолитный') ? 'checked="checked"' : '' ?>>
                                        <label for="build-checkbox-2">Монолитный</label>
                                    </a>
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Деревянный">
                                        <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                               value="Деревянный" <?= ($_REQUEST['buildType'] == 'Деревянный') ? 'checked="checked"' : '' ?>>
                                        <label for="build-checkbox-2">Деревянный</label>
                                    </a>
                                </div>
                            </div>
                            <!--<div class="main-form__add-item">
                                <label class="visually-hidden" for="build-year-1">Год постройки</label>
                                <input id="build-year-1" type="number" name="build-year"
                                       placeholder="Год постройки">
                            </div>-->
                        </div>
                        <div class="main-form__add-col">
                            <!--<div class="main-form__add-item">
                                        <label class="visually-hidden" for="parking-1">Парковка</label>
                                        <input id="parking-1" type="text" name="parking" placeholder="Парковка">
                                    </div>
                                    <div class="main-form__add-dropdown dropdown">
                                        <button class="main-form__dropdown-btn dropdown-toggle" type="button"
                                                id="balcony" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            <span>Балкон</span>
                                            <svg class="icon icon-chevron icon_gray">
                                                <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#chevron"></use>
                                            </svg>
                                        </button>
                                        <div class="main-form__dropdown-menu dropdown-menu js-dropdown-menu"
                                             aria-labelledby="balcony">
                                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="Балкон">
                                                <input id="balcony-checkbox-1" type="checkbox" name="balconyType[]"
                                                       value="Балкон">
                                                <label for="balcony-checkbox-1">Балкон</label>
                                            </a>
                                            <a class="dropdown-item" href="#" tabIndex="-1" data-value="Лоджия">
                                                <input id="balcony-checkbox-2" type="checkbox" name="balconyType[]"
                                                       value="Лоджия">
                                                <label for="balcony-checkbox-2">Лоджия</label>
                                            </a>
                                        </div>
                                    </div>-->
                            <div class="main-form__add-checks">
                                <input id="not-first" type="checkbox"
                                       name="not-first" <?= ($_REQUEST['not-first'] == 'on') ? 'checked="checked"' : '' ?>>
                                <label for="not-first">Не первый этаж</label>
                                <input id="not-last" type="checkbox"
                                       name="not-last" <?= ($_REQUEST['not-last'] == 'on') ? 'checked="checked"' : '' ?>>
                                <label for="not-last">Не последний этаж</label>
                            </div>
                            <div class="main-form__add-checks">
                                <!--<input id="town-side" type="checkbox" name="town-side">
                                <label for="town-side">В черте города</label>-->
                                <input id="with-photo" type="checkbox"
                                       name="with-photo" <?= ($_REQUEST['with-photo'] == 'on') ? 'checked="checked"' : '' ?>>
                                <label for="with-photo">Только с фото</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-form__buttons">
                    <button class="main-form__more btn btn-light btn-light_blue btn-icon" type="button"
                            data-toggle="collapse" data-target="#flour-add" aria-expanded="false"
                            aria-controls="flour-add">
                        <span class="default-text">Ещё детали</span>
                        <span class="open-text">Свернуть</span>
                        <svg class="icon icon-chevron icon_blue icon_right">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                        </svg>
                    </button>
                    <a class="btn btn-light btn-icon apply-filter-map" href="#">
                        <svg class="icon icon-placeholder icon_red icon_left">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#placeholder"></use>
                        </svg>
                        <span>На карте</span>
                    </a>
                    <button class="btn btn-primary apply-filter" type="submit">Посмотреть <strong> </strong>
                        объявления
                    </button>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="commerce" role="tabpanel" aria-labelledby="commerce-tab">
            <form class="main-form__item" method="get" action="/estate/">
                <div class="main-form__row">
                    <div class="main-form__add-radios">
                        <input id="type-radio-1-2" type="radio" name="ACTION_TYPE" value="Продажа" checked>
                        <label for="type-radio-1-2">Продажа</label>
                        <input id="type-radio-2-2" type="radio" name="ACTION_TYPE" value="Аренда">
                        <label for="type-radio-2-2">Аренда</label>
                    </div>
                    <div class="main-form__col">
                        <div class="dropdown">
                            <button class="main-form__dropdown-btn dropdown-toggle" type="button"
                                    id="buildType-2" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                <span>Тип здания</span>
                                <svg class="icon icon-chevron icon_gray">
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                                </svg>
                            </button>
                            <div class="main-form__dropdown-menu dropdown-menu js-dropdown-menu"
                                 aria-labelledby="buildType-2">
                                <a class="dropdown-item" href="#" tabIndex="-1" data-value="Кирпичный">
                                    <input id="build-checkbox-1" type="checkbox" name="buildType[]"
                                           value="Кирпичный">
                                    <label for="build-checkbox-1">Кирпичный</label>
                                </a>
                                <a class="dropdown-item" href="#" tabIndex="-1" data-value="Панельный">
                                    <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                           value="Панельный">
                                    <label for="build-checkbox-2">Панельный</label>
                                </a>
                                <a class="dropdown-item" href="#" tabIndex="-1" data-value="Блочный">
                                    <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                           value="Блочный">
                                    <label for="build-checkbox-2">Блочный</label>
                                </a>
                                <a class="dropdown-item" href="#" tabIndex="-1" data-value="Монолитный">
                                    <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                           value="Монолитный">
                                    <label for="build-checkbox-2">Монолитный</label>
                                </a>
                                <a class="dropdown-item" href="#" tabIndex="-1" data-value="Деревянный">
                                    <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                           value="Деревянный">
                                    <label for="build-checkbox-2">Деревянный</label>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="main-form__col main-form__col_special">
                        <div class="main-form__subcol">
                            <input id="price-start-1-2" type="text" placeholder="от" name="price_from" min="0">
                            <label for="price-start-1-2">Цена</label>
                        </div>
                        <div class="main-form__subcol">
                            <input id="price-end-1-2" type="text" min="0" name="price_to" placeholder="до">
                        </div>
                    </div>
                    <div class="main-form__col main-form__col_special m-r-none">
                        <div class="main-form__subcol">
                            <input id="square-start-1-2" type="number" name="square_from" min="0">
                            <label for="square-start-1-2">Площадь от</label>
                        </div>
                        <div class="main-form__subcol">
                            <input id="square-end-1-2" type="number" name="square_to" min="0">
                            <label for="square-end-1-2">до</label>
                        </div>
                    </div>
                    <div class="main-form__add collapse" id="commerce-add">
                        <div class="main-form__add-col">

                            <div class="main-form__add-checks">
                                <input id="commerce-chk-1" type="checkbox" name="commerce-chk-1">
                                <label for="commerce-chk-1">Складское</label>
                                <input id="commerce-chk-2" type="checkbox" name="commerce-chk-2">
                                <label for="commerce-chk-2">Офисное</label>
                            </div>
                            <div class="main-form__add-checks">
                                <input id="commerce-chk-3" type="checkbox" name="commerce-chk-3">
                                <label for="commerce-chk-3">Гостиница</label>
                                <input id="commerce-chk-4" type="checkbox" name="commerce-chk-4">
                                <label for="commerce-chk-4">Торговое</label>
                            </div>
                            <div class="main-form__add-checks">
                                <input id="commerce-chk-5" type="checkbox" name="commerce-chk-5">
                                <label for="commerce-chk-5">Свободного назначения</label>
                                <input id="commerce-chk-6" type="checkbox" name="commerce-chk-6">
                                <label for="commerce-chk-6">Производственное</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-form__buttons">
                    <button class="main-form__more btn btn-light btn-light_blue btn-icon" type="button"
                            data-toggle="collapse" data-target="#commerce-add" aria-expanded="false"
                            aria-controls="commerce-add">
                        <span class="default-text">Ещё детали</span>
                        <span class="open-text">Свернуть</span>
                        <svg class="icon icon-chevron icon_blue icon_right">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                        </svg>
                    </button>
                    <a class="btn btn-light btn-icon apply-filter-map" href="#">
                        <svg class="icon icon-placeholder icon_red icon_left">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#placeholder"></use>
                        </svg>
                        <span>На карте</span>
                    </a>
                    <button class="btn btn-primary apply-filter" type="submit">Посмотреть <strong></strong>
                        объявления
                    </button>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="houses" role="tabpanel" aria-labelledby="houses-tab">
            <form class="main-form__item" method="get" action="/estate/">
                <div class="main-form__row">
                    <div class="main-form__add-radios">
                        <input id="type-radio-1-3" type="radio" name="type" value="Дом" checked>
                        <label for="type-radio-1-3">Дом</label>
                        <input id="type-radio-2-3" type="radio" name="type" value="Дача">
                        <label for="type-radio-2-3">Дача</label>
                        <input id="type-radio-3-3" type="radio" name="type" value="Коттедж">
                        <label for="type-radio-3-3">Коттедж</label>
                        <input id="type-radio-4-3" type="radio" name="type" value="Таунхаус">
                        <label for="type-radio-4-3">Таунхаус</label>
                    </div>
                    <div class="main-form__col main-form__col_special">
                        <div class="main-form__subcol">
                            <input id="price-start-1-3" type="text" placeholder="от" name="price_from" min="0">
                            <label for="price-start-1-3">Цена</label>
                        </div>
                        <div class="main-form__subcol">
                            <input id="price-end-1-3" type="text" name="price_to" placeholder="до" min="0">
                        </div>
                    </div>
                    <div class="main-form__col main-form__col_special">
                        <div class="main-form__subcol">
                            <input id="square-start-1-3" type="number" name="square_from" min="0">
                            <label for="square-start-1-3">Площадь от</label>
                        </div>
                        <div class="main-form__subcol">
                            <input id="square-end-1-3" type="number" name="square_to" min="0">
                            <label for="square-end-1-3">до</label>
                        </div>
                    </div>
                    <div class="main-form__add collapse" id="houses-add">
                        <div class="main-form__add-col main-form__add-col_special">
                            <div class="main-form__add-checks">
                                <input id="house-chk-1" type="checkbox" name="house-chk-1">
                                <label for="house-chk-1">Одноэтажные</label>
                            </div>
                            <div class="main-form__add-checks">
                                <input id="house-chk-2" type="checkbox" name="house-chk-2">
                                <label for="house-chk-2">2х этажные</label>
                            </div>
                            <div class="main-form__add-checks">
                                <input id="house-chk-3" type="checkbox" name="house-chk-3">
                                <label for="house-chk-3">3х этажные и более</label>
                            </div>
                        </div>
                        <div class="main-form__add-col">
                            <div class="main-form__add-item main-form__add-item_whide main-form__add-item_special">
                                <label for="dist-start-1-3">До города от</label>
                                <input id="dist-start-1-3" type="number" name="dist-start" min="0">
                                <label for="dist-start-2-3">до</label>
                                <input id="dist-start-2-3" type="number" name="dist-end" min="0"><span
                                        class="main-form__postfix">км</span>
                            </div>
                            <div class="main-form__add-dropdown dropdown">
                                <button class="main-form__dropdown-btn dropdown-toggle" type="button"
                                        id="buildType" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false"><span>Тип здания</span>
                                    <svg class="icon icon-chevron icon_gray">
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                                    </svg>
                                </button>
                                <div class="main-form__dropdown-menu dropdown-menu js-dropdown-menu"
                                     aria-labelledby="buildType">
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Кирпичный">
                                        <input id="build-checkbox-1" type="checkbox" name="buildType[]"
                                               value="Кирпичный">
                                        <label for="build-checkbox-1">Кирпичный</label>
                                    </a>
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Панельный">
                                        <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                               value="Панельный">
                                        <label for="build-checkbox-2">Панельный</label>
                                    </a>
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Блочный">
                                        <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                               value="Блочный">
                                        <label for="build-checkbox-2">Блочный</label>
                                    </a>
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Монолитный">
                                        <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                               value="Монолитный">
                                        <label for="build-checkbox-2">Монолитный</label>
                                    </a>
                                    <a class="dropdown-item" href="#" tabIndex="-1" data-value="Деревянный">
                                        <input id="build-checkbox-2" type="checkbox" name="buildType[]"
                                               value="Деревянный">
                                        <label for="build-checkbox-2">Деревянный</label>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="main-form__add-col">
                            <div class="main-form__add-checks">
                                <input id="house-chk-4" type="checkbox" name="house-chk-4">
                                <label for="house-chk-4">Газ</label>
                                <input id="house-chk-5" type="checkbox" name="house-chk-5">
                                <label for="house-chk-5">Электричество</label>
                            </div>
                            <div class="main-form__add-checks">
                                <input id="house-chk-6" type="checkbox" name="house-chk-6">
                                <label for="house-chk-6">Канализация</label>
                                <input id="house-chk-7" type="checkbox" name="house-chk-7">
                                <label for="house-chk-7">Вода</label>
                            </div>
                            <div class="main-form__add-checks">
                                <input id="house-chk-8" type="checkbox" name="house-chk-8">
                                <label for="house-chk-8">Отопление</label>
                                <input id="house-chk-9" type="checkbox" name="house-chk-9">
                                <label for="house-chk-9">С участком</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-form__buttons">
                    <button class="main-form__more btn btn-light btn-light_blue btn-icon" type="button"
                            data-toggle="collapse" data-target="#houses-add" aria-expanded="false"
                            aria-controls="houses-add">
                        <span class="default-text">Ещё детали</span>
                        <span class="open-text">Свернуть</span>
                        <svg class="icon icon-chevron icon_blue icon_right">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                        </svg>
                    </button>
                    <a class="btn btn-light btn-icon apply-filter-map" href="#">
                        <svg class="icon icon-placeholder icon_red icon_left">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#placeholder"></use>
                        </svg>
                        <span>На карте</span></a>
                    <button class="btn btn-primary apply-filter" type="submit">Посмотреть <strong></strong>
                        объявления
                    </button>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="homesteads" role="tabpanel" aria-labelledby="homesteads-tab">
            <form class="main-form__item" method="get" action="/estate/">
                <input type="hidden" name="OBJECT_TYPE" value="5">
                <div class="main-form__row">
                    <div class="main-form__add-col">
                        <div class="main-form__add-item main-form__add-item_whide main-form__add-item_special">
                            <label for="dist-start-1-4">Расстояние до города от</label>
                            <input id="dist-start-1-4" type="number" name="dist-start" min="0">
                            <label for="dist-start-2-4">до</label>
                            <input id="dist-start-2-4" type="number" name="dist-end" min="0"><span
                                    class="main-form__postfix">км</span>
                        </div>
                    </div>
                    <div class="main-form__col main-form__col_special">
                        <div class="main-form__subcol">
                            <input id="price-start-1-4" type="text" placeholder="от" name="price_from" min="0">
                            <label for="price-start-1-4">Цена</label>
                        </div>
                        <div class="main-form__subcol">
                            <input id="price-end-1-4" type="text" name="price_to" placeholder="до" min="0">
                        </div>
                    </div>
                    <div class="main-form__col main-form__col_special">
                        <div class="main-form__subcol">
                            <input id="square-start-1-4" type="number" name="square_from" min="0">
                            <label for="square-start-1-4">Площадь от</label>
                        </div>
                        <div class="main-form__subcol">
                            <input id="square-end-1-4" type="number" name="square_to" min="0">
                            <label for="square-end-1-4">до</label>
                        </div>
                    </div>
                    <div class="main-form__add collapse" id="homesteads-add">
                        <div class="main-form__add-col">
                            <div class="main-form__add-checks">
                                <input id="homesteads-chk-1" type="checkbox" name="homesteads-chk-1">
                                <label for="homesteads-chk-1">В собственности</label>
                                <input id="homesteads-chk-2" type="checkbox" name="homesteads-chk-2">
                                <label for="homesteads-chk-2">Сельхозназначения</label>
                            </div>
                            <div class="main-form__add-checks">
                                <input id="homesteads-chk-3" type="checkbox" name="homesteads-chk-3">
                                <label for="homesteads-chk-3">ИЖС</label>
                                <input id="homesteads-chk-4" type="checkbox" name="homesteads-chk-4">
                                <label for="homesteads-chk-4">Промназначения</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-form__buttons">
                    <button class="main-form__more btn btn-light btn-light_blue btn-icon" type="button"
                            data-toggle="collapse" data-target="#homesteads-add" aria-expanded="false"
                            aria-controls="homesteads-add">
                        <span class="default-text">Ещё детали</span>
                        <span class="open-text">Свернуть</span>
                        <svg class="icon icon-chevron icon_blue icon_right">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                        </svg>
                    </button>
                    <a class="btn btn-light btn-icon apply-filter-map" href="#">
                        <svg class="icon icon-placeholder icon_red icon_left">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#placeholder"></use>
                        </svg>
                        <span>На карте</span></a>
                    <button class="btn btn-primary apply-filter" type="submit">Посмотреть <strong></strong>
                        объявления
                    </button>
                </div>
            </form>
        </div>
        <div class="tab-pane" id="garages" role="tabpanel" aria-labelledby="garages-tab">
            <form class="main-form__item" method="get" action="/estate/">
                <input type="hidden" name="OBJECT_TYPE" value="7">
                <div class="main-form__row">
                    <div class="main-form__add collapse show" id="garages-add">
                        <div class="main-form__add-col">
                            <div class="main-form__add-checks">
                                <input id="garages-chk-1" type="checkbox" name="garages-chk-1">
                                <label for="garages-chk-1">Состоит в кооперативе</label>
                                <input id="garages-chk-2" type="checkbox" name="garages-chk-2">
                                <label for="garages-chk-2">Отапливаемый</label>
                            </div>
                        </div>
                    </div>
                    <div class="main-form__col main-form__col_special">

                        <div class="main-form__subcol">
                            <input id="price-start-1-5" type="text" name="price_from" placeholder="от" min="0">
                            <label for="price-start-1-5">Цена</label>
                        </div>

                        <div class="main-form__subcol">
                            <input id="price-end-1-5" type="text" placeholder="до" name="price_to" min="0">
                        </div>
                    </div>
                    <div class="main-form__col main-form__col_special">
                        <div class="main-form__subcol">
                            <input id="square-start-1-5" type="number" name="square_from" min="0">
                            <label for="square-start-1-5">Площадь от</label>
                        </div>
                        <div class="main-form__subcol">
                            <input id="square-end-1-5" type="number" name="square_to" min="0">
                            <label for="square-end-1-5">до</label>
                        </div>
                    </div>
                </div>
                <div class="main-form__buttons">
                    <a class="btn btn-light btn-icon apply-filter-map" href="#">
                        <svg class="icon icon-placeholder icon_red icon_left">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#placeholder"></use>
                        </svg>
                        <span>На карте</span></a>
                    <button class="btn btn-primary apply-filter" type="submit">Посмотреть <strong></strong>
                        объявления
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<? $APPLICATION->IncludeComponent("bitrix:search.form", "search_filter", [
        "USE_SUGGEST" => "N",
        "PAGE" => "#SITE_DIR#estate/"
    ]
); ?>
