<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Loader;

Loader::includeModule('main');
Loader::includeModule('highloadblock');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$hlDomainID = 3;
$hlDomain = HL\HighloadBlockTable::getById($hlDomainID)->fetch();

$entityDomain = HL\HighloadBlockTable::compileEntity($hlDomain);
$entityDomainClass = $entityDomain->getDataClass();

$rsData = $entityDomainClass::getList(array(
    "select" => array("*"),
    "order" => array("UF_NAME" => "ASC"),
    'filter' => ['UF_ACTIVE' => 1],
));
$arCityList = [];
$arCityListByDivider = [];
$protocol = (CMain::IsHTTPS()) ? "https://" : "http://";
while ($arData = $rsData->Fetch()) {
    if (!empty($arData['UF_SUBDOMAIN'])) {
        $arCityList[] = [
            'NAME' => $arData['UF_NAME'],
            'DOMAIN' => $arData['UF_SUBDOMAIN'] . '.' . $arData['UF_DOMAIN'],
            'COUNTRY' => $arData['UF_COUNTRY']
        ];
        $arCityListByDivider[mb_substr($arData['UF_NAME'], 0, 1, 'UTF-8')][] = [
            'NAME' => $arData['UF_NAME'],
            'DOMAIN' => $protocol . $arData['UF_SUBDOMAIN'] . '.' . $arData['UF_DOMAIN'] . '?setCity=Y',
            'COUNTRY' => $arData['UF_COUNTRY']
        ];
    } else {
        $arCityList[] = [
            'NAME' => $arData['UF_NAME'],
            'DOMAIN' => $arData['UF_DOMAIN'],
            'COUNTRY' => $arData['UF_COUNTRY']
        ];
        $arCityListByDivider[mb_substr($arData['UF_NAME'], 0, 1, 'UTF-8')][] = [
            'NAME' => $arData['UF_NAME'],
            'DOMAIN' => $protocol . $arData['UF_DOMAIN'] . '?setCity=Y',
            'COUNTRY' => $arData['UF_COUNTRY']
        ];
    }
}

$hlDomainID = 8;
$hlDomain = HL\HighloadBlockTable::getById($hlDomainID)->fetch();

$entityDomain = HL\HighloadBlockTable::compileEntity($hlDomain);
$entityDomainClass = $entityDomain->getDataClass();

$rsData = $entityDomainClass::getList(array(
    "select" => array("*"),
    "order" => array("UF_COUNTRY_NAME" => "ASC"),
));
$arCountryList = [];
while ($arData = $rsData->Fetch()) {
    $arCountryList[] = [
        'ID' => $arData['ID'],
        'NAME' => $arData['UF_COUNTRY_NAME']
    ];
}
?>
<div class="regions" id="regions-modal">
    <div class="regions__section">
        <p class="regions__title">Выберите город</p>
        <div class="regions__search">
            <label class="visually-hidden" for="regions">Найти город</label>
            <input class="regions__search-input" id="regions-search" type="search" placeholder="Город">
            <button class="regions__search-button" type="button" aria-label="Поиск">
                <svg class="icon icon-search icon_gray">
                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#search"></use>
                </svg>
            </button>
            <button class="regions__search-button regions__clear hidden" type="button" aria-label="Очистить поиск">
                <svg class="icon icon-close icon_gray">
                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#close"></use>
                </svg>
            </button>
        </div>
    </div>
    <nav class="regions__tags">
        <? foreach ($arCountryList as $country) : ?>
            <a class="regions__tags-link <?= ($country['ID'] == 1) ? 'active' : '' ?>" href="#"
               data-id="cc_<?= $country['ID'] ?>"><?= $country['NAME'] ?></a>
        <? endforeach; ?>
    </nav>
    <div class="regions__section regions__section_scroll">
        <p class="regions__empty hidden">Поиск не дал результатов</p>
        <div class="regions__list">
            <? foreach ($arCityListByDivider as $liter => $arCity) : ?>
                <p class="regions__divider"><?= $liter ?></p>
                <? foreach ($arCity as $city): ?>
                    <a class="regions__item <?= ($city['COUNTRY'] != 1) ? 'hidden' : '';?>" href="<?= $city['DOMAIN'] ?>"
                       data-id="cc_<?= $city['COUNTRY'] ?>"
                       data-city="<?= $city['NAME'] ?>"><?= $city['NAME'] ?></a>
                <? endforeach; ?>
            <? endforeach; ?>
        </div>
    </div>
</div>

