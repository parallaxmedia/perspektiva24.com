<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('dev2fun.multidomain');
global $arCurCity;

$cityId = $arCurCity['PROPS']['CITY']['VALUE'];
?>
<section class="section subscribe">
    <div class="section__inner">
        <?
        global $arCurCity;
        $arSubScribeFilter['IBLOCK_ID'] = 2;
        $arSubScribeFilter['=PROPERTY_FILIAL_ID'] = $arCurCity['FILIALS'];
        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "NAME",
        ];
        $rsElementsProperty = CIBlockElement::GetList([], $arSubScribeFilter, false, false, $arSelect);
        $count = $rsElementsProperty->SelectedRowsCount();
        ?>
        <h3 class="section__title">Подпишитесь на обновление результатов поиска</h3>
        <p class="section__subtitle">Получайте обновления по <span><?= $count ?></span> объектам недвижимости в г. <?= $arCurCity['REL_NAME'] ?></p>
        <div class="subscribe__grid">
            <div class="subscribe__grid-item">
                <div class="subscribe__grid-image">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/phone.png" width="193" height="169" alt="">
                </div>
                <div class="subscribe__grid-content">
                    <p><strong>Получать сообщением</strong></p>
                    <p>Выберите удобный месенжер</p>
                </div>
                <ul class="socials">
                    <!--<li class="socials__item socials__item_tg">
                            <a href="#" aria-label="Telegram">
                                <svg class="icon icon-telegram icon_white">
                                    <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#telegram"></use>
                                </svg>
                            </a>
                        </li>-->
                    <li class="socials__item socials__item_wa">
                        <a target="_blank" href="https://api.whatsapp.com/send?phone=79251813843" aria-label="Whatsapp">
                            <svg class="icon icon-whatsapp icon_white">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#whatsapp"></use>
                            </svg>
                        </a>
                    </li>
                    <li class="socials__item socials__item_vk">
                        <a target="_blank" href="https://vk.com/write-99663221" aria-label="Вконтакте">
                            <svg class="icon icon-vk icon_white">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#vk"></use>
                            </svg>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="subscribe__grid-item">
                <div class="subscribe__grid-image">
                    <img class="subscribe__grid-image" src="<?= SITE_TEMPLATE_PATH ?>/images/laptop.png" width="305"
                         height="138" alt="">
                </div>
                <div class="subscribe__grid-content">
                    <p><strong>Получать на E-mail</strong></p>
                    <form class="subscribe__form">
                        <label class="visually-hidden" for="subscribe-email">E-mail</label>
                        <input id="subscribe-email" type="email" name="email" placeholder="example@mail.ru"
                               required>
                        <button class="btn btn-danger" type="submit" id="subscribe_btn">Подписаться</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>