<?php

use Bitrix\Main\Diag\Debug;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('dev2fun.multidomain');
global $arCurCity;

$arFilterObjects['FILIAL_ID.VALUE'] = $arCurCity['FILIALS'];


$arFilterObjects['!COMPLEX_ID.VALUE'] = false;

//$arFilterObjects['IBLOCK_ID'] = 2;

$arSelect = [
    'ID',
    'IBLOCK_ID',
    'NAME',
    'PROPERTY_COMPLEX_ID',
    'PROPERTY_CITY',
];

$elementClass = \Bitrix\Iblock\Iblock::wakeUp(2)->getEntityDataClass();

$rsElementsObject = $elementClass::getList([
    'select' => [
        'ID',
        'IBLOCK_ID',
        'NAME',
        'COMPLEX_PROP' => 'COMPLEX_ID',
        'CITY_PROP' => 'CITY',
    ],
    'filter' => $arFilterObjects,
    'cache' => ['ttl' => 3600],
]);

$arElementsObject = [];
$arFilterCitiesID = [];

if ($rsElementsObject->getSelectedRowsCount() > 0) {
    while ($arElementObject = $rsElementsObject->fetch()) {
        if (!is_null($arElementObject['COMPLEX_PROPVALUE'])) {
            $arElementsObject[$arElementObject['COMPLEX_PROPVALUE']] = (int)$arElementObject['COMPLEX_PROPVALUE'];
        }
        if (!is_null($arElementObject['CITY_PROPVALUE'])) {
            $arFilterCitiesID[] = (int)$arElementObject['CITY_PROPVALUE'];
        }
    }
}

/*if ($USER->IsAdmin()) {
    Debug::dump($arFilterCitiesID);
}*/

if (count($arElementsObject) > 0) {
    /** Получили недвижку с жилыми комплексами в текущем городе */
    /** Получаем дома и связанные с ними дома */
    $arFilterZHK['XML_ID'] = $arElementsObject;
    $arFilterZHK['PROPERTY_CITY.VALUE'] = $arFilterCitiesID;
    $arSelect = [
        "ID",
        "IBLOCK_ID",
        'PROPERTY_ID',
        "NAME",
        "PROPERTY_ADDRESS",
        "PROPERTY_PARENT_ID",
        "PROPERTY_CITY",
        "XML_ID"
    ];

    $rsElementsZHK = CIBlockElement::GetList([], $arFilterZHK, false, false, $arSelect);
    $arElementsZHK = [];
    $arElementsZHKId = [];
    $arElementsHouseId = [];

    while ($arElementsHouse = $rsElementsZHK->Fetch()) {
        if (!empty($arElementsHouse['PROPERTY_PARENT_ID_VALUE'])) {
            $arElementsHouseId[] = $arElementsHouse['PROPERTY_PARENT_ID_VALUE'];
        }
        $arElementsZHK[$arElementsHouse['ID']] = $arElementsHouse;
    }

    foreach ($arElementsZHK as $key => $arElementZHK) {
        if (!in_array($arElementZHK['PROPERTY_CITY_VALUE'], $arFilterCitiesID)) {
            unset($arElementsZHK[$key]);
        }
    }

    $arFilterZHK = [];
    if (!empty($arElementsHouseId)) {
        $arFilterZHK = [
            'PROPERTY_PARENT_ID' => $arElementsHouseId
        ];
        $rsElementsZHK = CIBlockElement::GetList([], $arFilterZHK, false, false, $arSelect);

        while ($arElementsHouse = $rsElementsZHK->Fetch()) {
            $arElementsZHK[$arElementsHouse['ID']] = $arElementsHouse;
        }
    }

    $arUniqueZHK = [];
    foreach ($arElementsZHK as $arElementZHK) {
        $arUniqueZHK[$arElementZHK['PROPERTY_PARENT_ID_VALUE']] = $arElementZHK;
        $arElementsZHKId[] = $arElementZHK['PROPERTY_ID_VALUE'];
    }
    $countComplex = count($arUniqueZHK);

    $arFilterObjects = [
        'PROPERTY_COMPLEX_ID' => $arElementsZHKId
    ];

    $arSelect = [
        "ID",
        "IBLOCK_ID",
        "NAME",
        "PROPERTY_COMPLEX_ID",
        "PROPERTY_PRICE",
        "PROPERTY_ROOMS",
        "PROPERTY_AREA",
    ];

    $rsElementsObject = CIBlockElement::GetList([], $arFilterObjects, false, false, $arSelect);

    $countObject = $rsElementsObject->SelectedRowsCount();
    $arElementsObject = [];
    while ($arElementObject = $rsElementsObject->Fetch()) {
        $arElementsObject[] = $arElementObject;
    }

    $arMinMaxPrice = getMinMaxPrice($arElementsObject);

    foreach ($arElementsZHK as $arElementZHK) {
        $arUniqueZHK[$arElementZHK['PROPERTY_PARENT_ID_VALUE']]['parents'][] = $arElementZHK['PROPERTY_ID_VALUE'];
    }

    foreach ($arElementsObject as $arElementObject) {
        if (isset($arUniqueZHK[$arElementObject['PROPERTY_COMPLEX_ID_VALUE']])) {
            $arUniqueZHK[$arElementObject['PROPERTY_COMPLEX_ID_VALUE']]['OBJECTS'][] = $arElementObject;
        } else {
            foreach ($arUniqueZHK as $key => $arUnique) {
                if (in_array($arElementObject['PROPERTY_COMPLEX_ID_VALUE'], $arUnique['parents'])) {
                    $arUniqueZHK[$key]['OBJECTS'][] = $arElementObject;
                }
            }
        }
    }

    foreach ($arUniqueZHK as $key => $arElementZHK) {
        foreach ($arElementZHK['OBJECTS'] as $arObject) {
            $arUniqueZHK[$key]['ROOMS'][$arObject['PROPERTY_ROOMS_VALUE']][] = $arObject;
        }
    }

    foreach ($arUniqueZHK as $key1 => $arElementZHK) {
        foreach ($arElementZHK['ROOMS'] as $key2 => $arRoomsObject) {
            $minArea = getMinArea($arRoomsObject);
            $minPrice = getMinMaxPrice($arRoomsObject);
            $arUniqueZHK[$key1]['ROOMS'][$key2]['min_price'] = $minPrice['min'];
            $arUniqueZHK[$key1]['ROOMS'][$key2]['min_area'] = $minArea;
        }
    }
}

function getMinMaxPrice($arr)
{
    $min = null;
    $max = null;
    while (list($k, $v) = each($arr)) {
        if ($v['PROPERTY_PRICE_VALUE'] > $max['PROPERTY_PRICE_VALUE'] or $max === null) {
            $max = $v;
        }

        if ($v['PROPERTY_PRICE_VALUE'] < $min['PROPERTY_PRICE_VALUE'] or $min === null) {
            $min = $v;
        }
    }
    return [
        'min' => $min['PROPERTY_PRICE_VALUE'],
        'max' => $max['PROPERTY_PRICE_VALUE']
    ];
}

function getMinArea($arr)
{
    $min = null;
    while (list($k, $v) = each($arr)) {
        if ($v['PROPERTY_AREA_VALUE'] < $min['PROPERTY_AREA_VALUE'] or $min === null) {
            $min = $v;
        }
    }
    return $min['PROPERTY_AREA_VALUE'];
}

?>

<?php if (isset($arUniqueZHK)) : ?>

    <section class="section products-slider-wrapper">
        <div class="section__inner">
            <h2 class="section__title">Новостройки в г. <?= $arCurCity['REL_NAME'] ?></h2>
            <p class="section__subtitle"><?= $countComplex ?> комплекса, <?= $countObject ?> квартир,
                от <?= CurrencyFormat($arMinMaxPrice['min'], 'RUB') ?>
                до <?= CurrencyFormat($arMinMaxPrice['max'], 'RUB') ?></p>
            <div class="products-slider-box j-products-swiper">
                <div class="swiper-button-prev">
                    <svg class="icon icon-chevron icon_black">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                    </svg>
                </div>
                <div class="swiper-button-next">
                    <svg class="icon icon-chevron icon_black">
                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                    </svg>
                </div>
                <div class="products-slider swiper-container">
                    <div class="swiper-wrapper">
                        <? foreach ($arUniqueZHK as $zhkId => $arElementZHK) : ?>
                            <div class="swiper-slide">
                                <div class="products__slider-item">
                                    <div class="products__slider-image">
                                        <!--<span class="products__slider-tag">Строится</span>-->
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/products/1.jpg" width="426"
                                             height="181"
                                             alt="ЖК Энтузиастов">
                                    </div>
                                    <div class="products__slider-body">
                                        <p class="products__slider-title"><?= explode(',', $arElementZHK['NAME'])[0] ?></p>
                                        <p class="products__slider-subtitle"><?= $arElementZHK['PROPERTY_ADDRESS_VALUE'] ?></p>
                                        <div class="products__slider-links">
                                            <? foreach ($arElementZHK['ROOMS'] as $roomCount => $arRoomObjects) : ?>
                                                <a href="#">
                                                    <? if ($roomCount == 0) : ?>
                                                        <span>Студии</span>
                                                    <? else : ?>
                                                        <span><?= $roomCount ?>-комнатные</span>
                                                    <? endif; ?>
                                                    <span class="mobile-hidden">от <?= $arRoomObjects['min_area'] ?> м<sup>2</sup></span>
                                                    <span>от <?= CurrencyFormat($arRoomObjects['min_price'], 'RUB') ?></span>
                                                </a>
                                            <? endforeach; ?>
                                        </div>
                                        <a class="btn btn-light"
                                           href="/residential/detail.php?ID=<?= $arElementZHK['ID'] ?>">Подробнее</a>
                                    </div>
                                </div>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="products-slider-button">
                <a class="btn btn-primary" href="/residential/">Все новостройки</a>
            </div>
        </div>
    </section>

<?php endif; ?>