var config = {
    baseUrl: "/local/templates/perspektiva/js",
    urlArgs: 'rev=1.2',
    waitSeconds: 0,
    paths: {
        main: 'main',
        modules: 'modules',

        jquery: 'vendor/jquery/dist/jquery.min',
        svg4everybody: 'vendor/svg4everybody/dist/svg4everybody.min',
        bootstrap: 'vendor/bootstrap/dist/js/bootstrap.bundle.min',
        doubleScroll: 'libs/jquery.doubleScroll',
        'jquery.touchswipe': 'vendor/jquery-touchswipe/jquery.touchSwipe.min',
        swiper: 'vendor/swiper/package/js/swiper.min',
        fancybox: 'vendor/fancybox/dist/jquery.fancybox.min',
        'ion-rangeslider': 'vendor/ion-rangeslider/js/ion.rangeSlider.min',
        rater: 'vendor/rater/rater.min',
        maskedinput: 'libs/jquery.maskedinput',
        select2: 'libs/select2/js/select2.min'
    }
};

requirejs.config(config);

require(['jquery', 'bootstrap'], function ($) {

    $(document).ready(function ($) {


        $('#downloadPDF').click(function (e) {
            e.preventDefault();
            let pdfIn = document.getElementsByClassName('compare')[0];
            var opt = {
                margin: 1,
                filename: 'myfile.pdf',
                image: {type: 'jpeg', quality: 0.98},
                html2canvas: {scale: 2},
                jsPDF: {unit: 'in', format: 'letter', orientation: 'landscape'}
            };
            html2pdf(pdfIn, opt);
        });


        /* Activate bootstrap tooltips */
        $('[data-toggle="tooltip"]').tooltip();

        /* Main menu */
        if ($('.main-menu').length) {
            require(['modules/mainMenu'], function (mainMenu) {
                new mainMenu({
                    root: $('.main-menu'),
                    control: $('.main-menu__control'),
                    parentControl: $('.main-menu__parent > a')
                });
            });
        }

        /* Regions modal filter */
        if ($('.regions').length) {
            require(['modules/regionsFilter'], function (regionsFilter) {
                new regionsFilter({
                    root: $('.regions'),
                    search: $('.regions').find('.regions__search-input'),
                    item: $('.regions').find('.regions__item'),
                    emptyLabel: $('.regions').find('.regions__empty'),
                    clearBtn: $('.regions').find('.regions__clear'),
                    divider: $('.regions').find('.regions__divider'),
                    tag: $('.regions').find('.regions__tags-link')
                });
            });
        }

        /* Main form mobile scroller */
        if ($('.main-form__wrapper .nav-tabs').length) {
            require(['modules/formNavScroller', 'jquery.touchswipe'], function (formNavScroller, swipe) {
                new formNavScroller({
                    prev: $('.main-form__scroller_left'),
                    next: $('.main-form__scroller_right'),
                    target: $('.main-form__wrapper .nav-tabs'),
                    interval: 153
                });
            });
        }

        /* Main form bootstrap dropdown with checkboxes actions */
        if ($('.js-dropdown-menu').length) {
            require(['modules/formDropdownCheckboxes'], function (formDropdownCheckboxes) {
                $('.js-dropdown-menu').each(function () {
                    new formDropdownCheckboxes({
                        root: $(this),
                        item: $(this).find('a')
                    });
                });
            });
        }

        /* Custom bootstrap dropdown close */
        if ($('.js-close-dropdown').length) {
            require(['modules/closeDropdown'], function (closeDropdown) {
                $('.js-close-dropdown').each(function () {
                    new closeDropdown({
                        root: $(this).parents('.dropdown'),
                        btnClose: $(this)
                    });
                });
            });
        }

        /* Show phone action */
        if ($('.js-toggle-phone').length) {
            require(['modules/togglePhone'], function (togglePhone) {
                $('.js-toggle-phone').each(function () {
                    new togglePhone({
                        root: $(this),
                        control: $(this).find('button'),
                        item: $(this).find('a')
                    });
                });
            });
        }

        /* Toggle share links */
        if ($('.js-share').length) {
            require(['modules/shareBlock'], function (shareBlock) {
                $('.js-share').each(function () {
                    new shareBlock({
                        root: $(this),
                        control: $(this).find('button')
                    });
                });
            });
        }

        /* Toggle share links */
        if ($('.js-rating').length) {
            require(['rater'], function (rate) {
                $(".js-rating").rate({
                    max_value: 5,
                    step_size: 1
                });
            });
        }

        /* Add svg sprite support */
        require(['svg4everybody'], function (svg4everybody) {
            svg4everybody({});
        });

        function initCalc(){
            require(['modules/calculator', 'ion-rangeslider'], function (calculator, ionRangeSlider) {

                $('.js-calc').each(function () {
                    new calculator({
                        root: $(this),
                        cost: $(this).find('.js-calc-cost'),
                        term: $(this).find('.js-calc-term'),
                        firstPay: $(this).find('.js-calc-first-pay'),
                        rate: $(this).find('.js-calc-rate'),
                        monthPay: $(this).find('.js-calc-month-pay'),
                        total: $(this).find('.js-calc-total'),
                        ranges: $(this).find('.js-range-slider')
                    });
                });

            });
        }
        /* Init calculator */
        if ($('.js-calc').length) {
            initCalc()

            $("#ipoteka24").change(function () {
                var changeval = $(this).data("changeval");
                var state = $(this).prop("checked");
                var currentRate = $('.js-calc-rate');
                if (state){
                    currentRate.val(currentRate.val()-changeval);
                }else{
                    currentRate.val(currentRate.data("defval"));
                }
                $("#currentRate").text(currentRate.val());
                initCalc();
            })
        }

        /* Responsive sliders */
        require(['swiper'], function (Swiper) {
            if ($('.j-gallery-swiper').length) {
                var gallerySwiper;
                $('.j-gallery-swiper').each(function (index) {
                    gallerySwiper[index] = new Swiper($(this).find('.swiper-container'), {
                        spaceBetween: 13,
                        slidesPerView: 1.2,
                        loop: true,
                        navigation: {
                            nextEl: $(this).find('.swiper-button-next'),
                            prevEl: $(this).find('.swiper-button-prev'),
                        },
                        breakpoints: {
                            500: {
                                slidesPerView: 2.5
                            },
                            640: {
                                slidesPerView: 3.5
                            },
                            990: {
                                slidesPerView: 5
                            },
                            1200: {
                                slidesPerView: 7,
                                spaceBetween: 6,
                                simulateTouch: false
                            }
                        }
                    });
                });

            }

            if ($('.j-products-swiper').length) {
                var productsSwiper = [];

                $('.j-products-swiper').each(function (index) {
                    productsSwiper[index] = new Swiper($(this).find('.swiper-container'), {
                        spaceBetween: 13,
                        slidesPerView: 1.2,
                        loop: false,
                        navigation: {
                            nextEl: $(this).find('.swiper-button-next'),
                            prevEl: $(this).find('.swiper-button-prev'),
                        },
                        breakpoints: {
                            500: {
                                slidesPerView: 1.5
                            },
                            640: {
                                slidesPerView: 2
                            },
                            990: {
                                slidesPerView: 3
                            },
                            1200: {
                                slidesPerView: 3,
                                spaceBetween: 30,
                                simulateTouch: false
                            }
                        }
                    });

                    if (productsSwiper[index].isBeginning && productsSwiper[index].isEnd) {
                        $(this).find('.swiper-button-next').hide();
                        $(this).find('.swiper-button-prev').hide();
                    } else {
                        if ($(window).width() >= 990) {
                            $(this).find('.swiper-button-next').show();
                            $(this).find('.swiper-button-prev').show();
                        }
                    }
                });

            }

            if ($('.j-products-swiper-small').length) {
                var productsSwiper = [];

                $('.j-products-swiper-small').each(function (index) {
                    productsSwiper[index] = new Swiper($(this).find('.swiper-container'), {
                        spaceBetween: 13,
                        slidesPerView: 1.2,
                        loop: false,
                        breakpoints: {
                            500: {
                                slidesPerView: 1.5
                            },
                            640: {
                                slidesPerView: 2
                            },
                            990: {
                                slidesPerView: 4
                            },
                            1200: {
                                slidesPerView: 6,
                                spaceBetween: 30,
                                simulateTouch: false
                            }
                        }
                    });
                });

            }

            if ($('.j-products-swiper-offers').length) {
                var productsSwiperOffers = new Swiper('.j-products-swiper-offers .swiper-container', {
                    spaceBetween: 13,
                    slidesPerView: 1.2,
                    loop: false,
                    navigation: {
                        nextEl: '.j-products-swiper-offers .swiper-button-next',
                        prevEl: '.j-products-swiper-offers .swiper-button-prev',
                    },
                    breakpoints: {
                        500: {
                            slidesPerView: 1.5
                        },
                        640: {
                            slidesPerView: 2
                        },
                        990: {
                            slidesPerView: 3
                        },
                        1200: {
                            slidesPerView: 4,
                            spaceBetween: 30
                        }
                    }
                });
            }

            if ($('.j-banner-swiper').length) {
                var productsSwiperOffers = new Swiper('.j-banner-swiper .swiper-container', {
                    slidesPerView: 1,
                    loop: false,
                    autoplay: {
                        delay: 2000,
                    },
                    speed: 1500,
                    pagination: {
                        el: '.j-banner-swiper .swiper-pagination',
                        type: 'bullets',
                        clickable: true
                    },
                    navigation: {
                        nextEl: '.j-banner-swiper .swiper-button-next',
                        prevEl: '.j-banner-swiper .swiper-button-prev',
                    }
                });
            }

            if ($('.js-gallery').length) {
                var galleryMain = [];
                var galleryThumbs = [];

                $('.js-gallery').each(function (index) {
                    galleryMain[index] = new Swiper($(this).find('.js-main-gallery'), {
                        slidesPerView: 1.2,
                        preloadImages: false,
                        lazy: true,
                        loop: true,
                        loopedSlides: 5,
                        spaceBetween: 7,
                        pagination: {
                            el: $(this).find('.js-main-gallery .swiper-pagination'),
                            type: 'bullets',
                            clickable: true
                        },
                        breakpoints: {
                            500: {
                                slidesPerView: 1,
                                spaceBetween: 3
                            }
                        }
                    });

                    galleryThumbs[index] = new Swiper($(this).find('.js-thumbs-gallery'), {
                        slidesPerView: 5,
                        spaceBetween: 5,
                        preloadImages: false,
                        lazy: true,
                        watchSlidesVisibility: true,
                        slidesPerView: 'auto',
                        touchRatio: 0.2,
                        slideToClickedSlide: true,
                        loop: true,
                        loopedSlides: 5,
                        direction: 'vertical'
                    });

                    galleryMain[index].controller.control = galleryThumbs[index];
                    galleryThumbs[index].controller.control = galleryMain[index];
                });
            }

            if ($('.js-gallery-detail').length) {
                var galleryDetailMain = [];
                var galleryDetailThumbs = [];

                $('.js-gallery-detail').each(function (index) {
                    galleryDetailMain[index] = new Swiper($(this).find('.js-main-gallery-detail'), {
                        slidesPerView: 1.2,
                        preloadImages: false,
                        lazy: true,
                        loop: true,
                        spaceBetween: 10,
                        pagination: {
                            el: $(this).find('.js-main-gallery-detail .swiper-pagination'),
                            type: 'bullets',
                            clickable: true
                        },
                        breakpoints: {
                            600: {
                                slidesPerView: 2,
                                spaceBetween: 10
                            },
                            768: {
                                slidesPerView: 1,
                                spaceBetween: 3,
                                loopedSlides: 2
                            },
                            1200: {
                                slidesPerView: 1,
                                spaceBetween: 3,
                                loopedSlides: 4
                            }
                        }
                    });

                    galleryDetailThumbs[index] = new Swiper($(this).find('.js-thumbs-gallery-detail'), {
                        preloadImages: false,
                        lazy: true,
                        spaceBetween: 8,
                        slidesPerView: 2,
                        loopedSlides: 2,
                        touchRatio: 0.2,
                        slideToClickedSlide: true,
                        loop: true,
                        navigation: {
                            nextEl: $(this).find('.js-thumbs-gallery-detail .swiper-button-next'),
                            prevEl: $(this).find('.js-thumbs-gallery-detail .swiper-button-prev'),
                        },
                        breakpoints: {
                            1200: {
                                slidesPerView: 4,
                                spaceBetween: 8,
                                loopedSlides: 4
                            }
                        }
                    });

                    galleryDetailMain[index].controller.control = galleryDetailThumbs[index];
                    galleryDetailThumbs[index].controller.control = galleryDetailMain[index];
                });
            }
        });

        /* Modals */
        require(['fancybox'], function () {
            $('[data-fancybox]').fancybox();
        });

        require(['maskedinput'], function () {
            $("input[name=user_phone]").mask("7 (999) 999-9999");
            //$("input[name=price_from]").mask("999 999 999");
            //$("input[name=price_to]").mask("999 999 999");
            $("#agent-form-phone").mask("7 (999) 999-9999");
            $("#consult-form-phone").mask("7 (999) 999-9999");
        });

        /*require(['select2'], function () {
            $("#main-search").select2();
        });*/

        /* Compare custom scrolls */

        /*if( $('.compare').length ){
          require(['doubleScroll'], function (doubleScroll) {
            $('.compare__inner').doubleScroll({
              resetOnWindowResize: true
            });
          });
        }*/

        function discharge(el) {
            $(el).val(String($(el).val().replace(/[^0-9.]/g, '')).replace(/\B(?=(\d{3})+(?!\d))/g, " "));
        }

        $("input[name=price_to]").keyup(function (e) {
            discharge($(this));
        });
        $("input[name=price_from]").keyup(function (e) {
            discharge($(this));
        });


        /************ Compare : begin ***************/


        $('.send-form').on('click', '.btn', function (e) {
            e.preventDefault();
            let $form = $('.send-form');
            $.ajax({
                method: 'POST',
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function (response) {
                    $form.find('.consult-form__fields').html(response);
                }
            });
        });

        var compareUrl;

        function changeCompareList(action, id, $cntContainer) {
            $.ajax({
                url: compareUrl + '?action=' + action + '&id=' + id,
                method: 'POST',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function (response) {

                    if (response.STATUS === "OK") {
                        //alert('Товар добавлен в список сравнения');
                    } else {
                        alert('Ошибка добавления в список сравнения');
                    }

                    // изменение количества избранных товаров в информационной плашке
                    if (response.COUNT !== '0') {
                        $cntContainer.removeClass('hidden').html(response.COUNT);
                    } else {
                        $cntContainer.addClass('hidden').html(response.COUNT);
                    }
                },
                error: function (request, status, error) {
                }

            });
        }

        function addToCompareList(id, $cntContainer) {
            changeCompareList('ADD_TO_COMPARE_LIST', id, $cntContainer);
        }

        function removeFromCompareList(id, $cntContainer) {
            changeCompareList('DELETE_FROM_COMPARE_LIST', id, $cntContainer);
        }

        $(document).ready(function () {
            $(document).on('click', '.jsToggleCompare', function () {
                var $this = $(this);
                var id = $this.attr('data-prod-id');
                var $cntContainer = $('.compare-cnt');

                compareUrl = $this.attr('href');

                if ($this.hasClass('active')) {
                    $this.removeClass('active');
                    $this.find('.label-compare').html('Сравнить');
                    removeFromCompareList(id, $cntContainer);
                } else {
                    $this.addClass('active');
                    $this.find('.label-compare').html('Удалить из сравнения');
                    addToCompareList(id, $cntContainer);
                }

                return false;
            });

            $('.compare__remove').on('click', function (e) {
                e.preventDefault();
                $(this).parents('.compare__card').hide();
                var $cntContainer = $('.compare-cnt');
                let id = $(this).data('id');
                removeFromCompareList(id, $cntContainer);
            });

            $('.compare__clean').on('click', function (e) {
                e.preventDefault();
                var $cntContainer = $('.compare-cnt');
                $('.compare__card').each(function () {

                    let id = $(this).attr('data-id');
                    removeFromCompareList(id, $cntContainer);
                });
                $('.compare__inner').html('');
            });


        });

        /************ Compare : end ***************/

        /************ Filter : begin ***************/

        $('.main-form__item').change(function (e) {
            let formData = $(this).serialize();
            $.ajax({
                type: 'GET',
                url: '/local/templates/perspektiva/include/filter.php',
                data: formData + '&count=Y',
            })
                .done(function (response) {
                    $('.apply-filter').find('strong').text(response);
                });
        });

        $('input[type=radio][name=type], input[name=not-first], input[name=not-last]').change(function (e) {
            let formData = $(this).parents('form').serialize();
            $.ajax({
                type: 'GET',
                url: '/local/templates/perspektiva/include/filter.php',
                data: formData + '&count=Y',
            })
                .done(function (response) {
                    $('.apply-filter').find('strong').text(response);
                });
        });

        $('.js-dropdown-menu > .dropdown-item').click(function () {
            let $this = $(this);
            setTimeout(function () {
                let form = $($this).parents('form');
                let checkboxes = $(form).find('input:checkbox:checked');
                let checkboxesData = $(checkboxes).serialize();
                let formData = $(form).serialize();
                $.ajax({
                    type: 'GET',
                    url: '/local/templates/perspektiva/include/filter.php',
                    data: formData + '&count=Y',
                })
                    .done(function (response) {
                        $('.apply-filter').find('strong').text(response);
                    });
            }, 1000);
        });

        $('.apply-filter-map').click(function (e) {
            e.preventDefault();
            $(this).parents('form').append('<input type="hidden" name="view" value="map">');
            $(this).parents('form').submit();
        })

        /************ Filter : end ***************/


        /*********** Subscribe : begin *************/

        $("#subscribe_btn").click(function (event) {
            event.preventDefault();
            let formData = $(this).parents('form').serialize();
            let $this = $(this);
            $.ajax({
                type: 'GET',
                url: '/local/templates/perspektiva/ajax/subscribe.php',
                data: formData,
            })
                .done(function (response) {
                    $this.parents('.subscribe__grid-content').html('<p><strong>Спасибо! Вы подписались на рассылку!</strong></p>');
                });
        });

        /*********** Subscribe : end *************/

        $('.write-us__form').on('submit', function (e) {
            e.preventDefault();
            let $form = $(this);
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (response) {
                    $form.find('.write-us__form-cols').html(response);
                }
            });
        });

        $('#ajax_form_flat').on('submit', function (e) {
            e.preventDefault();
            let $form = $(this);
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (response) {
                    $form.find('.modal-body').html(response);
                }
            });
        });

        $('.product__desc-form.desc-form').on('submit', function (e) {
            e.preventDefault();
            let $form = $(this);
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (response) {
                    $form.find('.desc-form__body').html(response);
                }
            });
        });

        $('.calc-wrapper__form > .desc-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (response) {
                    $('.calc-wrapper__form').html(response);
                }
            });
        });

        $('.product__partners-form.consult-form').on('submit', function (e) {
            e.preventDefault();
            let $form = $(this);
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (response) {
                    $form.find('.consult-form__fields').html(response);
                }
            });
        });

        $('.agent-forms__review > .review-form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                method: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (response) {
                    $('.agent-forms__review').html(response);
                }
            });
        });

        $('.agent__rating-buttons > .btn.btn-primary').click(function (e) {
            e.preventDefault();
            let rates = [];
            $('.agent__rating-item').each(function (index) {
                rates[index] = $(this).attr('data-rate-value');
            });
            let textRate = $('.agent__rating-textarea').val();
            let agentID = $('.agentId').val();
            let name = $('#login-name').val();
            if (textRate.length > 0) {
                $.ajax({
                    url: '/local/templates/perspektiva/ajax/agentRates.php',
                    data: {
                        rates: rates,
                        text: textRate,
                        agent: agentID,
                        name: name,
                    },
                    success: function (response) {
                        if (response == 'OK') {
                            $('.agent__rating').html('<p class="agent__rating-title">Спасибо! Ваша оценка принята!</p>');
                        }
                    }
                });
            }
        });

    });

});
