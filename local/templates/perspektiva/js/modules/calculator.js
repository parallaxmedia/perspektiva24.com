if (typeof define === "function" && define.amd) {
  define([], function () {
    var calculator = function(p) {
      var o               = this;
      this.root           = p.root;
      this.cost           = p.cost;
      this.term           = p.term;
      this.firstPay       = p.firstPay;
      this.rate           = p.rate;
      this.monthPay       = p.monthPay;
      this.total          = p.total;
      this.ranges         = p.ranges;

      this.init = function() {
        o.ranges.each( function() {
          var t     = $(this);
          var input = $('input', t);
          t.ionRangeSlider({
            onChange: function (data) {
              input.val(data.input[0].value);
              t.next('input').val(o.priceFormat(data.input[0].value));
              o.calculate();
            },
            prettify: function (n) {
              var result = '';
              if(t.data('valueType') == 'cost' || t.data('valueType') == 'first-pay'){
                if ( n >= 0 && n < 1000 ) {
                  result = Math.ceil(n);
                  result += ' руб.';
                } else if ( n >= 1000 && n < 1000000) {
                  result = Math.ceil(n / 1000);
                  result += ' тыс.';
                } else {
                  result = Math.ceil(n / 1000000);
                  result += ' млн.';
                }
              }else{
                result = n;
                if(result == 1){
                  result += ' год';
                }else{
                  result += ' лет';
                }
              }

              return result;
            }
          });

          var instance = t.data("ionRangeSlider");
          var inputField = t.next('input');
          inputField.on("input", function() {
            var val = parseInt($(this).prop("value").replace(/\s/g, ''));

            if (val < instance.options.min) {
              val = instance.options.min;
            } else if (val > instance.options.max) {
              val = instance.options.max;
            }

            instance.update({
              from: val
            });

            o.calculate();
            $(this).prop("value", o.priceFormat(val));
          });
        });

        o.calculate();
      }

      this.calculate = function() {
        var credit_summ = parseInt(o.cost.val()) - parseInt(o.firstPay.val());
        if (credit_summ > 0) {
          var rate = parseFloat(o.rate.val());
          var month_rate = rate / 12 / 100;

          var term = parseInt(o.term.val()) * 12;
          var common_rate = Math.pow((1 + month_rate), term);
          var month_pay = Math.ceil(credit_summ * month_rate * common_rate / (common_rate - 1));
          var total = Math.ceil(month_pay * term);

          o.monthPay.text(o.priceFormat(month_pay));
          o.total.text(o.priceFormat(credit_summ));
        } else {
          o.monthPay.text('0');
          o.total.text('0');
        }

      }

      this.priceFormat = function(data) {
        var price = Number.prototype.toFixed.call(parseFloat(data) || 0, 2),
              price_sep = price.replace(/[^0-9.]/g, ''),
              price_sep = price_sep.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 "),
              price_sep = price_sep.replace('.00', '');
        return price_sep;
      }

      this.init();
    }

    return calculator;
  });
}
