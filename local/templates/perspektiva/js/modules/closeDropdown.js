if (typeof define === "function" && define.amd) {
  define([], function () {
    var closeDropdown = function(p){
      var o               = this;
      this.root           = p.root;
      this.btnClose       = p.btnClose;

      this.init = function(){
        o.btnClose.click(function (e) {
          e.preventDefault();
          o.root.removeClass('show');
          o.root.find('.dropdown-toggle').attr('aria-expanded', 'false');
          o.root.find('.dropdown-menu').removeAttr('x-placement');
          o.root.find('.dropdown-menu').removeClass('show');
        });
      }

      this.init();
    }

    return closeDropdown;
  });
}
