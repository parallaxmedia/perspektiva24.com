if (typeof define === "function" && define.amd) {
  define([], function () {
    var formDropdownCheckboxes = function(p){
      var o     = this;
      this.root = p.root;
      this.item = p.item;

      this.init = function(){
        var options = [];
        o.item.on( 'click', function( event ) {
          var $target = $( event.currentTarget );
          var val = $target.attr( 'data-value' );
          var $inp = $target.find( 'input' );
          var idx;

          if ( ( idx = options.indexOf( val ) ) > -1 ) {
            options.splice( idx, 1 );
            //setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
            $inp.prop( 'checked', false );
            $inp.removeAttr('checked');
          } else {
            options.push( val );
            //setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
            $inp.prop( 'checked', true );
            $inp.attr('checked', 'checked');
          }

           $( event.target ).blur();

           return false;
        });
      }

      this.init();
    }

    return formDropdownCheckboxes;
  });
}
