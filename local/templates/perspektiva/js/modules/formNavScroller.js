if (typeof define === "function" && define.amd) {
  define([], function () {
    var formNavScroller = function(p){
      var o         = this;
      this.target   = p.target;
      this.prev     = p.prev;
      this.next     = p.next;
      this.interval = p.interval;

      this.init = function(){
        o.prev.click(function (e) {
          e.preventDefault();
          o.goScroll('left');
        });

        o.next.click(function (e) {
          e.preventDefault();
          o.goScroll('right');
        });

        o.target.swipe({
          swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
            if (direction == 'left') {
              o.goScroll('right');
            } else if (direction == 'right') {
              o.goScroll('left');
            }
          }
        });
      }

      this.goScroll = function(direction){
        var scrollValue = '+=' + o.interval;
        if (direction == 'left') {
          scrollValue = '-=' + o.interval;
        }

        o.target.animate({
          scrollLeft: scrollValue
        }, 1000);
      }

      this.init();
    }

    return formNavScroller;
  });
}
