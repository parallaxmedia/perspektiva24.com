if (typeof define === "function" && define.amd) {
  define([], function () {
    var mainMenu = function(p){
      var o               = this;
      this.root           = p.root;
      this.control        = p.control;
      this.parentControl  = p.parentControl;

      this.init = function(){
        o.control.click(function (e) {
          e.preventDefault();
          o.root.toggleClass('active');
        });

        if( $(window).width() < 990 ){
          o.parentControl.click(function (e) {
            e.preventDefault();
            $(this).parent('li').toggleClass('active');
            $(this).parent('li').find('.main-menu__sub:first').slideToggle();
          });
        }
      }

      this.init();
    }

    return mainMenu;
  });
}
