if (typeof define === "function" && define.amd) {
  define([], function () {
    var regionsFilter = function(p){
      var o               = this;
      this.root           = p.root;
      this.search         = p.search;
      this.items          = p.item;
      this.emptyLabel     = p.emptyLabel;
      this.clearBtn       = p.clearBtn;
      this.divider        = p.divider;
      this.tag            = p.tag;

      this.init = function () {
        o.search.on('keyup',function (e) {
          if ($(this).val().length > 0) {
            var searchWord = $(this).val().toLowerCase();

            o.clearTags();
            o.showAllCity();
            o.toggleHiddenClass(o.clearBtn, false);

            o.items.filter(function(){
              var currentCity = $(this).data('city').toLowerCase();
              if( !currentCity.startsWith(searchWord) ){
                return $(this);
              }
            }).addClass('hidden');

            o.toggleHiddenClass(o.divider, true);

            if (o.items.filter(".hidden" ).length == o.items.length) {
              o.toggleHiddenClass(o.emptyLabel, false);
            } else {
              o.toggleHiddenClass(o.emptyLabel, true);
            }
          } else {
            o.showAllCity();
            o.toggleHiddenClass(o.clearBtn, true);
            o.toggleHiddenClass(o.divider, false);
          }
        });

        o.clearBtn.click( function(e) {
          e.preventDefault();
          o.search.val('');
          o.showAllCity();
          o.toggleHiddenClass(o.clearBtn, true);
        });

        o.tag.click( function(e) {
          e.preventDefault();
          if( !$(this).hasClass('active') ){
            o.clearTags();
            o.clearSearch();
            o.toggleHiddenClass(o.divider, true);
            $(this).addClass('active');

            var currentID = $(this).data('id');
            if (currentID != 'cc_1') {
              o.toggleHiddenClass(o.divider, true);
            } else {
              o.toggleHiddenClass(o.divider, false);
            }

            o.items.filter(function(){
              if( $(this).data('id') !== currentID ){
                return $(this);
              }
            }).addClass('hidden');
          }else{
            $(this).removeClass('active');
            o.clearTags();
            o.clearSearch();
          }
        });
      }

      this.clearTags = function () {
        o.tag.each( function () {
          if ( $(this).hasClass('active') ) $(this).removeClass('active');
        });
      }

      this.clearSearch = function () {
        o.search.val('');
        o.showAllCity();
        o.toggleHiddenClass(o.clearBtn, true);
        o.toggleHiddenClass(o.emptyLabel, true);
        o.toggleHiddenClass(o.divider, false);
      }

      this.showAllCity = function () {
        o.toggleHiddenClass(o.emptyLabel, true);
        o.toggleHiddenClass(o.divider, false);
        o.toggleHiddenClass(o.items, false);
      }

      this.toggleHiddenClass = function (target, flag) {
        if (flag) {
          target.addClass('hidden');
        } else {
          target.removeClass('hidden');
        }
      }

      this.init();
    }

    return regionsFilter;
  });
}
