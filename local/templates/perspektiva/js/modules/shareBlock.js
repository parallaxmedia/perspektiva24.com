if (typeof define === "function" && define.amd) {
  define([], function () {
    var shareBlock = function(p){
      var o               = this;
      this.root           = p.root;
      this.control        = p.control;

      this.init = function(){
        o.control.click(function (e) {
          e.preventDefault();
          o.root.toggleClass('opened');
        });
      }

      this.init();
    }

    return shareBlock;
  });
}
