if (typeof define === "function" && define.amd) {
  define([], function () {
    var togglePhone = function(p){
      var o               = this;
      this.root           = p.root;
      this.control        = p.control;
      this.item           = p.item;

      this.init = function(){
        o.control.click(function (e) {
          e.preventDefault();
          o.control.hide();
          o.item.show();
        });
      }

      this.init();
    }

    return togglePhone;
  });
}
