﻿<!DOCTYPE html>
<html lang="ru">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="images/favicon.ico">
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="//st.yagla.ru/js/y.c.js?h=40616739d06e2b4b23e3a1ddd4b1f541"></script>
        <script type="text/javascript">
            !function(){
                var t=document.createElement("script");
                t.type="text/javascript",
                t.async=!0,
                t.src="https://vk.com/js/api/openapi.js?159",
                t.onload=function(){
                    VK.Retargeting.Init("VK-RTRG-305950-5ANF9"),
                    VK.Retargeting.Hit()},
                    document.head.appendChild(t)}();
        </script>
        <noscript>
            <img src="https://vk.com/rtrg?p=VK-RTRG-305950-5ANF9" style="position:fixed; left:-999px;" alt=""/>
        </noscript>
        <script>
        (function(w, d, s, h, id) {
            w.roistatProjectId = id; w.roistatHost = h;
            var p = d.location.protocol == "https:" ? "https://" : "http://";
            var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
            var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
        })(window, document, 'script', 'cloud.roistat.com', '3025cb76e897262e51ba6ff5c3e19f4c');
        </script>
</head>
<body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter49190740 = new Ya.Metrika2({
                    id:49190740,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49190740" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124840611-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-124840611-1');
</script>

<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.roistat_visit=[^;]+(.)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', '1c6aa3ab878735132a1a93af19e386cb');
</script>
<div class="loaderArea">
    <div class="clear-load loader">
        <span></span>
    </div>
</div>
<div class="bg yagla-216821" style="background-image: url(images/bg.jpg)">
    <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <a href="index.php" class="company">Отдел продаж новостроек в г.Уфа</a>
                    </div>
                    <div class="col-md-4">
                        <span class="city">Республика Башкортостан г.Уфа</span>
                    </div>
                    <div class="col-md-4">
                        <a href="mailto:cool.offers@ya.ru" class="phone">e-mail: cool.offers@ya.ru</a>
                        <a href="tel:+73472242058" class="phone">тел.: +7(347) 224-20-58</a>
                    </div>
                </div>
            </div>        
    </header>
    <div class="top-content">
        <div class="flex-center">
             <div class="container">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="composition">                       
                            <h1>Поможем приобрести квартиру по ценам застройщика и ниже</h1>
                            <p>Пройдите тест и получите лучшее предложение по новостройкам в Уфе</p>
                            <button type="button" id="GetOffers" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter" onclick="gtag('event', 'submitForm', { 'event_category': 'submit', 'event_action': 'GetOffers', }); return true;">Получить предложения</button>
                        </div> 
                    </div>
                </div>
            </div>   
        </div>
    </div>
    <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <p class="company footer-company">ООО «Недвижимость Уфы»</p>
                    </div>
                    <div class="col-md-4">
                        <span class="city">Адрес: г.Уфа, ул.Заки Валиди, д.64</span>
                    </div>
                </div>
            </div>  
    </footer>
    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Пройдите тест и узнайте какая квартира Вам подходит</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 20%;" aria-valuenow="20" aria-valuemin="20" aria-valuemax="100">20%</div>
                    </div>
                <form class="search-form" action='' data-toggle="validator" id="ajax_form" method="post">
                    <div class="owl-carousel">
                        <div>
                            <h5 class="poll">Выбери количество комнат</h5>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                 <input type="radio" name="Radios1" value="Студия" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit2', 'event_action': 'rooms', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>Студия
                                             </label>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                 <input type="radio" name="Radios1" value="1 комнатная" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit2', 'event_action': 'rooms', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>1 комнатная
                                             </label>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Radios1" value="2 комнатная" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit2', 'event_action': 'rooms', }); return true;">
                                                <i class="fa fa-2x icon-radio"></i>2 комнатная
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                             <label>
                                                 <input type="radio" name="Radios1" value="3 комнатная" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit2', 'event_action': 'rooms', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>3 комнатная
                                             </label>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                             <label>
                                                 <input type="radio" name="Radios1" value="4 комнатная" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit2', 'event_action': 'rooms', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>4 комнатная
                                             </label>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                             <label>
                                                 <input type="radio" name="Radios1" value="5 и более комнат" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit2', 'event_action': 'rooms', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>5 и более комнат
                                             </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h5 class="poll">Ваш бюджет</h5>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                 <input type="radio" name="Radios2" value="от 1 до 2 млн. рублей" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit3', 'event_action': 'price', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>от 1 до 2 млн. рублей
                                             </label>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                 <input type="radio" name="Radios2" value="от 2 до 3 млн. рублей" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit3', 'event_action': 'price', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>от 2 до 3 млн. рублей
                                             </label>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Radios2" value="от 3 до 4 млн. рублей" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit3', 'event_action': 'price', }); return true;">
                                                <i class="fa fa-2x icon-radio"></i>от 3 до 4 млн. рублей
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                             <label>
                                                 <input type="radio" name="Radios2" value="от 4 до 5 млн. рублей" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit3', 'event_action': 'price', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>от 4 до 5 млн. рублей
                                             </label>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                             <label>
                                                 <input type="radio" name="Radios2" value="свыше 5 млн. рублей" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit3', 'event_action': 'price', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>свыше 5 млн. рублей
                                             </label>
                                         </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h5 class="poll">Вид бюджета</h5>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                 <input type="radio" name="Radios3" value="наличные" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit4', 'event_action': 'typeprice', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>наличные
                                             </label>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                 <input type="radio" name="Radios3" value="ипотека" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit4', 'event_action': 'typeprice', }); return true;">
                                                 <i class="fa fa-2x icon-radio"></i>ипотека
                                             </label>
                                         </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Radios3" value="наличные + ипотека" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit4', 'event_action': 'typeprice', }); return true;">
                                                <i class="fa fa-2x icon-radio"></i>наличные + ипотека
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Radios3" value="материнский капитал" class="customNextBtn" onclick="gtag('event', 'submitForm', { 'event_category': 'submit4', 'event_action': 'typeprice', }); return true;">
                                                <i class="fa fa-2x icon-radio"></i>материнский капитал
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h5 class="poll">Желаемый район проживания</h5>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Центр">
                                                <i class="fa fa-2x icon-checkbox"></i>Центр
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Зеленая роща">
                                                <i class="fa fa-2x icon-checkbox"></i>Зеленая роща
                                            </div>
                                         </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Проспект">
                                                <i class="fa fa-2x icon-checkbox"></i>Проспект
                                            </div>
                                         </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Сипайлово">
                                                <i class="fa fa-2x icon-checkbox"></i>Сипайлово
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Черниковка">
                                                <i class="fa fa-2x icon-checkbox"></i>Черниковка
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Дёма">
                                                <i class="fa fa-2x icon-checkbox"></i>Дёма
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Затон">
                                                <i class="fa fa-2x icon-checkbox"></i>Затон
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Инорс">
                                                <i class="fa fa-2x icon-checkbox"></i>Инорс
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Шакша">
                                                <i class="fa fa-2x icon-checkbox"></i>Шакша
                                             </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Загородом">
                                                <i class="fa fa-2x icon-checkbox"></i>Загородом
                                             </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h5 class="poll">Отлично. Последний шаг!</h5>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                    <p>Заполни форму ниже для получения результатов теста</p>
                                    <div id="result_form" style="color: #DD0000; font-size: 18px;"></div>
                                    </div>
                                    <div class="col-md-7">
                                        <!--  -->
                                        <div class="form-group row">
                                            <label for="exampleInputName2" class="col-sm-3 col-form-label">Ваше имя</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" class="form-control required" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-tel-input" class="col-sm-3 col-form-label">Телефон</label>
                                            <div class="col-sm-9">
                                                <input class="form-control user-phone" type="phone" name="phone" id="phone" placeholder="">
                                            </div>
                                        </div>
                                        <input type="hidden" name="utm_medium" value="<?php echo $_GET['utm_medium'];?>">
                                        <input type="hidden" name="utm_term" value="<?php echo $_GET['utm_term'];?>">
                                        <input type="hidden" name="utm_source" value="<?php echo $_GET['utm_source'];?>">
                                        <input type="hidden" name="utm_content" value="<?php echo $_GET['utm_content'];?>">
                                        <input type="hidden" name="utm_phrase" value="<?php echo $_GET['utm_phrase'];?>">
                                        <input type="hidden" name="utm_banner" value="<?php echo $_GET['utm_banner'];?>">
                                        <input type="hidden" name="utm_campaign" value="<?php echo $_GET['utm_campaign'];?>">
                                        <input type="hidden" name="formid" value="<?php echo $_GET['formid'];?>">
                                        <input type="hidden" name="tranid" value="<?php echo $_GET['tranid'];?>">
                                        <!--  -->
                                        <button type="submit" id="btn" class="btn btn-primary" disabled onclick="yaCounter49190740.reachGoal('offer'); yaglaaction('offer'); gtag('event', 'submitForm', { 'event_category': 'submit6', 'event_action': 'contact', }); return true;">Отправить заявку</button>
                                        <!--  -->
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary customPrevBtn prev" style="visibility: hidden">Назад</button>
                    <button type="button" class="btn btn-outline-primary customNextBtn next" style="visibility: hidden">Далее</button>
                </div>
            </div>
        </div>
    </div>
</div>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

        <!-- <script src="js/bootstrap.min.js"></script> -->
        <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/validator.min.js"></script>
        <script src="js/jquery.maskedinput.min.js"></script>
        <script src="js/ajax.js"></script>
        <script src="js/script.js"></script>
</body>
</html>