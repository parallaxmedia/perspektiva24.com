$(window).on('load', function(){
    $preloader = $('.loaderArea'),
    $loader = $preloader.find('.loader');
    $loader.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});
$(document).ready(function(){
  var owl = $('.owl-carousel');
  owl.owlCarousel({
    items:1,
    mouseDrag: false,
    touchDrag: false,
    nav: false,
    dots: false,    
  });

  $('.customNextBtn').click(function() {
    var val = parseInt(document.querySelector(".progress-bar").style.width);
    if(val>=20){
      $(".prev").css("visibility", "visible");
    }
    val = val + 20;
    if(val==80){
      $(".next").css("visibility", "visible");
    }
    if(val==100){
      $(".next").css("visibility", "hidden");
      gtag('event', 'submitForm', { 'event_category': 'submit5', 'event_action': 'locality', });
    }
    val = val + "%";    
    document.querySelector(".progress-bar").style.width = val;
    document.querySelector(".progress-bar").innerHTML = val;
    owl.trigger('next.owl.carousel');
  });

  $('.customPrevBtn').click(function() {    
    var val = parseInt(document.querySelector(".progress-bar").style.width);
    if(val>100){val = 100;}
    if(val==40){
      $(".prev").css("visibility", "hidden");
    }
    val = val - 20;
    if(val==80){
      $(".next").css("visibility", "visible");
    }
    val = val + "%";    
    document.querySelector(".progress-bar").style.width = val;
    document.querySelector(".progress-bar").innerHTML = val;
    owl.trigger('prev.owl.carousel');
  });
  $('#ajax_form').validator({
    //
  });  
});

var $body;
$(document).ready(function(){
    $body = $('body');
    // $body
    //   .find('.user-phone').each(function(){
    //       $(this).mask("+7(999) 999-99-99", {autoсlear: false});
    //   });
    $body.on('keyup','.user-phone',function(){
      var phone = $(this),
          phoneVal = phone.val(),
          form = $(this).parents('form');
      if (phoneVal == '') {
        form.find('.btn').attr('disabled',true);
      } else {
        form.find('.btn').removeAttr('disabled');
      }
    });
});