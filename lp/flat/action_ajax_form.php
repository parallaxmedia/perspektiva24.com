<?php
    $contact_name = addslashes(htmlspecialchars(trim($_POST['name'])));     //Имя
    $contact_phone = addslashes(htmlspecialchars(trim($_POST['phone'])));   //Телефон
    $Radios1 = addslashes(htmlspecialchars(trim($_POST['Radios1'])));       //Вид сделки
    $Radios2 = addslashes(htmlspecialchars(trim($_POST['Radios2'])));       //Вид обьекта недвижимости
    $Radios3 = addslashes(htmlspecialchars(trim($_POST['Radios3'])));       //Желаема цена продажи, Ваш бюджет
    $Radios5 = addslashes(htmlspecialchars(trim($_POST['Radios5'])));       //Вариант обмена
    $Radios6 = addslashes(htmlspecialchars(trim($_POST['Radios6'])));  //В каком районе ваша недвижимость?
    $checkbox1 = addslashes(htmlspecialchars(implode(', ', $_POST['checkbox1']))); //Желаемый район проживания / Какой район обмена рассматриваете / В каком районе ваша недвижимость

    if(!$contact_name || !$contact_phone) return;

    $message = "Вид сделки: ".$Radios1;
    if($Radios1 =='Продажа'){
        $message1 = $message."\nВид объекта недвижимости: ".$Radios2."\nЖелаемая цена продажи: ".$Radios3."\nВ каком районе ваша недвижимость: ".$checkbox1;  
    }
    if($Radios1 =='Покупка'){
        $message1 = $message."\nВид объекта недвижимости: ".$Radios2."\nВаш бюджет: ".$Radios3."\nЖелаемый район проживания: ".$checkbox1;
    }
    if($Radios1 =='Обмен'){
        $message1 = $message."\nВариант обмена: ".$Radios5."\nВ каком районе ваша недвижимость: ".$Radios6."\nКакой район обмена рассматриваете: ".$checkbox1;
    }
     $message = "Имя: ".$contact_name."\nТелефон: ".$contact_phone."\n".$message1;
    //$to= 'airat92.92@gmail.com';
    $to= 'zayavki24@inbox.ru, agenstvovariant@yandex.ru,Mithat@mail.ru';
    $subject = "Новая заявка с сайта все-квартиры24.рф";
    $headers = "From: info@все-квартиры24.рф \r\n";
    mail($to, $subject, $message, $headers);


//AMOCRM -------------------------------------------------------------------------------------------
//ПРЕДОПРЕДЕЛЯЕМЫЕ ПЕРЕМЕННЫЕ

//-------------------------------------------------------------------------------------------
$lead_name = 'Заявка с сайта все-квартиры24.рф'; //Название добавляемой сделки
$responsible_user_id = 1538452; //id ответственного по сделке, контакту, компании

if($Radios1 == "Покупка"){
    $pipeline_id = 1707934;
}else{
    $pipeline_id = 1707937; // id воронки Продажа
}

//АВТОРИЗАЦИЯ
$subdomain='anperspektiva';
$user=array(
    'USER_LOGIN'=>'rim.khasanov2017@yandex.ru', #Ваш логин (электронная почта)
    'USER_HASH'=>'07290f0c42f7b11a83abfadba9f956dc38afe220' #Хэш для доступа к API (смотрите в профиле пользователя)
);

#Формируем ссылку для запроса
$link='https://'.$subdomain.'.amocrm.ru/private/api/auth.php?type=json';
$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_POST,true);
curl_setopt($curl,CURLOPT_POSTFIELDS,http_build_query($user));
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
curl_close($curl);  #Завершаем сеанс cURL
$Response=json_decode($out,true);
//echo '<b>Авторизация:</b>'; echo '<pre>'; print_r($Response); echo '</pre>';
//---------------------------------------------------------------------------------------------------------------------
//ПОЛУЧАЕМ ДАННЫЕ АККАУНТА
$link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/accounts/current'; #$subdomain

$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
curl_close($curl);
$Response=json_decode($out,true);
$account=$Response['response']['account'];
//----------------------------------
// $account_users=$Response['response']['account']['users'];
// $length_account_users=count($account_users);
//echo '<b>Данные аккаунта:</b>'; echo '<pre>'; print_r($account); echo '</pre>';
// $number=mt_rand(0, $length_account_users-1);
//echo '<pre>'; print_r($account_users[$number]['id']); echo '</pre>';
//$responsible_user_id = $account_users[$number]['id'];
//----------------------------------
//echo '<b>Данные аккаунта:</b>'; echo '<pre>'; print_r($Response); echo '</pre>';
//---------------------------------------------------------------------------------------------------------------------
//ПОЛУЧАЕМ СУЩЕСТВУЮЩИЕ ПОЛЯ
$amoAllFields = $account['custom_fields']; //Все поля
//echo '<b>Все поля из амо:</b>'; echo '<pre>'; print_r($amoAllFields); echo '</pre>';
$amoConactsFields = $account['custom_fields']['contacts']; //Поля контактов
//echo '<b>Поля из амо:</b>'; echo '<pre>'; print_r($amoConactsFields); echo '</pre>';
//ФОРМИРУЕМ МАССИВ С ЗАПОЛНЕННЫМИ ПОЛЯМИ КОНТАКТА
//Стандартные поля амо:
$sFields = array_flip(
    array(
        'PHONE', //Телефон. Варианты: WORK, WORKDD, MOB, FAX, HOME, OTHER
    )
);
//Проставляем id этих полей из базы амо
foreach($amoConactsFields as $afield) {
    if(isset($sFields[$afield['code']])) {
        $sFields[$afield['code']] = $afield['id'];
    }
}
//ДОБАВЛЯЕМ СДЕЛКУ
$lead = array(
        'name' => $lead_name,
        // 'status_id' => $lead_status_id, //id статуса
        'responsible_user_id' => $responsible_user_id, //id ответственного по сделке
        'pipeline_id' => $pipeline_id,
        'tags' => 'все-квартиры'
);
$lead['custom_fields'][]=array(
    'id'=>'473885',
    'values'=>array(
        array(
             'value'=>940257
        )
    )
);
$leads['request']['leads']['add'][]=$lead;
$link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/leads/set';
$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($leads));
curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
$Response=json_decode($out,true);
//echo '<b>Новая сделка:</b>'; echo '<pre>'; print_r($Response); echo '</pre>';
if(is_array($Response['response']['leads']['add']))
    foreach($Response['response']['leads']['add'] as $lead) {
        $lead_id = $lead["id"]; //id новой сделки
    };
//ДОБАВЛЯЕМ СДЕЛКУ - КОНЕЦ
//---------------------------------------------------------------------------------------------------------------------
//ДОБАВЛЯЕМ ПРИМЕЧАНИЕ
$data = array (
      'element_id' => $lead_id,
      'element_type' => '2',
      'text' => $message1,
      'note_type' => '4',

);
$set['request']['notes']['add'][]=$data;
$link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/notes/set';
$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($set));
curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
$Response=json_decode($out,true);
//echo '<b>Новое примечание:</b>'; echo '<pre>'; print_r($Response); echo '</pre>';
//ДОБАВЛЯЕМ ПРИМЕЧАНИЕ - КОНЕЦ
//---------------------------------------------------------------------------------------------------------------------
//ДОБАВЛЕНИЕ КОНТАКТА
        $contact = array(
            'name' => $contact_name,
            'linked_leads_id' => array($lead_id), //id сделки
            'responsible_user_id' => $responsible_user_id, //id ответственного
            'custom_fields'=>array(
                array(
                    'id' => $sFields['PHONE'],
                    'values' => array(
                        array(
                            'value' => $contact_phone,
                            'enum' => 'MOB'
                        )
                    )
                ),
            )
        );
        $set['request']['contacts']['add'][]=$contact;
        #Формируем ссылку для запроса
        $link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/contacts/set';
        $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
        #Устанавливаем необходимые опции для сеанса cURL
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL,$link);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
        curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($set));
        curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($curl,CURLOPT_HEADER,false);
        curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
        $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
        $Response=json_decode($out,true);
//ДОБАВЛЕНИЕ КОНТАКТА - КОНЕЦ
?>

