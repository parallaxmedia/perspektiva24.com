<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<?
    $contact_name = 'Тест';
    $contact_phone = '+79279999999';
    $user_message = 'Тестовое сообщение'; // Сообщение
    $user_budget = '4000000'; // Укажите Ваш примерный бюджет:
    $user_district = 'Центр'; // Укажите район:
    $user_type = 'Квартира'; // Выберите тип обьекта:
    $user_object = 'Машина'; // Укажите на что хотели бы обменять Ваш объект:
    $user_paytype = 'Наличные'; // Выберите способ оплаты:
    $message = '';
    if($user_message !=''){
        $message = $message."\nСообщение: ".$user_message;  
    }
    if($user_budget !=''){
        $message = $message."\nПримерный бюджет: ".$user_budget;  
    }
    if($user_district !=''){
        $message = $message."\nРайон: ".$user_district;  
    }
    if($user_type !=''){
        $message = $message."\nТип обьекта: ".$user_type;  
    }    
    if($user_object !=''){
        $message = $message."\nНа что хотели бы обменять Ваш объект: ".$user_object;  
    }
    if($user_paytype !=''){
        $message = $message."\nСпособ оплаты: ".$user_paytype;  
    }

    $link = 'perspektiva24.com/sell/';
    if(strpos($link, 'buy')){
        $pipeline_id = '1707934'; // id воронки
        $lead_name = 'Заявка с сайта perspektiva24.com/buy/'; //Название добавляемой сделки
        $custom = 'perspektiva24.com/buy';
    }elseif(strpos($link, 'sell')) {
        $pipeline_id = '1707937'; // id воронки
        $lead_name = 'Заявка с сайта perspektiva24.com/sell/'; //Название добавляемой сделки
        $custom = 'perspektiva24.com/sell/';
    }elseif(strpos($link, 'exchange')) {
        $pipeline_id = '1707937'; // id воронки
        $lead_name = 'Заявка с сайта perspektiva24.com/exchange/'; //Название добавляемой сделки
        $custom = 'perspektiva24.com/exchange/';       
    }

    $responsible_user_id = 1538452; //id ответственного по сделке, контакту, компании

    //АВТОРИЗАЦИЯ
    $subdomain='anperspektiva';
    $user=array(
        'USER_LOGIN'=>'analitik@perspektiva24.com', #Ваш логин (электронная почта)
        'USER_HASH'=>'8d7438f9994c26c1c4d8bf29d22f4c04f165f72c' #Хэш для доступа к API (смотрите в профиле пользователя)
    );

    #Формируем ссылку для запроса
    $link='https://'.$subdomain.'.amocrm.ru/private/api/auth.php?type=json';
    $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
    #Устанавливаем необходимые опции для сеанса cURL
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    curl_setopt($curl,CURLOPT_POST,true);
    curl_setopt($curl,CURLOPT_POSTFIELDS,http_build_query($user));
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
    $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
    $code=curl_getinfo($curl,CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
    curl_close($curl);  #Завершаем сеанс cURL
    $Response=json_decode($out,true);
    //echo '<b>Авторизация:</b>'; echo '<pre>'; print_r($Response); echo '</pre>';
    //---------------------------------------------------------------------------------------------------------------------
    //ПОЛУЧАЕМ ДАННЫЕ АККАУНТА
    $link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/accounts/current'; #$subdomain

    $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
    #Устанавливаем необходимые опции для сеанса cURL
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
    $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
    $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
    curl_close($curl);
    $Response=json_decode($out,true);
    $account=$Response['response']['account'];
    //-------
    //ПОЛУЧАЕМ СУЩЕСТВУЮЩИЕ ПОЛЯ
    $amoAllFields = $account['custom_fields']; //Все поля
    $amoConactsFields = $account['custom_fields']['contacts']; //Поля контактов
    //ФОРМИРУЕМ МАССИВ С ЗАПОЛНЕННЫМИ ПОЛЯМИ КОНТАКТА
    //Стандартные поля амо:
    $sFields = array_flip(
        array(
            'PHONE', //Телефон. Варианты: WORK, WORKDD, MOB, FAX, HOME, OTHER
        )
    );
    //Проставляем id этих полей из базы амо
    foreach($amoConactsFields as $afield) {
        if(isset($sFields[$afield['code']])) {
            $sFields[$afield['code']] = $afield['id'];
        }
    }
    //ДОБАВЛЯЕМ СДЕЛКУ
    $lead=array(
            'name' => $lead_name,
            // 'status_id' => $lead_status_id, //id статуса
            'responsible_user_id' => $responsible_user_id, //id ответственного по сделке
            'pipeline_id' => $pipeline_id,
            'tags' => 'perspektiva24',
    );
    $lead['custom_fields'][]=array(
        'id'=>'471087',
        'values'=>array(
            array(
                'value'=>$custom
            )
        )
    );
    $leads['request']['leads']['add'][]=$lead;
    $link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/leads/set';
    $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
    #Устанавливаем необходимые опции для сеанса cURL
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($leads));
    curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
    $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
    $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
    $Response=json_decode($out,true);
    if(is_array($Response['response']['leads']['add']))
        foreach($Response['response']['leads']['add'] as $lead) {
            $lead_id = $lead["id"]; //id новой сделки
        };
    //ДОБАВЛЯЕМ СДЕЛКУ - КОНЕЦ
    //
    //ДОБАВЛЯЕМ ПРИМЕЧАНИЕ
    $data = array (
          'element_id' => $lead_id,
          'element_type' => '2',
          'text' => $message,
          'note_type' => '4',

    );
    $set['request']['notes']['add'][]=$data;
    $link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/notes/set';
    $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
    #Устанавливаем необходимые опции для сеанса cURL
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($set));
    curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
    $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
    $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
    $Response=json_decode($out,true);
    //ДОБАВЛЯЕМ ПРИМЕЧАНИЕ - КОНЕЦ
    //---------------------------------------------------------------------------------------------------------------------
    //ДОБАВЛЕНИЕ КОНТАКТА
    $contact = array(
        'name' => $contact_name,
        'linked_leads_id' => array($lead_id), //id сделки
        'responsible_user_id' => $responsible_user_id, //id ответственного
        'custom_fields'=>array(
            array(
                'id' => $sFields['PHONE'],
                'values' => array(
                    array(
                        'value' => $contact_phone,
                        'enum' => 'MOB'
                    )
                )
            ),
        )
    );
    $set['request']['contacts']['add'][]=$contact;
    #Формируем ссылку для запроса
    $link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/contacts/set';
    $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
    #Устанавливаем необходимые опции для сеанса cURL
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($set));
    curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
    $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
    $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
    $Response=json_decode($out,true);
?>    
</body>
</html>

