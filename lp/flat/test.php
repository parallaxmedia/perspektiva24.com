<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
<?
//AMOCRM -------------------------------------------------------------------------------------------
//ПРЕДОПРЕДЕЛЯЕМЫЕ ПЕРЕМЕННЫЕ
// $responsible_user_id = 1602082; //id ответственного по сделке, контакту, компании
$lead_name = 'Заявка с сайта'; //Название добавляемой сделки
// $lead_status_id = 'status_id_unsorted'; //id этапа продаж, куда помещать сделку
$pipeline_id = 966106;

$contact_name = $name; //Имя добавляемого контакта
$contact_phone = $phone; //Телефон контакта
// $contact_email = isset($_POST['email']) ? $_POST['email'] : 'sd'; //Емейл контакта
$contact_message = $body_form; //Сообщение контакта


//АВТОРИЗАЦИЯ
$subdomain='anperspektiva';
$user=array(
    'USER_LOGIN'=>'analitik@perspektiva24.com', #Ваш логин (электронная почта)
    'USER_HASH'=>'8d7438f9994c26c1c4d8bf29d22f4c04f165f72c' #Хэш для доступа к API (смотрите в профиле пользователя)
);

#Формируем ссылку для запроса
$link='https://'.$subdomain.'.amocrm.ru/private/api/auth.php?type=json';
$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_POST,true);
curl_setopt($curl,CURLOPT_POSTFIELDS,http_build_query($user));
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
curl_close($curl);  #Завершаем сеанс cURL
$Response=json_decode($out,true);
echo '<b>Avtorizacia:</b>'; echo '<pre>'; print_r($Response); echo '</pre>';
//---------------------------------------------------------------------------------------------------------------------
//ПОЛУЧАЕМ ДАННЫЕ АККАУНТА
$link='https://'.$subdomain.'.amocrm.ru/private/api/v2/json/accounts/current'; #$subdomain уже объявляли выше

$curl=curl_init(); #Сохраняем дескриптор сеанса cURL
#Устанавливаем необходимые опции для сеанса cURL
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
curl_setopt($curl,CURLOPT_URL,$link);
curl_setopt($curl,CURLOPT_HEADER,false);
curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
$out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
$code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
curl_close($curl);
$Response=json_decode($out,true);
$account=$Response['response']['account'];
//----------------------------------
$account_users=$Response['response']['account']['users'];
$length_account_users=count($account_users);
//echo '<b>Данные аккаунта:</b>'; echo '<pre>'; print_r($account_users); echo '</pre>';
//$number=mt_rand(0, $length_account_users-1);
//echo '<pre>'; print_r($account_users[$number]['id']); echo '</pre>';
//$responsible_user_id = $account_users[$number]['id'];
//----------------------------------
echo '<b>Данные аккаунта:</b>'; echo '<pre>'; print_r($Response); echo '</pre>';
//---------------------------------------------------------------------------------------
//ПОЛУЧАЕМ СУЩЕСТВУЮЩИЕ ПОЛЯ
$amoAllFields = $account['custom_fields']; //Все поля
//echo '<b>Все поля из амо:</b>'; echo '<pre>'; print_r($amoAllFields); echo '</pre>';
$amoConactsFields = $account['custom_fields']['contacts']; //Поля контактов
echo '<b>Поля из контактов:</b>'; echo '<pre>'; print_r($amoConactsFields); echo '</pre>';
?>
</body>
</html>