		<div class="copyr_bl">
			<div class="copyr_txt">
				<?=(strpos($_SERVER['SCRIPT_NAME'], '/quiz.php') ? '' : '© 2019, ООО "ПЕРСПЕКТИВА24"')?>
			</div>
		</div>
	</div>
</div>
<div class="pop_bl" id="fb">
    <div class="pop_bg close_pop"></div>
    <div class="pop_cont">
        <div class="cl_btn close_pop"></div>
		<div class="pop_name">Обратный звонок</div>
		<div class="pop_descr">Оставьте заявку, мы свяжемся <br>с Вами в ближайшее время</div>
		<form class="fb_form">
			<input type="hidden" name="type" value="fb">
			
			<div class="input_bl">
				<input type="text" name="name" placeholder="Имя">
			</div>
			<div class="input_bl">
				<input type="tel" name="phone" required="" placeholder="Телефон">
			</div>
			<button class="s_btn r_btn fb_roi" type="submit"><i></i><span>заказать звонок</span></button>
		</form>
		<!-- <div class="priv_form">Нажимая "заказать звонок", Вы соглашаетесь с <a href="/privacy.pdf" target="_blank">политикой конфиденциальности</a></div> -->
    </div>
</div>
<div class="preloader">
  <div class='sk-fading-circle'>
    <div class='sk-circle sk-circle-1'></div>
    <div class='sk-circle sk-circle-2'></div>
    <div class='sk-circle sk-circle-3'></div>
    <div class='sk-circle sk-circle-4'></div>
    <div class='sk-circle sk-circle-5'></div>
    <div class='sk-circle sk-circle-6'></div>
    <div class='sk-circle sk-circle-7'></div>
    <div class='sk-circle sk-circle-8'></div>
    <div class='sk-circle sk-circle-9'></div>
    <div class='sk-circle sk-circle-10'></div>
    <div class='sk-circle sk-circle-11'></div>
    <div class='sk-circle sk-circle-12'></div>
  </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/jquery.maskedinput.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>