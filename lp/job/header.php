<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Вакансии АН Перспектива 24</title>
    <meta name="description" content="Все новостройки Барнаул">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta content="images/logo.png">
    <meta property="og:image" content="/images/logo.png"/>
	<!-- FAVICONS -->
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/site.webmanifest">
    <link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- FAVICONS -->
	<link class="lazy_css" data-href="//fonts.googleapis.com/css?family=Montserrat:400,500,700,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
       (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
       m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
       (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

       ym(56734858, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
       });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/56734858" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>
<div class="section <?=(strpos($_SERVER['SCRIPT_NAME'], '/quiz.php') ? 'quiz' : 'main')?>">
    <div class="container">
		<div class="header">
			<div class="logo"><img src="images/logo.png" alt="logo"></div>

			<a href="tel:83852560100" onclick="ym(56734858, 'reachGoal', 'call');" class="phone">+7 347 293-54-27</a>
			<div class="show_pop fb_btn" data-pop="fb"><span>Обратный звонок</span></div>
		</div>