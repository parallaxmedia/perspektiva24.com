<?php

if (empty($_SERVER["DOCUMENT_ROOT"])) {
    $_SERVER["DOCUMENT_ROOT"] = '/home/bitrix/www';
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use P24\Rest\Api\Main;

$inputJSON = file_get_contents('php://input');
$request = json_decode($inputJSON, true);
AddMessage2Log($inputJSON);
AddMessage2Log($request);
if (Main::checkKey($request['key'])) {
    try {
        $api = new Main($request, $_SERVER['REQUEST_URI']);
        echo json_encode($api->run());
    } catch (Exception $e) {
        echo json_encode(array('error' => $e->getMessage()));
    }
} else {
    echo 'Access denied.';
}
