<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
function getMinMaxPrice($arr)
{
    $min = null;
    $max = null;
    while (list($k, $v) = each($arr)) {
        if ($v['PROPERTY_PRICE_VALUE'] > $max['PROPERTY_PRICE_VALUE'] or $max === null) {
            $max = $v;
        }

        if ($v['PROPERTY_PRICE_VALUE'] < $min['PROPERTY_PRICE_VALUE'] or $min === null) {
            $min = $v;
        }
    }
    return [
        'min' => $min['PROPERTY_PRICE_VALUE'],
        'max' => $max['PROPERTY_PRICE_VALUE']
    ];
}

function getMinMaxPricePerMeter($arr)
{
    $min = null;
    $max = null;
    foreach ($arr as $key => &$item) {

        if ($item['PROPERTY_PRICE_PER_METER_VALUE'] > $max['PROPERTY_PRICE_PER_METER_VALUE'] or $max === null) {
            $max = $item;
        }

        if ($item['PROPERTY_PRICE_PER_METER_VALUE'] < $min['PROPERTY_PRICE_PER_METER_VALUE'] or $min === null) {
            $min = $item;
        }
    }
    return [
        'min' => $min['PROPERTY_PRICE_PER_METER_VALUE'],
        'max' => $max['PROPERTY_PRICE_PER_METER_VALUE']
    ];
}

function getMinMaxArea($arr)
{
    $min = null;
    $max = null;
    foreach ($arr as $key => &$item) {
        if ($item['PROPERTY_AREA_VALUE'] > $max['PROPERTY_AREA_VALUE'] or $max === null) {
            $max = $item;
        }
        if ($item['PROPERTY_AREA_VALUE'] < $min['PROPERTY_AREA_VALUE'] or $min === null) {
            $min = $item;
        }
    }
    return [
        'min' => $min['PROPERTY_AREA_VALUE'],
        'max' => $max['PROPERTY_AREA_VALUE']
    ];
}

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('dev2fun.multidomain');

if (isset($request['ID'])) {
    CIBlockElement::CounterInc($request['ID']);
    //Получаем текущий дом комплекса
    $rsElement = CIBlockElement::GetByID($request['ID'])->GetNextElement();
    $arElement = $rsElement->GetFields();
    $arElement['PROPS'] = $rsElement->GetProperties();

    $arSelect = [
        "ID",
        "IBLOCK_ID",
        'PROPERTY_ID',
        "NAME",
        "PROPERTY_ADDRESS",
        "PROPERTY_PARENT_ID",
        "PROPERTY_LITER",
        "PROPERTY_DEVELOPER",
        "PROPERTY_DESCRIPTION",
        "PROPERTY_LAT",
        "PROPERTY_LNG",
        "PROPERTY_DATE_OF_COMLPETION",
    ];

    $arFilterZHK = [];
    /** Список всех домов в жилом комплексе */
    $arElementsZHK = [];
    $arElementsZHK[$arElement['PROPS']['ID']['VALUE']] = $arElement;
    /** Список CRM_ID всех домов в жилом комплексе */
    $arElementsZHKId = [];
    $arElementsHouseId = [];

    //Получаем остальные дома комплекса
    if (!empty($arElement['PROPS']['PARENT_ID']['VALUE'])) {
        $arFilterZHK = [
            'PROPERTY_PARENT_ID' => $arElement['PROPS']['PARENT_ID']['VALUE']
        ];
        $rsElementsZHK = CIBlockElement::GetList([], $arFilterZHK, false, false, $arSelect);
        while ($arElementsHouse = $rsElementsZHK->Fetch()) {
            if (!empty($arElementsHouse['PROPERTY_PARENT_ID_VALUE'])) {
                $arElementsHouseId[] = $arElementsHouse['PROPERTY_PARENT_ID_VALUE'];
            }
            $arElementsZHK[$arElementsHouse['PROPERTY_ID_VALUE']] = $arElementsHouse;
        }
    }

    if (!empty($arElementsHouseId)) {
        $arFilterZHK = [
            'PROPERTY_PARENT_ID' => $arElementsHouseId
        ];
        $rsElementsZHK = CIBlockElement::GetList([], $arFilterZHK, false, false, $arSelect);
        while ($arElementsHouse = $rsElementsZHK->Fetch()) {
            $arElementsZHK[$arElementsHouse['PROPERTY_ID_VALUE']] = $arElementsHouse;
            $arElementsZHKId[] = $arElementsHouse['PROPERTY_ID_VALUE'];
        }
    }

    $arElement['COUNT_CORPUS'] = count($arElementsZHK);

    if (empty($arElementsZHKId)) {
        $arElementsZHKId[] = $arElement['PROPS']['ID']['VALUE'];
    }

    $arFilterObjects = [
        'PROPERTY_COMPLEX_ID' => $arElementsZHKId
    ];

    $arSelect = [
        "ID",
        "IBLOCK_ID",
        "NAME",
        "DETAIL_PAGE_URL",
        "CODE",
        "SECTION",
        "IBLOCK_SECTION_CODE",
        "PROPERTY_COMPLEX_ID",
        "PROPERTY_PRICE",
        "PROPERTY_ROOMS",
        "PROPERTY_AREA",
        "PROPERTY_FLOORS",
        "PROPERTY_ADDRESS",
        "PROPERTY_DISTRICT",
        "PROPERTY_DEVELOPER",
        "PROPERTY_HOUSETYPE",
        "PROPERTY_CELINGS",
        "PROPERTY_AGENT_ID",
        "PROPERTY_IMAGES",
        "PROPERTY_PRICE_PER_METER",
    ];

    $rsElementsObject = CIBlockElement::GetList([], $arFilterObjects, false, false, $arSelect);

    $arElementsObject = [];
    while ($arElementObject = $rsElementsObject->Fetch()) {

        $rsSection = CIBlockSection::GetByID($arElementObject['IBLOCK_SECTION_ID']);
        $arSection = $rsSection->GetNext();
        $sectionCode = $arSection['CODE'];

        $linkTemplate = $arElementObject['DETAIL_PAGE_URL'];
        $linkTemplate = str_replace('#SECTION_CODE#', $sectionCode, $linkTemplate);
        $linkTemplate = str_replace('#ELEMENT_ID#', $arElementObject['ID'], $linkTemplate);

        $arElementObject['DETAIL_PAGE_URL'] = $linkTemplate;

        $arElementsZHK[$arElementObject['PROPERTY_COMPLEX_ID_VALUE']]['ITEMS'][] = $arElementObject;
        $arElementsObject[] = $arElementObject;
        if (!empty($arElementObject['PROPERTY_AGENT_ID_VALUE'])) {
            $agentId = $arElementObject['PROPERTY_AGENT_ID_VALUE'];
        }
    }



    $arMinMaxPricePerMeter = getMinMaxPricePerMeter($arElementsObject);
    $arMinMaxArea = getMinMaxArea($arElementsObject);
    $arMinMaxPrice = getMinMaxPrice($arElementsObject);

    $arElementsObject = array_unique_key($arElementsObject, 'ID');

    foreach ($arElementsZHK as $key => $arElementZHK) {
        if (!isset($arElementZHK['ITEMS'])) {
            unset($arElementsZHK[$key]);
        } else {
            $arElementsZHK[$key]['ITEMS'] = array_unique_key($arElementsZHK[$key]['ITEMS'], 'ID');
        }
    }

    if (!empty($agentId)) {
        $rsAgent = CIBlockElement::GetByID($agentId)->GetNextElement();
        $arAgent = $rsAgent->GetFields();
        $arAgent['PROPS'] = $rsAgent->GetProperties();

        $arSelect = [
            "ID",
            "IBLOCK_ID",
        ];
        $arFilterAgentObjects = [
            'IBLOCK_ID' => 2,
            'PROPERTY_AGENT_ID' => $agentId
        ];
        $rsAgentObjects = CIBlockElement::GetList([], $arFilterAgentObjects, false, false, $arSelect);

        $countAgentObjects = $rsAgentObjects->SelectedRowsCount();
    }
}
?>
    <section class="section product product_build">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
            <h1><?= explode(',', $arElement['NAME'])[0] ?></h1>
            <div class="product__owner">
                <? if (!empty($arElement['DEVELOPER']['NAME'])) : ?>
                    <? if (!empty($arElement['DEVELOPER']['PICTURE']['SRC'])) : ?>
                        <img src="<?= $arElement['DEVELOPER']['PICTURE']['SRC'] ?>" width="35" height="35"
                             alt="ГК Первый трест">
                    <? endif; ?>
                    <span> <?= $arElement['DEVELOPER']['NAME'] ?></span>
                <? endif; ?>
            </div>
            <!--<div class="toolbar">
                <a class="toolbar__item" href="#">
                    <svg class="icon icon-email icon_gray">
                        <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#email"></use>
                    </svg>
                    <span>Отправить на почту</span>
                </a>
                <a class="toolbar__item" href="#">
                    <svg class="icon icon-print icon_gray">
                        <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#print"></use>
                    </svg>
                    <span>Печать</span></a>
                <a class="toolbar__item" href="#">
                    <svg class="icon icon-heart icon_gray">
                        <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#heart"></use>
                    </svg>
                    <span>В избранное</span></a>
                <a class="toolbar__item" href="#">
                    <svg class="icon icon-compare icon_gray">
                        <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#compare"></use>
                    </svg>
                    <span>Сравнить</span></a>
                <div class="share js-share" href="#">
                    <a class="share__item share__item_insta" href="#" aria-label="Instagram"></a>
                    <a class="share__item share__item_fb" href="#" aria-label="Facebook"></a>
                    <a class="share__item share__item_vk" href="#" aria-label="Vkontakte"></a>
                    <a class="share__item share__item_tg" href="#" aria-label="Telegram"></a>
                    <a class="share__item share__item_sk" href="#" aria-label="Skype"></a>
                    <a class="share__item share__item_wa" href="#" aria-label="Whatspapp"></a>
                    <a class="share__item share__item_link" href="#" aria-label="Прямая ссылка"></a>
                    <button class="share__button" type="button">
                        <svg class="icon icon-share icon_gray share__icon-open">
                            <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#share"></use>
                        </svg>
                        <svg class="icon icon-close icon_gray share__icon-close">
                            <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#close"></use>
                        </svg>
                        <span>Поделиться</span>
                    </button>
                </div>
            </div>-->
            <div class="product-wrapper">
                <div class="product-wrapper__intro">
                    <p class="product__price">от <?= CurrencyFormat($arMinMaxPricePerMeter['min'], 'RUB') ?> за
                        м<sup>2</sup></p>
                    <a class="product__hypothec" href="/services/mortgage/">Ипотека</a>
                    <p class="product__address">
                        <svg class="icon icon-direction icon_gray">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#direction"></use>
                        </svg>
                        <span><?= $arElement['PROPS']['ADDRESS']['VALUE'] ?></span>
                    </p>
                </div>
                <div class="product-wrapper__gallery">
                    <div class="product__gallery js-gallery-detail">
                        <div class="product__gallery-main">
                            <span class="product__total-badge">8</span>
                            <div class="swiper-container js-main-gallery-detail">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <a class="product__gallery-preview"
                                           href="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-detail.jpg"
                                           data-fancybox="gallery">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-detail.jpg"
                                                 width="882" height="498" alt="">
                                        </a>
                                    </div>
                                    <div class="swiper-slide">
                                        <a class="product__gallery-preview product__gallery-preview_video"
                                           href="https://www.youtube.com/embed/BUJUVbqWUhg?rel=0&amp;amp;showinfo=0"
                                           data-fancybox="gallery">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-detail.jpg"
                                                 width="882" height="498" alt="">
                                        </a>
                                    </div>
                                    <div class="swiper-slide">
                                        <a class="product__gallery-preview"
                                           href="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-detail.jpg"
                                           data-fancybox="gallery">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-detail.jpg"
                                                 width="882" height="498" alt="">
                                        </a>
                                    </div>
                                    <div class="swiper-slide"><a class="product__gallery-preview"
                                                                 href="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-detail.jpg"
                                                                 data-fancybox="gallery"><img
                                                    src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-detail.jpg"
                                                    width="882" height="498"
                                                    alt=""></a></div>
                                    <div class="swiper-slide"><a class="product__gallery-preview"
                                                                 href="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-detail.jpg"
                                                                 data-fancybox="gallery"><img
                                                    src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-detail.jpg"
                                                    width="882" height="498"
                                                    alt=""></a></div>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                        <div class="product__gallery-small">
                            <div class="swiper-container js-thumbs-gallery-detail">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <div class="product__thumb"><img
                                                    src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-thumb-1.jpg"
                                                    width="215" height="115" alt=""></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product__thumb"><img
                                                    src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-thumb-2.jpg"
                                                    width="215" height="115" alt=""></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product__thumb"><img
                                                    src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-thumb-3.jpg"
                                                    width="215" height="115" alt=""></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product__thumb"><img
                                                    src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-thumb-1.jpg"
                                                    width="215" height="115" alt=""></div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product__thumb"><img
                                                    src="<?= SITE_TEMPLATE_PATH ?>/images/buildings/buildings-thumb-4.jpg"
                                                    width="215" height="115" alt=""></div>
                                    </div>
                                </div>
                                <div class="swiper-button-prev">
                                    <svg class="icon icon-chevron icon_black">
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                                    </svg>
                                </div>
                                <div class="swiper-button-next">
                                    <svg class="icon icon-chevron icon_black">
                                        <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-wrapper__short">
                    <ul class="product__short-info">
                        <li>
                            <svg class="icon icon-eye icon_blue">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#eye"></use>
                            </svg>
                            <p class="mobile-hidden">Просмотров</p>
                            <p><?= $arElement['SHOW_COUNTER'] ?></p>
                        </li>
                    </ul>
                </div>
                <div class="product-wrapper__info">
                    <div class="product__info">
                        <table>
                            <tr>
                                <td>Количество квартир</td>
                                <td><?= count($arElementsObject) ?></td>
                            </tr>
                            <tr>
                                <td>Количество корпусов</td>
                                <td><?= $arElement['COUNT_CORPUS'] ?></td>
                            </tr>
                            <? if ($arElementsObject[0]['PROPERTY_HOUSETYPE_VALUE'] != null) : ?>
                                <tr>
                                    <td>Тип дома</td>
                                    <td><?= $arElementsObject[0]['PROPERTY_HOUSETYPE_VALUE'] ?></td>
                                </tr>
                            <? endif; ?>
                            <tr>
                                <td>Квартиры</td>
                                <td>от <?= $arMinMaxArea['min'] ?> - <?= $arMinMaxArea['max'] ?> м<sup>2</sup></td>
                            </tr>
                            <? if ($arElementsObject[0]['PROPERTY_CELINGS_VALUE'] != null) : ?>
                                <tr>
                                    <td>Высота потолков</td>
                                    <td><?= $arElementsObject[0]['PROPERTY_CELINGS_VALUE'] ?> м</td>
                                </tr>
                            <? endif; ?>
                            <tr>
                                <td>Цены</td>
                                <td>от <?= CurrencyFormat($arMinMaxPrice['min'], 'RUB') ?>
                                    - <?= CurrencyFormat($arMinMaxPrice['max'], 'RUB') ?></td>
                            </tr>
                            <? if ($arElement['PROPS']['DATE_OF_COMLPETION']['VALUE'] != null) : ?>
                                <tr>
                                    <td>Срок сдачи</td>
                                    <td><?= $arElement['PROPS']['DATE_OF_COMLPETION']['VALUE'] ?></td>
                                </tr>
                            <? endif; ?>
                        </table>
                        <div class="product__info-buttons">
                            <? if (!empty($agentId)) : ?>
                                <div class="product__info-phone js-toggle-phone">
                                    <a class="hidden"
                                       href="tel:<?= $arAgent['PROPS']['PHONE']['VALUE'] ?>"><?= phone_format($arAgent['PROPS']['PHONE']['VALUE']) ?></a>
                                    <button class="btn btn-primary" type="button">Показать телефон</button>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product__desc">
                <div class="product__desc-col">
                    <div class="product__desc-content">
                        <h3>Описание ЖК</h3>
                        <p><?= $arElement['PROPS']['DESCRIPTION']['VALUE'] ?></p>
                    </div>
                    <div class="product__desc-grid product__desc-grid_one">
                        <div class="product__desc-item">
                            <p class="product__desc-header">
                                <span>Дополнительное описание</span>
                            </p>
                            <div class="product__desc-body">
                                <ul class="icons-list">
                                    <li><img src="<?= SITE_TEMPLATE_PATH ?>/images/carriage.svg" width="15" height="15"><span>Уютный внутренний двор: со всех сторон огорожен от улицы и может жить своей обособленной жизнью.</span>
                                    </li>
                                    <li><img src="<?= SITE_TEMPLATE_PATH ?>/images/shield.svg" width="12"
                                             height="15"><span>Внутренний двор перекрыт для проезда машин, все парковки вынесены за периметр двора.</span>
                                    </li>
                                    <li><img src="<?= SITE_TEMPLATE_PATH ?>/images/cart.svg" width="15"
                                             height="15"><span>Внешняя публичная зона retail, где будут расположены магазины, кафе, коммерческие помещения.</span>
                                    </li>
                                    <li><img src="<?= SITE_TEMPLATE_PATH ?>/images/user-check.svg" width="18"
                                             height="15"><span>Высокий социальный контроль. Единое социальное окружение.</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <? if (!empty($arResult['PROPERTIES']['LNG']['VALUE']) && !empty($arResult['PROPERTIES']['LAT']['VALUE'])) : ?>
                        <div class="product__desc-map" id="productMap" style="height: 224px;"></div>
                        <script type="text/javascript">
                            // Функция ymaps.ready() будет вызвана, когда
                            // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
                            ymaps.ready(init);

                            function init() {
                                // Создание карты.
                                var myMap = new ymaps.Map("productMap", {
                                    center: [<?=$arResult['PROPERTIES']['LNG']['VALUE']?>, <?=$arResult['PROPERTIES']['LAT']['VALUE']?>],
                                    zoom: 14
                                });

                                var myGeoObject = new ymaps.GeoObject({
                                    geometry: {
                                        type: "Point", // тип геометрии - точка
                                        coordinates: [<?=$arResult['PROPERTIES']['LNG']['VALUE']?>, <?=$arResult['PROPERTIES']['LAT']['VALUE']?>] // координаты точки
                                    }
                                });

                                myMap.geoObjects.add(myGeoObject);
                            }
                        </script>
                    <? endif; ?>
                </div>
                <? if (!empty($agentId)) : ?>
                    <form class="product__desc-form desc-form" action="/local/templates/perspektiva/ajax/formProcessing.php">
                        <div class="desc-form__header">
                            <img class="desc-form__image" src="<?= $arAgent['PROPS']['AVATAR']['VALUE'] ?>" width="88"
                                 height="88" alt="<?= $arAgent['NAME'] ?>">
                            <div class="desc-form__content">
                                <p class="desc-form__title">Расскажет о ЖК</p>
                                <a class="desc-form__agent" href="#"><?= $arAgent['NAME'] ?></a>
                                <p class="desc-form__label">Обектов у агента <?= $countAgentObjects ?></p>
                            </div>
                        </div>
                        <div class="desc-form__phone js-toggle-phone">
                            <a class="hidden"
                               href="tel:<?= $arAgent['PROPS']['PHONE']['VALUE'] ?>"><?= phone_format($arAgent['PROPS']['PHONE']['VALUE']) ?></a>
                            <button class="btn btn-secondary" type="button">Показать телефон</button>
                        </div>
                        <div class="desc-form__body">
                            <p class="desc-form__subtitle">Написать <?= $arAgent['NAME'] ?></p>
                            <div class="desc-form__fields">
                                <label class="visually-hidden" for="agent-form-name">Имя</label>
                                <input id="agent-form-name" type="text" name="name" placeholder="Имя" required>
                                <label class="visually-hidden" for="agent-form-phone">Телефон</label>
                                <input id="agent-form-phone" type="tel" name="phone" placeholder="Телефон" required>
                                <label class="visually-hidden" for="agent-form-message">Сообщение</label>
                                <input id="agent-form-message" type="text" name="message"
                                       placeholder="Хочу посмотреть квартиру" required>
                            </div>
                            <button class="btn btn-primary" type="submit">Отправить</button>
                            <p class="desc-form__terms">Нажимая на кнопку, вы даете согласие на обработку своих
                                персональных
                                данных</p>
                        </div>
                    </form>
                <? endif; ?>
            </div>
        </div>
    </section>
<?php
$index = 0;
foreach ($arElementsZHK as $arElementZHK) :
    $index++;
    ?>
    <section class="section products-slider-wrapper special-abs-wrapper">
        <div class="section__inner">
            <h3 class="section__title">Квартиры в <?=$index?> корпусе <?=(!empty($arElementZHK['PROPERTY_LITER_VALUE']) ? '(литера '.$arElementZHK['PROPERTY_LITER_VALUE'].')' : '')?></h3>
            <p class="section__subtitle">Сдача <?=$arElementZHK['PROPERTY_DATE_OF_COMLPETION_VALUE']?></p>
            <div class="products-slider-box j-products-swiper-small">
                <div class="products-slider products-slider_small swiper-container">
                    <div class="swiper-wrapper">
                        <? foreach ($arElementZHK['ITEMS'] as $arElementObject): ?>
                            <div class="swiper-slide">
                                <a class="products__slider-item" href="<?=$arElementObject['DETAIL_PAGE_URL']?>">
                                    <div class="products__slider-image">
                                        <img src="<?= $arElementObject['PROPERTY_IMAGES_VALUE'] ?>" width="198"
                                             height="130" alt="">
                                    </div>
                                    <div class="products__slider-body">
                                        <? if ($arElementObject['PROPERTY_ROOMS_VALUE'] == 0) : ?>
                                            <p class="products__slider-title">Студия</p>
                                        <? else : ?>
                                            <p class="products__slider-title"><?= $arElementObject['PROPERTY_ROOMS_VALUE'] ?>
                                                -комнатная</p>
                                        <? endif; ?>
                                        <p class="products__slider-subtitle"><?= $arElementObject['PROPERTY_ADDRESS_VALUE'] ?></p>
                                        <table>
                                            <tr>
                                                <td>Этажность</td>
                                                <td><?= $arElementObject['PROPERTY_FLOORS_VALUE'] ?></td>
                                            </tr>
                                            <tr>
                                                <td>Площадь</td>
                                                <td><?= $arElementObject['PROPERTY_AREA_VALUE'] ?> м<sup>2</sup></td>
                                            </tr>
                                            <tr>
                                                <td>Цена</td>
                                                <td><?= CurrencyFormat($arElementObject['PROPERTY_PRICE_VALUE'], 'RUB') ?></td>
                                            </tr>
                                        </table>
                                        <a href="<?=$arElementObject['DETAIL_PAGE_URL']?>" class="btn btn-light">Подробнее</a>
                                    </div>
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <!--<a class="special-abs" href="#">
                <svg class="icon icon-lock icon_blue">
                    <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#lock"></use>
                </svg>в Й1
                <span>осталось 235 квартир</span></a>-->
        </div>
    </section>
<?php endforeach; ?>
    <!-- <section class="section reverse-seo">
        <div class="section__inner">
            <a class="btn btn-secondary btn-icon" href="/residential/">
                <svg class="icon icon-chevron icon_gray icon_left icon_revese">
                    <use xlink:href="<? /*= SITE_TEMPLATE_PATH */ ?>/images/sprite.svg#chevron"></use>
                </svg>
                <span>Вернуться в новостройки</span>
            </a>
            <div class="revese-seo__content">
                <p>Привлечение застройщиком многоквартирного дома денежных средств участников долевого строительства для
                    строительства многоквартирного дома осуществляется только в рамках договора участия в долевом
                    строительстве. Оплата цены объекта долевого строительства производится только после государственной
                    регистрации договора участия в долевом строительстве.</p>
            </div>
        </div>
    </section> -->
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>