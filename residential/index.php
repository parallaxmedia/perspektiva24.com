<?

use Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Недвижимость");

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('dev2fun.multidomain');
global $arCurCity;

$cityId = $arCurCity['ID'];
if ($cityId > 0) {
    $arFilterObjects['=PROPERTY_FILIAL_ID'] = $cityId;
}

$arFilterObjects['IBLOCK_ID'] = 2;

$arSelect = [
    "ID",
    "IBLOCK_ID",
    "NAME",
    "PROPERTY_COMPLEX_ID",
    "PROPERTY_CITY",
];

$rsElementsObject = CIBlockElement::GetList([], $arFilterObjects, false, false, $arSelect);
$arElementsObject = [];
$arFilterCitiesID = [];

if ($rsElementsObject->SelectedRowsCount() > 0) {
    while ($arElementObject = $rsElementsObject->Fetch()) {
        if (!is_null($arElementObject['PROPERTY_COMPLEX_ID_VALUE'])) {
            $arElementsObject[$arElementObject['PROPERTY_COMPLEX_ID_VALUE']] = (int)$arElementObject['PROPERTY_COMPLEX_ID_VALUE'];
        }
        if (!is_null($arElementObject['PROPERTY_CITY_VALUE'])) {
            $arFilterCitiesID[] = (int)$arElementObject['PROPERTY_CITY_VALUE'];
        }
    }
}

/** Получили недвижку с жилыми комплексами в текущем городе */
/** Получаем дома и связанные с ними дома */

$arFilterZHK['XML_ID'] = $arElementsObject;
$arFilterZHK['PROPERTY_CITY.VALUE'] = $arFilterCitiesID;
$arSelect = [
    "ID",
    "IBLOCK_ID",
    'PROPERTY_ID',
    "NAME",
    "PROPERTY_ADDRESS",
    "PROPERTY_PARENT_ID",
    "PROPERTY_CITY",
];

$rsElementsZHK = CIBlockElement::GetList([], $arFilterZHK, false, false, $arSelect);
$arElementsZHK = [];
$arElementsZHKId = [];
$arElementsHouseId = [];
while ($arElementsHouse = $rsElementsZHK->Fetch()) {
    $arElementsHouseId[] = $arElementsHouse['PROPERTY_PARENT_ID_VALUE'];
    $arElementsZHK[$arElementsHouse['ID']] = $arElementsHouse;
}

foreach ($arElementsZHK as $key => $arElementZHK) {
    if (!in_array($arElementZHK['PROPERTY_CITY_VALUE'], $arFilterCitiesID)) {
        unset($arElementsZHK[$key]);
    }
}

$arFilterZHK = [
    'PROPERTY_PARENT_ID' => $arElementsHouseId
];

$rsElementsZHK = CIBlockElement::GetList([], $arFilterZHK, false, false, $arSelect);

while ($arElementsHouse = $rsElementsZHK->Fetch()) {
    $arElementsZHK[$arElementsHouse['ID']] = $arElementsHouse;
}

$arUniqueZHK = [];
foreach ($arElementsZHK as $arElementZHK) {
    $arUniqueZHK[$arElementZHK['PROPERTY_PARENT_ID_VALUE']] = $arElementZHK;
    $arElementsZHKId[] = $arElementZHK['PROPERTY_ID_VALUE'];
}
$countComplex = count($arUniqueZHK);

$arFilterObjects = [
    'PROPERTY_COMPLEX_ID' => $arElementsZHKId
];

$arSelect = [
    "ID",
    "IBLOCK_ID",
    "NAME",
    "PROPERTY_COMPLEX_ID",
    "PROPERTY_PRICE",
    "PROPERTY_ROOMS",
    "PROPERTY_AREA",
    "PROPERTY_DISTRICT"
];

$rsElementsObject = CIBlockElement::GetList([], $arFilterObjects, false, false, $arSelect);

$countObject = $rsElementsObject->SelectedRowsCount();
$arElementsObject = [];
while ($arElementObject = $rsElementsObject->Fetch()) {
    $arElementsObject[] = $arElementObject;
}

$arMinMaxPrice = getMinMaxPrice($arElementsObject);

function getMinMaxPrice($arr)
{
    $min = null;
    $max = null;
    while (list($k, $v) = each($arr)) {
        if ($v['PROPERTY_PRICE_VALUE'] > $max['PROPERTY_PRICE_VALUE'] or $max === null) {
            $max = $v;
        }

        if ($v['PROPERTY_PRICE_VALUE'] < $min['PROPERTY_PRICE_VALUE'] or $min === null) {
            $min = $v;
        }
    }
    return [
        'min' => $min['PROPERTY_PRICE_VALUE'],
        'max' => $max['PROPERTY_PRICE_VALUE']
    ];
}

function getMinArea($arr)
{
    $min = null;
    while (list($k, $v) = each($arr)) {
        if ($v['PROPERTY_AREA_VALUE'] < $min['PROPERTY_AREA_VALUE'] or $min === null) {
            $min = $v;
        }
    }
    return $min['PROPERTY_AREA_VALUE'];
}

foreach ($arElementsZHK as $arElementZHK) {
    $arUniqueZHK[$arElementZHK['PROPERTY_PARENT_ID_VALUE']]['parents'][] = $arElementZHK['PROPERTY_ID_VALUE'];
}

foreach ($arElementsObject as $arElementObject) {
    if (isset($arUniqueZHK[$arElementObject['PROPERTY_COMPLEX_ID_VALUE']])) {
        $arUniqueZHK[$arElementObject['PROPERTY_COMPLEX_ID_VALUE']]['OBJECTS'][] = $arElementObject;
    } else {
        foreach ($arUniqueZHK as $key => $arUnique) {
            if (in_array($arElementObject['PROPERTY_COMPLEX_ID_VALUE'], $arUnique['parents'])) {
                $arUniqueZHK[$key]['OBJECTS'][] = $arElementObject;
            }
        }
    }
}

foreach ($arUniqueZHK as $key => $arElementZHK) {
    foreach ($arElementZHK['OBJECTS'] as $arObject) {
        $arUniqueZHK[$key]['ROOMS'][$arObject['PROPERTY_ROOMS_VALUE']][] = $arObject;
    }
}

foreach ($arUniqueZHK as $key1 => $arElementZHK) {
    foreach ($arElementZHK['ROOMS'] as $key2 => $arRoomsObject) {
        $minArea = getMinArea($arRoomsObject);
        $minPrice = getMinMaxPrice($arRoomsObject);
        $arUniqueZHK[$key1]['ROOMS'][$key2]['min_price'] = $minPrice['min'];
        $arUniqueZHK[$key1]['ROOMS'][$key2]['min_area'] = $minArea;
    }
}

$arDistrictId = [];
foreach ($arUniqueZHK as $key1 => $arElementZHK) {
    foreach ($arElementZHK['OBJECTS'] as $key2 => $arObject) {
        $arDistrictId[] = $arObject['PROPERTY_DISTRICT_VALUE'];
        $rsDistrict = CIBlockSection::GetByID($arObject['PROPERTY_DISTRICT_VALUE']);
        $arUniqueZHK['DISTRICTS'][$arObject['PROPERTY_DISTRICT_VALUE']]['ZHK'][$arElementZHK['ID']] = $arElementZHK;
        $arUniqueZHK['DISTRICTS'][$arObject['PROPERTY_DISTRICT_VALUE']]['NAME'] = $rsDistrict->Fetch()['NAME'];
    }
}
?>

    <section class="section main-form main-form_buildings">
        <div class="section__inner">
            <h1>Новостройки в г. <?= $arCurCity['NAME']; ?></h1>
            <?php require($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . '/include/filter.php'); ?>
        </div>
    </section>
    <section class="section intro">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
            <h2 class="section__title">Купить квартиру в новостройке</h2>
            <p class="section__subtitle"><?= $countComplex ?>
                комплекса, <?= $countObject ?> квартир, от <?= $arMinMaxPrice['min'] ?>
                до <?= $arMinMaxPrice['max'] ?> руб.</p>
            <p>Привлечение застройщиком многоквартирного дома денежных средств участников долевого строительства для
                строительства многоквартирного дома осуществляется только в рамках договора участия в долевом
                строительстве. Оплата цены объекта долевого строительства производится только после государственной
                регистрации договора участия в долевом строительстве</p>
        </div>
    </section>
    <section class="section product-links-wrapper thin">
        <div class="section__inner">
            <ul class="product-links">
                <? foreach ($arUniqueZHK['DISTRICTS'] as $arDistricts) : ?>
                    <li class="dropdown">
                        <button class="btn btn-outline product-links__dropdown-btn dropdown-toggle" type="button"
                                id="link-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span><?= $arDistricts['NAME'] ?></span>
                            <i>(<?= count($arDistricts['ZHK']) ?>)</i>
                            <svg class="icon icon-chevron icon_gray">
                                <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                            </svg>
                        </button>
                        <div class="product-links__dropdown-menu dropdown-menu" aria-labelledby="link-1">
                            <button class="products-links__close js-close-dropdown" type="button" aria-label="Закрыть">
                                <svg class="icon icon-close icon_gray">
                                    <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#close"></use>
                                </svg>
                            </button>
                            <? foreach ($arDistricts['ZHK'] as $zhkId => $arZHK) : ?>
                                <a class="dropdown-item" href="/residential/detail.php?ID=<?=$zhkId?>">
                                    <span><?= $arZHK['NAME'] ?></span>
                                    <i>(<?= count($arZHK['OBJECTS']) ?>)</i>
                                </a>
                            <? endforeach; ?>
                        </div>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
    </section>
    <section class="section products-slider-wrapper">
        <div class="section__inner">
            <? foreach ($arUniqueZHK['DISTRICTS'] as $arDistricts) : ?>
                <h3 class="section__title section__title_special"><?= $arDistricts['NAME'];?></h3>
                <div class="products-slider-box j-products-swiper">
                    <div class="swiper-button-prev">
                        <svg class="icon icon-chevron icon_black">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                        </svg>
                    </div>
                    <div class="swiper-button-next">
                        <svg class="icon icon-chevron icon_black">
                            <use xlink:href="<?= SITE_TEMPLATE_PATH ?>/images/sprite.svg#chevron"></use>
                        </svg>
                    </div>
                    <? //\Bitrix\Main\Diag\Debug::dump($arElementZHK); ?>
                    <div class="products-slider swiper-container">
                        <div class="swiper-wrapper">
                            <? foreach ($arDistricts['ZHK'] as $zhkId => $arZHK) : ?>
                                <div class="swiper-slide">
                                    <div class="products__slider-item">
                                        <div class="products__slider-image">
                                            <!--<span class="products__slider-tag">Строится</span>-->
                                            <!--<span class="products__slider-owner">Первый трест</span>-->
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/products/1.jpg" width="426"
                                                 height="181" alt="ЖК Энтузиастов">
                                        </div>
                                        <div class="products__slider-body">
                                            <p class="products__slider-title"><?= $arZHK['NAME'] ?></p>
                                            <p class="products__slider-subtitle"><?= $arZHK['PROPERTY_ADDRESS_VALUE'] ?></p>
                                            <div class="products__slider-links">
                                                <? foreach ($arZHK['ROOMS'] as $roomCount => $arRoomObjects) : ?>
                                                    <a disabled="">
                                                        <? if ($roomCount == 0) : ?>
                                                            <span>Студии</span>
                                                        <? else : ?>
                                                            <span><?= $roomCount ?>-комнатные</span>
                                                        <? endif; ?>
                                                        <span class="mobile-hidden">от <?= $arRoomObjects['min_area'] ?> м<sup>2</sup></span>
                                                        <span>от <?= $arRoomObjects['min_price'] ?> &#x20bd;</span>
                                                    </a>
                                                <? endforeach; ?>
                                            </div>
                                            <a class="btn btn-light" href="/residential/detail.php?ID=<?=$zhkId?>">Подробнее</a>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>