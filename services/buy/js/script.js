$(window).on('load', function () {
    $preloader = $('.loaderArea'),
        $loader = $preloader.find('.loader');
    $loader.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});
$(document).ready(function () {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 1,
        mouseDrag: false,
        touchDrag: false,
        nav: false,
        dots: false,
    });

    $('.customNextBtn').click(function () {
        var val = parseInt(document.querySelector(".progress-bar").style.width);
        // // Если Продажа
        // if (document.getElementById('realization').checked) {
        //     document.getElementById('object').style.display = "block";
        //     document.getElementById('price').style.display = "block";
        //     // document.getElementById('type_budget').style.display = "none";
        //     document.getElementById('variant').style.display = "none";
        //     document.getElementById('finale').style.display = "block";
        //     document.querySelector(".poll-price").innerHTML = 'Желаемая цена продажи';
        // }
        // // Если Покупка
        // if (document.getElementById('buy').checked) { 
        //     document.getElementById('object').style.display = "block";
        //     document.getElementById('price').style.display = "block";
        //     document.getElementById('type_budget').style.display = "block";
        //     document.getElementById('variant').style.display = "none";
        //     document.getElementById('finale').style.display = "block";
        //     document.querySelector(".poll-price").innerHTML = 'Ваш бюджет';
        // }
        // // Если обмен
        // if (document.getElementById('swap').checked) { 
        //     document.getElementById('variant').style.display = "block";
        //     document.getElementById('object').style.display = "none";
        //     document.getElementById('price').style.display = "none";
        //     document.getElementById('type_budget').style.display = "none";
            
        //     document.getElementById('finale').style.display = "block";
        // }
        if (val >= 20) {
            $(".prev").css("visibility", "visible");
        }
        val = val + 20;
        if (val == 80) {
            $(".next").css("visibility", "visible");
        }
        if(val==60){
            $(".next").css("visibility", "visible");
        }
        if (val == 100) {
            $(".next").css("visibility", "hidden");
        }
        val = val + "%";
        document.querySelector(".progress-bar").style.width = val;
        document.querySelector(".progress-bar").innerHTML = val;
        owl.trigger('next.owl.carousel');
    });

    $('.customPrevBtn').click(function () {
        var val = parseInt(document.querySelector(".progress-bar").style.width);
        if (val > 100) {
            val = 100;
        }
        if (val == 40) {
            $(".prev").css("visibility", "hidden");
        }
        val = val - 20;
        if (val == 80) {
            $(".next").css("visibility", "visible");
        }
        if (val == 60) {
            $(".next").css("visibility", "visible");
        }
        val = val + "%";
        document.querySelector(".progress-bar").style.width = val;
        document.querySelector(".progress-bar").innerHTML = val;
        owl.trigger('prev.owl.carousel');
    });

    $.mask.definitions['~'] = '[78]';
    $("input[name=phone]").mask("~ (999) 999-9999");

    $('#ajax_form').validator({});

    $('#ajax_form').on('submit', function (e) {
        console.log(true);
        e.preventDefault();
        let $form = $(this);
        $.ajax({
            method: 'POST',
            url: '/local/templates/perspektiva/ajax/serviceFlatProcessing.php',
            data: $(this).serialize(),
            success: function (response) {
                $('.modal-body').html(response);
            }
        });
    });

});

var $body;
$(document).ready(function () {
    $body = $('body');
    $body
    // .find('.user-phone').each(function(){
    //     $(this).mask("+7(999) 999-99-99", {autoсlear: false});
    // });
    $body.on('keyup', '.user-phone', function () {
        var phone = $(this),
            phoneVal = phone.val(),
            form = $(this).parents('form');
        if ((phoneVal.indexOf("_") != -1) || phoneVal == '') {
            form.find('.btn').attr('disabled', true);
        } else {
            form.find('.btn').removeAttr('disabled');
        }
    });
});