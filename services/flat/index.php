<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Продать недвижимость");
?>
    <section class="section section-bc">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
        </div>
    </section>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/style.css">

    <section class="section services">
        <div class="section__inner">
            <h1>Поможем продать вашу квартиру по привлекательной цене</h1>
            <p>Пройдите тест и получите лучшее предложение</p>

            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 20%;" aria-valuenow="20"
                         aria-valuemin="20" aria-valuemax="100">20%
                    </div>
                </div>
                <form class="search-form" data-toggle="validator" id="ajax_form_flat" method="post"
                      action="/local/templates/perspektiva/ajax/formSendProcessing.php">
                    <input type="hidden" name="type" value="Работа">
                    <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                    <div class="owl-carousel">
                        <!-- -------------------------------------------------------------------------------------- -->
                        <div>
                            <h5 class="poll">Вид сделки</h5>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Radios1" id="realization" value="Продажа"
                                                       class="customNextBtn">
                                                <i class="fa fa-2x icon-radio"></i>Продажа
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Radios1" id="buy" value="Покупка"
                                                       class="customNextBtn">
                                                <i class="fa fa-2x icon-radio"></i>Покупка
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="Radios1" id="swap" value="Обмен"
                                                       class="customNextBtn">
                                                <i class="fa fa-2x icon-radio"></i>Обмен
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- -------------------------------------------------------------------------------------- -->
                        <div>
                            <div id="object">
                                <h5 class="poll">Вид объекта недвижимости</h5>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios2" value="Доля"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Доля
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios2" value="Комната"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Комната
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios2" value="1 комнатная квартира"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>1 комнатная квартира
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios2" value="2 комнатная квартира"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>2 комнатная квартира
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios2" value="3 комнатная квартира"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>3 комнатная квартира
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios2"
                                                           value="4 комнатная и более квартира" class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>4 комнатная и более квартира
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios2" value="дом"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>дом
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios2" value="таунхаус"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>таунхаус
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios2" value="коттедж"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>коттедж
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- --------------------------------- -->
                            <div id="variant">
                                <h5 class="poll">Выберите вариант обмена</h5>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios5" value="равноценный обмен"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>равноценный обмен
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios5"
                                                           value="обмен с увеличением площади" class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>обмен с увеличением площади
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios5"
                                                           value="обмен с понижением площади" class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>обмен с понижением площади
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- -------------------------------------------------------------------------------------- -->
                        <div>
                            <div id="price">
                                <h5 class="poll poll-price">Желаемая цена продажи / Ваш бюджет</h5>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="до 1 млн. рублей"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>до 1 млн. рублей
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="от 1 до 2 млн. рублей"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>от 1 до 2 млн. рублей
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="от 2 до 3 млн. рублей"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>от 2 до 3 млн. рублей
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="от 3 до 4 млн. рублей"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>от 3 до 4 млн. рублей
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="от 4 до 5 млн. рублей"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>от 4 до 5 млн. рублей
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="от 5 млн. рублей и более"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>от 5 млн. рублей и более
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- --------------------------------------- -->
                            <div id="your_locality">
                                <h5 class="poll">В каком районе ваша недвижимость?</h5>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Центр"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Центр
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Зеленая роща"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Зеленая роща
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Проспект"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Проспект
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Сипайлово"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Сипайлово
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Черниковка"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Черниковка
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Дёма"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Дёма
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Затон"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Затон
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Инорс"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Инорс
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Шакша"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Шакша
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="Radios3" value="Загородом"
                                                           class="customNextBtn">
                                                    <i class="fa fa-2x icon-radio"></i>Загородом
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- -------------------------------------------------------------------------------------- -->
                        <!--                    <div id="type_budget">
                                                    <h5 class="poll">Вид бюджета</h5>
                                                    <div class="col-sm-12">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="radio">
                                                                    <label>
                                                                         <input type="radio" name="Radios4" value="наличные" class="customNextBtn">
                                                                         <i class="fa fa-2x icon-radio"></i>наличные
                                                                     </label>
                                                                 </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="radio">
                                                                    <label>
                                                                         <input type="radio" name="Radios4" value="ипотека" class="customNextBtn">
                                                                         <i class="fa fa-2x icon-radio"></i>ипотека
                                                                     </label>
                                                                 </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" name="Radios4" value="наличные + ипотека" class="customNextBtn">
                                                                        <i class="fa fa-2x icon-radio"></i>наличные + ипотека
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" name="Radios4" value="материнский капитал" class="customNextBtn">
                                                                        <i class="fa fa-2x icon-radio"></i>материнский капитал
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                        <!-- -------------------------------------------------------------------------------------- -->
                        <div id="love_locality">
                            <h5 class="poll poll-love">Желаемый район проживания / Какой район обмена рассматриваете / В
                                каком районе ваша недвижимость</h5>
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Центр">
                                                <i class="fa fa-2x icon-checkbox"></i>Центр
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Зеленая роща">
                                                <i class="fa fa-2x icon-checkbox"></i>Зеленая роща
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Проспект">
                                                <i class="fa fa-2x icon-checkbox"></i>Проспект
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Сипайлово">
                                                <i class="fa fa-2x icon-checkbox"></i>Сипайлово
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Черниковка">
                                                <i class="fa fa-2x icon-checkbox"></i>Черниковка
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Дёма">
                                                <i class="fa fa-2x icon-checkbox"></i>Дёма
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Затон">
                                                <i class="fa fa-2x icon-checkbox"></i>Затон
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Инорс">
                                                <i class="fa fa-2x icon-checkbox"></i>Инорс
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Шакша">
                                                <i class="fa fa-2x icon-checkbox"></i>Шакша
                                            </div>
                                        </label>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>
                                            <div class="checkbox">
                                                <input type="checkbox" name="checkbox1[]" value="Загородом">
                                                <i class="fa fa-2x icon-checkbox"></i>Загородом
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- -------------------------------------------------------------------------------------- -->
                        <!-- -------------------------------------------------------------------------------------- -->
                        <div>
                            <h5 class="poll">Отлично. Последний шаг!</h5>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5">
                                        <p>Заполни форму ниже для получения результатов теста</p>
                                        <div id="result_form" style="color: #DD0000; font-size: 18px;"></div>
                                    </div>
                                    <div class="col-md-7">
                                        <!--  -->
                                        <div class="form-group row">
                                            <label for="exampleInputName2" class="col-sm-3 col-form-label">Ваше
                                                имя</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name" class="form-control required"
                                                       required="required">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-tel-input"
                                                   class="col-sm-3 col-form-label">Телефон</label>
                                            <div class="col-sm-9">
                                                <input class="form-control user-phone" type="phone" name="phone"
                                                       id="phone" placeholder="">
                                            </div>
                                        </div>
                                        <!--  -->
                                        <button type="submit" id="btn" class="btn btn-primary" disabled>Отправить
                                            заявку
                                        </button>
                                        <!--  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- -------------------------------------------------------------------------------------- -->
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary customPrevBtn prev" style="visibility: hidden">
                    Назад
                </button>
                <button type="button" class="btn btn-outline-primary customNextBtn next" style="visibility: hidden">
                    Далее
                </button>
            </div>
        </div>
    </section>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
            integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/validator.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/script.js"></script>
    <script src="//st.yagla.ru/js/y.c.js?h=40616739d06e2b4b23e3a1ddd4b1f541"></script>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>