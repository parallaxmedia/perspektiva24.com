<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Услуги агентства недвижимости «Перспектива24»");
?>
    <section class="section section-bc">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
        </div>
    </section>
    <section class="section services">
        <div class="section__inner">
            <h1>Услуги агентства недвижимости «Перспектива24»</h1>
            <div class="services-grid">
                <div class="services-grid__item" href="#">
                    <div class="services-grid__image">
                        <span class="services-grid__tag">Выгодно</span>
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/services/1.jpg" width="426" height="182"
                             alt="Купить">
                    </div>
                    <div class="services-grid__body">
                        <a class="services-grid__title" target="_blank" href="https://perspektiva24.com/services/buy/">Купить</a>
                        <table>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/buy/">Новостройку</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/buy/">Комнату</a></td>
                            </tr>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/buy/">Квартиру</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/buy/">Дом, дачу</a></td>
                            </tr>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/buy/">Коммерческую</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/buy/">Земельный участок</a></td>
                            </tr>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/buy/">Коттедж</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/buy/">Гараж, машиноместо</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="services-grid__item" href="#">
                    <div class="services-grid__image"><span class="services-grid__tag">Удобно</span><img
                                src="<?= SITE_TEMPLATE_PATH ?>/images/services/2.jpg" width="426" height="182"
                                alt="Купить"></div>
                    <div class="services-grid__body"><a class="services-grid__title" target="_blank" href="https://perspektiva24.com/services/sell/">Продать</a>
                        <table>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/sell/">Новостройку</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/sell/">Комнату</a></td>
                            </tr>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/sell/">Квартиру</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/sell/">Дом, дачу</a></td>
                            </tr>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/sell/">Коммерческую</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/sell/">Земельный участок</a></td>
                            </tr>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/sell/">Коттедж</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/sell/">Гараж, машиноместо</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="services-grid__item" href="#">
                    <div class="services-grid__image"><span class="services-grid__tag">Оптимально</span>
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/services/3.jpg" width="426" height="182" alt="Купить">
                    </div>
                    <div class="services-grid__body"><a class="services-grid__title" target="_blank" href="https://perspektiva24.com/services/change/">Обмен</a>
                        <table>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/change/">Новостройки</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/change/">Комнаты</a></td>
                            </tr>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/change/">Квартиры</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/change/">Домы, дачи</a></td>
                            </tr>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/change/">Коммерческой</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/change/">Земельного участка</a></td>
                            </tr>
                            <tr>
                                <td><a target="_blank" href="https://perspektiva24.com/services/change/">Коттеджа</a></td>
                                <td><a target="_blank" href="https://perspektiva24.com/services/change/">Гаража, машиноместа</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="services-boxes">
                <div class="services-boxes__item services-boxes__item_w">
                    <div class="services-boxes__content">
                        <p class="services-boxes__title">Ипотека</p>
                        <p class="services-boxes__desc">Рассчитайте ежемесячный платеж онлайн</p>
                        <a class="btn btn-primary" href="/services/mortgage/">Подробно</a>
                    </div>
                </div>
                <div class="services-boxes__item services-boxes__item_m">
                    <div class="services-boxes__content">
                        <p class="services-boxes__title">Юридическое сопровождение</p>
                        <p class="services-boxes__desc">Собственный штат юристов на все типы сделок бесплатно</p><a
                                class="btn btn-primary" href="/services/uruslugi/">Подробно</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>