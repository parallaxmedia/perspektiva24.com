<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Ипотечный калькулятор");
?>
    <section class="section calc-wrapper">
        <div class="section__inner">
            <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            ); ?>
            <h1><? $APPLICATION->ShowTitle(); ?></h1>
            <p class="section__subtitle">
                Оформить ипотеку у нас дешевле, чем в банке. Закажите звонок и проконсультируйтесь со специалистом
            </p>
            <div class="calc-wrapper__row">
                <div class="calc-wrapper__content">
                    <div class="calc js-calc" data-price="3000000" data-calc="">
                        <input class="js-calc-rate" type="hidden" name="rate" value="9.4" data-defval="9.4">
                        <div class="calc__main">
                            <div class="calc__main-row">
                                <p class="calc__main-label">Стоимость объекта</p>
                                <input class="calc__main-input js-range-slider js-calc-cost" type="text" name="cost" data-value-type="cost" data-step="10000" data-min="100000" data-max="30000000" data-from="3000000" data-grid="true" data-grid-num="6" data-hide-min-max="true" data-skin="round" data-hide-from-to="true">
                                <input class="calc__result-input" type="text" name="cost-result" value="3 000 000">
                            </div>
                            <div class="calc__main-row">
                                <p class="calc__main-label">Первоначальный взнос</p>
                                <input class="calc__main-input js-range-slider js-calc-first-pay" type="text" name="first-pay" data-value-type="first-pay" data-step="10000" data-min="0" data-max="3000000" data-from="1500000" data-grid="true" data-grid-num="6" data-hide-min-max="true" data-skin="round" data-hide-from-to="true">
                                <input class="calc__result-input" type="text" name="first-pay-result" value="1 500 000">
                            </div>
                            <div class="calc__main-row">
                                <p class="calc__main-label">Срок кредитования</p>
                                <input class="calc__main-input js-range-slider js-calc-term" type="text" name="term" data-value-type="term" data-step="1" data-min="1" data-max="25" data-from="15" data-grid="true" data-grid-num="6" data-hide-min-max="true" data-skin="round" data-hide-from-to="true">
                                <input class="calc__result-input" type="text" name="term-result" value="15">
                            </div>
                            <div class="calc__main-row">
                                <p class="">Оформляя ипотеку с помощью специалистов Перспектива24, снижаем ставку на 0,5% на весь срок ипотеки"</p>
                                    <input type="checkbox" id="ipoteka24" data-changeval="0.5">
                                <label for="ipoteka24">
                                    Оформить ипотеку в Перспектива24
                                </label>
                            </div>
                        </div>
                        <div class="calc__result">
                            <p class="calc__result-title">Наше предложение</p>
                            <div class="calc__result-blocks">
                                <div class="calc__result-block">
                                    <p class="calc__result-label">Ежемесячный платеж</p>
                                    <p class="calc__result-value"><span class="js-calc-month-pay">0</span> &#x20bd;</p>
                                </div>
                                <div class="calc__result-block">
                                    <p class="calc__result-label">Ставка</p>
                                    <p class="calc__result-value"><span id="currentRate">9.4</span> %</p>
                                </div>
                                <div class="calc__result-block">
                                    <p class="calc__result-label">Сумма кредита</p>
                                    <p class="calc__result-value"><span class="js-calc-total">0</span> &#x20bd;</p>
                                </div>
                            </div>
                            <div class="calc__result-footer">
                                <p class="calc__result-count">Уже одобрено <br><strong>95%</strong> заявок</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="calc-wrapper__terms">
                    <p>*Расчет сделан с использованием усредненной процентной ставки по кредиту. Данный расчет является ознакомительным, и позволяет Вам сделать начальную оценку Ваших финансовых возможностей. Окончательный расчет кредитного продукта производится в индивидуальном порядке.</p>
                </div>
                <div class="calc-wrapper__form">
                    <form class="desc-form desc-form_whide" action="/local/templates/perspektiva/ajax/formConsultMortageProcessing.php">
                        <div class="desc-form__body">
                            <div class="desc-form__fields">
                                <label class="visually-hidden" for="agent-form-name">Имя</label>
                                <input id="agent-form-name" type="text" name="name" placeholder="Имя" required>
                                <label class="visually-hidden" for="agent-form-phone">Телефон</label>
                                <input id="agent-form-phone" type="tel" name="phone" placeholder="Телефон" required>
                            </div>
                            <button class="btn btn-primary getconsultation" type="submit">Получить консультацию</button>
                            <p class="desc-form__terms">Нажимая на кнопку, вы даете согласие на обработку своих персональных данных</p>
                        </div>
                    </form>
                </div>
            </div>
            <div class="m-t-lg">
                <h5>Оформляйте ипотеку по сниженной % ставке в банках партнерах</h5>
                <div class="partners-grid">
                    <div class="partners-grid__item"><img src="<?=SITE_TEMPLATE_PATH?>/images/partners/logo-gazprombank.png" width="69" height="17" alt="Газпромбанк"></div>
                    <div class="partners-grid__item"><img src="<?=SITE_TEMPLATE_PATH?>/images/partners/logo-sberbank.png" width="61" height="16" alt="Сбербанк"></div>
                    <div class="partners-grid__item"><img src="<?=SITE_TEMPLATE_PATH?>/images/partners/logo-rosbank.png" width="74" height="15" alt="Росбанк"></div>
                    <div class="partners-grid__item"><img src="<?=SITE_TEMPLATE_PATH?>/images/partners/logo-promsvyazbank.png" width="53" height="16" alt="ПСБ"></div>
                    <div class="partners-grid__item"><img src="<?=SITE_TEMPLATE_PATH?>/images/partners/logo-alfa-bank.png" width="63" height="17" alt="Альфа Банк"></div>
                    <div class="partners-grid__item"><img src="<?=SITE_TEMPLATE_PATH?>/images/partners/logo-unicredit-bank.png" width="60" height="15" alt="UniCredit Bank"></div>
                    <div class="partners-grid__item"><img src="<?=SITE_TEMPLATE_PATH?>/images/partners/logo-bank-otkrytie.png" width="66" height="18" alt="Банк Открытие"></div>
                    <div class="partners-grid__item"><img src="<?=SITE_TEMPLATE_PATH?>/images/partners/logo-ubrir.png" width="69" height="12" alt="Уральский Банк"></div>
                    <div class="partners-grid__item"><img src="<?=SITE_TEMPLATE_PATH?>/images/partners/logo-ak-bars-bank.png" width="35" height="20" alt="АК БАРС Банк"></div>
                </div>
            </div>
        </div>
    </section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>